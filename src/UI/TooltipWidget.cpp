#include "TooltipWidget.h"
#include "ResourceManager.h"

#include <sstream>
#include <limits>

TooltipWidget::TooltipWidget(const std::string& name) : UIWidget(WIDGET_TOOLTIP, name) {
	visible = true;
	type = nullptr;
	ttType = TooltipType::TT_GENERIC;
}

void TooltipWidget::setType(TooltipType type) {
	ttType = type;
}

void TooltipWidget::associateItem(Item& item) {
	type = &item.getType();
}

void TooltipWidget::render(sf::RenderWindow& window) {
	if (!type) return;

	int startX = sf::Mouse::getPosition(window).x + 8;
	int startY = sf::Mouse::getPosition(window).y + 8;
	int width = 0;
	int height = 0;

	// The background
	sf::RectangleShape shape;
	shape.setFillColor(sf::Color(0, 0, 0, 220));
	shape.setSize(sf::Vector2f(300, 120));
	shape.setPosition(startX, startY);
	shape.setOutlineThickness(3.0f);
	shape.setOutlineColor(sf::Color::Black);

	// The item icon
	sf::Sprite itemSprite(ResourceManager::getInstance().getTexture("resources/items/itemset.png"));
	int locX = type->getGLoc().x;
	int locY = type->getGLoc().y;

	itemSprite.setTextureRect(sf::IntRect(sf::Vector2i(locX * 64, locY * 64), sf::Vector2i(64, 64)));
	itemSprite.setPosition(startX + 8, startY + 8);
	if (itemSprite.getGlobalBounds().left + itemSprite.getGlobalBounds().width > startX + width) {
		width = itemSprite.getGlobalBounds().left + itemSprite.getGlobalBounds().width - startX;
	}
	if (itemSprite.getGlobalBounds().top + itemSprite.getGlobalBounds().height > startY + height) {
		height = itemSprite.getGlobalBounds().top + itemSprite.getGlobalBounds().height - startY;
	}

	// The item name
	sf::Font font = ResourceManager::getInstance().getFont("resources/fonts/OptimusPrinceps.ttf");
	sf::Text text(type->getName(), font);
	text.setCharacterSize(20);
	text.setColor(sf::Color::Cyan);
	text.setPosition(startX + 80, startY + 4);
	if (text.getGlobalBounds().left + text.getGlobalBounds().width > startX + width) {
		width = text.getGlobalBounds().left + text.getGlobalBounds().width - startX;
	}
	if (text.getGlobalBounds().top + text.getGlobalBounds().height > startY + height) {
		height = text.getGlobalBounds().top + text.getGlobalBounds().height - startY;
	}

	// The stat bonuses
	std::stringstream ss;
	for (int i = 0; i < (int)Stat::COUNT; i++) {
		if (!Stat::isUseful((Stat::Type)i)) continue;

		float val = type->getStatValue((Stat::Type)i);
		if (val < std::numeric_limits<float>::epsilon()) {
			continue;
		}
		ss << "+" << val << " " << Stat::toString((Stat::Type)i) << "\n";
	}
	ss << "\n";

	sf::Text statText(ss.str(), font);
	statText.setCharacterSize(14);
	statText.setPosition(startX + 80, startY + 34);
	if (statText.getGlobalBounds().left + statText.getGlobalBounds().width > startX + width) {
		width = statText.getGlobalBounds().left + statText.getGlobalBounds().width - startX;
	}
	if (statText.getGlobalBounds().top + statText.getGlobalBounds().height - 16 > startY + height) {
		height = statText.getGlobalBounds().top + statText.getGlobalBounds().height - 16 - startY;
	}

	// The weight
	std::stringstream weightss;
	weightss << "Weight: " << type->getWeight() * 1000 << " g";
	sf::Text weightText(weightss.str(), font);
	weightText.setCharacterSize(14);
	weightText.setPosition(startX + 80, startY + height);
	if (weightText.getGlobalBounds().left + weightText.getGlobalBounds().width > startX + width) {
		width = weightText.getGlobalBounds().left + weightText.getGlobalBounds().width - startX;
	}
	if (weightText.getGlobalBounds().top + weightText.getGlobalBounds().height > startY + height) {
		height = weightText.getGlobalBounds().top + weightText.getGlobalBounds().height - startY;
	}

	// Actions (if any)
	std::stringstream actionss;
	if (type->isEquippable()) {
		if (ttType == TT_BACKPACK) {
			actionss << "Right-click to equip.";
		} else if (ttType == TT_CHARINFO) {
			actionss << "Left-click to unequip.";
		}
	}
	sf::Text actionText(actionss.str(), font);
	actionText.setCharacterSize(14);
	actionText.setColor(sf::Color(255, 255, 0));
	actionText.setPosition(startX + 8, startY + height);
	if (type->isEquippable()) {
		if (actionText.getGlobalBounds().left + actionText.getGlobalBounds().width > startX + width) {
			width = actionText.getGlobalBounds().left + actionText.getGlobalBounds().width - startX;
		}
		if (actionText.getGlobalBounds().top + actionText.getGlobalBounds().height > startY + height) {
			height = actionText.getGlobalBounds().top + actionText.getGlobalBounds().height - startY;
		}
	}

	// Actually render stuff now that we know the bounds
	shape.setSize(sf::Vector2f(width + 8, height + 8));
	window.draw(shape);
	window.draw(itemSprite);
	window.draw(text);
	window.draw(statText);
	window.draw(weightText);
	if (type->isEquippable()) {
		window.draw(actionText);
	}
}

void TooltipWidget::setVisibility(bool show) {
	visible = show;
}

bool TooltipWidget::isVisible() {
	return visible;
}
