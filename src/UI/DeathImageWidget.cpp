#include "DeathImageWidget.h"
#include "ResourceManager.h"

#include <sstream>
#include <limits>

void DeathImageWidget::callback() {
	if (player) {
		player->revive();
	}
}

DeathImageWidget::DeathImageWidget(const std::string name) : ImageWidget(name) {
	player = nullptr;
	image.setTexture(ResourceManager::getInstance().getTexture("resources/ui/death.png"));

	btn = new ButtonWidget(name + "_bw");
	btn->setNormalTexture(&ResourceManager::getInstance().getTexture("resources/ui/button/death_continue_normal.png"));
	btn->setHoverTexture(&ResourceManager::getInstance().getTexture("resources/ui/button/death_continue_hover.png"));
	btn->setCallback(std::bind(&DeathImageWidget::callback, this));
	visible = false;
}

void DeathImageWidget::render(sf::RenderWindow& window) {
	if (!visible) return;

	sf::Vector2u diff = window.getSize() - image.getTexture()->getSize();
	sf::Vector2f diff2(window.getSize().x - btn->getSize().x, window.getSize().y - btn->getSize().y);
	image.setPosition(diff.x / 2.0f, diff.y / 2.0f - 40);
	btn->setPosition(sf::Vector2f(diff2.x / 2.0f, diff.y / 2.0f + image.getTexture()->getSize().y));
	window.draw(image);

	btn->render(window);
	//ImageWidget::render(window);
}

void DeathImageWidget::update(TimeStep timeStep) {
	visible = player && player->getHitPoints() <= 0;
	btn->update(timeStep);
}

void DeathImageWidget::associatePlayer(Player* player) {
	this->player = player;
}
