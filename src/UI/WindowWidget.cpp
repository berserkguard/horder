#include "WindowWidget.h"

#define TEXTURE_SIZE 128
#define CORNER_SIZE 8
#define BUTTON_SIZE 16

WindowWidget::WindowWidget(const std::string name) : UIWidget(WIDGET_WINDOW, name) {
	size = sf::Vector2f(240.0f, 320.0f);
	tex[0] = nullptr;
	tex[1] = nullptr;
	visible = true;
	resizable = true;
	resizing = false;
	resizeMode = -1;
	dragMode = -1;
	content.create(1024, 1024);
}

WindowWidget::~WindowWidget() { }

void WindowWidget::setResizable(bool resizable) {
	this->resizable = resizable;
}

bool WindowWidget::handleEvent(sf::Event& event) {
	if (event.type == sf::Event::MouseButtonPressed) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			if (event.mouseButton.x >= position.x && event.mouseButton.x <= position.x + size.x + 2 * CORNER_SIZE) {
				if (event.mouseButton.y >= position.y && event.mouseButton.y <= position.y + size.y + 2 * CORNER_SIZE) {
					// Check if they clicked a corner - if they did, start resizing
					if (event.mouseButton.x < position.x + CORNER_SIZE && event.mouseButton.y < position.y + CORNER_SIZE) {
						// Clicked upper-left corner
						resizing = resizable; // if resizable is true, resizing will work. otherwise, it won't
						resizeMode = 0;
						anchor = position + size;
						offset.x = 0;//position.x + CORNER_SIZE - event.mouseButton.x;
						offset.y = 0;//position.y + CORNER_SIZE - event.mouseButton.y;
					} else if (event.mouseButton.x > position.x + size.x && event.mouseButton.y < position.y + CORNER_SIZE) {
						// Clicked upper-right corner
						resizing = resizable;
						resizeMode = 1;
						anchor = position + sf::Vector2f(0.0f, size.y);
						offset.x = 0;//position.x + size.x + CORNER_SIZE - event.mouseButton.x;
						offset.y = 0;//position.y + CORNER_SIZE - event.mouseButton.y;
					} else if (event.mouseButton.x < position.x + CORNER_SIZE && event.mouseButton.y > position.y + size.y) {
						// Clicked lower-left corner
						resizing = resizable;
						resizeMode = 2;
						anchor = position + sf::Vector2f(size.x, 0.0f);
						offset.x = 0;
						offset.y = 0;
					} else if (event.mouseButton.x > position.x + size.x && event.mouseButton.y > position.y + size.y) {
						// Clicked lower-right corner
						resizing = resizable;
						resizeMode = 3;
						anchor = position;
						offset.x = 0;
						offset.y = 0;
					} else {
						// Clicked inside window; propagate to children. If no child clicked, start dragging window
						dragMode = 0;
						anchor = sf::Vector2f();
						mousePos = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
					}
					return true;
				}
			}
		}
	}
	if (event.type == sf::Event::MouseButtonReleased) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			// Always stop dragging when left mouse button is released
			dragMode = -1;
			resizing = false;
		}
	}
	if (event.type == sf::Event::MouseMoved) {
		if (dragMode == 0) {
			sf::Vector2f diff(event.mouseMove.x - mousePos.x, event.mouseMove.y - mousePos.y);
			anchor += diff;
			mousePos += diff;
			if (anchor.x * anchor.x + anchor.y * anchor.y > 2000) {
				dragMode = 1;
				position += anchor;
			}
		} else if (dragMode == 1) {
			sf::Vector2f diff(event.mouseMove.x - mousePos.x, event.mouseMove.y - mousePos.y);
			position += diff;
			mousePos += diff;
		} else if (resizing) {
			switch (resizeMode) {
			case 0:
				// Upper-left resize, lower-right is anchor
				size.x = anchor.x - event.mouseMove.x + offset.x;
				size.y = anchor.y - event.mouseMove.y + offset.y;
				if (size.x < getMinSize().x) size.x = getMinSize().x;
				if (size.y < getMinSize().y) size.y = getMinSize().y;
				position = anchor - size;
				break;
			case 1:
				// Upper-right resize, lower-left is anchor
				size.x = event.mouseMove.x - anchor.x + offset.x;
				size.y = anchor.y - event.mouseMove.y + offset.y;
				if (size.x < getMinSize().x) size.x = getMinSize().x;
				if (size.y < getMinSize().y) size.y = getMinSize().y;
				position.y = anchor.y - size.y;
				break;
			case 2:
				// Lower-left resize
				size.x = anchor.x - event.mouseMove.x + offset.x;
				size.y = event.mouseMove.y - anchor.y + offset.y;
				if (size.x < getMinSize().x) size.x = getMinSize().x;
				if (size.y < getMinSize().y) size.y = getMinSize().y;
				position.x = anchor.x - size.x;
				break;
			case 3:
				// Lower-right resize
				size.x = event.mouseMove.x - anchor.x + offset.x;
				size.y = event.mouseMove.y - anchor.y + offset.y;
				if (size.x < getMinSize().x) size.x = getMinSize().x;
				if (size.y < getMinSize().y) size.y = getMinSize().y;
				break;
			default:
				break;
			}
			// Resize the window's content
			if(content.getSize().x < size.x || content.getSize().y < size.y) {
				// TODO resize here
				//content.create(size.x, size.y);
			}
		} else {
			// Propagate to children
		}
	}
	return false;
}

void WindowWidget::setTexture(sf::Texture* tex) {
	this->tex[0] = tex;
}
void WindowWidget::setButtonTexture(sf::Texture* tex) {
	this->tex[1] = tex;
}

void WindowWidget::update(TimeStep timeStep) { }

void WindowWidget::setSize(sf::Vector2f size) {
	UIWidget::setSize(size);

	if (size.x < getMinSize().x) size.x = getMinSize().x;
	if (size.y < getMinSize().y) size.y = getMinSize().y;
}

void WindowWidget::render(sf::RenderWindow& window) {
	if (!tex[0] && !tex[1]) {
		return;
	}

	renderContent();

	// TODO: render to RenderTexture once for performance

	// Render background
	sf::Sprite bg(*tex[0], sf::IntRect(sf::Vector2i(CORNER_SIZE, CORNER_SIZE), sf::Vector2i(TEXTURE_SIZE - 2 * CORNER_SIZE, TEXTURE_SIZE - 2 * CORNER_SIZE)));
	bg.setPosition(position + sf::Vector2f(CORNER_SIZE, CORNER_SIZE));
	bg.setScale(size.x / (TEXTURE_SIZE - 2 * CORNER_SIZE), size.y / (TEXTURE_SIZE - 2 * CORNER_SIZE));
	window.draw(bg);

	// Render borders
	sf::Sprite lBorder(*tex[0], sf::IntRect(sf::Vector2i(0, CORNER_SIZE), sf::Vector2i(CORNER_SIZE, TEXTURE_SIZE - 2 * CORNER_SIZE)));
	lBorder.setPosition(position + sf::Vector2f(0.0f, CORNER_SIZE));
	lBorder.setScale(1.0f, size.y / (TEXTURE_SIZE - 2 * CORNER_SIZE));
	window.draw(lBorder);

	sf::Sprite rBorder(*tex[0], sf::IntRect(sf::Vector2i(TEXTURE_SIZE - CORNER_SIZE, CORNER_SIZE), sf::Vector2i(CORNER_SIZE, TEXTURE_SIZE - 2 * CORNER_SIZE)));
	rBorder.setPosition(position + sf::Vector2f(size.x + CORNER_SIZE, CORNER_SIZE));
	rBorder.setScale(1.0f, size.y / (TEXTURE_SIZE - 2 * CORNER_SIZE));
	window.draw(rBorder);

	sf::Sprite uBorder(*tex[0], sf::IntRect(sf::Vector2i(CORNER_SIZE, 0), sf::Vector2i(TEXTURE_SIZE - 2 * CORNER_SIZE, CORNER_SIZE)));
	uBorder.setPosition(position + sf::Vector2f(CORNER_SIZE, 0.0f));
	uBorder.setScale(size.x / (TEXTURE_SIZE - 2 * CORNER_SIZE), 1.0f);
	window.draw(uBorder);

	sf::Sprite bBorder(*tex[0], sf::IntRect(sf::Vector2i(CORNER_SIZE, TEXTURE_SIZE - CORNER_SIZE), sf::Vector2i(TEXTURE_SIZE - 2 * CORNER_SIZE, CORNER_SIZE)));
	bBorder.setPosition(position + sf::Vector2f(CORNER_SIZE, size.y + CORNER_SIZE));
	bBorder.setScale(size.x / (TEXTURE_SIZE - 2 * CORNER_SIZE), 1.0f);
	window.draw(bBorder);

	// Render corners
	sf::Sprite ulCorner(*tex[0], sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(CORNER_SIZE, CORNER_SIZE)));
	ulCorner.setPosition(position);
	window.draw(ulCorner);

	sf::Sprite urCorner(*tex[0], sf::IntRect(sf::Vector2i(TEXTURE_SIZE - CORNER_SIZE, 0), sf::Vector2i(CORNER_SIZE, CORNER_SIZE)));
	urCorner.setPosition(position + sf::Vector2f(size.x + CORNER_SIZE, 0.0f));
	window.draw(urCorner);

	sf::Sprite llCorner(*tex[0], sf::IntRect(sf::Vector2i(0, TEXTURE_SIZE - CORNER_SIZE), sf::Vector2i(CORNER_SIZE, CORNER_SIZE)));
	llCorner.setPosition(position + sf::Vector2f(0.0f, size.y + CORNER_SIZE));
	window.draw(llCorner);

	sf::Sprite lrCorner(*tex[0], sf::IntRect(sf::Vector2i(TEXTURE_SIZE - CORNER_SIZE, TEXTURE_SIZE - CORNER_SIZE), sf::Vector2i(CORNER_SIZE, CORNER_SIZE)));
	lrCorner.setPosition(position + sf::Vector2f(size.x + CORNER_SIZE, size.y + CORNER_SIZE));
	window.draw(lrCorner);

	// Render the content
	content.display();
	sf::Sprite contentSprite(content.getTexture(), sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(size.x, size.y)));
	contentSprite.setPosition(position + sf::Vector2f(CORNER_SIZE, CORNER_SIZE));
	window.draw(contentSprite);
}

void WindowWidget::setVisibility(bool show) {
	visible = show;
	dragMode = -1;
	resizing = false;
}

bool WindowWidget::isVisible() {
	return visible;
}
