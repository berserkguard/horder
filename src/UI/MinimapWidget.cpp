#include "MinimapWidget.h"

#include "ResourceManager.h"
#include "Tile.h"
#include "Map.h"
#include "MapType.h"

const float CHECK_FREQ = 0.5f;

const sf::Color PATH_COLOR(150, 150, 150);
const sf::Color BG_COLOR(42, 38, 31);
const sf::Color WALL_COLOR(0, 0, 0);
const sf::Color PLAYER_COLOR(0, 255, 255);

enum MinimapTile              {PATH                    , BG                   , WALL              , PLAYER};

void put(unsigned char pixels[196][196][4], int x, int y, sf::Color color) {
	pixels[y][x][0] = color.r;
	pixels[y][x][1] = color.g;
	pixels[y][x][2] = color.b;
	pixels[y][x][3] = color.a;
}
sf::Color get(unsigned char pixels[196][196][4], int x, int y) {
	return sf::Color(pixels[y][x][0], pixels[y][x][1], pixels[y][x][2], pixels[y][x][3]);
}

MinimapWidget::MinimapWidget(const std::string name) : UIWidget(WIDGET_MINIMAP, name) {
	visible = true;
	changed = false;

	// Make the arrow rotation look nice
	ResourceManager::getInstance().getTexture("resources/ui/minimap/arrow.png").setSmooth(true);
}

MinimapWidget::~MinimapWidget() {
	forget();
}

// TODO: optimize
void MinimapWidget::makeMinimapTexture() {
	Map* map = player->getMap();
	auto& chunks = memory[map->getType().getName()];

	unsigned char pixels[196][196][4];
	unsigned int paxels[196][196];

	for (int y = 0; y < 196; y++) {
		for (int x = 0; x < 196; x++) {
			put(pixels, x, y, BG_COLOR);
			paxels[y][x] = BG;
		}
	}

	sf::Vector2i ppos = player->getGroundPos();

	for (int y = -24; y <= 24; y++) {
		for (int x = -24; x <= 24; x++) {
			Coord pos = ppos + Coord(x, y);
			Coord chunkPos = vecFloor(sf::Vector2f(pos.x / (float)CHUNK_SIZE, pos.y / (float)CHUNK_SIZE));
			Coord relativePos = pos - chunkPos * CHUNK_SIZE;
			auto iter = chunks.find(chunkPos);
			if (iter != chunks.end()) {
				unsigned char* chunk = iter->second;
				if (chunk[relativePos.x + relativePos.y * CHUNK_SIZE] == PATH) {
					for (int y0 = 0; y0 < 4; y0++) {
						for (int x0 = 0; x0 < 4; x0++) {
							int x1 = (x + 24) * 4 + x0;
							int y1 = (y + 24) * 4 + y0;
							put(pixels, x1, y1, PATH_COLOR);
							paxels[y1][x1] = PATH;
						}
					}
				}
			}
		}
	}
	for (int x = 0 + 1; x < 196 - 1; x++) {
		for (int y = 0 + 1; y < 196 - 1; y++) {
			if (paxels[y][x] == PATH) continue;
			if (paxels[y][x + 1] == PATH || paxels[y + 1][x] == PATH || paxels[y + 1][x + 1] == PATH ||
					paxels[y][x - 1] == PATH || paxels[y - 1][x] == PATH || paxels[y - 1][x - 1] == PATH ||
					paxels[y - 1][x + 1] == PATH || paxels[y + 1][x - 1] == PATH) {
				put(pixels, x, y, WALL_COLOR);
				paxels[y][x] = WALL;
			}
		}
	}
	put(pixels, 97, 97, PLAYER_COLOR);
	put(pixels, 98, 97, PLAYER_COLOR);
	put(pixels, 97, 98, PLAYER_COLOR);
	put(pixels, 98, 98, PLAYER_COLOR);

	sf::Image img;
	img.create(196, 196, (unsigned char*)pixels);

	tex.loadFromImage(img);
}

void MinimapWidget::render(sf::RenderWindow& window) {
	if (player && changed) {
		makeMinimapTexture();
		changed = false;
	}

	sf::Sprite spr(tex);
	spr.setPosition(position);
	window.draw(spr);

	sf::Sprite overlay(ResourceManager::getInstance().getTexture("resources/ui/minimap/overlay.png"));
	overlay.setPosition(position - sf::Vector2f(2.0f, 2.0f));
	window.draw(overlay);

	sf::Sprite arrow(ResourceManager::getInstance().getTexture("resources/ui/minimap/arrow.png"));
	arrow.setPosition(position + sf::Vector2f(196 / 2, 196 / 2));
	arrow.setOrigin(5, 37);

	std::vector<int> ents;
	/// TODO: Get entrances here and rotate arrow to closest one!
	//map->getEntrances(ents, ..., ...);
	arrow.rotate(0.0f);
	window.draw(arrow);
}

void MinimapWidget::setVisibility(bool show) {
	visible = show;
}

bool MinimapWidget::isVisible() {
	return visible;
}

typedef unsigned char test[CHUNK_SIZE][CHUNK_SIZE];
void MinimapWidget::update(TimeStep timeStep) {
	if (!timeStep.hasPeriodPassed(TimeStep::HALF)) return;
	if (!player) return;
	Map* map = player->getMap();
	if (!map) return;
	Chunk* curChunk = map->getChunkContainingPos(player->getGroundPos());
	if (!curChunk) return;

	changed = true;
	auto& chunks = memory[map->getType().getName()];
	unsigned char* curChunkMemory = nullptr;

	for (int x = -8; x <= 8; x++) {
		for (int y = -8; y <= 8; y++) {
			Coord pos = player->getGroundPos() + Coord(x, y);
			Coord inChunkPos = pos - curChunk->getPositionInTiles();
			if (inChunkPos.x < 0 || inChunkPos.y < 0 || inChunkPos.x >= CHUNK_SIZE || inChunkPos.y >= CHUNK_SIZE) {
				curChunk = map->getChunkContainingPos(pos);
				if (!curChunk) return;
				inChunkPos = pos - curChunk->getPositionInTiles();
				curChunkMemory = nullptr;
			}
			if (!curChunkMemory) {
				Coord chunkPos = curChunk->getPosition();
				auto pair = chunks.emplace(chunkPos, new unsigned char[CHUNK_SIZE * CHUNK_SIZE]);
				curChunkMemory = pair.first->second;
				if (pair.second) {
					for (int i = 0; i < CHUNK_SIZE * CHUNK_SIZE; i++) {
						curChunkMemory[i] = BG;
					}
				}
			}
			Tile& tile = curChunk->getTile(inChunkPos);
			if (tile.isPath()) {
				// if it is path, test distance and raycast to see if it is seen
				b2RayCastInput input;
				input.p1 = toPhysVec(sf::Vector2f(pos.x + 0.5f, pos.y + 0.5f));
				b2Vec2 tp = toPhysVec(player->getPosition());
				b2Vec2 dif = tp - input.p1;
				dif.Normalize();
				input.p2 = dif + input.p1;
				float distsqr = (input.p1 - tp).LengthSquared();
				if (distsqr < 8 * 8) {
					input.maxFraction = 8;

					b2AABB bb;
					bb.lowerBound = b2Vec2(input.p1.x - 8, input.p1.y - 8);
					bb.upperBound = b2Vec2(input.p1.x + 8, input.p1.y + 8);
					float f = map->rayCast(input, bb);

					if (f * f > distsqr) {
						curChunkMemory[inChunkPos.x + inChunkPos.y * CHUNK_SIZE] = PATH;
					}
				}
			}
		}
	}
}

void MinimapWidget::associatePlayer(Player& p) {
	player = &p;
}

void MinimapWidget::forget() {
	for (auto iter = memory.begin(); iter != memory.end(); ++iter) {
		for (auto jter = iter->second.begin(); jter != iter->second.end(); jter++) {
			delete jter->second;
		}
	}
	memory.clear();
}
