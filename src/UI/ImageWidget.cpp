#include "ImageWidget.h"
#include "ResourceManager.h"

#include <sstream>
#include <limits>

ImageWidget::ImageWidget(const std::string name) : UIWidget(WIDGET_IMAGE, name) {
	visible = true;
}

ImageWidget::~ImageWidget() { }

void ImageWidget::setImage(std::string file) {
	image.setTexture(ResourceManager::getInstance().getTexture(file));
}

void ImageWidget::render(sf::RenderWindow& window) {
	window.draw(image);
}

void ImageWidget::setPosition(sf::Vector2f pos) {
	position = pos;
	image.setPosition(position);
}

void ImageWidget::setVisibility(bool show) {
	visible = show;
}

bool ImageWidget::isVisible() {
	return visible;
}
