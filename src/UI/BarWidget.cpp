#include "BarWidget.h"
#include "ResourceManager.h"

#include <sstream>
#include <limits>

const sf::Vector2f offsets[] = {
	sf::Vector2f(-2, -2), sf::Vector2f(-2, 0), sf::Vector2f(-2, 2),
	sf::Vector2f(0, -2), sf::Vector2f(0, 2),
	sf::Vector2f(2, -2), sf::Vector2f(2, 0), sf::Vector2f(2, 2)
};

BarWidget::BarWidget(const std::string name) : UIWidget(WIDGET_BAR, name) {
	mob = nullptr;
	visible = true;
	type = BarType::BAR_HEALTH;
}

void BarWidget::render(sf::RenderWindow& window) {
	sf::Texture* tex = nullptr;
	if (type == BAR_HEALTH) {
		tex = &ResourceManager::getInstance().getTexture("resources/ui/bar/hb_back.png");
	} else if (type == BAR_MANA) {
		tex = &ResourceManager::getInstance().getTexture("resources/ui/bar/mb_back.png");
	}
	sf::Sprite backSprite(*tex);
	backSprite.setPosition(position);
	window.draw(backSprite);

	if (type == BAR_HEALTH) {
		tex = &ResourceManager::getInstance().getTexture("resources/ui/bar/hb_middle.png");
	}else if (type == BAR_MANA) {
		tex = &ResourceManager::getInstance().getTexture("resources/ui/bar/mb_middle.png");
	}
	sf::Sprite middleSprite(*tex);
	middleSprite.setPosition(position);
	if (mob) {
		float percent = 1.0f;
		if (type == BAR_HEALTH) {
			percent = mob->getHitPoints() / mob->getMaxHitPoints();
		} else if (type == BAR_MANA) {
			// TODO
		}
		middleSprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(percent * 180, 20)));
	}
	window.draw(middleSprite);

	sf::Sprite frontSprite(ResourceManager::getInstance().getTexture("resources/ui/bar/hb_front.png"));
	frontSprite.setPosition(position);
	window.draw(frontSprite);

	// Render health text
	if (mob) {
		std::stringstream ss;
		if (type == BAR_HEALTH) {
			ss << std::ceil(mob->getHitPoints()) << " / " << std::ceil(mob->getMaxHitPoints());
		} else if (type == BAR_MANA) {
			ss << 25 << " / " << 25;
		}

		sf::Font font = ResourceManager::getInstance().getFont("resources/fonts/dimbo.ttf");
		sf::Text text(ss.str(), font);
		text.setCharacterSize(16);
		float xPos = 180 / 2 - text.getLocalBounds().width / 2;

		// Render outline
		text.setColor(sf::Color::Black);
		for (sf::Vector2f offset : offsets) {
			text.setPosition(position + sf::Vector2f(xPos + offset.x, 1 + offset.y));
			window.draw(text);
		}

		text.setColor(sf::Color::White);
		text.setPosition(position + sf::Vector2f(xPos, 1));
		window.draw(text);
	}
}

void BarWidget::setVisibility(bool show) {
	visible = show;
}

bool BarWidget::isVisible() {
	return visible;
}

void BarWidget::associateMob(Mob& mob, BarType type) {
	this->mob = &mob;
	this->type = type;
}
