#include "UIManager.h"

UIManager::UIManager() {
	currentScene = INITIAL_SCENE;
}

UIManager::~UIManager() { }

void UIManager::addWidget(UIWidget* widget) {
	int scene = (int)currentScene;
	if (widgets[scene].count(widget->getName()) > 0) {
		return;
	}
	oldVisibilities[scene].insert(std::pair<std::string, bool>(widget->getName(), widget->isVisible()));
	widgets[scene].insert(std::pair<std::string, UIWidget*>(widget->getName(), widget));
}

UIWidget* UIManager::removeWidget(std::string name) {
	int scene = (int)currentScene;
	if (widgets[scene].count(name) == 0) {
		return NULL;
	}
	UIWidget* w = widgets[scene].at(name);
	widgets[scene].erase(name);
	oldVisibilities[scene].erase(name);
	return w;
}

void UIManager::update(TimeStep timeStep) {
	int scene = (int)currentScene;

	for (auto it = widgets[scene].begin(); it != widgets[scene].end(); ++it) {
		UIWidget* widget = (*it).second;
		widget->update(timeStep);
		oldVisibilities[scene].at(widget->getName()) = widget->isVisible();
	}
}

void UIManager::setScene(Scene newScene) {
	if (newScene == currentScene) return;
	currentScene = newScene;

	// Hide all widgets not in the current scene
	for (int i = 0; i < (int)SCENE_COUNT; i++) {
		for (auto it = widgets[i].begin(); it != widgets[i].end(); ++it) {
			UIWidget* widget = (*it).second;
			if (i == (int)currentScene) {
				widget->setVisibility(oldVisibilities[i][widget->getName()]);
			} else {
				oldVisibilities[i][widget->getName()] = widget->isVisible();
				widget->setVisibility(false);
			}
		}
	}
}

Scene UIManager::getScene() {
	return currentScene;
}

bool UIManager::wasClicked() {
	/*
	int scene = (int)currentScene;
	for (auto it = widgets[scene].begin(); it != widgets[scene].end(); ++it) {
		if (!(*it).second->isVisible()) continue;
		float x = (*it).second->getPosition().x;
		float y = (*it).second->getPosition().y;
		float width = (*it).second->getSize().x;
		float height = (*it).second->getSize().y;
	}
	*/

	// TODO what is this?

	// Call *CUSTOM* SFGUI::Desktop function
	return false;//desktop.custom_WasWidgetClicked();
}

bool UIManager::handleEvent(sf::Event& event) {
	int scene = (int)currentScene;
	for (auto it = widgets[scene].begin(); it != widgets[scene].end(); ++it) {
		if (!(*it).second->isVisible()) continue;
		if ((*it).second->handleEvent(event)) {
			return true;
		}
	}
	return false;
}

void UIManager::bringToFront(UIWidget* widget) {
	//widget->bringToFront(desktop);
}

void UIManager::render(sf::RenderWindow& window, int layer) {
	int scene = (int)currentScene;
	for (auto it = widgets[scene].begin(); it != widgets[scene].end(); ++it) {
		if (!(*it).second->isVisible()) continue;
		if (layer == -1 || (*it).second->getLayer() == layer) {
			(*it).second->render(window);
		}
	}
}

UIWidget* UIManager::getWidget(const std::string name) {
	int scene = (int)currentScene;
	if (widgets[scene].count(name) == 0) {
		return NULL;
	}
	return widgets[scene].at(name);
}
