#include "UIWidget.h"

UIWidget::UIWidget(const UIWidgetType type, const std::string name) : type(type), layer(0), name(name) { }

std::string UIWidget::getName() const {
	return name;
}

void UIWidget::render(sf::RenderWindow& window) { }

void UIWidget::update(TimeStep timeStep) { }

bool UIWidget::handleEvent(sf::Event& event) {
	return false;
}

int UIWidget::getLayer() {
	return layer;
}

void UIWidget::setLayer(int layer) {
	this->layer = layer;
}

void UIWidget::setPosition(sf::Vector2f pos) {
	position = pos;
}

void UIWidget::setSize(sf::Vector2f size) {
	this->size = size;
}

sf::Vector2f UIWidget::getPosition() {
	return position;
}

sf::Vector2f UIWidget::getSize() {
	return size;
}
