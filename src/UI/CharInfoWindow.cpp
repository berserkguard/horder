#include "CharInfoWindow.h"
#include "ResourceManager.h"

#include <sstream>

#define PADDING 10
#define Y_MARGIN (PADDING + 20)
#define X_MARGIN 0

// Slot offsets relative to the top-left corner of the image.
const sf::Vector2f slotOffsets[] = {
	sf::Vector2f(65.0f, 8.0f),		// HEAD
	sf::Vector2f(9.0f, 115.0f),		// HAND
	sf::Vector2f(212.0f, 117.0f),	// OFFHAND
	sf::Vector2f(44.0f, 62.0f),		// BODY
	sf::Vector2f(38.0f, 217.0f)		// TOES
};

CharInfoWindow::CharInfoWindow(const std::string name) : WindowWidget(name) {
	// Make sure the window isn't resizable
	WindowWidget::setResizable(false);

	player = nullptr;
	if (size.x < getMinSize().x) size.x = getMinSize().x;
	if (size.y < getMinSize().y) size.y = getMinSize().y;

	idx = -1;

	// Leaving the scrollbar here but forcing it to be hidden, just in case we need it in the future
	scrollbar = new ScrollbarWidget(name + "_sb");
	scrollbar->setVisibility(false);

	tooltip = new TooltipWidget(name + "_tt");
	tooltip->setType(TT_CHARINFO);
}

void CharInfoWindow::setSize(sf::Vector2f size) {
	WindowWidget::setSize(size);

	scrollbar->setSize(sf::Vector2f(16.0f, this->size.y - 16.0f));
}

sf::Vector2f CharInfoWindow::getMinSize() {
	return sf::Vector2f(270 + 128, 246 + 45);
}

void CharInfoWindow::update(TimeStep timeStep) { }

void CharInfoWindow::render(sf::RenderWindow& window) {
	WindowWidget::render(window);

	// Draw the scrollbar
	if (scrollbar->isVisible()) {
		scrollbar->setPosition(position + sf::Vector2f(this->size.x - 8.0f, 16.0f));
		scrollbar->render(window);
	}

	// Draw the item tooltip
	if (idx > -1) {
		tooltip->render(window);
	}
}

void CharInfoWindow::renderContent() {
	content.clear(sf::Color::Transparent);

	// Render the window title
	sf::Font font = ResourceManager::getInstance().getFont("resources/fonts/dimbo.ttf");
	sf::Text titleText("Character Info", font);
	titleText.setCharacterSize(20);
	titleText.setColor(sf::Color::White);
	titleText.setPosition(PADDING, 2);
	content.draw(titleText);

	sf::Sprite charinfoSprite(ResourceManager::getInstance().getTexture("resources/ui/charinfo.png"));
	charinfoSprite.setPosition(X_MARGIN, Y_MARGIN);
	content.draw(charinfoSprite);

	if (!player) return;

	sf::Sprite itemset(ResourceManager::getInstance().getTexture("resources/items/itemset.png"));

	for (int i = 0; i < (int)EquipSlot::COUNT; i++) {
		Item& item = player->getInventory().getEquippedItem((EquipSlot)i);
		if (!item.isNull()) {
			int locX = item.getType().getGLoc().x;
			int locY = item.getType().getGLoc().y;

			itemset.setTextureRect(sf::IntRect(sf::Vector2i(locX * 64, locY * 64), sf::Vector2i(64, 64)));
			itemset.setScale(32.0f / 64.0f, 32.0f / 64.0f);
			itemset.setPosition(slotOffsets[i] + sf::Vector2f(X_MARGIN, Y_MARGIN));
			content.draw(itemset);
		}
	}

	// Render player stats
	std::stringstream statss;
	for (int i = 0; i < Stat::COUNT; i++) {
		if (!Stat::isUseful((Stat::Type)i)) continue;

		float stat = player->getStatValue((Stat::Type)i);
		std::string name = Stat::toString((Stat::Type)i);
		statss << name << ": " << stat << "\n";
	}
	sf::Text statText(statss.str(), font);
	statText.setCharacterSize(16);
	statText.setColor(sf::Color::White);
	statText.setPosition(261, 27);
	content.draw(statText);
}

bool CharInfoWindow::handleEvent(sf::Event& event) {
	// First handle scrollbar's events
	if (scrollbar->handleEvent(event)) {
		return true;
	}

	bool ret = WindowWidget::handleEvent(event);

	if (event.type == sf::Event::MouseButtonPressed) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			// Player clicked on an equipped item, so unequip it
			int slotIdx = getSlotIndex(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
			if (slotIdx > -1) {
				player->unequipItem((EquipSlot)slotIdx);

				idx = getSlotIndex(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
				if (idx > -1) {
					tooltip->setPosition(sf::Vector2f(event.mouseButton.x + 5, event.mouseButton.y + 5));
					tooltip->associateItem(player->getInventory().getEquippedItem((EquipSlot)idx));
				}
			}
		}
	} else if (event.type == sf::Event::MouseMoved) {
		idx = getSlotIndex(sf::Vector2f(event.mouseMove.x, event.mouseMove.y));
		if (idx > -1) {
			tooltip->setPosition(sf::Vector2f(event.mouseMove.x + 5, event.mouseMove.y + 5));
			tooltip->associateItem(player->getInventory().getEquippedItem((EquipSlot)idx));
		}
	}
	return ret;
}

void CharInfoWindow::associatePlayer(Player& player) {
	this->player = &player;
}

int CharInfoWindow::getSlotIndex(sf::Vector2f cursor) {
	for (int i = 0; i < (int)EquipSlot::COUNT; i++) {
		float startX = position.x + X_MARGIN + slotOffsets[i].x + 8;
		float startY = position.y + Y_MARGIN + slotOffsets[i].y + 8;
		if (cursor.x >= startX && cursor.x <= startX + 32.0f && cursor.y >= startY && cursor.y <= startY + 32.0f) {
			if (player->getInventory().getEquippedItem((EquipSlot)i).isNull()) {
				continue;
			}
			return i;
		}
	}
	return -1;
}
