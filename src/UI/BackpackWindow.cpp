#include "BackpackWindow.h"
#include "ResourceManager.h"

#include <sstream>
#include "Map.h"

#define PADDING 10

BackpackWindow::BackpackWindow(const std::string name) : WindowWidget(name) {
	player = nullptr;
	container = nullptr;
	if (size.x < getMinSize().x) size.x = getMinSize().x;
	if (size.y < getMinSize().y) size.y = getMinSize().y;

	idx = -1;
	numRows = 0;
	numCols = 0;

	scrollbar = new ScrollbarWidget(name + "_sb");
	tooltip = new TooltipWidget(name + "_tt");
	tooltip->setType(TT_BACKPACK);
}

void BackpackWindow::setSize(sf::Vector2f size) {
	numCols = (size.x - 2 * PADDING - 12.0f) / 40 + 1;
	numRows = (size.y - 2 * PADDING - 18 - 20) / 40 + 1;
	this->size.x = numCols * 40 + 1 * PADDING + 12.0f;
	this->size.y = numRows * 40 + 1 * PADDING + 18 + 20;

	scrollbar->setSize(sf::Vector2f(16.0f, this->size.y - 16.0f));
}

sf::Vector2f BackpackWindow::getMinSize() {
	return sf::Vector2f(180, 220);
}

void BackpackWindow::update(TimeStep timeStep) {
	if (!player || isPlayerBackpack()) {
		// We are in the player's backpack, which isn't bound to a position.
		return;
	}

	float dx = player->getPosition().x - pos.x;
	float dy = player->getPosition().y - pos.y;
	if (dx * dx + dy * dy > 1.0f) {
		// Player moved away from ground pile, so hide loot window
		setVisibility(false);
	}
}

void BackpackWindow::render(sf::RenderWindow& window) {
	WindowWidget::render(window);

	// Draw the scrollbar
	if (scrollbar->isVisible()) {
		scrollbar->setPosition(position + sf::Vector2f(this->size.x - 8.0f, 16.0f));
		scrollbar->render(window);
	}

	// Draw the item tooltip
	if (idx > -1) {
		tooltip->render(window);
	}
}

void BackpackWindow::renderContent() {
	content.clear(sf::Color::Transparent);

	// Render the window title
	sf::Font font = ResourceManager::getInstance().getFont("resources/fonts/dimbo.ttf");
	sf::Text titleText(isPlayerBackpack() ? "Inventory" : "Loot", font);
	titleText.setCharacterSize(20);
	titleText.setColor(sf::Color::White);
	titleText.setPosition(PADDING, 2);
	content.draw(titleText);

	sf::Sprite borderSprite(ResourceManager::getInstance().getTexture("resources/items/item_border.png"));
	for (int i = 0; i < numCols; i++) {
		for (int j = 0; j < numRows; j++) {
			borderSprite.setPosition(PADDING + i * 40, PADDING + 20 + j * 40);
			content.draw(borderSprite);
		}
	}

	if (!player || !container) {
		return;
	}

	sf::Sprite itemSprite(ResourceManager::getInstance().getTexture("resources/items/itemset.png"));
	int ct = 0;
	if (container->getNumItems() > numRows * numCols) {
		// Need scrollbar to see every item
		int maxVal = container->getNumItems() / numCols;// - numRows;
		if (container->getNumItems() % numCols > 0) {
			maxVal++;
		}
		scrollbar->configure(0, 1, 3, maxVal);
		scrollbar->setStepsShown(numRows);
		ct += scrollbar->getValue() * numCols;
	} else {
		scrollbar->configure(0, 1, 3, 0);
		scrollbar->setStepsShown(1);
	}
	for (int j = 0; j < numRows; j++) {
		for (int i = 0; i < numCols; i++) {
			if (ct >= container->getNumItems()) {
				break;
			}
			Item item = container->getItemAt(ct++);
			int locX = item.getType().getGLoc().x;
			int locY = item.getType().getGLoc().y;

			itemSprite.setTextureRect(sf::IntRect(sf::Vector2i(locX * 64, locY * 64), sf::Vector2i(64, 64)));
			itemSprite.setScale(32.0f / 64.0f, 32.0f / 64.0f);
			itemSprite.setPosition(PADDING + i * 40, PADDING + 20 + j * 40);
			content.draw(itemSprite);
		}
	}

	// Render the weight label
	std::stringstream ss;
	ss << "Total Weight: " << container->getWeight() << " kg";
	sf::Text weightText(ss.str(), font);
	weightText.setCharacterSize(16);
	weightText.setColor(sf::Color::White);
	weightText.setPosition(PADDING, size.y - 23);
	content.draw(weightText);
}

int BackpackWindow::getItemIndex(sf::Vector2f cursor) {
	int nX = (int)fabs(cursor.x - position.x - PADDING) / 40;
	int nY = (int)fabs(cursor.y - position.y - PADDING - 20) / 40;
	int id = numCols * (nY + scrollbar->getValue()) + nX;

	if (cursor.x - position.x - PADDING - nX * 40 >= 8 && cursor.y - position.y - PADDING - 20 - nY * 40 >= 8) {
		if (nX < numCols && nY < numRows && id < container->getNumItems()) {
			return id;
		}
	}
	return -1;
}

bool BackpackWindow::handleEvent(sf::Event& event) {
	// First handle scrollbar's events
	if (scrollbar->handleEvent(event)) {
		return true;
	}

	bool ret = WindowWidget::handleEvent(event);

	if (event.type == sf::Event::MouseButtonPressed) {
		// Fix for death-backpack exploit (picking-up items after death sequence starts was leaving them in backpack)
		if (player && player->getHitPoints() == 0) return ret;

		if (event.mouseButton.button == sf::Mouse::Left) {
			int index = getItemIndex(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
			if (index > -1) {
				Item item = container->getItemAt(index);

				if (isPlayerBackpack()) {
					bool success = player->getMap()->addItem(player->getGroundPos(), item, false);
					if (success) container->removeItemAt(index);
				} else {
					// This backpack is for a ground pile
					bool success = player->getInventory().getItemContainer().addItem(item);
					if (success) container->removeItemAt(index);
					if (container->getNumItems() == 0) {
						// Last item was taken, so close inventory window
						setVisibility(false);
					}
				}
				if (player->getHome() == player->getMap()) player->recalcHoardStats(player->getGroundPos());

				// Refresh tooltip since item gets dropped
				refreshTooltip(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
			}
		} else if (event.mouseButton.button == sf::Mouse::Right) {
			int index = getItemIndex(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
			if (index > -1) {
				Item item = container->getItemAt(index);

				if (item.getType().isEquippable() && &player->getInventory().getItemContainer() == container) {
					// This backpack is the player's inventory
					player->equipItem(container->removeItemAt(index));

					// Refresh tooltip since item gets equipped
					refreshTooltip(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
				}
			}
		}
	} else if (event.type == sf::Event::MouseMoved) {
		if (resizing) {
			// Make window size snap to grid
			setSize(size);
			switch (resizeMode) {
			case 0:
				// Upper-left resize, lower-right is anchor
				position = anchor - size;
				break;
			case 1:
				// Upper-right resize, lower-left is anchor
				position.y = anchor.y - size.y;
				break;
			case 2:
				// Lower-left resize
				position.x = anchor.x - size.x;
				break;
			default:
				break;
			}
		} else {
			// Show tooltip when hovering over an item
			refreshTooltip(sf::Vector2f(event.mouseMove.x, event.mouseMove.y));
		}
	}
	return ret;
}

void BackpackWindow::associateItems(ItemContainer& container, Player& player) {
	this->container = &container;
	this->player = &player;
	this->pos = player.getPosition();

	if (!isPlayerBackpack()) {
		tooltip->setType(TT_GENERIC);
	}
}

void BackpackWindow::refreshTooltip(sf::Vector2f cursor) {
	idx = getItemIndex(cursor);
	if (idx > -1) {
		tooltip->setPosition(cursor + sf::Vector2f(5, 5));
		tooltip->associateItem(container->getItemAt(idx));
	}
}

bool BackpackWindow::isPlayerBackpack() {
	return player && &player->getInventory().getItemContainer() == container;
}
