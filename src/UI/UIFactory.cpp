#include "UIFactory.h"

// Include the necessary custom widget headers
#include "ButtonWidget.h"
#include "WindowWidget.h"
#include "BackpackWindow.h"
#include "CharInfoWindow.h"
#include "BarWidget.h"
#include "MinimapWidget.h"
#include "ImageWidget.h"
#include "DeathImageWidget.h"

UIWidget* UIFactory::createWidget(UIWidgetType type, const std::string name, UIManager* ui) {
	UIWidget* widget;

	switch (type) {
	case WIDGET_BAR:
		widget = new BarWidget(name);
		break;
	case WIDGET_BUTTON:
		widget = new ButtonWidget(name);
		break;
	case WINDOW_BACKPACK:
		widget = new BackpackWindow(name);
		break;
	case WINDOW_CHARINFO:
		widget = new CharInfoWindow(name);
		break;
	case WIDGET_MINIMAP:
		widget = new MinimapWidget(name);
		break;
	case WIDGET_IMAGE:
		widget = new ImageWidget(name);
		break;
	case WIDGET_DEATH_IMAGE:
		widget = new DeathImageWidget(name);
		break;
	default:
		widget = nullptr;
		break;
	}

	if (widget) {
		ui->addWidget(widget);
	}
	return widget;
}
