#include "ScrollbarWidget.h"
#include "ResourceManager.h"

#include <sstream>
#include <limits>

ScrollbarWidget::ScrollbarWidget(const std::string name) : UIWidget(WIDGET_SCROLLBAR, name) {
	visible = true;
	size = sf::Vector2f(16, 128);
	val = 0;
	dragVal = 0;
	stepsShown = 0;
	dragging = false;
	configure(0, 1, 1, 0);
}

void ScrollbarWidget::validate() {
	if (val > maxVal - stepsShown) val = maxVal - stepsShown;
	if (val < minVal) val = minVal;
}

void ScrollbarWidget::setStepsShown(int stepsShown) {
	this->stepsShown = stepsShown;
}

bool ScrollbarWidget::handleEvent(sf::Event& event) {
	if (event.type == sf::Event::MouseButtonPressed) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			if (event.mouseButton.x >= position.x && event.mouseButton.x <= position.x + size.x) {
				if (event.mouseButton.y < position.y || event.mouseButton.y > position.y + size.y) {
					return false;
				}

				// For calculating scrubber region
				float scale = size.x / 16.0f;
				float ratio = 1.0f;
				if (stepsShown == 0 || maxVal - minVal == 0 || stepsShown >= (maxVal - minVal)) {
					ratio = 1.0f;
				} else {
					ratio = (float)stepsShown / (float)(maxVal - minVal);
				}
				float barHeight = (size.y - size.x * 2 - 2 * 6.0f * scale) * ratio + 2 * 6.0f * scale;
				float barPos = (size.y - 2 * size.x - barHeight) * (float)val / (float)(maxVal - stepsShown);

				if (event.mouseButton.y <= position.y + size.x) {
					// Clicked on up button
					val -= step;
				} else if (event.mouseButton.y >= position.y + size.y - size.x) {
					// Clicked on down button
					val += step;
				} else if (event.mouseButton.y >= position.y + barPos && event.mouseButton.y <= position.y + barPos + barHeight) {
					// Clicked on scrubber
					if (!dragging) {
						dragging = true;
						dragVal = val;
						mousePos = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);
					}
				} else if (event.mouseButton.y < position.y + barPos) {
					// Clicked above the scrubber
					val -= bigStep;
				} else if (event.mouseButton.y > position.y + barPos + barHeight) {
					// Clicked below the scrubber
					val += bigStep;
				}
				validate();
				return true;
			}
		}
	} else if (event.type == sf::Event::MouseButtonReleased) {
		if (event.mouseButton.button == sf::Mouse::Left) {
			dragging = false;
		}
	} else if (event.type == sf::Event::MouseMoved) {
		if (dragging) {
			float dy = event.mouseMove.y - mousePos.y;
			float percent = dy / (size.y - 2 * size.x);
			int dstep = percent * (float)(maxVal - minVal);
			val = dragVal + dstep;
			validate();
		}
	}
	return false;
}

void ScrollbarWidget::update(TimeStep timeStep) { }

void ScrollbarWidget::render(sf::RenderWindow& window) {
	sf::Texture& tex = ResourceManager::getInstance().getTexture("resources/ui/ui.png");

	float scale = size.x / 16.0f;

	// Bar background
	sf::Sprite bar(tex, sf::IntRect(sf::Vector2i(0, 16), sf::Vector2i(16, 16)));
	bar.setScale(scale, (size.y - 16.0f) / 16.0f);
	bar.setPosition(getPosition() + sf::Vector2f(0.0f, size.x / 2.0f));
	window.draw(bar);

	// Position indicator
	float ratio = 1.0f;
	if (stepsShown == 0 || maxVal - minVal == 0 || stepsShown >= (maxVal - minVal)) {
		ratio = 1.0f;
	} else {
		ratio = (float)stepsShown / (float)(maxVal - minVal);
	}
	float barHeight = (size.y - size.x * 2 - 2 * 6.0f * scale) * ratio + 2 * 6.0f * scale;
	float barPos = (size.y - 2 * size.x - barHeight) * (float)val / (float)(maxVal - stepsShown);
	sf::Sprite scrubberTop(tex, sf::IntRect(sf::Vector2i(16, 0), sf::Vector2i(16, 6)));
	scrubberTop.setScale(scale, scale);
	scrubberTop.setPosition(getPosition() + sf::Vector2f(0.0f, size.x + barPos));
	window.draw(scrubberTop);

	sf::Sprite scrubberMiddle(tex, sf::IntRect(sf::Vector2i(16, 6), sf::Vector2i(16, 4)));
	scrubberMiddle.setScale(scale, (size.y - size.x * 2 - 2 * 6.0f * scale) * ratio / 4.0f);
	scrubberMiddle.setPosition(getPosition() + sf::Vector2f(0.0f, size.x + 6.0f * scale + barPos));
	window.draw(scrubberMiddle);

	sf::Sprite scrubberBottom(tex, sf::IntRect(sf::Vector2i(16, 10), sf::Vector2i(16, 6)));
	scrubberBottom.setScale(scale, scale);
	scrubberBottom.setPosition(getPosition() + sf::Vector2f(0.0f, size.x + barHeight - 6.0f * scale + barPos));
	window.draw(scrubberBottom);

	// Up button
	sf::Sprite buttonUp(tex, sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(16, 16)));
	buttonUp.setScale(scale, scale);
	buttonUp.setPosition(getPosition());
	window.draw(buttonUp);

	// Down button
	sf::Sprite buttonDown(tex, sf::IntRect(sf::Vector2i(0, 32), sf::Vector2i(16, 16)));
	buttonDown.setScale(scale, scale);
	buttonDown.setPosition(getPosition() + sf::Vector2f(0.0f, size.y - size.x));
	window.draw(buttonDown);
}

void ScrollbarWidget::setSize(sf::Vector2f size) {
	this->size = size;
}

void ScrollbarWidget::setVisibility(bool show) {
	visible = show;
}

bool ScrollbarWidget::isVisible() {
	return visible;
}

int ScrollbarWidget::getValue() {
	return val;
}

void ScrollbarWidget::configure(int minVal, int step, int bigStep, int maxVal) {
	this->minVal = minVal;
	this->step = step;
	this->bigStep = bigStep;
	this->maxVal = maxVal;
	if (this->maxVal < minVal) maxVal = minVal;

	validate();
}

void ScrollbarWidget::setValue(int val) {
	this->val = val;
	validate();
}
