#include "ButtonWidget.h"
#include "ResourceManager.h"

#include <sstream>
#include <limits>

ButtonWidget::ButtonWidget(const std::string name) : UIWidget(WIDGET_BUTTON, name) {
	inside = false;
	alt = false;
	size = sf::Vector2f();
	position = sf::Vector2f();
	tex[0] = nullptr;
	tex[1] = nullptr;
	callback = nullptr;
	visible = true;
}

void ButtonWidget::setCallback(std::function<void(void)> callback){
	this->callback = callback;
}

void ButtonWidget::setNormalTexture(sf::Texture* tex) {
	this->tex[0] = tex;
	size = sf::Vector2f(tex->getSize().x, tex->getSize().y);
}

void ButtonWidget::setHoverTexture(sf::Texture* tex) {
	this->tex[1] = tex;
}

void ButtonWidget::update(TimeStep timeStep) {
	if (!sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
		alt = false;
	}
}

void ButtonWidget::render(sf::RenderWindow& window) {
	if (!tex[0] || !tex[1]) {
		return;
	}

	sf::Vector2i mouse = sf::Mouse::getPosition(window);
	if (mouse.x > position.x - size.x / 2 && mouse.x < position.x + size.x / 2 &&
			mouse.y > position.y - size.y / 2 && mouse.y < position.y + size.y / 2) {
		if (!alt && inside && sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
			if (callback) {
				callback();
			}
			alt = true;
		}
		inside = true;
	} else {
		inside = false;
	}

	sf::Texture* btn = tex[inside ? 1 : 0];
	sf::Sprite btnSprite(*btn);
	btnSprite.setOrigin(btn->getSize().x / 2, btn->getSize().y / 2);
	btnSprite.setPosition(sf::Vector2f(position.x, position.y));

	window.draw(btnSprite);
}

void ButtonWidget::setPosition(sf::Vector2f pos) {
	position = pos;
}

void ButtonWidget::setVisibility(bool show) {
	visible = show;
}

bool ButtonWidget::isVisible() {
	return visible;
}
