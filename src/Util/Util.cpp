#include "Util.h"

#include <math.h>
#include <iostream>
#include <sstream>
#include "VecUtil.h"

String capitalize(String& s) {
	s[0] = toupper((unsigned char)s[0]);
	return s;
}

String toLowerCase(String s) {
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

String pluralize(const String& s) {
	switch(s[s.size() - 1]) {
		case 'e' : {
			if (s[s.size() - 2] == 'f') {
				return s.substr(0, s.size() - 2) + "ves";
			} else return s + "s";
		} break;
		case 'f' : {
			if (s[s.size() - 2] == 'f') {
				return s.substr(0, s.size() - 2) + "ves";
			} else return s.substr(0, s.size() - 1) + "ves";
		} break;
		case 'h' : {
			char c = s[s.size() - 2];
			if (c == 'c'  || c == 's') {
				return s + "es";
			} else return s + "s";
		} break;
		case 'n' : {
			if (s[s.size() - 2] == 'o') {
				return s.substr(0, s.size() - 2) + "a";
			} else return s + "s";
		} break;
		//case 'o' : return s + "es"; break;
		case 's' : {
			if (s[s.size() - 2] == 'u') {
				return s.substr(0, s.size() - 2) + "odes";
			} else return s + "es";
		} break;
		case 'x' : return s + "es"; break;
		case 'y' : {
			char c = s[s.size() - 2];
			if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
				return s + "s";
			} else return s.substr(0, s.size() - 1) + "ies";
		} break;
		case 'z' : return s + "es"; break;
		default: return s + "s"; break;
	}
}

String article(const String& s) {
	if (s[0] == 'a' || s[0] == 'e' || s[0] == 'i' || s[0] == 'o' || s[0] == 'u') return "an";
	return "a";
}

String romanNumerals[4][10] = {{"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"},
							{"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"},
							{"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"},
							{"", "M", "MM", "MMM", "MMMM", "MMMMM", "MMMMMM", "MMMMMMM", "MMMMMMMM", "MMMMMMMMM"}};
String toRomanNumerals(int num) {
	String s;
	int digit = 0;
	while (num) {
		s = romanNumerals[digit][num % 10] + s;
		num /= 10;
		digit++;
	}
	return s;
}

sf::Vector2f strToVec(String toVec) {
	sf::Vector2f foof;
	std::stringstream ss(toVec);
	while(ss.peek() == ' ') ss.ignore();
	ss >> foof.x;
	while(ss.peek() == ' ' || ss.peek() == ',') ss.ignore();
	ss >> foof.y;
	return foof;
}

Direction strToDir(const String& dir) {
	String s = dir;
	s = toLowerCase(s);
	if (s == "none") return NONE;
	else if (s == "up") return UP;
	else if (s == "down") return DOWN;
	else if (s == "north") return NORTH;
	else if (s == "south") return SOUTH;
	else if (s == "east") return EAST;
	else if (s == "west") return WEST;
	else if (s == "northwest") return NORTHWEST;
	else if (s == "southwest") return SOUTHWEST;
	else if (s == "northeast") return NORTHEAST;
	else if (s == "southeast") return SOUTHEAST;
	return NONE;
}

sf::Color strToColor(const String& color) {
	String s = color;
	s = toLowerCase(s);
	if (s.size() == 8 && s[0] == '0' && s[1] == 'x') {
		unsigned int col;
		std::stringstream ss;
		ss << std::hex << s;
		ss >> col;
		return sf::Color(col >> 16, (col >> 8) & 0xff, col & 0xff);
	} else if (s.size() == 10 && s[0] == '0' && s[1] == 'x') {
		unsigned int col;
		std::stringstream ss;
		ss << std::hex << s;
		ss >> col;
		return sf::Color(col >> 24, (col >> 16) & 0xff, (col >> 8) & 0xff, col & 0xff);
	} else if (s == "black") return sf::Color::Black;
	else if (s == "white")   return sf::Color::White;
	else if (s == "red")     return sf::Color::Red;
	else if (s == "green")   return sf::Color::Green;
	else if (s == "blue")    return sf::Color::Blue;
	else if (s == "yellow")  return sf::Color::Yellow;
	else if (s == "magenta") return sf::Color::Magenta;
	else if (s == "cyan")    return sf::Color::Cyan;
	return sf::Color::White;
}

float strToWeight(const String& string) {
	static bool init = false;
	static std::map<String, float> stringToWeightMap;
	if (!init) {
		stringToWeightMap["g"] = stringToWeightMap["grams"] = stringToWeightMap["gram"]                               = 1.0f;
		stringToWeightMap["lb"] = stringToWeightMap["lbs"] = stringToWeightMap["pounds"] = stringToWeightMap["pound"] = 453.592f;
		stringToWeightMap["kg"] = stringToWeightMap["kilograms"] = stringToWeightMap["kilogrammes"]                   = 1000.0f;
		stringToWeightMap["oz"] = stringToWeightMap["ounces"] = stringToWeightMap["ounce"]                            = 28.3495f;
		stringToWeightMap["tons"] = stringToWeightMap["ton"]                                                          = 1016000.f;
		stringToWeightMap["tonnes"] = stringToWeightMap["tonne"] = stringToWeightMap["Mg"] = stringToWeightMap["t"]   = 1000000.f;
		stringToWeightMap["slugs"] = stringToWeightMap["slug"] = stringToWeightMap["s"]                               = 453.592f;
		stringToWeightMap["mg"] = stringToWeightMap["milligrams"] = stringToWeightMap["milligrammes"]                 = 0.001f;
	}

	float weight;
	String units;

	std::stringstream ss(string);
	ss >> weight >> units;
	return weight * stringToWeightMap[units];
}

String readYAMLStr(YAML::Node& node, String key, String def, String errorMess, std::ostream& lerr) {
	YAML::Node n = node[key];
	if (n) {
		return n.as<String>();
	} else {
		if (!errorMess.empty()) lerr << " err: " << errorMess << "\n";
		return def;
	}
}
int readYAMLInt(YAML::Node& node, String key, int def, String errorMess, std::ostream& lerr) {
	YAML::Node n = node[key];
	if (n) {
		String s = n.as<String>();
		if (s == "yes" || s == "true") return 1;
		else if (s == "no" || s == "false") return 0;
		return node[key].as<int>();
	} else {
		if (!errorMess.empty()) lerr << " err: " << errorMess << "\n";
		return def;
	}
}
float readYAMLNum(YAML::Node& node, String key, float def, String errorMess, std::ostream& lerr) {
	YAML::Node n = node[key];
	if (n) {
		return n.as<float>();
	} else {
		if (!errorMess.empty()) lerr << " err: " << errorMess << "\n";
		return def;
	}
}
std::pair<int, int> readYAMLPair(YAML::Node& node, String key, int def, String errorMess) {
	YAML::Node n = node[key];
	if (n.IsSequence() && n.size() == 2) {
		return std::make_pair(n[0].as<int>(), n[1].as<int>());
	} else {
		if (!errorMess.empty()) std::cout << " err: " << errorMess << "\n";
		return std::make_pair(def, def);
	}
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("util/pluralization", "Test the pluralize() method.") {
	String str1 = "taco";
	String str2 = "leaf";
	String str3 = "box";
	REQUIRE(pluralize(str1) == "tacos");
	REQUIRE(pluralize(str2) == "leaves");
	REQUIRE(pluralize(str3) == "boxes");
}
TEST_CASE("util/roman numerization", "Test the toRomanNumerals() method.") {
	REQUIRE(toRomanNumerals(9) == "IX");
	REQUIRE(toRomanNumerals(50) == "L");
	REQUIRE(toRomanNumerals(256) == "CCLVI");
}
