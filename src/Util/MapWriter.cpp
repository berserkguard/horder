#include "MapWriter.h"

#include <sstream>
#include "Monster.h"
#include "Rand.h"
#include "World.h"

MapWriter::MapWriter(std::string prefix) : pathPrefix(prefix) {
	seed = Rand::d(0.0, 1.0);
}

void MapWriter::loadChunk(Game& game, Chunk& chunk) {
	std::stringstream ss;
	ss << pathPrefix << game.getWorld().getPlayer().getMap()->getName() << "/" << chunk.getPosition().x << "_" << chunk.getPosition().y;
	std::ifstream file(ss.str(), std::ios::binary);
	if (!file) {
		// Chunk doesn't exist on the hard drive
		printf("Chunk not saved to file: %s\n", ss.str().c_str());
		return;
	}

	// Read game seed
	double gameSeed = 0.0;
	file.read(reinterpret_cast<char*>(&gameSeed), sizeof(double));
	if (gameSeed != seed) {
		printf("Seed '%f' differs from '%f', aborting.\n", gameSeed, seed);
		file.close();
		return;
	}

	// Read number of items
	unsigned int numItems = 0;
	file.read(reinterpret_cast<char*>(&numItems), sizeof(unsigned int));
	printf("numItems: %i\n", numItems);

	// Iterate over the number of items and add each item to the appropriate tile
	for (unsigned int i = 0; i < numItems; i++) {
		unsigned short id = 0;
		unsigned char x = 0;
		unsigned char y = 0;
		file.read(reinterpret_cast<char*>(&id), sizeof(unsigned short));
		file.read(reinterpret_cast<char*>(&x), sizeof(unsigned char));
		file.read(reinterpret_cast<char*>(&y), sizeof(unsigned char));
		printf("%i,", id);
		Item item(*(game.getValuables().getItemType(id)));
		chunk.getTile(x, y).addItem(item);
	}
	return;
	// Read number of enemies
	unsigned int numEnemies = 0;
	file.read(reinterpret_cast<char*>(&numEnemies), sizeof(unsigned int));

	// Iterate over the number of enemies and add them to the map
	Map* map = game.getWorld().getPlayer().getMap();
	for (unsigned int i = 0; i < numEnemies; i++) {
		unsigned short type = 0;
		unsigned char x = 0;
		unsigned char y = 0;
		unsigned short hp = 0;
		file.read(reinterpret_cast<char*>(&type), sizeof(unsigned short));
		file.read(reinterpret_cast<char*>(&x), sizeof(unsigned char));
		file.read(reinterpret_cast<char*>(&y), sizeof(unsigned char));
		file.read(reinterpret_cast<char*>(&hp), sizeof(unsigned short));

		const MonsterType* mt = game.getValuables().getMonsterType(type);
		Monster* m = new Monster(*mt, map->getEffects(), sf::Vector2f(x, y));
		m->setPlace(*map);
		map->addEntity(Entity::Ptr(m));
	}

	file.close();
	printf("Chunk loaded: %s\n", ss.str().c_str());
}

void MapWriter::writeChunk(Game& game, Chunk& chunk) {
	std::stringstream ss;
	Map* map = game.getWorld().getPlayer().getMap();
	ss << pathPrefix << map->getName() << "/" << chunk.getPosition().x << "_" << chunk.getPosition().y;
	std::ofstream file(ss.str(), std::ios::binary | std::ios::trunc);

	// Write game seed
	file.write(reinterpret_cast<const char*>(&seed), sizeof(double));

	// Write number of items (just placeholder for now!)
	unsigned int numItems = 0;
	long numItemsPos = file.tellp();
	file.write(reinterpret_cast<const char*>(&numItems), sizeof(unsigned int));

	// Iterate through each tile, keeping track of number of items
	for (unsigned char x = 0; x < CHUNK_SIZE; x++) {
		for (unsigned char y = 0; y < CHUNK_SIZE; y++) {
			Tile& t = chunk.getTile(x, y);
			int tileItemCount = t.getNumItems();
			if (tileItemCount == 0) {
				continue;
			}
			numItems += tileItemCount;
			for (int i = 0; i < tileItemCount; i++) {
				Item& item = t.getItem(i);
				unsigned short id = (unsigned short)item.getType().getID();
				file.write(reinterpret_cast<const char*>(&id), sizeof(unsigned short));
				file.write(reinterpret_cast<const char*>(&x), sizeof(unsigned char));
				file.write(reinterpret_cast<const char*>(&y), sizeof(unsigned char));
			}
		}
	}

	// Insert the updated numItems!
	file.seekp(numItemsPos);
	file.write(reinterpret_cast<const char*>(&numItems), sizeof(unsigned int));

	// Write number of enemies (just placeholder for now!)
	unsigned int numEnemies = 0;
	file.seekp(0, std::ios_base::end);
	long numEnemiesPos = file.tellp();
	file.write(reinterpret_cast<const char*>(&numEnemies), sizeof(unsigned int));

	// Iterate through each tile, keeping track of number of items
	for (auto iter = map->entitiesBegin(); iter != map->entitiesEnd(); ++iter) {
		Entity::Ptr ent = iter->first;
		if (!(ent->getType() == Entity::MOB && ((Mob&)*ent).getMobType() == Mob::MONSTER)) continue; // Only monsters are saved at the moment
		Monster& mon = (Monster&)*ent;
		unsigned short type = (unsigned short)mon.getMonsterType().getID();
		float xpos = mon.getPosition().x;
		float ypos = mon.getPosition().y;
		unsigned short hp = (unsigned short)mon.getHitPoints();

		file.write(reinterpret_cast<const char*>(&type), sizeof(unsigned short));
		file.write(reinterpret_cast<const char*>(&xpos), sizeof(float));
		file.write(reinterpret_cast<const char*>(&ypos), sizeof(float));
		file.write(reinterpret_cast<const char*>(&hp), sizeof(unsigned short));

		numEnemies++;
	}

	// Insert the updated numEnemies!
	file.seekp(numEnemiesPos);
	file.write(reinterpret_cast<const char*>(&numEnemies), sizeof(unsigned int));

	file.close();
	printf("Chunk saved: %s\n", ss.str().c_str());
}
