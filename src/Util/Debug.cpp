#include "Debug.h"

void* operator new(size_t size){
	//printf("%i\n", size);
	Debug* d = Debug::getInstance();
	d->current_memory += size;
	if(d->current_memory > d->max_memory){
		d->max_memory = d->current_memory;
	}
	void* ptr = malloc(size + sizeof(size_t));
	((size_t*)ptr)[0] = size;
	return ptr + sizeof(size_t);
}

void* operator new[](size_t size){
    return (operator new)(size);
}

void operator delete(void* ptr) noexcept {
	size_t* sz = (size_t*)ptr - 1;
	//printf("%u\n", (unsigned int)&sz[0]);
	Debug* d = Debug::getInstance();
	//d->current_memory -= sz[0];
	free(sz);
}

void operator delete[](void* ptr) noexcept {
	(operator delete)(ptr);
}

Debug* Debug::getInstance(){
	if(!instance){
		instance = (Debug*)malloc(sizeof(Debug));
		instance->current_memory = 0;
		instance->max_memory = 0;
	}
	return instance;
}

unsigned long Debug::getMemory(){
	return this->current_memory;
}
unsigned long Debug::getMaxMemory(){
	return this->max_memory;
}

Debug::Debug() : current_memory(0), max_memory(0){}

Debug* Debug::instance = NULL;
