/*  _   _  _____ ______ ______  _____ ______
 * | | | ||  _  || ___ \|  _  \|  ___|| ___ \
 * | |_| || | | || |_/ /| | | || |__  | |_/ /
 * |  _  || | | ||    / | | | ||  __| |    /
 * | | | |\ \_/ /| |\ \ | |/ / | |___ | |\ \
 * \_| |_/ \___/ \_| \_||___/  \____/ \_| \_|
*/

#include "Rand.h"
#include <time.h>
#include <iostream>
#include <assert.h>

std::mt19937 gen = std::mt19937(time(0));

void Rand::setSeed(unsigned int seed) {
	gen.seed(seed);
}

void Rand::discard(unsigned long long z) {
	gen.discard(z);
}

unsigned Rand::simple(unsigned high) {
	assert(high);
	std::uniform_int_distribution<int> die(0, high - 1);
	return die(gen);
}

int Rand::i(int low, int high) {
	assert(high > low);
	std::uniform_int_distribution<int> die(low, high - 1);
	return die(gen);
}

float Rand::f(float low, float high) {
	assert(high > low);
	std::uniform_real_distribution<float> die(low, high);
	return die(gen);
}

double Rand::d(double low, double high) {
	assert(high > low);
	std::uniform_real_distribution<double> die(low, high);
	return die(gen);
}

double Rand::norm(double mean, double standardDeviation) {
	std::normal_distribution<double> die(mean, standardDeviation);
	return die(gen);
}

double Rand::lognorm(double mean, double standardDeviation) {
	std::lognormal_distribution<double> die(mean, standardDeviation);
	return die(gen);
}

double Rand::exp(double lambda) {
	std::exponential_distribution<double> die(lambda);
	return die(gen);
}

unsigned int Rand::dice(unsigned int sides, unsigned int num) {
	assert(sides);
	std::uniform_int_distribution<unsigned int> die(1, sides);
	unsigned int sum = 0;
	for (unsigned int i = 0; i < num; i++) {
		sum += die(gen);
	}
	return sum;
}

const int RANDA = 14741;
const int RANDB = 7919;
const int RANDC = 44318;
const int RANDM = 1804289383;
const int RANDN = 846930886;
int Rand::consistent(int low, int high, int a, int b, int c) {
	unsigned short r = (RANDA * a + RANDB * b + RANDC * c + RANDM) ^ RANDN;
	return r % (high - low) + low;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("util/rand", "Test Rand.") {
	int fah = Rand::dice(10, 10);
	REQUIRE(fah >= 10);
	REQUIRE(fah <= 100);
	Rand::setSeed(fah);

	int bah = Rand::i(0, 10);
	REQUIRE(bah >= 0);
	REQUIRE(bah < 10);
	REQUIRE(Rand::consistent(1, 1000, -99, bah) == Rand::consistent(1, 1000, -99, bah));
	Rand::discard(100);
	double blah = Rand::d();
	REQUIRE(blah >= 0.);
	REQUIRE(blah < 1.);

	Rand::setSeed(fah);
	REQUIRE(Rand::i(0, 10) == bah);
	Rand::discard(100);
	REQUIRE(Rand::d() == blah);

	Rand::setSeed(time(0));
}
