#include "StringMatcher.h"

StringMatcher::StringMatcher(std::string strings[], int ints[], size_t len, int defaultValue): def(defaultValue) {
	for (size_t i = 0; i < len; i++) {
		strMap[strings[i]] = ints[i];
	}
}

int StringMatcher::match(std::string str) {
	auto iter = strMap.find(str);
	if (iter == strMap.end()) return def;
	return iter->second;
}
