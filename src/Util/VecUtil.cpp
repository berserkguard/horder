#include "VecUtil.h"

const Direction rotes[] = {NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST};
const int invRotes[] = {-1, 0, 2, 1, 4, -1, 3, -1, 6, 7, -1, -1, 5};
Direction turn(Direction dir, int degrees) {
	assert(!(degrees % 45) && invRotes[dir] != -1 && degrees >= -360);
	return rotes[(invRotes[dir] + ((degrees + 360) % 360) / 45) % 8];
}

const float TAU = 6.2831853f;
Angle dirToAngle(Direction dir) {
	switch(dir) {
		case NORTH:     return 0;           break;
		case NORTHEAST: return TAU / 8;     break;
		case EAST:      return TAU / 4;     break;
		case SOUTHEAST: return TAU / 8 * 3; break;
		case SOUTH:     return TAU / 2;     break;
		case SOUTHWEST: return TAU / 8 * 5; break;
		case WEST:      return TAU / 4 * 3; break;
		case NORTHWEST: return TAU / 8 * 7; break;
		default: return 0;
	}
}


//                                  0000  , 0001        , 0010       , 0011        , 0100       , 0101  , 0110       , 0111  , 1000        , 1001         , 1010  , 1011  , 1100
//                                  NONE  , NORTH       , EAST       , NORTHEAST   , SOUTH      , X     , SOUTHEAST  , X     , WEST        , NORTHWEST    , X     , X     , SOUTHWEST
const Coord directionVectors[] = {ORIGIN, Coord(0, -1), Coord(1, 0), Coord(1, -1), Coord(0, 1), ORIGIN, Coord(1, 1), ORIGIN, Coord(-1, 0), Coord(-1, -1), ORIGIN, ORIGIN, Coord(-1, 1)};
Coord dirVector(Direction dir) {
	return directionVectors[dir];
}

Coord pairToCoord(std::pair<int, int> pair) {
	return Coord(pair.first, pair.second);
}

bool toRotateClockwise(float from, float to) {
	float direction = cos(from) * sin(to) - cos(to) * sin(from);
	if (direction > 0.0f) {
		return true;
	} else {
		return false;
	}
}

b2Vec2 toPhysVec(sf::Vector2f vec) {
	return b2Vec2(vec.x, vec.y);
}

sf::Vector2f toSFMLVec(b2Vec2 vec) {
	return sf::Vector2f(vec.x, vec.y);
}

Coord vecFloor(sf::Vector2f vec) {
	return Coord(std::floor(vec.x), std::floor(vec.y));
}
sf::Vector2f vecFloorF(sf::Vector2f vec) {
	return sf::Vector2f(std::floor(vec.x), std::floor(vec.y));
}

float vecSqrMag(sf::Vector2f vec) {
	return vec.x * vec.x + vec.y * vec.y;
}
float vecMagnitude(sf::Vector2f vec) {
	return std::sqrt(vec.x * vec.x + vec.y * vec.y);
}
sf::Vector2f vecNormalize(sf::Vector2f vec) {
	float mag = vecMagnitude(vec);
	assert(mag != 0);
	return sf::Vector2f(vec.x / mag, vec.y / mag);
}
Angle vecAngle(sf::Vector2f vec) {
	return std::atan2(vec.x, -vec.y);
}
sf::Vector2f angleVec(Angle angle) {
	return sf::Vector2f(std::sin(angle), -std::cos(angle));
}
sf::Vector2f vecRotate(sf::Vector2f vec, Angle angle) {
	return sf::Vector2f(vec.x * cos(angle) - vec.y * sin(angle), vec.y * cos(angle) - vec.x * sin(angle));
}
