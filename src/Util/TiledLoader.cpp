#include "TiledLoader.h"

#include "ValuablesManager.h"
#include "TiledLoader.h"
#include "Map.h"
#include "Interactible.h"
#include <base64.h>
#include <zlib.h>

using namespace tinyxml2;

TiledLoader::TiledLoader(ValuablesManager& valuables): valuables(&valuables) { }

// takes the data from a tiled layer, converts from base 64 then uncompresses
std::unique_ptr<unsigned[]> dedatifyText(XMLElement& element, int size) {
	String s = element.FirstChildElement()->GetText();
	s.erase(remove_if(s.begin(), s.end(), isspace)); //trim
	const String& text = base64_decode(s); //from base 64

	// uncompress
	char* outc = new char[size * sizeof(unsigned)];
	uLongf outputSize = size * sizeof(unsigned);
	uncompress((Bytef*)outc, &outputSize, (Bytef*)text.c_str(), text.size());
	unsigned* out = (unsigned*)outc;

	return std::unique_ptr<unsigned[]>(out);
}

void TiledLoader::loadFile(Map& toLoad, String filename) {
	std::cout << "  Loading " << filename << std::endl;

	map = &toLoad;

	XMLDocument doc; // first load the file
	int success = doc.LoadFile(filename.c_str());
	if (success != XML_SUCCESS) success = doc.LoadFile(("resources/" + filename).c_str());
	if (success != XML_SUCCESS) {
		std::cout << "   Could not find or load " << filename << " or resources/" << filename << std::endl;
	}

	// get basic info
	XMLElement* mapBase = doc.RootElement();
	size = Coord(mapBase->IntAttribute("width"), mapBase->IntAttribute("height"));
	Coord tileSize(mapBase->IntAttribute("tilewidth"), mapBase->IntAttribute("tileheight"));
	assert(tileSize.x == TILE_SIZE && tileSize.y == TILE_SIZE);

	loadTilesetOffsets(mapBase->FirstChildElement("tileset"));
	int mainOffset = offsets["Main"];
	int objOffset = mainOffset;

	map->allocateChunks(size, Chunk::LOADED); // Allocate all the chunks needed to store the entire map

	// parse the tile info
	XMLElement* curLayer;
	for (curLayer = mapBase->LastChildElement("tileset")->NextSiblingElement(); curLayer; curLayer = curLayer->NextSiblingElement()) {
		String type = curLayer->Value();
		String name = curLayer->Attribute("name");
		if (type == "layer") {
			if (name == "Floor") {
				loadFloors(*curLayer, mainOffset);
			} else if (name == "Paths") {
				loadPaths(*curLayer);
			} else if (name == "Objects") {
				loadTileObjs(*curLayer, objOffset);
			}
		} else if (type == "objectgroup") {
			if (name == "Interaction") {
				loadInteract(*curLayer);
			} else if (name == "Walls") {
				loadWalls(*curLayer);
			} else if (name == "Objects") {
				loadObjObjs(*curLayer, objOffset);
			}
		}
	}
}

void TiledLoader::loadFloors(XMLElement& layer, int offset) {
	std::unique_ptr<unsigned[]> out = dedatifyText(layer, size.x * size.y);
	for (int x = 0; x < size.x; x++) {
		for (int y = 0; y < size.y; y++) {
			int rid = out[y * size.x + x];
			int id = rid - offset;
			Coord gloc(id % TILE_TEX_SIZE, id / TILE_TEX_SIZE);
			Tile& tile = map->getTile(x, y);
			const TileType* type = valuables->getTileType(gloc);
			if (type) {
				tile.setFloor(*type);
			} else if (rid) {
				type = valuables->createTileType(gloc);
				if (type) tile.setFloor(*type);
			}
		}
	}
}

void TiledLoader::loadPaths(XMLElement& layer) {
	std::unique_ptr<unsigned[]> out = dedatifyText(layer, size.x * size.y);
	for (int x = 0; x < size.x; x++) {
		for (int y = 0; y < size.y; y++) {
			int rid = out[y * size.x + x];
			Tile& tile = map->getTile(x, y);
			if (rid) {
				tile.setPath(Tile::YES);
			} else {
				tile.setPath(Tile::NO);
			}
		}
	}
}

void TiledLoader::loadTileObjs(XMLElement& element, int offset) {
	std::unique_ptr<unsigned[]> out = dedatifyText(element, size.x * size.y);
	for (int x = 0; x < size.x; x++) {
		for (int y = 0; y < size.y; y++) {
			int id = out[y * size.x + x] - offset;
			sf::IntRect rect(Coord(id % TILE_TEX_SIZE, id / TILE_TEX_SIZE) * TILE_SIZE, Coord(TILE_SIZE, TILE_SIZE));
			map->addObject(Coord(x, y) * TILE_SIZE, rect);
		}
	}
}

void TiledLoader::loadInteract(XMLElement& element) {
	XMLElement* currentObject = element.FirstChildElement("object");
	while(currentObject) {
		sf::Vector2f position(currentObject->IntAttribute("x"), currentObject->IntAttribute("y"));
		sf::Vector2f size(currentObject->IntAttribute("width"), currentObject->IntAttribute("height"));
		sf::FloatRect rect(position, size);
		String name = currentObject->Attribute("name");

		std::vector<Coord> positions;
		for (int y = position.y / TILE_SIZE; y < (position.y + size.y) / TILE_SIZE; y++) {
			for (int x = position.x / TILE_SIZE; x < (position.x + size.x) / TILE_SIZE; x++) {
				positions.push_back(Coord(x, y));
			}
		}
		Interactible& newInteract = map->addInteractible(name, positions);

		loadProperties(currentObject->FirstChildElement("properties"));

		String type = currentObject->Attribute("type");
		if (type == "Special") {
			newInteract.setSpecial();
		} else if (type == "Connection") {
			newInteract.setConnection(strToDir(getProperty("direction")), strToVec(getProperty("destination")));
		}

		currentObject = currentObject->NextSiblingElement("object");
	}
}

void TiledLoader::loadWalls(tinyxml2::XMLElement& element) {
	XMLElement* currentObject = element.FirstChildElement("object");
	while(true) {
		XMLElement* polyline = currentObject->FirstChildElement("polyline");

		std::vector<b2Vec2> vertices;
		b2Vec2 pos(currentObject->IntAttribute("x") / (float)TILE_SIZE, currentObject->IntAttribute("y") / (float)TILE_SIZE);
		String points = String(polyline->Attribute("points")) + "  ";
		std::istringstream ss(points);
		char a;
		b2Vec2 curr;
		// parses the long number pair string
		while(ss >> curr.x >> a >> curr.y) {
			curr.x /= (float)TILE_SIZE;
			curr.y /= (float)TILE_SIZE;
			vertices.push_back(curr + pos);
		}
		b2ChainShape chain;
		chain.CreateChain(&vertices[0], vertices.size());
		map->addWall(chain, *map->getChunk(Coord(0, 0))); // TODO eerrherer

		if (currentObject == element.LastChildElement("object")) break;
		currentObject = currentObject->NextSiblingElement("object");
	}
}

void TiledLoader::loadObjObjs(tinyxml2::XMLElement& layer, int offset) {
	XMLElement* currentObject = layer.FirstChildElement("object");
	while(true) {
		sf::Vector2i pos(currentObject->IntAttribute("x"), currentObject->IntAttribute("y") - TILE_SIZE);

		int id = currentObject->IntAttribute("gid") - offset;
		sf::Vector2i texPos = Coord(id % TILE_TEX_SIZE, id / TILE_TEX_SIZE) * TILE_SIZE;

		loadProperties(currentObject->FirstChildElement("properties"));
		Angle rotation = getIntProperty("rotation") / 360.f * TAU;
		sf::Vector2i size(getIntProperty("width", 1) * TILE_SIZE, getIntProperty("height", 1) * TILE_SIZE);

		MapObject mapObject(pos, size, texPos);
		mapObject.setRotation(rotation);
		map->addObject(mapObject);

		if (currentObject == layer.LastChildElement("object")) break;
		currentObject = currentObject->NextSiblingElement("object");
	}
}

void TiledLoader::loadTilesetOffsets(XMLElement* element) {
	offsets.clear();
	while(String(element->Value()) == "tileset") {
		offsets[element->Attribute("name")] = element->IntAttribute("firstgid");
		element = element->NextSiblingElement();
	}
}

void TiledLoader::loadProperties(XMLElement* element) {
	properties.clear();
	if (element) {
		XMLElement* currentProperty = element->FirstChildElement("property");
		while(currentProperty) {
			properties[currentProperty->Attribute("name")] = currentProperty->Attribute("value");
			currentProperty = currentProperty->NextSiblingElement("property");
		}
	}
}

String TiledLoader::getProperty(String name, String def) {
	auto iter = properties.find(name);
	if (iter == properties.end()) return def;
	return iter->second;
}
int TiledLoader::getIntProperty(String name, int def) {
	auto iter = properties.find(name);
	if (iter == properties.end()) return def;
	std::stringstream ss(iter->second);
	int res;
	ss >> res;
	return res;
}
