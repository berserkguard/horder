#include "TimeStep.h"

const float PERIOD_DURATIONS[TimeStep::PERIOD_COUNT] = {0.1f, 0.2f, 0.5f, 1.0f};

TimeStep::TimeStep(float secondsPassed) {
	diff = secondsPassed;
}

TimeStep::TimeStep(sf::Time timePassed, PeriodManager& periodManager): diff(timePassed.asMicroseconds() / 1000000.f) {
	for (int i = 0; i < PERIOD_COUNT; i++) {
		periodManager.periodCurrents[i] += diff;
		if (periodManager.periodCurrents[i] >= PERIOD_DURATIONS[i]) {
			periods[i] = true;
			periodManager.periodCurrents[i] -= PERIOD_DURATIONS[i];
		} else {
			periods[i] = false;
		}
	}
}

float TimeStep::milli() const {
	return diff * 1000.f;
}

float TimeStep::sec() const {
	return diff;
}

float TimeStep::min() const {
	return diff / 60.f;
}

bool TimeStep::hasPeriodPassed(Period period) const {
	return periods[period];
}

PeriodManager::PeriodManager() {
	for (int i = 0; i < TimeStep::PERIOD_COUNT; i++) {
		periodCurrents[i] = 0;
	}
}
