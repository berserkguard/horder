#include "ResourceManager.h"

ResourceManager::ResourceManager() { }

ResourceManager& ResourceManager::getInstance() {
	static ResourceManager instance;
	return instance;
}

sf::Texture& ResourceManager::getTexture(std::string name) {
	auto iter = textures.find(name);
	if (iter == textures.end()) {
		// Resource not yet loaded, so load it
		sf::Texture& tex = textures[name];
		tex.loadFromFile(name);
		return tex;
	}
	return iter->second;
}

sf::Shader& ResourceManager::getShader(std::string name) {
	//assert(sf::Shader::isAvaliable());
	auto iter = shaders.find(name);
	if (iter == shaders.end()) {
		sf::Shader& shader = shaders[name];
		if (shader.loadFromFile("resources/shaders/" + name + ".vert", "resources/shaders/" + name + ".frag")) return shader;
		if (shader.loadFromFile("resources/shaders/" + name + ".vert", sf::Shader::Vertex)) return shader;
		if (shader.loadFromFile("resources/shaders/" + name + ".frag", sf::Shader::Fragment)) return shader;
	}
	return iter->second;
}

sf::Font& ResourceManager::getFont(std::string name) {
	auto iter = fonts.find(name);
	if (iter == fonts.end()) {
		// Resource not yet loaded, so load it
		sf::Font& font = fonts[name];
		font.loadFromFile(name);
		return font;
	}
	return iter->second;
}

sf::SoundBuffer& ResourceManager::getSoundBuffer(std::string name) {
	auto iter = sounds.find(name);
	if (iter == sounds.end()) {
		// Resource not yet etc.
		sf::SoundBuffer& sound = sounds[name];
		sound.loadFromFile(name);
		return sound;
	}
	return iter->second;
}

#include "catch.hpp"
//* UNIT TESTS
// missing: shader
TEST_CASE("resource/manager", "Test if things are there.") {
	ResourceManager& rm = ResourceManager::getInstance();

	sf::Font& font = rm.getFont("unittest/what.ttf");
	REQUIRE(font.getLineSpacing(12) == 13);
	sf::SoundBuffer& buffer = rm.getSoundBuffer("unittest/slime.wav");
	REQUIRE(buffer.getDuration().asMilliseconds() == 337);
	sf::Texture& texture = rm.getTexture("unittest/horder.png");
	REQUIRE(texture.getSize() == sf::Vector2u(512, 512));
}
