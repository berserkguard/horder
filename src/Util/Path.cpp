#include "Path.h"

#include <sstream>
#include <queue>
#include <map>
#include <cmath>
#include <cstddef>
#include <iostream>

const int DIAG_NUM = 8;
const Coord DIAG_DIRS[DIAG_NUM] = {Coord(0, 1), Coord(-1, 0), Coord(1, 0), Coord(0, -1), Coord(-1, 1), Coord(1, 1), Coord(-1, -1), Coord(1, -1)};
const int ORTH_NUM = 4;
const Coord ORTH_DIRS[ORTH_NUM] = {Coord(0, 1), Coord(-1, 0), Coord(1, 0), Coord(0, -1)};

const int ORTH_DIST = 10;
const int DIAG_DIST = 14;

struct Node {
	unsigned int g, h;
	int f;
	Coord loc;
	Node* parent;
	Coord dir;

	void setParent(Node& p) {
		dir = loc - p.loc;
		parent = &p;
	}
	Node(Node* p, Coord loc, Coord dest) : loc(loc), dir(ORIGIN) {
		if (p) setParent(*p);
		else parent = nullptr;
		refresh(dest);
	}
	unsigned int getG(Node& p) {
		Coord tdir = loc - p.loc;
		if (tdir.x && tdir.y /* if diagonal*/ ) {
			return p.g + DIAG_DIST;
		} else {
			if (tdir == p.dir) {
				return p.g + ORTH_DIST;
			}
			return p.g + ORTH_DIST + 1;
		}
	}
	unsigned int getH(Coord dest) {
		return (std::abs(dest.x - loc.x) + std::abs(dest.y - loc.y)) * 10;
	}
	void refresh(Coord dest) {
		if (parent) {
			g = getG(*parent);
		} else {
			g = 0;
		}
		h = getH(dest);
		f = g + h;
	}
};
struct CompareNode: public std::binary_function<Node*, Node*, bool> {
	bool operator()(const Node* lhs, const Node* rhs) const {
		return lhs->f > rhs->f;
	}
};

Path::Path(): dist(-1), grid(NULL) { }
Path::Path(Grid& grid, void* mob, int flags): dist(-1), grid(&grid), mob(mob) {
	setFlags(flags);
}

void Path::setGrid(Grid& g) {
	grid = &g;
}

void Path::setMob(void* m) {
	mob = m;
}

void Path::setFlags(int flags) {
	diagonal = flags & DIAGONAL;
	smooth = flags & SMOOTH;
}

bool Path::generate(sf::Vector2f s, sf::Vector2f d) {
	start = s;
	dest = d;
	path.clear();

	//first set up the grid
	Coord gridStart = vecFloor(start);
	Coord gridDest = vecFloor(dest);
	Coord gridSize(grid->getWidth(), grid->getHeight());
	Coord off;
	if (gridSize.x == -1) {
		gridSize = Coord(std::abs(gridStart.x - gridDest.x) + 6, std::abs(gridStart.y - gridDest.y) + 6);
		off = Coord(std::min(gridStart.x, gridDest.x) - 3, std::min(gridStart.y, gridDest.y) - 3);
	}

	std::pair<int, Node*> arr[gridSize.x][gridSize.y]; //0 = unvisited, 1 = open, 2 = closed
	std::priority_queue<Node*, std::vector<Node*>, CompareNode> open;
	for (int j = 0; j < gridSize.y; j++) {
		for (int i = 0; i < gridSize.x; i++) {
			if (grid->isObstructed(Coord(i, j) + off, mob)) {
				arr[i][j] = std::pair<int, Node*>(2, NULL);
			} else {
				arr[i][j] = std::pair<int, Node*>(0, NULL);
			}
		}
	}
	arr[gridDest.x - off.x][gridDest.y - off.y].first = 0;
	open.push(new Node(NULL, gridStart, gridDest));
	arr[gridStart.x - off.x][gridStart.y - off.y] = std::pair<int, Node*>(1, open.top());
	Node* current;

	int numDirs;
	const Coord* dirs;
	if (diagonal) {
		numDirs = DIAG_NUM;
		dirs = DIAG_DIRS;
	} else {
		numDirs = ORTH_NUM;
		dirs = ORTH_DIRS;
	}

	//next start the loop
	bool done = false;
	bool success = false;
	while(!done) {
		current = open.top();
		open.pop();
		arr[current->loc.x - off.x][current->loc.y - off.y].first = 2;
		for (int i = 0; i < numDirs; i++) {
			sf::Vector2i curr = dirs[i] + current->loc - off;
			if (curr.x < 0 || curr.x >= gridSize.x || curr.y < 0 || curr.y >= gridSize.y) continue;
			int val = arr[curr.x][curr.y].first;
			if (val == 2) continue;
			else if (val == 1) {
				Node* n = arr[curr.x][curr.y].second;
				if (n->getG(*current) < n->g) {
					Node* temp = n;
					temp->setParent(*current);
					std::vector<Node*> foonies;
					while (!open.empty()) {
						foonies.push_back(open.top());
						open.pop();
					}
					temp->refresh(gridDest);
					for (unsigned int i = 0; i < foonies.size(); i++) {
						open.push(foonies[i]);
					}
				}
			} else {
				arr[curr.x][curr.y] = std::pair<int, Node*>(1, new Node(current, curr + off, gridDest));
				open.push(arr[curr.x][curr.y].second);
			}
        }
		if (arr[gridDest.x - off.x][gridDest.y - off.y].first == 2) {
			done = true;
			success = true;
		} else if (open.empty()) {
			done = true;
			success = false;
		}
	}
	if (success) {
		dist = current->g;
		int len = 0;

		Node* temp = current;
		while(temp) {
			while (smooth && temp->parent && temp->parent->parent) {
				if (walkable(temp->loc, temp->parent->parent->loc)) {
					temp->parent = temp->parent->parent;
				} else break;
			}
			len++;
			temp = temp->parent;
		}

		path.resize(len);

		temp = current;
		for (int i = 1; temp; i++) {
			path[len - i] = sf::Vector2f(temp->loc.x + .5f, temp->loc.y + .5f);
			temp = temp->parent;
		}

		path[0] = start;
		path[len - 1] = dest;
	} else {
		dist = -1;
	}
	for (int j = 0; j < gridSize.y; j++) {
		for (int i = 0; i < gridSize.x; i++) {
			delete arr[i][j].second;
		}
	}
	return success;
}

int Path::length() {
	return path.size();
}
float Path::distance() {
	return dist / (float)ORTH_DIST;
}
sf::Vector2f Path::at(unsigned int i) {
	assert(i < path.size());
	return path[i];
}

std::string Path::toString() {
	std::ostringstream ss;
	for (size_t i = 0; i < path.size(); i++) {
		ss << path[i].x << "," << path[i].y << " ";
	}
	return ss.str();
}

bool Path::walkable(Coord from, Coord to) {
	int dx = abs(to.x - from.x);
	int dy = abs(to.y - from.y);
	int x = from.x;
	int y = from.y;
	int n = 1 + dx + dy;
	int xInc = (to.x > from.x) ? 1 : -1;
	int yInc = (to.y > from.y) ? 1 : -1;
	int error = dx - dy;
	dx *= 2;
	dy *= 2;
	for (; n > 0; --n) {
		if (grid->isObstructed(Coord(x, y), mob)) {
			return false;
		} else if (error > 0) {
			x += xInc;
			error -= dy;
		} else {
			y += yInc;
			error += dx;
		}
	}
	return true;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("util/path", "Test Path.") {
	class TestGrid: public Grid {
		bool isObstructed(sf::Vector2i position, void* mob) {
			return arr[position.y][position.x];
		}
		int getWidth() {return 10;}
		int getHeight() {return 10;}

		bool arr[10][10] = {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		                    {0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
		                    {0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
		                    {0, 0, 1, 0, 1, 1, 1, 1, 1, 1},
		                    {0, 0, 1, 0, 1, 0, 0, 1, 0, 0},
		                    {0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
		                    {0, 0, 0, 1, 0, 1, 0, 1, 0, 0},
		                    {0, 0, 0, 1, 1, 1, 0, 1, 0, 0},
		                    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
		                    {0, 0, 0, 0, 0, 0, 0, 1, 0, 0}};
	};
	TestGrid testGrid;
	Path path(testGrid, NULL, Path::NONE);

	REQUIRE(path.generate(sf::Vector2f(3, 3), sf::Vector2f(9, 2)) == true);
	REQUIRE(path.length() == 34);
	REQUIRE(vecFloor(path.at(8)) == Coord(6, 8));

	path.setFlags(Path::DIAGONAL);
	REQUIRE(path.generate(sf::Vector2f(3, 3), sf::Vector2f(9, 2)) == true);
	REQUIRE(path.length() == 7);
	REQUIRE(vecFloor(path.at(3)) == Coord(6, 2));

	REQUIRE(path.generate(sf::Vector2f(3, 3), sf::Vector2f(9, 4)) == false);
}

