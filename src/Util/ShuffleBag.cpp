#include "ShuffleBag.h"

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("util/bag/size", "Test ShuffleBag size.") {
	ShuffleBag<int> shuffleBag;

	REQUIRE(shuffleBag.size() == 0);

	shuffleBag.add(2);
	shuffleBag.add(3);
	shuffleBag.add(1);

	REQUIRE(shuffleBag.size() == 3);

	shuffleBag.grab();

	REQUIRE(shuffleBag.size() == 2);
}
TEST_CASE("util/bag/multi", "Test ShuffleBag multiple same.") {
	ShuffleBag<int> shuffleBag;

	shuffleBag.add(4);
	shuffleBag.add(4);
	shuffleBag.add(4);

	REQUIRE(shuffleBag.grab() == 4);
	REQUIRE(shuffleBag.grab() == 4);
	REQUIRE(shuffleBag.grab() == 4);
	REQUIRE(shuffleBag.size() == 0);
}
TEST_CASE("util/bag/throw", "Test ShuffleBag out_of_range error.") {
	ShuffleBag<std::string> shuffleBag;

	shuffleBag.add("bah");

	REQUIRE(shuffleBag.grab() == "bah");
	//REQUIRE_THROWS(shuffleBag.grab());
}
TEST_CASE("util/bag/accurate", "Test ShuffleBag accuracy.") {
	ShuffleBag<float> shuffleBag;

	shuffleBag.add(0.1f);
	shuffleBag.add(0.4f);
	shuffleBag.add(0.3f);
	shuffleBag.add(0.9f);
	shuffleBag.add(0.2f);

	std::set<float> shuffleSet;
	shuffleSet.insert(0.1f);
	shuffleSet.insert(0.4f);
	shuffleSet.insert(0.3f);
	shuffleSet.insert(0.9f);
	shuffleSet.insert(0.2f);

	float bah = shuffleBag.grab();
	REQUIRE(shuffleSet.count(bah) == 1);
	REQUIRE(shuffleBag.grab() != bah);

	shuffleBag.add(10.101f);
	shuffleBag.add(10101.f);
	REQUIRE(shuffleBag.size() == 5);
}
