#include "Map.h"
#include "EffectsManager.h"
#include "Monster.h"
#include "Chunk.h"
#include "Rand.h"
#include "MapType.h"
#include "ValuablesManager.h"
#include "Interactible.h"

Map::Map(const MapType& type, EffectsManager& effects): type(&type), mobSpawner(*this, effects), effects(&effects) {
	b2BodyDef wallsDef;
	wallsDef.userData = NULL;
	wallsDef.type = b2_staticBody;
	walls = Get::physWorld().CreateBody(&wallsDef);

	seed = time(0);

	active = false;
	walls->SetActive(false);

	player = NULL;
	safe = 1;

	rayCastCloseShapesSetUp = false;
}

Map::~Map() {
	// clear all the chunks
	while(!chunks.empty()) {
		destroyChunk(*(chunks.begin()->second));
	}

	// clear all the userdata
	for (b2Fixture* fixture = walls->GetFixtureList(); fixture; fixture = fixture->GetNext()) {
		delete (PhysUserData*)fixture->GetUserData();
	}
	Get::physWorld().DestroyBody(walls);
}

void Map::update(TimeStep timeStep) {
	std::vector<std::tuple<Entity::Ptr, Chunk*, Chunk*>> removals;
	for (auto iter = entitiesBegin(); iter != entitiesEnd(); ++iter) {
		Entity::Ptr entity = iter->first;
		Coord coord = vecFloor(entity->getPosition());

		// do monster line of sight
		if (timeStep.hasPeriodPassed(TimeStep::TENTH) && entity->getType() == Entity::MOB) {
			Mob& mob = (Mob&)*entity;
			if (mob.getMobType() == Mob::MONSTER) {
				bool seen = monsterSight((Monster&)mob);
				if (seen) {
					safe -= 1;
					if (safe < 0) safe = 0;
				} else {
					safe += 0.1;
					if (safe > 1) safe = 1;
				}
			}
		}

		// check of the entity has changed chunks
		Chunk& overChunk = createChunkContainingPos(coord);
		if (timeStep.hasPeriodPassed(TimeStep::TENTH)) {
			Chunk& curChunk = iter->second;
			if (&overChunk != &curChunk) {
				removals.emplace_back(entity, &curChunk, &overChunk);
			}
		}

		// traps
		if (entity->getType() == Entity::MOB) {
			Tile& tile = overChunk.getTile(coord - overChunk.getPositionInTiles());
			if (tile.getSpecial() == Tile::TRAP_TRIGGER) {
				tile.getTrap().entityOverTrigger(entity);
			} else if (tile.getSpecial() == Tile::TRAP_ACTION) {
				tile.getTrap().entityOverAction(entity);
			}
		}
	}
	for (size_t i = 0; i < removals.size(); i++) {
		Entity::Ptr entity = std::get<0>(removals[i]);
		Chunk* curChunk    = std::get<1>(removals[i]);
		Chunk* overChunk   = std::get<2>(removals[i]);
		curChunk->removeEntity(entity);
		overChunk->addEntity(entity);
	}

	for (auto iter = chunks.begin(); iter != chunks.end(); ++iter) {
		iter->second->update(timeStep);
	}

	rayCastCloseShapesSetUp = false;
}

void Map::activate() {
	active = true;
	walls->SetActive(true);
}
void Map::deactivate() {
	active = false;
	walls->SetActive(false);
}

String Map::getName() {
	return type->getName();
}

const MapType&  Map::getType() {
	return *type;
}

size_t Map::getNumChunks() {
	return chunks.size();
}

std::unordered_map<unsigned long, std::unique_ptr<Chunk>>::iterator Map::getChunkBegin() {
	return chunks.begin();
}

std::unordered_map<unsigned long, std::unique_ptr<Chunk>>::iterator Map::getChunkEnd() {
	return chunks.end();
}

unsigned long getIDX(Coord location)  {
	// A sort of hacked-together way to index chunks without knowing exact size of map
	return location.x * 1000000 + location.y;
}

Chunk* Map::getChunk(Coord location) {
	auto iter = chunks.find(getIDX(location));
	if (iter != chunks.end()) {
		return iter->second.get();
	}
	return nullptr;
}

Chunk& Map::createChunk(Coord location) {
	Chunk* maybe(getChunk(location));
	if (maybe) return *maybe;

	Chunk* newChunk = new Chunk(location, Context(*this, *effects));
	chunks.insert(std::make_pair(getIDX(location), std::unique_ptr<Chunk>(newChunk)));
	return *newChunk;
}

std::unordered_map<unsigned long, std::unique_ptr<Chunk>>::iterator Map::destroyChunk(Chunk& chunk) {
	// Find iterator to chunk
	auto sIter = chunks.find(getIDX(chunk.getPosition()));
	assert(sIter != chunks.end());
	removeWalls(chunk);

	// Delete chunk
	chunks.erase(sIter++);
	return sIter;
}
void Map::removeWalls(Chunk& chunk) {
	auto& chunkWalls = chunk.getWalls();
	for (auto iter = chunkWalls.begin(); iter != chunkWalls.end(); ++iter) {
		removeWall(*(*iter));
	}
}

void Map::addEntity(Entity::Ptr entity) {
	Chunk& chunk = createChunkContainingPos(vecFloor(entity->getPosition()));
	chunk.addEntity(entity);
	entity->getCollisionBody().SetActive(true);
	if (entity->getType() == Entity::MOB && ((Mob&)*entity).getMobType() == Mob::MONSTER) {
		((Monster&)*entity).setTarget(player);
	}
}
bool Map::removeEntity(Entity::Ptr entity) {
	auto iter = chunks.begin();
	for (; iter != chunks.end(); ++iter) {
		if (iter->second->removeEntity(entity)) {
			entity->getCollisionBody().SetActive(false);
			return true;
		}
	}
	return false;
}

Map::EntityIterator::EntityIterator() { }
Map::EntityIterator::EntityIterator(Map& map, bool begin): map(&map) {
	if (begin) {
		chunkIter = map.chunks.begin();
		if (chunkIter != map.chunks.end()) {
			entityIter = chunkIter->second->getEntities().begin();
			endOfChunkCheck();
		}
	} else {
		chunkIter = map.chunks.end();
	}
}
Map::EntityIterator& Map::EntityIterator::operator++() {
	++entityIter;
	endOfChunkCheck();
	return *this;
}
void Map::EntityIterator::endOfChunkCheck() {
	while (entityIter == chunkIter->second->getEntities().end()) {
		++chunkIter;
		if (chunkIter != map->chunks.end()) {
			entityIter = chunkIter->second->getEntities().begin();
		} else break;
	}
	if (chunkIter != map->chunks.end()) {
		tmpEpcp = std::unique_ptr<Epcp>(new Epcp(*entityIter, *chunkIter->second));
	}
}
Map::EntityIterator::Epcp  Map::EntityIterator::operator*()  {return *tmpEpcp;}
Map::EntityIterator::Epcp* Map::EntityIterator::operator->() {return tmpEpcp.get();}
const Map::EntityIterator::Epcp* Map::EntityIterator::operator->() const {return tmpEpcp.get();}

Map::EntityIterator Map::entitiesBegin() {
	return EntityIterator(*this, true);
}
Map::EntityIterator Map::entitiesEnd() {
	return EntityIterator(*this, false);
}

void Map::getEntitiesInRect(std::vector<Entity::Ptr>& entitiesOut, sf::FloatRect rect) {
	for (auto iter = entitiesBegin(); iter != entitiesEnd(); ++iter) {
		sf::Vector2f pos = iter->first->getPosition();
		if (rect.contains(pos)) {
			entitiesOut.push_back(iter->first);
		}
	}
}

MobSpawner& Map::getSpawner() {
	return mobSpawner;
}

void Map::setPlayer(Entity::Ptr p) {
	player = p;
}

Chunk* Map::getChunkContainingPos(Coord pos) {
	return getChunk(vecFloor(sf::Vector2f(pos) * (1.f / CHUNK_SIZE)));
}
Chunk& Map::createChunkContainingPos(Coord pos) {
	return createChunk(vecFloor(sf::Vector2f(pos) * (1.f / CHUNK_SIZE)));
}

Tile& Map::getTile(Coord pos) {
    Chunk* chunk = getChunkContainingPos(pos);
    assert(chunk);
    return chunk->getTile(pos.x - chunk->getPositionInTiles().x, pos.y - chunk->getPositionInTiles().y);
}
Tile& Map::getTile(int x, int y) {
	return getTile(Coord(x, y));
}
Tile* Map::getTileSafe(Coord pos) {
    Chunk* chunk = getChunkContainingPos(pos);
    if (!chunk) return NULL;
    return &chunk->getTile(pos.x - chunk->getPositionInTiles().x, pos.y - chunk->getPositionInTiles().y);
}
Tile* Map::getTileSafe(int x, int y) {
	return getTileSafe(Coord(x, y));
}
bool Map::hasTile(Coord pos) {
	return getChunkContainingPos(pos);
}
bool Map::hasTile(int x, int y) {
	return getChunkContainingPos(Coord(x, y));
}

bool Map::addItem(Coord pos, Item item, bool allowAlt) {
	if (allowAlt) {
		sf::Vector2i cur = pos;
		Direction dir = NORTH;
		int dur = 1;
		int durT = 1;
		int i = 0;
		while (i++ < 10) {
			Tile* tile = getTileSafe(cur);
			if (!tile) return false;
			if (tile->addItem(item)) return true;
			cur += dirVector(dir);
			durT--;
			if (!durT) {
				dir = turn(dir, 90);
				if (dir == NORTH || dir == SOUTH) {
					dur++;
				}
				durT = dur;
			}
		}
		return false;
	} else {
		Tile* tile = getTileSafe(pos);
		if (!tile) return false;
		return tile->addItem(item);
	}
}

b2Fixture* Map::addWall(b2EdgeShape& innerWall, b2EdgeShape& outerWall, Chunk& chunk) {
	// adds a wall with the given inner and outer components
	PhysUserData* userData = new PhysUserData(UD_WALL);
	b2EdgeShape* lShape = new b2EdgeShape();

	// calculate the half wall, used in lighting
	b2Vec2 half1 = (innerWall.m_vertex1 + outerWall.m_vertex1);
	b2Vec2 half2 = (innerWall.m_vertex2 + outerWall.m_vertex2);
	half1 *= 0.5f;
	half2 *= 0.5f;

	lShape->Set(half1, half2);
	userData->shape.lighting = lShape;
	userData->shape.graphic  = new b2EdgeShape(outerWall);
	return addWall(innerWall, userData, chunk);
}
b2Fixture* Map::addWall(b2Shape& innerWall, Chunk& chunk) {
	return addWall(innerWall, new PhysUserData(UD_WALL), chunk);
}
b2Fixture* Map::addWall(b2Shape& innerWall, PhysUserData* userData, Chunk& chunk) {
	b2FixtureDef innerFixDef;
	innerFixDef.shape = &innerWall;
	innerFixDef.friction = 0.1f;
	innerFixDef.userData = userData;
	b2Fixture* innerWallFix = walls->CreateFixture(&innerFixDef);
	chunk.getWalls().insert(innerWallFix);
	return innerWallFix;
}

float Map::rayCast(b2RayCastInput& input, b2AABB& bb) {
	b2Transform transform(b2Vec2(0, 0), b2Rot(0)); //empty transform

	// which shapes to raycast only need be calculated occasionally
	if (!rayCastCloseShapesSetUp) {
		rayCastCloseShapesSetUp = true;
		rayCastShapes.clear();
		b2Vec2 lb = bb.lowerBound;
		b2Vec2 ub = bb.upperBound;
		for (const b2Fixture* fixture = walls->GetFixtureList(); fixture; fixture = fixture->GetNext()) {
			const b2Shape* shape;
			PhysUserData* userData = (PhysUserData*)fixture->GetUserData();
			if (userData && userData->shape.lighting) {
				shape = userData->shape.lighting;
			} else {
				shape = fixture->GetShape();
			}
			b2AABB shapeBB;
			shape->ComputeAABB(&shapeBB, transform, 0);
			b2Vec2 slb = shapeBB.lowerBound;
			b2Vec2 sub = shapeBB.upperBound;
			if ((lb.x < sub.x) && (ub.x > slb.x) && (lb.y < sub.y) && (ub.y > slb.y)) {
				rayCastShapes.push_back(shape);
			}
		}
	}

	// raycast each of those shapes
	float minDist = input.maxFraction;
	for (size_t i = 0; i < rayCastShapes.size(); i++) {
		const b2Shape* shape = rayCastShapes[i];
		b2RayCastOutput output;
		if (shape->RayCast(&output, input, transform, 0)) {
			if (output.fraction < minDist) {
				minDist = output.fraction;
			}
		}
	}
	return minDist;
}

std::vector<const b2Shape*>& Map::getRayCastShapes() {
	return rayCastShapes;
}

bool Map::isPlayerSafe() {
	return safe > 0.95;
}

bool Map::monsterSight(Monster& monster) {
	Entity::Ptr target = monster.getTarget();
	bool seen = false;
	if (target.get()) {
		b2RayCastInput input;
		input.p1 = toPhysVec(monster.getPosition());
		b2Vec2 tp = toPhysVec(target->getPosition());
		b2Vec2 dif = tp - input.p1;
		dif.Normalize();
		input.p2 = dif + input.p1;
		float distsqr = (input.p1 - tp).LengthSquared();
		if (distsqr < (LIGHT_DIST + 1) * (LIGHT_DIST + 1)) {
			input.maxFraction = LIGHT_DIST + 1;

			b2AABB bb;
			bb.lowerBound = b2Vec2(input.p1.x - LIGHT_DIST - 1, input.p1.y - LIGHT_DIST - 1);
			bb.upperBound = b2Vec2(input.p1.x + LIGHT_DIST + 1, input.p1.y + LIGHT_DIST + 1);
			float f = rayCast(input, bb);

			if (f * f > distsqr) {
				monster.setSeesTarget(true);
				seen = true;
			} else {
				monster.setSeesTarget(false);
			}
		}
	}
	return seen;
}

void Map::removeWall(b2Fixture& wall) {
	delete (PhysUserData*)wall.GetUserData();
	walls->DestroyFixture(&wall);
}

const b2Body& Map::getWallsBody() {
	return *walls;
}

Interactible* Map::getInteractible(Coord pos) {
	Tile* tile = getTileSafe(pos);
	if (tile && tile->getSpecial() == Tile::INTERACT) {
		return &tile->getInteract();
	}
    return nullptr;
}
Interactible& Map::addInteractible(String name, Coord pos) {
	Chunk* chunk = getChunkContainingPos(pos);
	assert(chunk);
	return chunk->addInteractible(name, pos - chunk->getPositionInTiles());
}
Interactible& Map::addInteractible(String name, const std::vector<Coord>& positions) {
	Chunk* chunk = getChunkContainingPos(positions[0]);
	assert(chunk);
	std::vector<Coord> confirmedPositions;
	for (size_t i = 0; i < positions.size(); i++) {
		Coord pos = positions[i] - chunk->getPositionInTiles();
		if (chunk->hasTile(pos)) confirmedPositions.push_back(pos);
	}
	return chunk->addInteractible(name, confirmedPositions);
}

MapObject& Map::addObject(sf::Vector2i pos, sf::IntRect texRect) {
	return addObject(MapObject(pos, sf::Vector2i(texRect.width, texRect.height), sf::Vector2i(texRect.left, texRect.top)));
}
MapObject& Map::addObject(MapObject object) {
	sf::Vector2f center = (sf::Vector2f(object.getGPosition()) + sf::Vector2f(object.getGSize()) * 0.5f) * (1.f / TILE_SIZE);
	Chunk* chunk = getChunkContainingPos(vecFloor(center));
	if (chunk) {
		object.setGPosition(object.getGPosition() - chunk->getPositionInTiles() * TILE_SIZE);
		return chunk->addObject(object);
	}
	chunk = getChunk(ORIGIN);
	assert(chunk);
	return chunk->addObject(object);
}
size_t Map::getNumObjects() {
	size_t num = 0;
	for (auto iter = chunks.begin(); iter != chunks.end(); ++iter) {
		num += iter->second->getNumObjects();
	}
	return num;
}
MapObject& Map::getObject(size_t index) {
	size_t num = index;
	for (auto iter = chunks.begin(); iter != chunks.end(); ++iter) {
		size_t chunkNum = iter->second->getNumObjects();
		if (num < chunkNum) {
			return iter->second->getObject(num);
		}
		num -= chunkNum;
	}
	assert(false);
}

Map* Map::getAdjacentMap(Direction direction) {
    auto iter = adjacentMaps.find(direction);
    if (iter == adjacentMaps.end()) return NULL;
    else return iter->second;
}
void Map::addAdjacentMap(Direction direction, Map* map) {
    adjacentMaps[direction] = map;
}

int Map::getSeed() {
    return seed;
}

EffectsManager& Map::getEffects() {
	return *effects;
}

void Map::getEntrances(std::vector<int>& entrances, Coord chunkPos, Direction dir) {
    switch(dir) {
        case NORTH: getEntrances(entrances, chunkPos + dirVector(NORTH), SOUTH); break;
        case WEST: getEntrances(entrances, chunkPos + dirVector(WEST), EAST); break;
        case EAST: {
            Rand::setSeed(getSeed() + getIDX(chunkPos));
            for (int i = 0; i < 3; i++) {
                entrances.push_back(Rand::i(0, CHUNK_SIZE));
            }
        } break;
        case SOUTH: {
            Rand::setSeed(getSeed() + getIDX(chunkPos));
            Rand::discard(40);
            for (int i = 0; i < 3; i++) {
                entrances.push_back(Rand::i(0, CHUNK_SIZE));
            }
        } break;
        default: entrances.push_back(CHUNK_SIZE / 2); break;
    }
}

void Map::allocateChunks(Coord size, Chunk::State chunkState) {
	Coord numChunks((int)ceil(size.x / (float)CHUNK_SIZE), (int)ceil(size.y / (float)CHUNK_SIZE));
	for (int j = 0; j < numChunks.y; j++) {
		for (int i = 0; i < numChunks.x; i++) {
			createChunk(Coord(i, j)).setState(chunkState);
		}
	}
	printf("%i x %i chunks made, each %i x %i tiles in size.\n", numChunks.x, numChunks.y, CHUNK_SIZE, CHUNK_SIZE);
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("place/map", "Test Map.") {
	ValuablesManager valuables;
	YAML::Node mapNode = YAML::LoadFile("unittest/map.yaml");
	const MapType* mapType = valuables.loadMapType(mapNode);
	EffectsManager effects;
	Map map(*mapType, effects);

	SECTION("entity iteration") {
		b2BodyDef defBodyDef;
		Entity::Ptr entity1(new Entity(Entity::NONE, defBodyDef, effects));
		Entity::Ptr entity2(new Entity(Entity::NONE, defBodyDef, effects));
		Entity::Ptr entity3(new Entity(Entity::NONE, defBodyDef, effects));
		std::set<Entity::Ptr> entities;
		entities.insert(entity1);
		entities.insert(entity2);
		entities.insert(entity3);
		map.addEntity(entity1);
		map.addEntity(entity2);
		map.addEntity(entity3);
		for (auto iter = map.entitiesBegin(); iter != map.entitiesEnd(); ++iter) {
			REQUIRE(entities.count(iter->first));
			entities.erase(entities.find(iter->first));
		}
		REQUIRE(entities.empty());
	}
}
