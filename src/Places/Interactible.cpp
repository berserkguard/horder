#include "Interactible.h"

Interactible::Interactible(String name): name(name), type(NONE) { }

String Interactible::getName() {
	return name;
}
Interactible::Type Interactible::getType() {
	return type;
}

void Interactible::setSpecial() {
	type = SPECIAL;
}

void Interactible::setConnection(Direction dir, sf::Vector2f dest) {
	type = CONNECTION;
	direction = dir;
	destination = dest;
}
Direction Interactible::getDirection() {
	assert(type == CONNECTION);
	return direction;
}
sf::Vector2f Interactible::getDestination() {
	assert(type == CONNECTION);
	return destination;
}
