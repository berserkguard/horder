#include "TileType.h"

#include "Util.h"

TileType::TileType(String name, Coord gloc): name(name), gloc(gloc), material(NONE) { }

TileType TileType::EMPTY = TileType("", ORIGIN);

String TileType::getName() const {
	return name;
}
Coord TileType::getGLoc() const {
	return gloc;
}
int TileType::getID() const {
	return gloc.x + gloc.y * TILE_TEX_SIZE;
}

TileType::Material TileType::getMaterial() const {
	return material;
}
void TileType::setMaterial(Material mat) {
	material = mat;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("tile/type", "Test TileType.") {
	TileType tileType("Test Tile", Coord(1, 7));
	tileType.setMaterial(TileType::STONE);

	REQUIRE(tileType.getName() == "Test Tile");
	REQUIRE(tileType.getMaterial() == TileType::STONE);
	REQUIRE(tileType.getGLoc() == Coord(1, 7));
}
