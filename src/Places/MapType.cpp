#include "MapType.h"

#include "TileType.h"
#include "Rand.h"

MapType::MapType(String name): name(name) {
	persistent = infinite = dark = gross = top = home = false;
	init();
}
MapType::MapType(String name, int flags): name(name) {
	persistent = flags & PERSISTENT;
	infinite   = flags & BOUNDLESS;
	dark       = flags & DARK;
	gross      = flags & GROSS;
	top        = flags & TOP;
	home       = flags & HOME;
	init();
}

void MapType::init() {
	for (int i = 0; i < GENT_COUNT; i++) {
		genTiles[i] = nullptr;
	}
	for (unsigned int i = 0; i < DIR_COUNT; i++) {
		adjacentMaps[i] = nullptr;
	}
}

String MapType::getName() const {
	return name;
}

String MapType::getFile() const {
	return file;
}
void MapType::setFile(String newFile) {
	file = newFile;
}

void MapType::addEncounter(int level, Encounter& encounter) {
	while ((int)encounters.size() < level + 1) encounters.emplace_back(new EncounterBag());
	encounters[level]->add(encounter);
}
EncounterBag* MapType::getEncounters(int level) const {
	if (encounters.empty()) return nullptr;
	if ((int)level > (int)encounters.size() - 1) level = encounters.size() - 1;
	for (size_t i = level; i < encounters.size(); i++) {
		if (encounters[i]->numEncounters()) {
			return encounters[i].get();
			break;
		}
	}
	return nullptr;
}

void MapType::addTrap(const TrapType* trap) {
	traps.push_back(trap);
}
const TrapType* MapType::getRandomTrap() const {
	if (traps.empty()) return nullptr;
	return traps[Rand::i(0, traps.size())];
}

void MapType::addChestItem(TreasureChest::Type chest, const ItemType& item) {
	chestItems[chest].push_back(&item);
}
void MapType::fillChest(TreasureChest& chest) const {
	int numItems = 0;
	int typeI = TreasureChest::BRONZE;
	float f = Rand::f();
	switch(chest.getChestType()) {
		case TreasureChest::BRONZE: numItems = Rand::i(2, 3); break;
		case TreasureChest::SILVER: {
			if (f < 0.4f) {
				numItems = Rand::i(4, 6);
			} else {
				numItems = Rand::i(2, 3);
				typeI = TreasureChest::SILVER;
			}
		} break;
		case TreasureChest::GOLD: {
			if (f < 0.1f) {
				numItems = Rand::i(8, 12);
			} else if (f < 0.5f) {
				numItems = Rand::i(4, 6);
				typeI = TreasureChest::SILVER;
			} else {
				numItems = Rand::i(2, 3);
				typeI = TreasureChest::GOLD;
			}
		} break;
		default: break;
	}

	const std::vector<const ItemType*>& items = chestItems[typeI];
	if (!items.empty()) {
		for (int i = 0; i < numItems; i++) {
			chest.addItem(Item(*items[Rand::i(0, items.size())]));
		}
	}
}

bool MapType::isPersistent() const {return persistent;}
bool MapType::isInfinite()   const {return infinite;  }
bool MapType::isDark()       const {return dark;      }
bool MapType::isGross()      const {return gross;     }
bool MapType::isTop()        const {return top;       }
bool MapType::isHome()       const {return home;      }

const TileType& MapType::getGenTile(GenTile genTile) const {
	if (genTiles[genTile]) return *genTiles[genTile];
	return TileType::EMPTY;
}
void MapType::setGenTile(GenTile genTile, const TileType& tile) {
    genTiles[genTile] = &tile;
}

sf::IntRect MapType::getGenObj(GenObject genObj) const {
	return genObjs[genObj];
}
void MapType::setGenObj(GenObject genObj, sf::IntRect object) {
	genObjs[genObj] = object;
}

void MapType::addAdjacentMap(Direction direction, const MapType& map) {
	adjacentMaps[direction] = &map;
}
const MapType* MapType::getAdjacentMap(Direction direction) const {
	return adjacentMaps[direction];
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("map/type", "Test MapType.") {

}
