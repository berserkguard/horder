#include "World.h"

#include "Map.h"
#include "MapType.h"

World::World(ValuablesManager& valuables, EffectsManager& effects)
	: player(new Player(effects)), valuables(&valuables), effects(&effects), tiledLoader(valuables) { }

Player& World::getPlayer() {
	return *player;
}

void World::addMap(const MapType& type) {
	Map* map = new Map(type, *effects);
	maps.emplace_back(map);
	map->setPlayer(player->getEntity());

	if (type.isHome()) player->setHome(*map);
	if (type.getFile() != "") {
		tiledLoader.loadFile(*map, type.getFile());
	}
	mapNameMap[map->getName()] = map;
}

Map* World::getMap(String mapName) {
	auto iter = mapNameMap.find(mapName);
	if (iter == mapNameMap.end()) return NULL;
	return iter->second;
}

void World::connectMaps() {
	for (size_t i = 0; i < maps.size(); i++) {
		Map& map = *maps[i];
		const MapType& type = map.getType();
		for (unsigned int i = NONE; i < DIR_COUNT; i++) {
			const MapType* t = type.getAdjacentMap((Direction)i);
			if (t) {
				map.addAdjacentMap((Direction)i, getMap(t->getName()));
			}
		}
	}
}

void World::BeginContact(b2Contact* contact) {
	b2Fixture* fixtures[2];
	fixtures[0] = contact->GetFixtureA();
	fixtures[1] = contact->GetFixtureB();
	PhysUserData* userDatas[2];
	int entitiesInvolved = 0;
	int wallsInvolved = 0;
	for (int i = 0; i < 2; i++) {
		PhysUserData* userData = (PhysUserData*)fixtures[i]->GetUserData();
		if (!userData) {
			userData = (PhysUserData*)fixtures[i]->GetBody()->GetUserData();
		}
		if (userData) {
			switch(userData->ownerType) {
				case UD_ENTITY: entitiesInvolved++; break;
				case UD_WALL:   wallsInvolved++;    break;
				default: break;
			}
		}
		userDatas[i] = userData;
	}
	if (entitiesInvolved == 2) {
		// entity on entity collision
		userDatas[0]->entity->onEntityCollision(*userDatas[1]->entity);
		userDatas[1]->entity->onEntityCollision(*userDatas[0]->entity);
	} else if (entitiesInvolved == 1 && wallsInvolved == 1) {
		// entity on wall collision
		if (userDatas[0]->ownerType == UD_ENTITY) {
			userDatas[0]->entity->onWallCollision();
		} else {
			userDatas[1]->entity->onWallCollision();
		}
	}
}
