#include "Tile.h"

#include <cassert>
#include <climits>
#include "Rand.h"

Tile::Tile() {
    floor = &TileType::EMPTY;
    special = NOTHING;
    path = YES;
    rand = Rand::i(0, USHRT_MAX);
}

Tile::PathType Tile::isPath() {
	return path;
}
void Tile::setPath(PathType p) {
	path = p;
}

const TileType& Tile::getFloor() {
	return *floor;
}
void Tile::setFloor(const TileType& type) {
	floor = &type;
}

ItemContainer* Tile::getItemContainer() {
	return items.get();
}

size_t Tile::getNumItems() {
	return items.get() ? items->getNumItems() : 0;
}

Item& Tile::getItem(size_t index) {
	assert(items.get());
	return items->getItemAt(index);
}

bool Tile::addItem(Item item) {
    if (!items.get()) {
        items.reset(new ItemContainer());
        items->setLimit(MAX_ITEMS);
    }
    return items->addItem(item);
}

Item Tile::removeItem(size_t index) {
	Item item = items->removeItemAt(index);
	if (!items->getNumItems()) {
		items.release();
	}
	return item;
}

Tile::Special Tile::getSpecial() {
	return special;
}
Trap& Tile::getTrap() {
	assert(special == TRAP_TRIGGER || special == TRAP_ACTION);
	return *s.trap;
}
Interactible& Tile::getInteract() {
	assert(special == INTERACT);
	return *s.interactible;
}
void Tile::setTrapTrigger(Trap& trap) {
	special = TRAP_TRIGGER;
	s.trap = &trap;
}
void Tile::setTrapAction(Trap& trap) {
	special = TRAP_ACTION;
	s.trap = &trap;
}
void Tile::setInteract(Interactible& interact) {
	special = INTERACT;
	s.interactible = &interact;
}

int Tile::getRand() {
	return rand;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("tile/items", "Test Tile item having.") {

	SECTION("Make sure of size.") {
		REQUIRE(sizeof(std::unique_ptr<ItemContainer>) == sizeof(ItemContainer*));
	}

	SECTION("Do actual tests.") {
		Tile tile;
		tile.addItem(Item());
		REQUIRE(tile.getNumItems() == 1);
		REQUIRE(tile.getItemContainer() != NULL);
		REQUIRE(tile.removeItem(0).isNull());
		REQUIRE(tile.getNumItems() == 0);
	}
}
