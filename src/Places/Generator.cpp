#include "Generator.h"

#include "Chunk.h"
#include "Rand.h"
#include "MobSpawner.h"
#include "Map.h"
#include "ShuffleBag.h"
#include "TrapArrowAction.h"
#include "Interactible.h"

Map* map;
Chunk* chunk;
std::vector<Coord> nodeQueue;
std::set<Coord, Coordcmp> empties;

template <class T>
T randomPop(std::vector<T>& vect) {
	assert(!vect.empty());
    unsigned int r = Rand::simple(vect.size());
    std::swap(vect[r], vect[vect.size() - 1]);
    T val = vect[vect.size() - 1];
    vect.pop_back();
    return val;
}

void refreshLighting(Chunk& chunk) {
	auto& walls = chunk.getWalls(NONE);
	for (auto iter = walls.begin(); iter != walls.end(); ++iter) {
		map->addLightingShape(*(*iter));
	}
}

void Generator::generateChunk(Map& m, Chunk& c) {
	map = &m;
	chunk = &c;
	nodeQueue.clear();
	empties.clear();

	// Initially, all is wall
	for (int y = 0; y < CHUNK_SIZE; y++) {
		for (int x = 0; x < CHUNK_SIZE; x++) {
			doWall(Coord(x, y));
		}
	}

	generatePaths();
	generateRooms();

	doWall(Coord(0, 0));
	doWall(Coord(CHUNK_SIZE - 1, 0));
	doWall(Coord(0, CHUNK_SIZE - 1));
	doWall(Coord(CHUNK_SIZE - 1, CHUNK_SIZE - 1));

	if (c.getPosition() == sf::Vector2i(0, 0) && m.getType().isInfinite()) {
		addLadder();
	}

	generateTraps();
	generatePhysicsWalls();

	Chunk* chunk1 = map->getChunk(c.getPosition() + Coord(0, 1));
	Chunk* chunk2 = map->getChunk(c.getPosition() + Coord(0, -1));
	Chunk* chunk3 = map->getChunk(c.getPosition() + Coord(1, 0));
	Chunk* chunk4 = map->getChunk(c.getPosition() + Coord(-1, 0));
	if (chunk1) refreshLighting(*chunk1);
	if (chunk2) refreshLighting(*chunk2);
	if (chunk3) refreshLighting(*chunk3);
	if (chunk4) refreshLighting(*chunk4);

	sf::IntRect rect(chunk->getPositionInTiles(), sf::Vector2i(CHUNK_SIZE, CHUNK_SIZE));
	for (int i = 0; i < 5; i++) {
		map->getSpawner().spawnEncounter(rect);
	}

	chunk->setState(Chunk::GENERATED);
}

void Generator::generatePaths() {
	std::vector<int> northEntrances, southEntrances, eastEntrances, westEntrances;
    map->getEntrances(northEntrances, chunk->getPosition(), NORTH);
    map->getEntrances(southEntrances, chunk->getPosition(), SOUTH);
    map->getEntrances(westEntrances , chunk->getPosition(), WEST);
    map->getEntrances(eastEntrances , chunk->getPosition(), EAST);

    Rand::setSeed(map->getSeed() + chunk->getPosition().x + chunk->getPosition().y * 1000);
    Rand::discard(256); // Discard the random used for entrances

	if (Rand::simple(2)) {
        // Initial path is horizontal
        int y1 = randomPop(westEntrances);
        int y2 = randomPop(eastEntrances);
        createPath(Coord(0, y1), Coord(CHUNK_SIZE - 1, y2));
    } else {
        // Initial path is vertical
        int x1 = randomPop(northEntrances);
        int x2 = randomPop(southEntrances);
        createPath(Coord(x1, 0), Coord(x2, CHUNK_SIZE - 1));
    }

    // Use remaining entrances
    for (auto iter = northEntrances.begin(); iter != northEntrances.end(); ++iter) {
        createPath(Coord(*iter, 0), randomPop(nodeQueue));
    }
    for (auto iter = southEntrances.begin(); iter != southEntrances.end(); ++iter) {
        createPath(Coord(*iter, CHUNK_SIZE - 1), randomPop(nodeQueue));
    }
    for (auto iter = westEntrances.begin(); iter != westEntrances.end(); ++iter) {
        createPath(Coord(0, *iter), randomPop(nodeQueue));
    }
    for (auto iter = eastEntrances.begin(); iter != eastEntrances.end(); ++iter) {
        createPath(Coord(CHUNK_SIZE - 1, *iter), randomPop(nodeQueue));
    }
}

void Generator::generateRooms() {
    while(!nodeQueue.empty()) {
		Coord node = randomPop(nodeQueue);
		int dir = Rand::i(0, 4);
		for (int i = 0; i < 2; i++) {
			switch(dir + i) {
				case 0:
				case 4: if (growRoom(Coord(node.x, node.y - 1))) doFloor(Coord(node.x, node.y - 1), MapType::ROOM); break; // NORTH
				case 1: if (growRoom(Coord(node.x, node.y + 1))) doFloor(Coord(node.x, node.y + 1), MapType::ROOM); break; // SOUTH
				case 2: if (growRoom(Coord(node.x + 1, node.y))) doFloor(Coord(node.x + 1, node.y), MapType::ROOM); break; // EAST
				case 3: if (growRoom(Coord(node.x - 1, node.y))) doFloor(Coord(node.x - 1, node.y), MapType::ROOM); break; // WEST
			}
		}
    }
}

void Generator::generateTraps() {
	int numTraps = Rand::i(5, 10);
	for (int i = 0; i < numTraps; i++) {
		auto iter = empties.begin();
		std::advance(iter, Rand::i(0, empties.size()));
		Coord empty = *iter;
		empties.erase(iter);

		const TrapType* trapType = map->getType().getRandomTrap();
		if (!trapType) return;

		Trap* trap = NULL;

		switch (trapType->getAction()) {
			case TrapType::ARROW: {

				// find where to shoot from
				std::vector<Direction> bah;
				bah.push_back(NORTH);
				bah.push_back(SOUTH);
				bah.push_back(EAST);
				bah.push_back(WEST);
				Direction dir = NONE;
				Coord cur;
				while(!dir && !bah.empty()) {
					int i = Rand::i(0, bah.size());
					Direction d = bah[i];
					bah.erase(bah.begin() + i);
					Coord c = empty + dirVector(d);
					if (Chunk::inBounds(c) && chunk->getTile(c).isPath()) {
						cur = empty + dirVector(d);
						bool success = true;
						int dist = 0;
						while (chunk->getTile(cur).isPath()) {
							cur += dirVector(d);
							dist++;
							if (dist > 10 || !Chunk::inBounds(cur)) {
								success = false;
								break;
							}
						}
						if (success) {
							cur -= dirVector(d);
							dir = d;
						}
					}
				}

				// create rest of trap
				if (!dir) continue;
				trap = &chunk->addTrap(*trapType, cur, empty);
				TrapArrowAction& trigger = (TrapArrowAction&)trap->getAction();
				trigger.setAmmunition(Rand::i(2, 5));
				trigger.setDirection(turn(dir, 180));
			} break;
			case TrapType::FLAME: {
				trap = &chunk->addTrap(*trapType, empty);
			} break;
			default: break;
		}
	}
}

void Generator::generatePhysicsWalls() {
	b2Vec2 pos = toPhysVec(sf::Vector2f(chunk->getPositionInTiles()));
	int begin = -1;
	b2EdgeShape line;
	//first do horizontal lines
	for (int y = 0; y < CHUNK_SIZE - 1; y++) {
		for (int x = 0; x < CHUNK_SIZE; x++) {
			bool solid1 = !chunk->getTile(x, y).isPath();
			bool solid2 = !chunk->getTile(x, y + 1).isPath();
			if (begin > -1 && solid1 == solid2) {
				line.Set(b2Vec2(begin, y + 1) + pos, b2Vec2(x, y + 1) + pos);
				map->addWall(line, *chunk);
				begin = -1;
			} else if (solid1 != solid2 && begin == -1) {
				begin = x;
			}
		}
		if (begin > -1) {
			line.Set(b2Vec2(begin, y + 1) + pos, b2Vec2(CHUNK_SIZE, y + 1) + pos);
			map->addWall(line, *chunk);
			begin = -1;
		}
	}
	//then do vertical lines
	for (int x = 0; x < CHUNK_SIZE - 1; x++) {
		for (int y = 0; y < CHUNK_SIZE; y++) {
			bool solid1 = !chunk->getTile(x, y).isPath();
			bool solid2 = !chunk->getTile(x + 1, y).isPath();
			if (begin > -1 && solid1 == solid2) {
				line.Set(b2Vec2(x + 1, begin) + pos, b2Vec2(x + 1, y) + pos);
				map->addWall(line, *chunk);
				begin = -1;
			} else if (solid1 != solid2 && begin == -1) {
				begin = y;
			}
		}
		if (begin > -1) {
			line.Set(b2Vec2(x + 1, begin) + pos, b2Vec2(x + 1, CHUNK_SIZE) + pos);
			map->addWall(line, *chunk);
			begin = -1;
		}
	}

	doConnectingWalls(NORTH);
	doConnectingWalls(SOUTH);
	doConnectingWalls(EAST);
	doConnectingWalls(WEST);
}

bool Generator::growRoom(Coord begin) {
	sf::IntRect rect(begin.x, begin.y, 1, 1);

	if (begin.x < 0 || begin.x >= CHUNK_SIZE || begin.y < 0 || begin.y >= CHUNK_SIZE) return false;
	if (chunk->getTile(begin.x, begin.y).isPath()) return false;

	bool doneNorthing = false;
	bool doneSouthing = false;
	bool doneEasting = false;
	bool doneWesting = false;
	bool northed = false;
	bool southed = false;
	bool easted = false;
	bool wested = false;
	while(!(doneNorthing && doneSouthing && doneEasting && doneWesting)) {
		if (!doneNorthing) {
			if (rect.top - 1 < 0) doneNorthing = true;
			else {
				for (int i = 0; i < rect.width; i++) {
					Tile& t = chunk->getTile(rect.left + i, rect.top - 1);
					if (t.isPath()) {
						doneNorthing = true;
						break;
					}
				}
				if (!doneNorthing) {
					rect.top--;
					rect.height++;
					northed = true;
				}
			}
		}
		if (!doneWesting) {
			if (rect.left - 1 < 0) doneWesting = true;
			else {
				for (int i = 0; i < rect.height; i++) {
					Tile& t = chunk->getTile(rect.left - 1, rect.top + i);
					if (t.isPath()) {
						doneWesting = true;
						break;
					}
				}
				if (!doneWesting) {
					rect.left--;
					rect.width++;
					wested = true;
				}
			}
		}
		if (!doneSouthing) {
			if (rect.top + rect.height + 1 >= CHUNK_SIZE) doneSouthing = true;
			else {
				for (int i = 0; i < rect.width; i++) {
					Tile& t = chunk->getTile(rect.left + i, rect.top + rect.height);
					if (t.isPath()) {
						doneSouthing = true;
						break;
					}
				}
				if (!doneSouthing) {
					rect.height++;
					southed = true;
				}
			}
		}
		if (!doneEasting) {
			if (rect.left + rect.width + 1 >= CHUNK_SIZE) doneEasting = true;
			else {
				for (int i = 0; i < rect.height; i++) {
					Tile& t = chunk->getTile(rect.left + rect.width, rect.top + i);
					if (t.isPath()) {
						doneEasting = true;
						break;
					}
				}
				if (!doneEasting) {
					rect.width++;
					easted = true;
				}
			}
		}

		if (rect.width >= 10) {
			doneEasting = true;
			doneWesting = true;
		}
		if (rect.height >= 10) {
			doneNorthing = true;
			doneSouthing = true;
		}
	}

	if (rect.width >= 5 && rect.height >= 5 && (northed + easted + southed + wested) >= 3) {
		// room is large enough to actually create
		for (int y = rect.top + 1; y < rect.top + rect.height - 1; y++) {
			for (int x = rect.left + 1; x < rect.left + rect.width - 1; x++) {
				doFloor(Coord(x, y), MapType::ROOM);
			}
		}

		fillRoom(sf::IntRect(rect.left + 1, rect.top + 1, rect.width - 2, rect.height - 2));

		return true;
	}
	return false;
}

void Generator::fillRoom(sf::IntRect rect) {

	// get valid edge positions in room
	ShuffleBag<Coord> edgePositions;
	for (int i = 0; i < rect.width; i++) {
		edgePositions.add(Coord(i + rect.left, rect.top));
		edgePositions.add(Coord(i + rect.left, rect.top + rect.height - 1));
	}
	for (int i = 0; i < rect.height; i++) {
		edgePositions.add(Coord(rect.left, i + rect.top));
		edgePositions.add(Coord(rect.left + rect.width - 1, i + rect.top));
	}

	int numChests = rect.width * rect.height * Rand::i(1, 3) / 30;
	for (int i = 0; i < numChests; i++) {
		Coord pos = edgePositions.grab() + chunk->getPositionInTiles();
		TreasureChest::Type type;
		float randf = Rand::f();
		if (randf < 0.05) {
			type = TreasureChest::GOLD;
		} else if (randf < 0.25) {
			type = TreasureChest::SILVER;
		} else {
			type = TreasureChest::BRONZE;
		}
		TreasureChest* newChest = new TreasureChest(type, map->getEffects(), sf::Vector2f(pos.x + 0.5f, pos.y + 0.5f));
		map->getType().fillChest(*newChest);
		map->addEntity(Entity::Ptr(newChest));
	}
}

void Generator::createPath(Coord begin, Coord end) {
    if (std::abs(begin.x - end.x) > std::abs(begin.y - end.y)) {
        // Horizontals
        if (begin.x > end.x) std::swap(begin, end);
        int d = 0;
        if (end.x - begin.x > 1) d = Rand::i(1, end.x - begin.x);
        for (int i = 0; i < d; i++) {
            doFloor(Coord(begin.x + i, begin.y), MapType::FLOOR);
        }
        for (int i = std::min(begin.y, end.y); i <= std::max(begin.y, end.y); i++) {
            doFloor(Coord(begin.x + d, i), MapType::FLOOR);
        }
        for (int i = d + 1; i <= end.x - begin.x; i++) {
            doFloor(Coord(begin.x + i, end.y), MapType::FLOOR);
        }
        // Add new nodes
        nodeQueue.push_back(Coord(begin.x + d, begin.y));
        nodeQueue.push_back(Coord(begin.x + d, end.y));
		nodeQueue.push_back(Coord(begin.x + Rand::i(0, d + 1), begin.y));
        nodeQueue.push_back(Coord(begin.x + d + Rand::i(0, end.x - begin.x - d + 1), end.y));
    } else {
        // Verticals
        if (begin.y > end.y) std::swap(begin, end);
        int d = 0;
        if (end.y - begin.y > 1) d = Rand::i(1, end.y - begin.y);
        for (int i = 0; i < d; i++) {
            doFloor(Coord(begin.x, begin.y + i), MapType::FLOOR);
        }
        for (int i = std::min(begin.x, end.x); i <= std::max(begin.x, end.x); i++) {
            doFloor(Coord(i, begin.y + d), MapType::FLOOR);
        }
        for (int i = d + 1; i <= end.y - begin.y; i++) {
            doFloor(Coord(end.x, begin.y + i), MapType::FLOOR);
        }
        // Add new nodes
        nodeQueue.push_back(Coord(begin.x, begin.y + d));
        nodeQueue.push_back(Coord(end.x, begin.y + d));
		nodeQueue.push_back(Coord(begin.x, begin.y + Rand::i(0, d + 1)));
        nodeQueue.push_back(Coord(end.x, begin.y + d + Rand::i(0, end.y - begin.y - d + 1)));
    }
}

void Generator::doWall(Coord pos) {
	chunk->getTile(pos).setFloor(TileType::EMPTY);
	chunk->getTile(pos).setPath(Tile::NO);
}
void Generator::doFloor(Coord pos, MapType::GenTile tile) {
    chunk->getTile(pos).setFloor(map->getType().getGenTile(tile));
    chunk->getTile(pos).setPath(Tile::YES);
    empties.insert(pos);
}

void Generator::doConnectingWalls(Direction dir) {
	Chunk* adjChunk = map->getChunk(chunk->getPosition() + dirVector(dir));
	if (adjChunk) {
		b2EdgeShape line;
		b2Vec2 pos = toPhysVec(sf::Vector2f(chunk->getPositionInTiles()));
		int begin = -1;
		for (int i = 0; i < CHUNK_SIZE + 1; i++) {
			bool wel = false;
			if (i == CHUNK_SIZE) {
				if (begin > -1) wel = true;
			} else {
				Coord a, b;
				switch(dir) {
					case NORTH:
						a = Coord(i, 0);
						b = Coord(i, CHUNK_SIZE - 1);
						break;
					case SOUTH:
						a = Coord(i, CHUNK_SIZE - 1);
						b = Coord(i, 0);
						break;
					case EAST:
						a = Coord(CHUNK_SIZE - 1, i);
						b = Coord(0, i);
						break;
					case WEST:
						a = Coord(0, i);
						b = Coord(CHUNK_SIZE - 1, i);
						break;
					default: return;
				}
				bool solid1 = !chunk->getTile(a).isPath();
				bool solid2 = !adjChunk->getTile(b).isPath();
				if (begin > -1 && solid1 == solid2) {
					wel = true;
				} else if (solid1 != solid2 && begin == -1) {
					begin = i;
				}
			}
			if (wel) {
				switch(dir) {
					case NORTH: line.Set(b2Vec2(begin, 0) + pos, b2Vec2(i, 0) + pos); break;
					case SOUTH: line.Set(b2Vec2(begin, CHUNK_SIZE) + pos, b2Vec2(i, CHUNK_SIZE) + pos); break;
					case WEST: line.Set(b2Vec2(0, begin) + pos, b2Vec2(0, i) + pos); break;
					case EAST: line.Set(b2Vec2(CHUNK_SIZE, begin) + pos, b2Vec2(CHUNK_SIZE, i) + pos); break;
					default: return;
				}
				map->addWall(line, *chunk, dir);
				begin = -1;
			}
		}
	}
}

void Generator::addLadder() {
	sf::Vector2i pos = map->getSpawner().getFloorNear(sf::Vector2i(CHUNK_SIZE / 2, CHUNK_SIZE / 2));
	chunk->addObject(pos * TILE_SIZE, map->getType().getGenObj(MapType::LADDER));
	Interactible& ladder = chunk->addInteractible("Ladder", pos);
	if (map->getType().isTop()) {
		ladder.setConnection(UP, sf::Vector2f(20, 23.5f));
	} else {
		ladder.setConnection(UP, sf::Vector2f(CHUNK_SIZE / 2, CHUNK_SIZE / 2));
	}
	empties.erase(pos);
}
