#include "Chunk.h"

#include "Trap.h"
#include "Place.h"
#include "Interactible.h"
#include "TreasureChest.h"
#include "Trap.h"

Chunk::Chunk(Coord position, Context context): position(position), context(context), state(State::EMPTY) { }

Coord Chunk::getPosition() {
    return position;
}
Coord Chunk::getPositionInTiles() {
    return position * CHUNK_SIZE;
}

Tile& Chunk::getTile(int x, int y) {
	assert(x < CHUNK_SIZE);
	assert(y < CHUNK_SIZE);
    return tiles[x][y];
}
Tile& Chunk::getTile(Coord pos) {
    assert(pos.x < CHUNK_SIZE);
	assert(pos.y < CHUNK_SIZE);
	assert(pos.x >= 0);
	assert(pos.y >= 0);
    return tiles[pos.x][pos.y];
}

std::set<b2Fixture*>& Chunk::getWalls() {
	return walls;
}

Trap& Chunk::addTrap(const TrapType& type, Coord actionPos, Coord triggerPos) {
	Trap* newTrap = new Trap(type, context);
	if (triggerPos != Coord(-1, -1)) {
		tiles[triggerPos.x][triggerPos.y].setTrapTrigger(*newTrap);
		newTrap->getTrigger().setPosition(triggerPos + getPositionInTiles());
		if (type.getNumTriggerFrames()) {
			newTrap->getTrigger().setMapObject(addObject(triggerPos * TILE_SIZE, type.getTriggerGraphic(0)));
		}
	}
	if (actionPos != Coord(-1, -1)) {
		tiles[actionPos.x][actionPos.y].setTrapAction(*newTrap);
		newTrap->getAction().setPosition(actionPos + getPositionInTiles());
		if (type.getNumActionFrames()) {
			newTrap->getAction().setMapObject(addObject(actionPos * TILE_SIZE, type.getActionGraphic(0)));
		}
	}
	traps.emplace_back(newTrap);
	return *newTrap;
}

void Chunk::update(TimeStep timeStep) {
	for (size_t i = 0; i < traps.size(); i++) {
		traps[i]->update(timeStep);
	}
	for (auto iter = entities.begin(); iter != entities.end();) {
		Entity::Ptr entity = *iter;
		if (entity->update(timeStep)) {
			entities.erase(iter++);
		} else ++iter;
	}
}

bool Chunk::addItem(Coord pos, Item item, bool) {
	if (hasTile(pos)) {
		return getTile(pos).addItem(item);
	}
	return false;
}

void Chunk::addEntity(Entity::Ptr entity) {
	entities.insert(entity);
}
bool Chunk::removeEntity(Entity::Ptr entity) {
	auto iter = entities.find(entity);
	if (iter == entities.end()) return false;
	entities.erase(iter);
	return true;
}
const std::set<Entity::Ptr>& Chunk::getEntities() {
	return entities;
}
void Chunk::getEntitiesInRect(std::vector<Entity::Ptr>& entitiesOut, sf::FloatRect rect) {
	for (auto iter = entities.begin(); iter != entities.end(); ++iter) {
		sf::Vector2f pos = (*iter)->getPosition();
		if (pos.x >= rect.left && pos.x <= rect.left + rect.width && pos.y >= rect.top && pos.y <= rect.top + rect.height) {
			entitiesOut.push_back(*iter);
		}
	}
}

MapObject& Chunk::addObject(sf::Vector2i pos, sf::IntRect texRect) {
	MapObject* newObject = new MapObject(pos, sf::Vector2i(texRect.width, texRect.height), sf::Vector2i(texRect.left, texRect.top));
	objects.emplace_back(newObject);
	return *newObject;
}
MapObject& Chunk::addObject(MapObject object) {
	MapObject* newObject = new MapObject(object);
	objects.emplace_back(newObject);
	return *newObject;
}
size_t Chunk::getNumObjects() {
	return objects.size();
}
MapObject& Chunk::getObject(size_t index) {
	assert(index < objects.size());
	return *objects[index];
}

Interactible& Chunk::addInteractible(String name, Coord position) {
	Interactible* newInteract = new Interactible(name);
	interactibles.emplace_back(newInteract);
	getTile(position).setInteract(*newInteract);
	return *newInteract;
}
Interactible& Chunk::addInteractible(String name, const std::vector<Coord>& positions) {
	Interactible* newInteract = new Interactible(name);
	interactibles.emplace_back(newInteract);
	for (size_t i = 0; i < positions.size(); i++) {
		getTile(positions[i]).setInteract(*newInteract);
	}
	return *newInteract;
}

bool Chunk::hasTile(Coord pos) {
	return hasTile(pos.x, pos.y);
}
bool Chunk::hasTile(int x, int y) {
	return x >= 0 && y >= 0 && x < CHUNK_SIZE && y < CHUNK_SIZE;
}

int Chunk::getWidth() {
	return CHUNK_SIZE;
}
int Chunk::getHeight() {
	return CHUNK_SIZE;
}

Chunk::State Chunk::getState() const {
	return state;
}

void Chunk::setState(State s) {
	state = s;
}
