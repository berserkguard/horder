#include "Settings.h"

#include "Util.h"

Settings::Settings() {
    vsync = true;
    debug = DEBUG_OFF;
    fullscreen = false;
    screenSize.x = WINDOW_WIDTH;
    screenSize.y = WINDOW_HEIGHT;
    windowSettingsHaveChanged = true;
    for (int i = 0; i < G_COUNT; i++) graphicSettings[i] = 2;
}

Settings::KeyAction Settings::getBinding(sf::Keyboard::Key key) {
    auto iter = keyBindings[keyboardLayout].find(key);
    if (iter == keyBindings[keyboardLayout].end()) return NONE;
    else return iter->second;
}

bool Settings::loadYAML(YAML::Node& node) {
	if (node["KeyBindings"]) {
		keyBindings.clear();

		YAML::Node layouts = node["KeyBindings"];
		for (auto iter = layouts.begin(); iter != layouts.end(); ++iter) {
			String name = iter->first.as<String>();
			std::map<sf::Keyboard::Key, KeyAction> keys;
			for (auto jter = iter->second.begin(); jter != iter->second.end(); ++jter) {
				KeyAction to = stringToKeyAction(jter->first.as<String>());
				sf::Keyboard::Key from = (sf::Keyboard::Key)jter->second.as<int>();
				keys[from] = to;
			}
			keyBindings.insert(std::pair<String, std::map<sf::Keyboard::Key, KeyAction>>(name, keys));
		}
	}

    if (node["KeyboardLayout"]) {
		keyboardLayout = node["KeyboardLayout"].as<String>();
    }

	if (node["VSync"]) {
		vsync = readYAMLInt(node, "VSync", true);
	}

	if (node["Debug"]) {
		String debugStr = readYAMLStr(node, "Debug", "off");
		if (debugStr == "off" || debugStr == "no") {
			debug    = DEBUG_OFF;
		} else if (debugStr == "on" || debugStr == "basic" || debugStr == "yes") {
			debug    = DEBUG_BASIC;
		} else if (debugStr == "mega") {
			debug    = DEBUG_MEGA;
		}
	}

	if (node["Window"]) {
		YAML::Node windowNode = node["Window"];
		if (windowNode.IsSequence()) {
			screenSize.x = windowNode[0].as<int>();
			screenSize.y = windowNode[1].as<int>();
			windowSettingsHaveChanged = true;
			fullscreen = false;
		} else if (windowNode && windowNode.as<String>() == "Fullscreen") {
			fullscreen = true;
			windowSettingsHaveChanged = true;
		}
	}

	if (node["Graphics"]) {
		YAML::Node graphicsNode = node["Graphics"];
		if (graphicsNode.IsMap()) {
			for (auto iter = graphicsNode.begin(); iter != graphicsNode.end(); ++iter) {
				String valName = iter->second.as<String>();

				int value = 0;
				if (valName == "Off") value = 0;
				else if (valName == "Low") value = 1;
				else if (valName == "Medium") value = 2;
				else if (valName == "High") value = 3;
				else std::cout << "'" << valName << "' is not an acceptable graphical quality." << std::endl;

				String name = iter->first.as<String>();
				if (name == "Lighting") graphicSettings[LIGHTING] = value;
				else std::cout << "'" << name << "' is not an existing graphical setting." << std::endl;
			}
		}
	}

    return true;
}

void Settings::apply(sf::RenderWindow& window) {
    if (windowSettingsHaveChanged) {
        // reopens the window with new settings
        sf::ContextSettings settings = window.getSettings();
        window.close();
        if (fullscreen) {
            window.create(sf::VideoMode::getDesktopMode(), "Horder!", sf::Style::Fullscreen, settings);
        } else {
            window.create(sf::VideoMode(screenSize.x, screenSize.y), "Horder!", sf::Style::Close, settings);
        }

        windowSettingsHaveChanged = false;
    }
    window.setVerticalSyncEnabled(vsync);
}

int Settings::getGraphicQuality(GraphicSetting setting) {
	return graphicSettings[setting];
}

DebugMode Settings::getDebugMode() {
    return debug;
}
void Settings::setDebugMode(DebugMode mode) {
	debug = mode;
}

bool Settings::isFullscreen() {
    return fullscreen;
}

Settings::KeyAction Settings::stringToKeyAction(String s) {
    if (s == "Up") return UP;
    if (s == "Down") return DOWN;
    if (s == "Left") return LEFT;
    if (s == "Right") return RIGHT;
    if (s == "Interact") return INTERACT;
    if (s == "Backpack") return BACKPACK;
    if (s == "Pause") return PAUSE;
    if (s == "CharInfo") return CHARINFO;
    return NONE;
}
