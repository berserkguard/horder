#include "MobSpawner.h"

#include "Monster.h"
#include "Map.h"
#include "Rand.h"
#include "MapType.h"
#include "VecUtil.h"

MobSpawner::MobSpawner(Map& map, EffectsManager& effects): map(&map), effects(&effects) { }

void MobSpawner::spawnEncounter(const sf::IntRect& range) {
	Coord pos(Rand::i(range.left + 1, range.width + range.left - 1), Rand::i(range.top + 1, range.height + range.top - 1));
	int level = (int)(vecMagnitude(sf::Vector2f(pos)) / 100);
	EncounterBag* encounters = map->getType().getEncounters(level);
	if (!encounters) return;
	Encounter& encounter = encounters->grab();
	spawnMonsters(pos, encounter.getMonster(), encounter.randomSize());
}

Monster* MobSpawner::spawnMonster(sf::Vector2i position, const MonsterType& type, bool allowAlt) {
	sf::Vector2i pos;
	if (allowAlt) {
		pos = getFloorNear(position);
	} else pos = position;

	Monster* newMonster = new Monster(type, *effects, sf::Vector2f(pos.x + Rand::f(0.2f, 0.8f), pos.y + Rand::f(0.2f, 0.8f)));
	newMonster->setPlace(*map);
	map->addEntity(Entity::Ptr(newMonster));
	return newMonster;
}

void MobSpawner::spawnMonsters(Coord pos, const MonsterType& type, int number) {
	for (int i = 0; i < number; i++) {
		spawnMonster(pos, type);
		usedPositions.insert(pos);
	}
	usedPositions.clear();
}

sf::Vector2i MobSpawner::getFloorNear(sf::Vector2i position) {
	sf::Vector2i cur = position;
	Direction dir = NORTH;
	int dur = 1;
	int durT = 1;
	while (map->isObstructed(cur, NULL) || usedPositions.count(cur)) {
		cur += dirVector(dir);
		durT--;
		if (!durT) {
			dir = turn(dir, 90);
			if (dir == NORTH || dir == SOUTH) {
				dur++;
			}
			durT = dur;
		}
	}
	return cur;
}
