#include "Player.h"

#include <SFML/Audio.hpp>
#include "TextEffect.h"
#include "MobSpawner.h"
#include "ResourceManager.h"
#include "EffectsManager.h"
#include "FadeEffect.h"
#include "TextEffect.h"
#include "ValuablesManager.h"
#include "Interactible.h"
#include "Map.h"

b2BodyDef* playerBodyDef;
b2BodyDef& initPlayerBody() {
	playerBodyDef = new b2BodyDef();
	playerBodyDef->type = b2_dynamicBody;
	playerBodyDef->linearDamping = .1f;
	playerBodyDef->fixedRotation = true;
	playerBodyDef->userData = NULL;
	return *playerBodyDef;
}

Player::Player(EffectsManager& effects)
		: Mob(PLAYER, initPlayerBody(), effects) {
	delete playerBodyDef;

	itself = Entity::Ptr(this);

	for (int i = 0; i < Stat::COUNT; i++) {
		baseStats[i] = Stat::getDefaultValue((Stat::Type)i);
		equipStats[i] = 0;
		hoardStats[i] = 0;
	}
	baseStats[Stat::KNOCKBACK]    = 1.0f;
	baseStats[Stat::MAX_TARGETS]  = 4;
	baseStats[Stat::SIZE]         = (20 / 64.f);
	baseStats[Stat::ATTACK]       = 3.5f;
	baseStats[Stat::CRITICAL]     = 30;
	baseStats[Stat::ATTACK_SPEED] = 2.0f;
	baseStats[Stat::RANGE]        = 1.5f;
	baseStats[Stat::ATTACK_WIDTH] = 1.5f;

	isBlocking = false;
	dead = false;

	home = nullptr;
	currentShownInteract = nullptr;
	shownInteractEffect = nullptr;
	deathEffect = nullptr;

	curFootSound = nullptr;
	curFootSoundMaterial = TileType::NONE;
	playingFootSound = false;

	init();

	// NOTE: HARDCODED, SHOULD PROBABLY BE READ FROM YAML AT SOME POINT
	getAnims().newAnimation("Swing", 8, 1.0f);
	getAnims().newAnimation("Block", 8, 1.0f);
}

void Player::loadYAML(YAML::Node& node, Map& map, ValuablesManager& valuables) {

	YAML::Node posNode = node["Position"];
	getCollisionBody().SetTransform(b2Vec2(posNode[0].as<float>(), posNode[1].as<float>()), 0);

	// add bounding circle
	YAML::Node sizeNode = node["Size"];
	b2CircleShape circle;
	circle.m_radius = sizeNode.as<int>() / (float)TILE_SIZE;
	b2FixtureDef playerFix;
	playerFix.shape = &circle;
	playerFix.density = 1.f;
	playerFix.friction = .01f;
	playerFix.restitution = .01f;
	playerFix.userData = NULL;
	getCollisionBody().CreateFixture(&playerFix);

	setMap(map);
	map.activate();
	map.addEntity(itself);

	const Model* model = valuables.getModel(readYAMLStr(node, "Model", ""));
	if (model) setModel(*model, 0.5f);
}

Entity::Ptr Player::getEntity() {
	return itself;
}

bool Player::update(TimeStep ts) {
	// dampen the player velocity as needed
	b2Vec2 velocity = getCollisionBody().GetLinearVelocity();
	float m = getCollisionBody().GetMass();
	float factor = getSpeed() * getRubbleSlow();
	float speed = SPEED_FACTOR * factor;
	float moveForce = MOVE_FORCE * factor;
	if (isMovingX() && isMovingY()) {
		// diagonal velocity dampening
		if (std::abs(velocity.x) > speed / 1.41f) {
			float force = (speed / 1.41f - velocity.x) * m / ts.sec();
			if (force > 0) force = std::min(moveForce, force);
			else force = std::max(-moveForce, force);
			getCollisionBody().ApplyLinearImpulse(b2Vec2(ts.sec() * force, 0),  getCollisionBody().GetLocalCenter());
		}
		if (std::abs(velocity.y) > speed / 1.41f) {
			float force = (speed / 1.41f - velocity.y) * m / ts.sec();
			if (force > 0) force = std::min(moveForce, force);
			else force = std::max(-moveForce, force);
			getCollisionBody().ApplyLinearImpulse(b2Vec2(0, ts.sec() * force),  getCollisionBody().GetLocalCenter());
		}
	} else {
		// orthogonal velocity dampening
		if (!isMovingX()) {
			float force = -velocity.x * m / ts.sec();
			if (force > 0) force = std::min(moveForce, force);
			else force = std::max(-moveForce, force);
			getCollisionBody().ApplyLinearImpulse(b2Vec2(ts.sec() * force, 0),  getCollisionBody().GetLocalCenter());
		}
		if (!isMovingY()) {
			float force = -velocity.y * m / ts.sec();
			if (force > 0) force = std::min(moveForce, force);
			else force = std::max(-moveForce, force);
			getCollisionBody().ApplyLinearImpulse(b2Vec2(0, ts.sec() * force),  getCollisionBody().GetLocalCenter());
		}
	}

	if (curFootSound) {
		float vel = velocity.LengthSquared();
		if (vel < 0.1f) {
			curFootSound->pause();
			playingFootSound = false;
		} else {
			curFootSound->setVolume(vel / (speed * speed) * 100.f);
		}
	}

	// do normal mob update
	Mob::update(ts);

	// Attacking animation
	Angle angle = getAngle();
	Animation* swingAnim = getAnims().getAnimation("Swing");
	int frame = swingAnim->getFrame();
	sf::Vector2f handPos = getModel().getJoint(RIGHT_HAND, frame).getPosition();
	float rotX = cosf(angle) * handPos.x - sinf(angle) * handPos.y;
	float rotY = sinf(angle) * handPos.x + cosf(angle) * handPos.y;
	spriteLayers.at(getSpriteIndex(SL_WEAPONS)).setPosition(sf::Vector2f(rotX, rotY));
	spriteLayers.at(getSpriteIndex(SL_WEAPONS)).rotate(getModel().getJoint(RIGHT_HAND, frame).getRotation());

	swingAnim->apply(spriteLayers.at(getSpriteIndex(SL_ARMS_RIGHT)), SPRITE_SIZE, SPRITE_SIZE);

	// Blocking animation
	Animation* blockAnim = getAnims().getAnimation("Block");
	frame = blockAnim->getFrame();
	handPos = getModel().getJoint(LEFT_HAND, frame).getPosition();
	rotX = cosf(angle) * handPos.x - sinf(angle) * handPos.y;
	rotY = sinf(angle) * handPos.x + cosf(angle) * handPos.y;
	spriteLayers.at(getSpriteIndex(SL_SHIELDS)).setPosition(sf::Vector2f(rotX, rotY));
	spriteLayers.at(getSpriteIndex(SL_SHIELDS)).rotate(getModel().getJoint(LEFT_HAND, frame).getRotation());

	if (isBlocking && blockAnim->isLastFrame()) {
		blockAnim->pause();
	}
	blockAnim->apply(spriteLayers.at(getSpriteIndex(SL_ARMS_LEFT)), SPRITE_SIZE, SPRITE_SIZE);

	// move player if they are in a wall
	Coord tilePos = getGroundPos();
	Chunk* chunk = getMap()->getChunkContainingPos(tilePos);
	if (chunk && getPlace()->isObstructed(tilePos, this)) {
		if (chunk != NULL) {
			Coord cur = getMap()->getSpawner().getFloorNear(tilePos);
			setPosition(cur.x + .5f, cur.y + .5f);
		}
	}

	// set spacial sound stuff
	sf::Listener::setPosition(getPosition().x, getPosition().y, 0);
	sf::Listener::setDirection(1.f, 0, 0);
	sf::Listener::setGlobalVolume(65.f);

	return false;
}

void Player::die() {
	if (dead) return;
	setVisible(false);

	deathEffect = &getEffects().graphical().createFadeEffect(1.0f, 0, sf::Vector2f(0, 0));
	deathEffect->setColor(sf::Color(255, 0, 0, 0), sf::Color(60, 0, 0, 230));
	deathEffect->setCallback(std::bind(&Player::deathFadeCallback, this, std::placeholders::_1));

	if (!getMap()) return;
	Coord pos = getGroundPos();
	for (int i = 0; i < inventory.getItemContainer().getNumItems(); i++) {
		getMap()->addItem(pos, inventory.getItemContainer().getItemAt(i));
	}
	inventory.getItemContainer().clear();
	isBlocking = false;
	getAnims().getAnimation("Block")->setFrame(0);
	dead = true;
}

void Player::getDamaged(float amount, Angle direction, int flags) {
	if (dead) return;

	Angle r = std::fmod(getAngle() - direction + TAU, TAU);
	if (isBlocking && !(flags & Mob::NO_BLOCK) && (r < TAU / 8 || r > TAU - (TAU / 8))) {
		amount = 0;

		TextEffect& effect = getEffects().graphical().createTextEffect("Blocked!", 0.5f, getPosition());
		effect.setSize(12);
		effect.setColor(sf::Color(255, 127, 0));

		SoundPtr sound = getEffects().sound().newSound(S_CLINK);
		sound->setVolume(50.f);
		sound->play();
	} else {
		Mob::getDamaged(amount, direction, flags);
	}
}

void Player::move(int dir, TimeStep timeStep) {
	if (dead) return;
	Tile* groundTile = getGroundTile();

	// interaction text
	if (timeStep.hasPeriodPassed(TimeStep::FIFTH) && groundTile) {
		Interactible* toInteractWith = nullptr;
		if (groundTile->getSpecial() == Tile::INTERACT) toInteractWith = &groundTile->getInteract();
		if (toInteractWith != currentShownInteract) {
			if (shownInteractEffect) shownInteractEffect->resume();
			if (toInteractWith && toInteractWith->getType() != Interactible::NONE) {
				currentShownInteract = toInteractWith;
				String str = "Press SPACE to use " + toInteractWith->getName();
				shownInteractEffect = &getEffects().graphical().createTextEffect(str, 0.2f, getPosition());
				shownInteractEffect->setSize(12);
				shownInteractEffect->pause();
			} else {
				currentShownInteract = nullptr;
			}
		}
	}

	float amnt = timeStep.sec();
	switch(dir) {
		case NORTH:     moveRaw(0    , -amnt); break;
		case NORTHEAST: moveRaw(amnt , -amnt); break;
		case EAST:      moveRaw(amnt ,  0);    break;
		case SOUTHEAST: moveRaw(amnt ,  amnt); break;
		case SOUTH:     moveRaw(0    ,  amnt); break;
		case SOUTHWEST: moveRaw(-amnt,  amnt); break;
		case WEST:      moveRaw(-amnt,  0);    break;
		case NORTHWEST: moveRaw(-amnt, -amnt); break;
		default: break;
	}

	// footstep sound
	if (groundTile) {
		TileType::Material material = groundTile->getFloor().getMaterial();
		if (material != curFootSoundMaterial) {
			curFootSoundMaterial = material;
			Sound s;
			switch(curFootSoundMaterial) {
				case TileType::GRASS: s = S_STEPS_GRASS; break;
				case TileType::WOOD:  s = S_STEPS_WOOD;  break;
				case TileType::STONE: s = S_STEPS_STONE; break;
				default: s = S_NONE;
			}
			if (curFootSound.use_count()) curFootSound->stop();
			if (s) {
				curFootSound = getEffects().sound().newSound(s);
				curFootSound->setLoop(true);
			} else {
				curFootSound = nullptr;
			}
			playingFootSound = false;
		}
	}
	if (curFootSound && !playingFootSound) {
		curFootSound->play();
		playingFootSound = true;
	}
}

float Player::getSpeed() {
	return getStatValue(Stat::MOVE_SPEED);
}

Item Player::unequipItem(EquipSlot slot) {
	Item& item = inventory.getEquippedItem(slot);
	if (item.isNull() || dead) {
		std::cout << "No item was equipped." << std::endl;
		return item;
	}

	sf::Sprite* sprite = nullptr;
	if (item.getType().getSlot() == EquipSlot::HAND) {
		sprite = &spriteLayers.at(getSpriteIndex(SL_WEAPONS));
	} else if (item.getType().getSlot() == EquipSlot::OFFHAND) {
		sprite = &spriteLayers.at(getSpriteIndex(SL_SHIELDS));
	} else if (item.getType().getSlot() == EquipSlot::HEAD) {
		sprite = &spriteLayers.at(getSpriteIndex(SL_HAT));
	}

	if (sprite) {
		sf::Vector2i topLeft(0, 0);
		sprite->setTextureRect(sf::IntRect(topLeft, sf::Vector2i(SPRITE_WEAPON_WIDTH, SPRITE_WEAPON_HEIGHT)));
	}
	for (int i = 0; i < Stat::COUNT; i++) {
		equipStats[i] -= item.getType().getStatValue((Stat::Type)i);
	}

	inventory.getItemContainer().addItem(inventory.unequipItem(slot));

	return item;
}

void Player::equipItem(Item item) {
	if (dead) return;

	if (!item.getType().isEquippable()) {
		std::cout << "Item not equippable: " << item.getType().getName() << std::endl;
		return;
	}
	Item oldItem = inventory.unequipItem(item.getType().getSlot());
	inventory.equipItem(item);
	if (!oldItem.isNull()) {
		// unequip last item
		for (int i = 0; i < Stat::COUNT; i++) {
			equipStats[i] -= oldItem.getType().getStatValue((Stat::Type)i);
		}
		inventory.getItemContainer().addItem(oldItem);
	}

	sf::Sprite* sprite = nullptr;
	sf::Vector2f origin;

	if (item.getType().getSlot() == EquipSlot::HAND) {
		sprite = &spriteLayers.at(getSpriteIndex(SL_WEAPONS));
		origin = sf::Vector2f(SPRITE_WEAPON_WIDTH / 2, 0.75f * SPRITE_WEAPON_HEIGHT);
	} else if (item.getType().getSlot() == EquipSlot::OFFHAND) {
		sprite = &spriteLayers.at(getSpriteIndex(SL_SHIELDS));
		origin = sf::Vector2f(SPRITE_WEAPON_WIDTH / 2, SPRITE_WEAPON_HEIGHT / 2);
	} else if (item.getType().getSlot() == EquipSlot::HEAD) {
		sprite = &spriteLayers.at(getSpriteIndex(SL_HAT));
		origin = sf::Vector2f(SPRITE_WEAPON_WIDTH / 2, SPRITE_WEAPON_HEIGHT / 2);
	}

	if (sprite) {
		sf::Vector2i topLeft(SPRITE_WEAPON_WIDTH * item.getType().getEquipGLoc().x, SPRITE_WEAPON_HEIGHT * item.getType().getEquipGLoc().y);
		sprite->setTextureRect(sf::IntRect(topLeft, sf::Vector2i(SPRITE_WEAPON_WIDTH, SPRITE_WEAPON_HEIGHT)));
		sprite->setOrigin(origin);
	}
	for (int i = 0; i < Stat::COUNT; i++) {
		equipStats[i] += item.getType().getStatValue((Stat::Type)i);
	}

	getEffects().sound().newSound(S_GRAB)->play();
}

void Player::interact() {
	if (dead || !getMap()) return;
	Tile* tile = getGroundTile();
	if (tile && tile->getSpecial() == Tile::INTERACT) {
		Interactible& toInteractWith = tile->getInteract();
		std::cout << "Interacting with " << toInteractWith.getName() << "." << std::endl;
		if (toInteractWith.getType() == Interactible::CONNECTION) {
			if (toInteractWith.getName() == "Trapdoor") {
				homeEnter = getPosition();
			}
			Map* nextMap = getMap()->getAdjacentMap(toInteractWith.getDirection());
			if (nextMap) {
				// move to the next map
				setMap(*nextMap);
				setPosition(toInteractWith.getDestination());
			}
		} else if (toInteractWith.getName() == "Bed") {
			// Restore player's health
			FadeEffect& effect = getEffects().graphical().createFadeEffect(0.5f, 0.5f, sf::Vector2f(0, 0));
			effect.setColor(sf::Color::Transparent, sf::Color::Black);
			effect.setCallback(std::bind(&Player::sleepEnd, this, std::placeholders::_1));
		}
	}
}

void Player::block(bool value) {
	if (value == isBlocking || dead) return;

	Animation* blockAnim = getAnims().getAnimation("Block");
	if (value) {
		// Setting duration resets animation, so only do it right before blocking
		blockAnim->setLength(1.0f / getStatValue(Stat::ATTACK_SPEED));
	}
	blockAnim->setDirection(value ? ANIM_FORWARD : ANIM_BACKWARD);
	blockAnim->play();

	isBlocking = value;
}

bool Player::attack() {
	if (isBlocking || dead) return false;

	Animation* swingAnim = getAnims().getAnimation("Swing");
	if (Mob::attack()) {
		swingAnim->setLength(1.0f / getStatValue(Stat::ATTACK_SPEED));
		swingAnim->play();
		return true;
	}
	return false;
}

float Player::getStatValue(Stat::Type stat) {
	return baseStats[stat] + equipStats[stat] + hoardStats[stat];
}

Inventory& Player::getInventory() {
    return inventory;
}

void Player::recalcHoardStats(Coord pos) {
	if (pos.x >= CHUNK_SIZE || pos.y >= CHUNK_SIZE || pos.x < 0 || pos.y < 0) return;
	if (!home) return;
	if (pos == Coord(-1, -1)) {
		// recalculate the whole house
		for (int y = 0; y < CHUNK_SIZE; y++) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				recalcHoardStats(Coord(x, y));
			}
		}
	} else {
		// recalculate one tile
		if (!home->hasTile(pos)) return;
		ItemContainer* items = home->getTile(pos).getItemContainer();
		for (int i = 0; i < Stat::COUNT; i++) {
			hoardStats[i] -= hoardTileStats[pos.x][pos.y][i];
			hoardTileStats[pos.x][pos.y][i] = 0;
		}
		if (items) {
			for (int i = 0; i < items->getNumItems(); i++) {
				const ItemType& itemType = items->getItemAt(i).getType();
				for (int j = 0; j < Stat::COUNT; j++) {
					hoardTileStats[pos.x][pos.y][j] += itemType.getStatValue((Stat::Type)j) * 0.1f;
				}
			}
			for (int i = 0; i < Stat::COUNT; i++) {
				hoardStats[i] += hoardTileStats[pos.x][pos.y][i];
			}
		}
	}
}

Map* Player::getMap() {
    return (Map*)getPlace();
}

void Player::setMap(Map& map) {
	if (getMap()) {
		getMap()->removeEntity(itself);
		getMap()->deactivate();
		curFootSound->pause();
	}
	setPlace(map);
	getMap()->addEntity(itself);
	getMap()->activate();
}

void Player::setHome(Map& map) {
	home = &map;
	for (int i = 0; i < Stat::COUNT; i++) {
		for (int y = 0; y < CHUNK_SIZE; y++) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				hoardTileStats[x][y][i] = 0;
			}
		}
	}
	recalcHoardStats();
}

Map* Player::getHome() {
	return home;
}

void Player::fastReturn() {
	if (!dead && getMap() != home && getMap()->isPlayerSafe()) {
		FadeEffect& effect = getEffects().graphical().createFadeEffect(0.5f, 0.5f, sf::Vector2f(0, 0));
		effect.setColor(sf::Color::Transparent, sf::Color::Black);
		effect.setCallback(std::bind(&Player::fastReturnTeleport, this, std::placeholders::_1));
	}
}

void Player::fastReturnTeleport(FadeEvent evt) {
	if (home && evt == FadeEvent::FADE_MIDDLE) {
		setMap(*home);
		setPosition(homeEnter);
	}
}

void Player::deathFadeCallback(FadeEvent evt) {
	if (evt == FadeEvent::FADE_MIDDLE) {
		if (dead) {
			deathEffect->pause();
		}
	}
}

void Player::sleepEnd(FadeEvent evt) {
	if (evt == FadeEvent::FADE_MIDDLE) {
		restoreHitPoints(getStatValue(Stat::Type::HP));
		std::cout << "Restored player's health." << std::endl;
	}
}

void Player::revive() {
	if (deathEffect) {
		deathEffect->resume();
		fastReturnTeleport(FadeEvent::FADE_MIDDLE);
		setVisible(true);
		restoreHitPoints(10000);
		dead = false;
	}
}
