#include "Mob.h"

#include <iostream>
#include <sstream>
#include "Tile.h"
#include "AnimData.h"
#include "TextEffect.h"
#include "Place.h"
#include "Rand.h"
#include "EffectsManager.h"
#include "ResourceManager.h"
#include "TreasureChest.h"

Mob::Mob(Type type, b2BodyDef& def, EffectsManager& effects): Entity(MOB, def, effects), type(type) {
	movingX = movingY = false;
	angle = targetAngle = 0;
	rotateTowardDelay = 0;
	hitPoints = 0;
	cooldown = 0;
	place = nullptr;

	model = nullptr;
	for (int i = 0; i < (int)SL_END; i++) {
		spriteIndices[i] = -1;
	}
}

void Mob::init() {
	hitPoints = getStatValue(Stat::HP);
}

Mob::Type Mob::getMobType() {
	return type;
}

bool Mob::update(TimeStep timeStep) {
	Entity::update(timeStep);

	// Update the animations
	animations.update(timeStep);

    // rotating (but only if moving)
    b2Vec2 velocity = getCollisionBody().GetLinearVelocity();
    if (rotateTowardDelay <= 0) {
        if (isMoving()) {
            targetAngle = vecAngle(toSFMLVec(velocity));
        }
    } else rotateTowardDelay -= timeStep.sec();

    doRotes(timeStep.sec());

	movingX = false;
    movingY = false;

    cooldown -= timeStep.sec();

	if (hitPoints <= 0) {
		die();
		return true;
	}
	hitPoints += getStatValue(Stat::REGEN) * timeStep.min();
	if (hitPoints > getStatValue(Stat::HP)) hitPoints = getStatValue(Stat::HP);

	return false;
}

void Mob::rotateToward(Angle angle, float dur) {
	targetAngle = angle;
	rotateTowardDelay = dur;
}

void Mob::moveRaw(float x, float y) {
	// accelerate the body in the directions
	float v = getCollisionBody().GetLinearVelocity().Length();
	float m = getCollisionBody().GetMass();
	float t = std::sqrt(x * x + y * y);
	float factor = getSpeed() * getRubbleSlow();
	float force = std::min(MOVE_FORCE * factor, (SPEED_FACTOR * factor - v) * m / t);
	getCollisionBody().ApplyLinearImpulse(b2Vec2(x * force, y * force),  getCollisionBody().GetLocalCenter());
	if (x) movingX = true;
	if (y) movingY = true;
}

float Mob::getRubbleSlow() {
	Tile* tile = getGroundTile();
	if (tile) return 10.f / (10 + tile->getNumItems());
	return 1;
}

bool Mob::attack() {
	if (cooldown > 0) return false;
	if (!place) return false;
	cooldown = 1.f / getStatValue(Stat::ATTACK_SPEED);

	// gets all the necessary stats
	float width = getStatValue(Stat::ATTACK_WIDTH);
	int maxTargets = getStatValue(Stat::MAX_TARGETS);
	int hitSoFar = 0;

	std::vector<Entity::Ptr> entities;
	float areaR = getStatValue(Stat::SIZE) * (getStatValue(Stat::RANGE) + 1) * 2;
	place->getEntitiesInRect(entities, sf::FloatRect(getPosition() - sf::Vector2f(areaR, areaR), sf::Vector2f(areaR * 2, areaR * 2)));

	// check for all mob entities
	for (auto iter = entities.begin(); iter != entities.end(); ++iter) {
		Entity::Ptr e = *iter;
		if (e.get() == this) continue;

		if (inRange(e)) {
			// check if in angle
			sf::Vector2f diff = e->getPosition() - getPosition();
			Angle theirAngle = vecAngle(diff);
			Angle r = std::fmod(angle - theirAngle + TAU, TAU);
			if (r < width / 2 || r > TAU - (width / 2)) {
				// hit!

				// knockback
				b2Body& body = e->getCollisionBody();
				float kb = getStatValue(Stat::KNOCKBACK) / 10.0f;
				body.ApplyLinearImpulse(toPhysVec(angleVec(theirAngle) * kb), b2Vec2(0, 0));

				if (e->getType() == MOB) {
					hitMob((Mob&)*e);
				} else if (e->getType() == CHEST) {
					((TreasureChest&)*e).open(*place);
					getEffects().sound().newSound(S_OPEN, e->getPosition())->play();
				}
				hitSoFar++;
				if (hitSoFar >= maxTargets) break;
			}
		}
	}
	if (!hitSoFar) {
		std::shared_ptr<sf::Sound> sound = getEffects().sound().newSound(S_MISS, getPosition());
		sound->setVolume(Rand::f(40.f, 80.f));
		sound->setPitch(Rand::f(0.9, 1.1));
		sound->play();
	}
	return true;
}

bool Mob::inRange(Entity::Ptr entity) {
	sf::Vector2f diff = entity->getPosition() - getPosition();
	float rar = getStatValue(Stat::SIZE) * (getStatValue(Stat::RANGE) + 1);
	if (entity->getType() == MOB) {
		rar += ((Mob&)*entity).getStatValue(Stat::SIZE);
	}
	return diff.x * diff.x + diff.y * diff.y <= rar * rar;
}

void Mob::doRotes(float diff) {
	Angle change = diff * ROTATE_SPEED * 1.1f;
	Angle c = std::min(std::abs(angle - targetAngle + TAU), std::abs(angle - targetAngle - TAU));
	if (c < change || std::abs(angle - targetAngle) < change) {
		// if its about to rotate to the target angle, just set it to the target angle
		angle = targetAngle;
	} else {
		if (toRotateClockwise(angle, targetAngle)) {
			angle += diff * ROTATE_SPEED;
		} else {
			angle -= diff * ROTATE_SPEED;
		}
	}
	angle = std::fmod(angle, TAU);
	for (auto iter = spriteLayers.begin(); iter != spriteLayers.end(); ++iter) {
		iter->setRotation(angle / TAU * 360.f);
	}
}

bool Mob::isMoving() {
	return movingX || movingY;
}
bool Mob::isMovingX() {
	return movingX;
}
bool Mob::isMovingY() {
	return movingY;
}

float Mob::getHitPoints() {
	return hitPoints < 0.0f ? 0.0f : hitPoints;
}

float Mob::getMaxHitPoints() {
	return getStatValue(Stat::Type::HP);
}

float Mob::getSpeed() {
	return 1.f;
}

void Mob::restoreHitPoints(int amount) {
	hitPoints += amount;
	int max = getStatValue(Stat::HP);
	if (hitPoints > max) hitPoints = max;
}

void Mob::getDamaged(float amount, float direction, int flags) {
	int fontSize = 20;
	std::stringstream ss;

	ss << (int)amount;
	if (flags & Mob::CRIT) {
		ss << "!";
		fontSize = 30;
	}

	if (!(flags & Mob::NO_SOUND)) {
		SoundPtr sound = getEffects().sound().newSound(S_HIT, getPosition());
		sound->setVolume(75.f);
		sound->play();
	}

	if (!(flags & Mob::NO_TEXT)) {
		TextEffect& effect = getEffects().graphical().createTextEffect(ss.str(), 0.5f, getPosition());
		effect.setSize(fontSize);
		effect.setColor(sf::Color(255, 127, 0));
	}

	hitPoints -= amount;
	getAnimData().addAnimation(AnimData::FLASH, .1f);
	getAnimData().addAnimation(AnimData::HP, 2.f);
}

void Mob::hitMob(Mob& mob) {

	float hitChance = std::pow(0.5, (mob.getStatValue(Stat::EVASION) - getStatValue(Stat::ACCURACY) + 10) / 100);
	if (Rand::f() >  hitChance) {
		// missed!
		TextEffect& effect = getEffects().graphical().createTextEffect("miss", 0.5f, mob.getPosition());
		effect.setSize(20);
		effect.setColor(sf::Color::White);

		SoundPtr sound = getEffects().sound().newSound(S_MISS, getPosition());
		sound->setVolume(85.f);
		sound->play();
	} else {
		// damage
		float baseDam = getStatValue(Stat::ATTACK);
		float def = std::pow(0.5, mob.getStatValue(Stat::DEFENSE) / 100);
		float dam = def * Rand::norm(baseDam, baseDam / 5.f);
		float critChance = 1 - std::pow(0.5, getStatValue(Stat::CRITICAL) / 100);

		DamageFlags crit = NONE;
		if (Rand::f() < critChance) {
			dam *= 2;
			crit = CRIT;

			getEffects().sound().newSound(S_HIT, getPosition())->play();
		}

		mob.getDamaged(dam, angle + TAU / 2, crit);
	}
}

void Mob::setPlace(Place& p) {
	place = &p;
}
Place* Mob::getPlace() {
	return place;
}

Coord Mob::getGroundPos() {
	return vecFloor(getPosition());
}
Tile* Mob::getGroundTile() {
	if (!place->hasTile(getGroundPos())) return nullptr;
	else return &place->getTile(getGroundPos());
}

Angle Mob::getAngle() {
	return angle;
}

void Mob::setModel(const Model& m, float scale, sf::Color color) {
	if (model) return;
	model = &m;
	for (int i = 0; i < SL_END; i++) {
		if (!model->hasSprite((SpriteLayer)i)) {
			continue;
		}
		sf::Sprite& spr = model->createSprite((SpriteLayer)i, spriteLayers, spriteIndices);

		spr.setColor(color);
		sf::FloatRect r = spr.getLocalBounds();
		spr.setOrigin(r.width / 2, r.height / 2);
		spr.setScale(scale, scale);
	}
	model->fillAnimationContainer(getAnims());
}
const Model& Mob::getModel() {
	if (model) return *model;
	return Model::getBlank();
}

AnimationContainer& Mob::getAnims() {
	return animations;
}

int Mob::getSpriteIndex(SpriteLayer type) {
	return spriteIndices[type];
}
