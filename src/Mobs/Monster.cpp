#include "Monster.h"

#include <iostream>
#include "EffectsManager.h"
#include "MonsterType.h"
#include "Place.h"
#include "Tile.h"
#include "Rand.h"

enum AIState {WAIT, CHASE, ATTACK, DONE};

b2BodyDef* monsterBodyDef;
b2BodyDef& initMonsterBody(sf::Vector2f position) {
	monsterBodyDef = new b2BodyDef();
	monsterBodyDef->type = b2_dynamicBody;
	monsterBodyDef->position = toPhysVec(position);
	monsterBodyDef->fixedRotation = true;
	monsterBodyDef->userData = NULL;
	return *monsterBodyDef;
}

Monster::Monster(const MonsterType& monsterType, EffectsManager& effects, sf::Vector2f position)
		: Mob(MONSTER, initMonsterBody(position), effects), type(&monsterType) {
	delete monsterBodyDef;
	b2Body& monsterBody = getCollisionBody();
	monsterBody.SetLinearDamping(getStatValue(Stat::DAMPING));

	b2CircleShape circle;
	circle.m_radius = getStatValue(Stat::SIZE);
	b2FixtureDef monFix;
	monFix.shape = &circle;
	monFix.density = getStatValue(Stat::DENSITY);
	monFix.friction = .01f;
	monFix.restitution = getStatValue(Stat::ELASTICITY);
	monFix.userData = NULL;
	monsterBody.CreateFixture(&monFix);

	setModel(type->getModel(), type->getStatValue(Stat::GSCALE), type->getColor());

	target = NULL;
	awareness = 0;
	pointOnPath = -1;
	path.setMob(this);
	state = WAIT;
	specialCooldown = 0;

	init();
}

bool Monster::attack() {
	if (!Mob::attack()) return false;

	// If the mob has a swing animation, play it
	if (Animation* swingAnim = getAnims().getAnimation("Swing")) {
		swingAnim->setLength(0.3f);
		swingAnim->play();
	}

	return true;
}

float Monster::getSpeed() {
	return getStatValue(Stat::MOVE_SPEED);
}

bool Monster::update(TimeStep timeStep) {
	if (isMoving()) {
		getCollisionBody().SetLinearDamping(.1);
	} else {
		getCollisionBody().SetLinearDamping(getStatValue(Stat::DAMPING) * 10);
	}
	ai(timeStep.sec());

	// Update animation
	if (Animation* swingAnim = getAnims().getAnimation("Swing")) {
		swingAnim->apply(spriteLayers.at(getSpriteIndex(SL_ARMS_RIGHT)), SPRITE_SIZE, SPRITE_SIZE);
	}

	return Mob::update(timeStep.sec());
}

const MonsterType& Monster::getMonsterType() {
	return *type;
}

float Monster::getStatValue(Stat::Type stat) {
	return type->getStatValue(stat);
}

void Monster::die() {
	// drop random item on tile
	if (getPlace()) {
		const ItemType* drop = type->getRandomDrop();
		if (drop) {
			getPlace()->addItem(vecFloor(getPosition()), Item(*drop));
		}
	}
	if (type->getSound()) {
		SoundPtr sound = getEffects().sound().newSound(type->getSound(), getPosition());
		sound->setPitch(Rand::f(0.8, 1.2));
		sound->setVolume(Rand::f(80.f, 100.f));
		sound->play();
	}
}

void Monster::setTarget(Entity::Ptr t) {
	target = t;
}

Entity::Ptr Monster::getTarget() {
	return target;
}

void Monster::setSeesTarget(bool does) {
	// eventually awareness will do more interesting things
	if (does) awareness += 1.0;
	else awareness -= 1.0;

	if (awareness > 2.0f) awareness = 2.0f;
	else if (awareness < 0.0f) awareness = 0.0f;
}

void Monster::onEntityCollision(Entity& collidingWith) {
	Entity::onEntityCollision(collidingWith);
	if (&collidingWith == target.get() && type->getAI() == AI_HOP) {
		attack();
	}
}

void Monster::ai(TimeStep timeStep) {
	if (!getPlace()) return;
	specialCooldown -= timeStep.sec();
	switch(state) {
		case WAIT:
		if (target && awareness >= 1) {
			if (type->getAI() != AI_NONE && path.generate(getPosition(), target->getPosition())) {
				pointOnPath = 0;
				state = CHASE;
			} else awareness -= 0.2;
		} break;
		case CHASE: {
			if (!target) {
				state = WAIT;
				break;
			}
			if (type->getAI() == AI_SPRINT && inRange(target)) {
				sf::Vector2f posDiff = target->getPosition() - getPosition();
				rotateToward(vecAngle(posDiff));
				attack();
				break;
			}
			sf::Vector2f pos = getPosition();
			sf::Vector2i ddest = vecFloor(path.at(path.length() - 1));
			sf::Vector2i tpos = vecFloor(target->getPosition());
			if (ddest != tpos) {
				if (path.generate(getPosition(), target->getPosition())) {
					pointOnPath = 0;
				} else state = WAIT;
				break;
			}
			sf::Vector2f dest = path.at(pointOnPath);
			if (vecSqrMag(pos - dest) < .5 * .5) {
				pointOnPath++;
				if (pointOnPath >= path.length()) {
					state = WAIT;
					pointOnPath = -1;
				}
			} else {
				float speed = getStatValue(Stat::MOVE_SPEED);
				sf::Vector2f go = vecNormalize(dest - pos) * speed;
				if (type->getAI() == AI_SPRINT) {
					moveRaw(go.x * timeStep.sec() * 10, go.y * timeStep.sec() * 10);
				} else if (type->getAI() == AI_HOP) {
					sf::Vector2f posDiff = target->getPosition() - getPosition();
					rotateToward(vecAngle(posDiff));
					b2Body& body = getCollisionBody();
					if (specialCooldown <= 0 && (body.GetLinearVelocity().LengthSquared() < 0.05f * 0.05f * speed)) {
						specialCooldown = 0.6f;
						go = vecRotate(go, Rand::f(-0.4, 0.4));
						float dist = Rand::f(1.0, 1.5);
						if (inRange(target)) {
							body.ApplyLinearImpulse(b2Vec2(go.x * -dist, go.y * -dist), body.GetLocalCenter());
						} else {
							body.ApplyLinearImpulse(b2Vec2(go.x * dist * 1.6, go.y * dist * 1.6), body.GetLocalCenter());
						}
						getEffects().sound().newSound(S_SLIME, getPosition())->play();
					}
				}
			}
		} break;
		default: break;
	}
}

void Monster::setPlace(Place& place) {
	Mob::setPlace(place);
	path.setGrid(place);
}

Path& Monster::getPath() {
	return path;
}
