#include "EncounterBag.h"

#include <cassert>
#include <iostream>

EncounterBag::EncounterBag(): totalWeight(0) { }

void EncounterBag::add(Encounter encounter) {
	totalWeight += encounter.getWeight();
	encounters.push_back(encounter);
}

size_t EncounterBag::numEncounters() {
	return encounters.size();
}

void EncounterBag::reshuffle() {
	bag.clear();
	fillBag(2);
}

void EncounterBag::fillBag(int copies) {
	for (size_t i = 0; i < encounters.size(); i++) {
		Encounter* encounter = &encounters[i];
		assert(encounter->getWeight() < 20); // a really high weight will be slow and take much memory
		for (int j = 0; j < encounter->getWeight(); j++) {
			for (int k = 0; k < copies; k++) {
				bag.add(encounter);
			}
		}
	}
}

Encounter& EncounterBag::grab() {
	assert(encounters.size());
	if (bag.size() < totalWeight) {
		reshuffle();
	} else if (bag.size() == totalWeight) {
		fillBag(1);
	}
	return *bag.grab();
}
