#include "Encounter.h"

#include "StringMatcher.h"
#include "Util.h"
#include "Rand.h"

Encounter::Encounter(const MonsterType& monster): minS(1), maxS(1), weight(1), monster(&monster) { }

const MonsterType& Encounter::getMonster() {
	return *monster;
}

void Encounter::setRarity(Rarity rarity) {
	weight = (short)rarity;
}

int Encounter::getWeight() {
	return weight;
}

String rStrings[] = {    "rare",          "uncommon",          "common",          "extra common"};
int rInts[] = {Encounter::RARE, Encounter::UNCOMMON, Encounter::COMMON, Encounter::EXTRA_COMMON};
StringMatcher strToRarityMatch(rStrings, rInts, 4, Encounter::NOWHERE);
Encounter::Rarity Encounter::strToRarity(String str) {
	return (Encounter::Rarity)strToRarityMatch.match(toLowerCase(str));
}

void Encounter::setSize(int min, int max) {
	assert(min > 0);
	assert(max >= min || max == -1);

	minS = min;
	if (max == -1) maxS = min;
	else maxS = max;
}

int Encounter::randomSize() {
	return Rand::i(minS, maxS + 1);
}

