#include "MonsterType.h"

#include <cassert>
#include "Rand.h"
#include "Util.h"

MonsterType::MonsterType(String name, int id): name(name), id(id) {
	for (int i = 0; i < Stat::COUNT; i++) {
		stats[i] = Stat::getDefaultValue((Stat::Type)i);
	}
	color = sf::Color::White;
	ai = AI_NONE;
	model = nullptr;
	sound = S_NONE;
}

const String& MonsterType::getName() const {
	return name;
}

float MonsterType::getStatValue(Stat::Type stat) const {
	return stats[stat];
}
void MonsterType::setStatValue(Stat::Type stat, float value) {
	stats[stat] = value;
}

const ItemType* MonsterType::getRandomDrop() const {
	if (drops.empty()) return NULL;
	return drops[Rand::i(0, drops.size())];
}

void MonsterType::addDrop(const ItemType& itemType) {
	drops.push_back(&itemType);
}

int MonsterType::getID() const {
	return id;
}

AIType MonsterType::getAI() const {
	return ai;
}

void MonsterType::setAI(AIType newAI) {
	ai = newAI;
}

sf::Color MonsterType::getColor() const {
	return color;
}

const Model& MonsterType::getModel() const {
	if (model) return *model;
	return Model::getBlank();
}

void MonsterType::setGraphic(const Model& newModel, sf::Color c) {
	model = &newModel;
	color = c;
}

Sound MonsterType::getSound() const {
	return sound;
}

void MonsterType::setSound(Sound s) {
	sound = s;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("entity/mob/monster/type", "Test MonsterType.") {
	MonsterType monsterType("Test Monster", 3);
	monsterType.setAI(AI_HOP);
	monsterType.setStatValue(Stat::ATTACK, 4);

	REQUIRE(monsterType.getName() == "Test Monster");
	REQUIRE(monsterType.getID() == 3);
	REQUIRE(monsterType.getAI() == AI_HOP);
	REQUIRE(monsterType.getRandomDrop() == nullptr);

	REQUIRE(monsterType.getStatValue(Stat::ATTACK)  == 4);
	REQUIRE(monsterType.getStatValue(Stat::EVASION) == 0);
}
