#include "Joint.h"

Joint::Joint(float x, float y, float rotation) {
	this->position = sf::Vector2f(x, y);
	this->rotation = rotation;
}

const sf::Vector2f Joint::getPosition() const {
	return position;
}

float Joint::getRotation() const {
	return rotation;
}
