#include "Projectile.h"

#include "EffectsManager.h"
#include "TextEffect.h"
#include "Mob.h"
#include "Place.h"
#include "Tile.h"

b2BodyDef* projBodyDef;
b2BodyDef& initProjDef(sf::Vector2f position) {
	projBodyDef = new b2BodyDef();
	projBodyDef->type = b2_dynamicBody;
	projBodyDef->userData = NULL;
	projBodyDef->position = toPhysVec(position);
	projBodyDef->bullet = true;
	return *projBodyDef;
}


Projectile::Projectile(Place& place, EffectsManager& effects, sf::Vector2f position)
		: Entity(PROJECTILE, initProjDef(position), effects), place(&place) {
	delete projBodyDef;

	b2CircleShape circle;
	circle.m_radius = 0.01f;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 0.01f;
	fixtureDef.userData = NULL;
	getCollisionBody().CreateFixture(&fixtureDef);

	itemType = NULL;
	damage = 0;
	deaded = false;
}

void Projectile::go(sf::Vector2f velocity) {
	getCollisionBody().SetLinearVelocity(toPhysVec(velocity));
}

void Projectile::go(Angle angle, float speed) {
	go(sf::Vector2f(cos(angle), sin(angle)) * speed);
}

void Projectile::setItem(const ItemType& item) {
	itemType = &item;
}
void Projectile::setHit(float d) {
	damage = d;
}

void Projectile::setSprite(sf::Texture& texture, int index) {
	sf::Sprite sprite(texture, sf::IntRect((index % 16) * 64, (index / 16) * 64, 64, 64));
	sf::FloatRect r = sprite.getLocalBounds();
    sprite.setOrigin(r.width / 2, r.height / 2);

    sprite.setRotation(getAngle() / TAU * 360.f + 180.f);
	spriteLayers.push_back(sprite);
}

bool Projectile::update(TimeStep timeStep) {
	return deaded;
}

Angle Projectile::getAngle() {
	return vecAngle(toSFMLVec(getCollisionBody().GetLinearVelocity()));
}

void Projectile::onEntityCollision(Entity& collidingWith) {
	Entity::onEntityCollision(collidingWith);
	if (collidingWith.getType() == Entity::MOB) {
		Mob& mob = (Mob&)collidingWith;
		mob.getDamaged(damage, getAngle() + TAU / 2, Mob::CRIT);
	}
	die();
}
void Projectile::onWallCollision() {
	Entity::onWallCollision();
	die();
}

void Projectile::die() {
	deaded = true;
	if (itemType) {
		place->addItem(vecFloor(getPosition()), Item(*itemType));
	}
}
