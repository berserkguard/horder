#include "Construction.h"

#include "MapType.h"
#include <cassert>

Construction::Construction(Blueprint& blueprint, Map& map): blueprint(&blueprint), map(&map) { }

void Construction::construct() {
	constructTiles();
	constructWalls();
}

void Construction::constructTiles() {
	// places the necessary tiles in their cells according to the blueprint
	const MapType& map = blueprint->getMap();
	for (int y = 0; y < CHUNK_SIZE; y++) {
		for (int x = 0; x < CHUNK_SIZE; x++) {
			Tile& tile = blueprint->getChunk().getTile(x, y);
			switch(blueprint->getCell(x, y)) {
				case Blueprint::WALL:
					tile.setFloor(map.getGenTile(MapType::WALL));
					tile.setPath(Tile::NO);
					break;
				case Blueprint::ROOM:
					tile.setFloor(map.getGenTile(MapType::ROOM));
					tile.setPath(Tile::YES);
					break;
				case Blueprint::PATH:
					tile.setFloor(map.getGenTile(MapType::FLOOR));
					tile.setPath(Tile::YES);
					break;
				default: assert(false); break;
			}
		}
	}
}
void Construction::constructWalls() {
	// adds the inner and outer walls to the map

	auto& innerWalls = blueprint->getInnerWalls();
	auto& outerWalls = blueprint->getOuterWalls();
	assert(innerWalls.size() == outerWalls.size());

	sf::Vector2f cPos(blueprint->getChunk().getPositionInTiles());
	b2EdgeShape innerLine, outerLine;
	for (size_t i = 0; i < innerWalls.size(); i++) {
		b2Vec2 innerEnd1 = toPhysVec(innerWalls[i].first  + cPos);
		b2Vec2 innerEnd2 = toPhysVec(innerWalls[i].second + cPos);
		b2Vec2 outerEnd1 = toPhysVec(outerWalls[i].first  + cPos);
		b2Vec2 outerEnd2 = toPhysVec(outerWalls[i].second + cPos);
		innerLine.Set(innerEnd1, innerEnd2);
		outerLine.Set(outerEnd1, outerEnd2);
		map->addWall(innerLine, outerLine, blueprint->getChunk());
    }
}
