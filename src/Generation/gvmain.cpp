#include "main.h"

#include "GenViewer.h"

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include <Box2D/Box2D.h>
#include <memory>

std::unique_ptr<b2World> phys;
b2World& Get::physWorld() {
    return *phys;
}

void unitTest(int argc, char** argv) {
	Catch::Session session;
	session.run(argc, argv);
}

int main(int argc, char** argv) {
	phys = std::unique_ptr<b2World>(new b2World(b2Vec2(0.0f, 0.0f)));
	unitTest(argc, argv);
	GenViewer view;
	return view.show();
}
