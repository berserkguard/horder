#include "Blueprint.h"

#include "Rand.h"
#include "MapType.h"
#include "Generation.h"
#include <iostream>
#include <functional>
#include <queue>

Blueprint::Blueprint(const MapType& mapType, unsigned int mapSeed, Chunk& chunk)
		: mapType(&mapType), chunk(&chunk), seed(mapSeed) {
	// initialize all the cells
	for (size_t y = 0; y < CHUNK_SIZE; y++) {
		for (size_t x = 0; x < CHUNK_SIZE; x++) {
			roomsInCells[y][x] = nullptr;
			nodesInCells[y][x] = nullptr;
		}
	}
	for (size_t y = 0; y < CHUNK_SIZE + 2; y++) {
		for (size_t x = 0; x < CHUNK_SIZE + 2; x++) {
			cells[y][x] = WALL;
		}
	}
}

int calcDist(Coord pos1, Coord pos2) {
	return std::abs(pos1.x - pos2.x) + std::abs(pos1.y - pos2.y);
}
void Blueprint::generate() {
	calcRooms();
	calcEntrances();
	Rand::setSeed(seed);

	connectEverything();
	createWalls();
	weirdRooms();
}
void Blueprint::connectEverything() {
	// connect nodes
	std::vector<Node*> failures;
	ShuffleBag<Node*> bagCopy = nodeBag;
	while (bagCopy.size() >= 2) {
		Node& node1 = *bagCopy.grab();
		Node& node2 = *bagCopy.grab();
		if (!connectHalls(node1, node2)) {
			failures.push_back(&node1);
			failures.push_back(&node2);
		}
	}

	// connect failures
	if (bagCopy.size()) failures.push_back(bagCopy.grab());
	for (size_t i = 0; i < failures.size(); i++) {
		connectHalls(*failures[i], *hallNodes[Rand::i(0, hallNodes.size())]);
	}

	// connect rooms
	for (size_t i = 0; i < rooms.size(); i++) {
		if (!Chunk::inBounds(rooms[i]->center)) continue;
		Coord exit = createRandomExit(*rooms[i]);
		if (exit == Coord(-1, -1)) exit = createRandomExit(*rooms[i]); // second attempt
		if (exit != Coord(-1, -1)) {
			Node& exitNode = newNode(exit);
			mergeMegaNodes(*exitNode.owner, *rooms[i]->owner);
			bool success = connectHalls(exitNode, *hallNodes[Rand::i(0, hallNodes.size())]);
			if (success) setCell(exit, PATH);
		}
	}

	// final merging
	for (int i = 0; i < 10; i++) {
		if (megaNodes.size() == 1) break;
		int minSize = INT_MAX;
		MegaNode* minNode = nullptr;
		for (size_t j = 0; j < megaNodes.size(); j++) {
			int size = megaNodes[j]->nodes.size() + megaNodes[j]->rooms.size();
			if (size < minSize) {
				minSize = size;
				minNode = megaNodes[j].get();
			}
		}
		mergeMegaNodeInward(*minNode);
	}
}

void Blueprint::calcRooms() {
	// gets the rectangles for all the rooms in groups
	std::vector<std::vector<sf::IntRect> > tempRooms;
	Coord chunkPos = chunk->getPosition();
	for (    int y = chunkPos.y * ROOM_COLUMNS - 1; y < (chunkPos.y + 1) * ROOM_COLUMNS + 1; y++) {
		for (int x = chunkPos.x * ROOM_COLUMNS - 1; x < (chunkPos.x + 1) * ROOM_COLUMNS + 1; x++) {
			sf::IntRect roomRect = Generation::getRoomRect(seed, Coord(x, y));
			roomRect.left -= chunk->getPositionInTiles().x;
			roomRect.top  -= chunk->getPositionInTiles().y;
			if (roomRect.width) {
				sf::IntRect bigRoomRect(roomRect.left - 1, roomRect.top - 1, roomRect.width + 2, roomRect.height + 2);
				for (size_t i = 0; i < tempRooms.size(); i++) {
					for (size_t j = 0; j < tempRooms[i].size(); j++) {
						if (tempRooms[i][j].intersects(bigRoomRect)) {
							tempRooms[i].push_back(roomRect);
							goto exitLoop0;
						}
					}
				}
				tempRooms.emplace_back();
				tempRooms[tempRooms.size() - 1].push_back(roomRect);
				exitLoop0:;
			}
		}
	}

	// creates rooms out of the rectangle groups and fills the cells with room info
	for (size_t i = 0; i < tempRooms.size(); i++) {
		rooms.emplace_back(new Room(tempRooms[i]));
		Room& room = *rooms[rooms.size() - 1];
		if (room.cells.empty()) {
			rooms.pop_back();
			continue;
		}
		if (Chunk::inBounds(room.center)) {
			megaNodes.emplace_back(new MegaNode(room));
		}
		for (size_t j = 0; j < room.cells.size(); j++) {
			Coord pos = room.cells[j];
			setCell(pos, ROOM);
			if (Chunk::inBounds(pos)) {
				roomsInCells[pos.y][pos.x] = &room;
			}
		}
	}
}

Blueprint::Room::Room(const std::vector<sf::IntRect>& rects): owner(nullptr) {
	Coord sum;
	int total = 0;
	std::set<Coord, Coordcmp> newCells;
	for (size_t i = 0; i < rects.size(); i++) {
		for (int x = 0; x < rects[i].width; x++) {
			for (int y = 0; y < rects[i].height; y++) {
				Coord cell(x + rects[i].left, y + rects[i].top);
				// the cells are put into a set so the duplicates are removed
				newCells.insert(cell);
				sum += cell;
				total++;
			}
		}
	}
	for (auto iter = newCells.begin(); iter != newCells.end(); ++iter) {
		// push the set of cells into the vector
		Coord c = *iter;
		if (c.x >= -1 && c.y >= -1 && c.x <= CHUNK_SIZE && c.y <= CHUNK_SIZE) {
			cells.push_back(c);
		}
	}
	// center is calculated as the average position of all the cells in the room
	center = Coord(sum.x / total, sum.y / total);
}
Coord Blueprint::createRandomExit(Room& room, Direction dir) {
	if (room.cells.empty()) return Coord(-1, -1);

	// chooses a random direction and position for the exit to be in
	Coord p = room.cells[Rand::i(0, room.cells.size())];
	Coord cDir;
	if (dir == NONE) {
		switch(Rand::i(0, 4)) {
			case 0: cDir = Coord(0,  1); break;
			case 1: cDir = Coord(0, -1); break;
			case 2: cDir = Coord(1,  0); break;
			case 3: cDir = Coord(-1, 0); break;
		}
	} else cDir = dirVector(dir);

	while (getCell(p) == ROOM) {
		p += cDir;
		if (!Chunk::inBounds(p)) return Coord(-1, -1);
	}
	// if the exit is out of bounds or leads into another room, the exit creation is deemed failed
	if (p.x == 0 || p.y == 0 || p.x == CHUNK_SIZE - 1 || p.y == CHUNK_SIZE - 1) return Coord(-1, -1);
	Cell leftCell  = getCell(p.x - cDir.y, p.y - cDir.x);
	Cell rightCell = getCell(p.x + cDir.y, p.y + cDir.x);
	if (leftCell == ROOM || rightCell == ROOM) return Coord(-1, -1);
	return p;
}

Blueprint::Node& Blueprint::newNode(Coord pos) {
	assert(Chunk::inBounds(pos));
	hallNodes.emplace_back(new Node(pos));
	Node& newNode = *hallNodes[hallNodes.size() - 1];
	megaNodes.emplace_back(new MegaNode(newNode));
	nodeBag.add(&newNode);
	return newNode;
}

void Blueprint::entrance(Coord pos, Coord dir) {
	assert(Chunk::inBounds(pos));
	Coord orth(dir.y, dir.x);
	bool wallBehind = getCell(pos - dir) || getCell(pos - dir + orth) || getCell(pos - dir - orth);
	if (!wallBehind && !getCell(pos) && !getCell(pos + orth) && !getCell(pos - orth)) {
		setCell(pos - dir, PATH);
		setCell(pos, PATH);
		Coord fPos = pos + dir;
		if (getCell(fPos) != ROOM) {
			setCell(fPos, PATH);
			if (!isObstructed(Coord(fPos))) {
				nodesInCells[fPos.y][fPos.x] = &newNode(fPos);
			}
		}
	}
}
void Blueprint::calcEntrances() {
	// creates all the entrances into this chunk
	std::vector<int> topEntrances = Generation::getEntrances(seed, chunk->getPosition(), NORTH);
	std::vector<int> botEntrances = Generation::getEntrances(seed, chunk->getPosition(), SOUTH);
	std::vector<int> lefEntrances = Generation::getEntrances(seed, chunk->getPosition(), WEST);
	std::vector<int> ritEntrances = Generation::getEntrances(seed, chunk->getPosition(), EAST);
	for (int i = 0; i < 4; i++) {
		entrance(Coord(topEntrances[i], 0)             , Coord(0,  1));
		entrance(Coord(botEntrances[i], CHUNK_SIZE - 1), Coord(0, -1));
		entrance(Coord(0, lefEntrances[i])             , Coord(1,  0));
		entrance(Coord(CHUNK_SIZE - 1, ritEntrances[i]), Coord(-1, 0));
	}
}

bool Blueprint::mergeCheck(Coord c, MegaNode& node) {
	// checks if the coord can be merged with the node
	if (Chunk::inBounds(c)) {
		Node* inCell = nodesInCells[c.y][c.x];
		if (inCell && inCell->owner != &node) {
			mergeMegaNodes(*inCell->owner, node);
			return true;
		}
	}
	return false;
}
bool Blueprint::orthMergeCheck(Coord c, MegaNode& node) {
	// checks all the orthogonal directions for merging
	return mergeCheck(c + Coord(1, 0) , node) ||
	       mergeCheck(c + Coord(-1, 0), node) ||
	       mergeCheck(c + Coord(0, 1) , node) ||
	       mergeCheck(c + Coord(0, -1), node);
}
bool Blueprint::connectHalls(Node& hall1, Node& hall2) {
	// connects two halls together with a path
	Path path(*this, nullptr, 0);
	bool success = path.generate(sf::Vector2f(hall1.pos), sf::Vector2f(hall2.pos));
	if (success) {
		Node* node = &newNode(vecFloor(path.at(path.length() / 2)));
		int end = path.length() - 1;
		// iterate through the path and create PATH and merge all adjacent mega coordinates
		for (int i = 1; i < end; i++) {
			Coord c = vecFloor(path.at(i));
			setCell(c, PATH);
			orthMergeCheck(c, *hall1.owner);
			nodesInCells[c.y][c.x] = node;
		}
		mergeMegaNodes(*hall1.owner, *hall2.owner);
	}
	return success;
}

void Blueprint::mergeMegaNodes(MegaNode& node1, MegaNode& node2) {
	// merges two mega nodes together, emptying the second one into the first
	if (&node1 == &node2) return;
	for (auto iter = megaNodes.rbegin(); iter != megaNodes.rend(); ++iter) {
		if (iter->get() == &node1) {
			node2.merge(node1);
			megaNodes.erase((++iter).base());
			break;
		} else if (iter->get() == &node2) {
			node1.merge(node2);
			megaNodes.erase((++iter).base());
			break;
		}
	}
}

void Blueprint::MegaNode::merge(MegaNode& other) {
	// empties other into this
	for (size_t i = 0; i < other.nodes.size(); ++i) {
		nodes.push_back(other.nodes[i]);
		other.nodes[i]->owner = this;
	}
	for (size_t i = 0; i < other.rooms.size(); ++i) {
		rooms.push_back(other.rooms[i]);
		other.rooms[i]->owner = this;
	}
}

Direction getInwardDirection(Coord pos) {
	// gets the orthogonal direction from the position that would be toward the center of the chunk
	bool bNW = (pos.x - pos.y) > 0;
	bool bNE = (CHUNK_SIZE - pos.x - pos.y) > 0;
	Direction dir;
	if (bNW) {
		if (bNE) dir = SOUTH;
		else dir = WEST;
	} else {
		if (bNE) dir = EAST;
		else dir = NORTH;
	}
	return dir;
}
void Blueprint::mergeMegaNodeInward(MegaNode& node) {
	// merges this mega node by creating a path from it toward the center
	if (node.rooms.empty()) {
		Node& randomHallNode = *node.nodes[Rand::i(0, node.nodes.size())];
		straightHall(randomHallNode.pos, getInwardDirection(randomHallNode.pos), node);
	} else {
		// merging from a room is preferred
		Room& room = *node.rooms[Rand::i(0, node.rooms.size())];
		Direction dir = getInwardDirection(room.center);
		Coord pos = createRandomExit(room, dir);
		straightHall(pos, dir, node);
	}
}
void Blueprint::straightHall(Coord pos, Direction dir, MegaNode& node) {
	// creates a straight normal hall in the given direction until it collides with something
	Coord dirPos = dirVector(dir);
	Coord cur = pos;
	while (Chunk::inBounds(cur)) {
		if (getCell(cur) == WALL) {
			setCell(cur, PATH);
		}
		if (orthMergeCheck(cur, node)) return;
		cur += dirPos;
	}
}

bool Blueprint::hasRoom(int x, int y) {
	return roomsInCells[y][x];
}
const Blueprint::Room& Blueprint::getRoom(int x, int y) {
	return *roomsInCells[y][x];
}
bool Blueprint::hasPath(int x, int y) {
	return nodesInCells[y][x];
}
Coord Blueprint::getPath(int x, int y) {
	return nodesInCells[y][x]->pos;
}

bool Blueprint::isObstructed(sf::Vector2i pos, void* mob) {
	for (int x = pos.x - 1; x <= pos.x + 1; x++) {
		for (int y = pos.y - 1; y <= pos.y + 1; y++) {
			if (!Chunk::inBounds(Coord(x, y))) return true;
			if (getCell(x, y) == ROOM) return true;
		}
	}
	return false;
}

size_t Blueprint::getNumRooms() {
	return rooms.size();
}
const Blueprint::Room& Blueprint::getRoom(size_t index) {
	return *rooms[index];
}

Blueprint::Cell Blueprint::getCell(int x, int y) {
	assert(x >= -1 && x <= CHUNK_SIZE && y >= -1 && y <= CHUNK_SIZE);
	return cells[y + 1][x + 1];
}
Blueprint::Cell Blueprint::getCell(Coord pos) {
	return getCell(pos.x, pos.y);
}
void Blueprint::setCell(int x, int y, Cell cell) {
	assert(x >= -1 && x <= CHUNK_SIZE && y >= -1 && y <= CHUNK_SIZE);
	cells[y + 1][x + 1] = cell;
}
void Blueprint::setCell(Coord pos, Cell cell) {
	setCell(pos.x, pos.y, cell);
}

const MapType& Blueprint::getMap() {
	return *mapType;
}

Chunk& Blueprint::getChunk() {
	return *chunk;
}

void Blueprint::addInnerWall(sf::Vector2f end1, sf::Vector2f end2, std::map<Coord, size_t, Coordcmp>& endpointMap) {
	innerWalls.emplace_back(end1, end2);
	endpointMap[Coord(end1)] = innerWalls.size() - 1;
	endpointMap[Coord(end2)] = innerWalls.size() - 1;
}
void Blueprint::createWallsInDirection(Coord aDir, std::map<Coord, size_t, Coordcmp>& endpointMap) {
	// creates all the walls in the given orthogonal direction
	Coord bDir(aDir.y, aDir.x);
	int begin = -2; // begin marks where the current wall starts
	for (    Coord a; a.x <  CHUNK_SIZE && a.y <  CHUNK_SIZE; a += aDir) {
		for (Coord b; b.x <= CHUNK_SIZE && b.y <= CHUNK_SIZE; b += bDir) {
			Coord pos = a + b;
			bool solid1 = (getCell(pos)        == WALL);
			bool solid2 = (getCell(pos + aDir) == WALL);
			if (begin > -2 && solid1 == solid2) {
				// if the solids are the same, a wall should end and be created
				sf::Vector2f end1(a + aDir + bDir * begin);
				sf::Vector2f end2(pos + aDir);
				addInnerWall(end1, end2, endpointMap);
				begin = -2;
			} else if (solid1 != solid2 && begin == -2) {
				// if the solids are different, this is where a wall begins
				begin = b.x + b.y;
			}
		}
		if (begin > -2) {
			// if it reaches the end and a wall hasen't finished, this finishes it
			sf::Vector2f end1(a + aDir + bDir * begin);
			sf::Vector2f end2(a + aDir + bDir * CHUNK_SIZE);
			addInnerWall(end1, end2, endpointMap);
			begin = -2;
		}
	}
}

Blueprint::Room* Blueprint::getRoomPoint(Coord pos) {
	// returns the room that a corner position belongs to
	// if there is no room or it is not a corner, null is returned

	if (!Chunk::inBounds(pos) || !Chunk::inBounds(pos - Coord(1, 1))) {
		return nullptr;
	}
	Room* room = nullptr;
	for (int x = pos.x - 1; x <= pos.x; x++) {
		for (int y = pos.y - 1; y <= pos.y; y++) {
			if (roomsInCells[y][x]) {
				room = roomsInCells[y][x];
			} else if (getCell(x, y) == PATH) {
				return nullptr;
			}
		}
	}
	return room;
}
Coord getOther(Blueprint::Wall wall, Coord one) {
	if (sf::Vector2f(one) == wall.first) {
		return Coord(wall.second);
	}
	return Coord(wall.first);
}

bool Blueprint::chainWall(std::pair<Coord, size_t>& val, Room* room, EPTWM& endpMap, bool front) {
	// creates a wall chain starting at the given corner and wall index

	// get next point
	Coord pos    = val.first;
	size_t lineI = val.second;
	Wall line = innerWalls[lineI];
	Coord other = getOther(line, pos);

	// add line to wall chain
	if (front) {
		room->wallChain.insert(room->wallChain.begin(), lineI);
	} else {
		room->wallChain.push_back(lineI);
	}

	// if its not the same or or there is no more room, this chain is finished
	if (getRoomPoint(other) != room) return true;

	// go to the next corner/wall index
	auto valIter = endpMap.find(other);
	if (valIter == endpMap.end()) return true;
	val = *valIter;
	endpMap.erase(valIter);
	pos = val.first;

	return false;
}
void Blueprint::createWalls() {
	EPTWM endpMapV; // EPTWM = endpoint to wall map
	createWallsInDirection(Coord(1, 0), endpMapV);
	EPTWM endpMapH;
	createWallsInDirection(Coord(0, 1), endpMapH);

	// create wall chains
	while (!endpMapV.empty()) {
		// forwards (starting with vertical)
		std::pair<Coord, size_t> val = *endpMapV.begin();
		endpMapV.erase(endpMapV.begin());
		Room* room = getRoomPoint(val.first);
		if (!room) continue;
		while (true) {
			if (chainWall(val, room, endpMapH, false)) break;
			if (chainWall(val, room, endpMapV, false)) break;
		}

		// backwards (starting with horizontal)
		val = *endpMapH.begin();
		endpMapH.erase(endpMapH.begin());
		if (getRoomPoint(val.first) != room) continue;
		while (true) {
			if (chainWall(val, room, endpMapV, true)) break;
			if (chainWall(val, room, endpMapH, true)) break;
		}
	}

	// create the outer walls by shifting the inner walls in the direction of wall
	outerWalls = innerWalls;
	for (size_t i = 0; i < outerWalls.size(); i++) {
		outerWalls[i].first  = shiftWallCorner(outerWalls[i].first);
		outerWalls[i].second = shiftWallCorner(outerWalls[i].second);
	}
}
bool Blueprint::pathCell(Coord pos) {
	return getCell(pos) == WALL;
}
bool Blueprint::shouldShift(Coord top1, Coord top2, Coord bot1, Coord bot2) {
	return pathCell(top1) + pathCell(top2) > pathCell(bot1) + pathCell(bot2);
}
sf::Vector2f Blueprint::shiftWallCorner(sf::Vector2f pos) {
	// shifts a wall corner by finding which directions it should shift then changing the x and y slightly
	Coord se = vecFloor(pos);
	if (se.x <= -1 || se.y <= -1 || se.x > CHUNK_SIZE || se.y > CHUNK_SIZE) return pos;
	Coord nw = se + Coord(-1, -1);
	Coord ne = se + Coord( 0, -1);
	Coord sw = se + Coord(-1,  0);
	if (shouldShift(nw, ne, sw, se)) {
		pos.y -= 0.3f;
	} else if (shouldShift(sw, se, nw, ne)) {
		pos.y += 0.3f;
	}
	if (shouldShift(nw, sw, ne, se)) {
		pos.x -= 0.3f;
	} else if (shouldShift(ne, se, nw, sw)) {
		pos.x += 0.3f;
	}
	return pos;
}

const std::vector<Blueprint::Wall>& Blueprint::getInnerWalls() {
	return innerWalls;
}
const std::vector<Blueprint::Wall>& Blueprint::getOuterWalls() {
	return outerWalls;
}

void Blueprint::weirdRooms() {
	// randomly decide to fill the corners of some rooms
	for (size_t i = 0; i < rooms.size(); i++) {
		Room& room = *rooms[i];
		if (room.wallChain.size() <= 1) continue;
		int r = Rand::i(0, 10);
		if (r == 1) {
			fillCorner(room, Rand::i(0, room.wallChain.size() - 1), 3);
		} else if (r == 2) {
			fillCorners(room);
		}
	}
}
void Blueprint::fillCorner(Room& room, int r, float max) {
	// pushes in the given corner of a room

	int wall1I = room.wallChain[r];
	int wall2I = room.wallChain[r + 1];
	Wall wall1 = innerWalls[wall1I];
	Wall wall2 = innerWalls[wall2I];
	bool success = matchWalls(wall1, wall2);
	if (success) {
		Wall outerWall1 = outerWalls[wall1I];
		Wall outerWall2 = outerWalls[wall2I];
		matchWalls(outerWall1, outerWall2);
		sf::Vector2f diff = outerWall1.first - wall1.first;

		float len1 = vecMagnitude(wall1.first - wall1.second);
		float len2 = vecMagnitude(wall2.first - wall2.second);
		float length = Rand::f(0.5, std::min(2.f, std::min(len1, len2)));
		if (wall1.first == wall1.second || wall2.first == wall2.second) return;
		wall1.first += vecNormalize(wall1.second - wall1.first) * length;
		wall2.first += vecNormalize(wall2.second - wall2.first) * length;
		outerWall1.first = wall1.first + diff;
		outerWall2.first = wall2.first + diff;
		Wall newWall = std::make_pair(wall1.first, wall2.first);

		innerWalls.push_back(newWall);
		Wall outerNewWall = std::make_pair(outerWall1.first, outerWall2.first);
		outerWalls.push_back(outerNewWall);
		room.wallChain.push_back(outerWalls.size() - 1);

		innerWalls[wall1I] = wall1;
		innerWalls[wall2I] = wall2;
		outerWalls[wall1I] = outerWall1;
		outerWalls[wall2I] = outerWall2;
	}
}
void Blueprint::fillCorners(Room& room) {
	// fills all the corners of a room
	size_t size = room.wallChain.size() - 1;
	for (size_t i = 0; i < size; i++) {
		fillCorner(room, i, 2);
	}
}
bool Blueprint::matchWalls(Wall& wall1, Wall& wall2) {
	// checks if two walls share a point
	// if they do, it make sure the shared point is first in each pair

	bool success = false;
	if (wall1.first == wall2.first) {
		return true;
	} else if (wall1.first == wall2.second) {
		std::swap(wall2.first, wall2.second);
		success = true;
	} else if (wall1.second == wall2.first) {
		std::swap(wall1.first, wall1.second);
		success = true;
	} else if (wall1.second == wall2.second) {
		std::swap(wall1.first, wall1.second);
		std::swap(wall2.first, wall2.second);
		success = true;
	}
	if (success) {
		int rooms = 0;
		for (int x = wall1.first.x - 1; x <= wall1.first.x; x++) {
			for (int y = wall1.first.y - 1; y <= wall1.first.y; y++) {
				if (roomsInCells[y][x]) rooms++;
			}
		}
		// dont match the walls if the shared point is not a corner
		return rooms == 1;
	}
	return false;
}

#include "Map.h"
#include "EffectsManager.h"

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("gen/blueprint/basic", "Test blueprint generation.") {

	EffectsManager effects;
	MapType mapType("Test Map", MapType::BOUNDLESS & MapType::PERSISTENT);
	Map map(mapType, effects);
	Blueprint blueprint(mapType, 15, map.createChunk(Coord(3, 2)));
	blueprint.generate();

	REQUIRE(blueprint.getNumRooms() > 1);
	REQUIRE(blueprint.getCell(0, 0) == Blueprint::WALL);

	SECTION("Floodfill to test connectivity.") {
		bool floodMap[CHUNK_SIZE][CHUNK_SIZE];
		Coord start(-1, -1);
		for (int y = 0; y < CHUNK_SIZE; y++) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				if (blueprint.getCell(x, y) == Blueprint::WALL) {
					floodMap[y][x] = true;
				} else {
					if (start == Coord(-1, -1)) {
						start = Coord(x, y);
						floodMap[y][x] = true;
					} else floodMap[y][x] = false;
				}
			}
		}
		REQUIRE(start != Coord(-1, -1));
		std::queue<Coord> cells;
		cells.push(start);
		while (!cells.empty()) {
			Coord next = cells.front();
			cells.pop();
			for (int r = 0; r < 360; r += 90) {
				Coord pos = next + dirVector(turn(NORTH, r));
				if (Chunk::inBounds(pos) && floodMap[pos.y][pos.x] == false) {
					floodMap[pos.y][pos.x] = true;
					cells.push(pos);
				}
			}
		}
		for (int y = 0; y < CHUNK_SIZE; y++) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				if (!floodMap[y][x]) {
					if (blueprint.hasRoom(x, y)) {
						REQUIRE(!Chunk::inBounds(blueprint.getRoom(x, y).center));
					} else {
						REQUIRE(false);
					}
				}
			}
		}
	}
}

