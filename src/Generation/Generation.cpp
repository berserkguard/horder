#include "Generation.h"

#include "Rand.h"
#include "Chunk.h"

const int   ROOM_SQR_SIZE = CHUNK_SIZE / ROOM_COLUMNS;
const float ROOM_DENSITY = 0.3f;

sf::IntRect Generation::getRoomRect(int seed, Coord pos) {
	if (Rand::consistent(0, 1000, seed, pos.x, pos.y) / 1000.f < ROOM_DENSITY) {
		int roomX = pos.x * ROOM_SQR_SIZE + Rand::consistent(-ROOM_SQR_SIZE / 2, ROOM_SQR_SIZE / 2, seed + 1, pos.x, pos.y);
		int roomY = pos.y * ROOM_SQR_SIZE + Rand::consistent(-ROOM_SQR_SIZE / 2, ROOM_SQR_SIZE / 2, seed + 2, pos.x, pos.y);
		int roomW =                         Rand::consistent( ROOM_SQR_SIZE / 2, ROOM_SQR_SIZE    , seed + 3, pos.x, pos.y);
		int roomH =                         Rand::consistent( ROOM_SQR_SIZE / 2, ROOM_SQR_SIZE    , seed + 4, pos.x, pos.y);
		return sf::IntRect(roomX - roomW / 2, roomY - roomH / 2, roomW, roomH);
	} else return sf::IntRect(0, 0, 0, 0);
}

std::vector<int> Generation::getEntrances(int seed, Coord chunkPos, Direction dir) {
	if (dir == SOUTH) {
		chunkPos.y++;
		dir = NORTH;
	} else if (dir == EAST) {
		chunkPos.x++;
		dir = WEST;
	}
	std::vector<int> entrances;
	for (int i = 0; i < 4; i++) {
		entrances.push_back(Rand::consistent(1, CHUNK_SIZE - 2, chunkPos.x, chunkPos.y, seed + i + dir * 4));
	}
	return entrances;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("gen/rooms", "Test gen rooms.") {
	sf::IntRect rect = Generation::getRoomRect(1, Coord(7, 3));
	REQUIRE(rect == sf::IntRect(52, 25, 4, 5));
}
TEST_CASE("gen/entrances", "Test gen entrances.") {
	std::vector<int> entrances = Generation::getEntrances(1, Coord(-1, -1), NORTH);
	REQUIRE(entrances.size() == 4);
	REQUIRE(entrances[0] == 24);
	REQUIRE(entrances[1] == 17);
	REQUIRE(entrances[2] == 1);
	REQUIRE(entrances[3] == 12);
	std::vector<int> entrances2 = Generation::getEntrances(1, Coord(-1, -2), SOUTH);
	for (int i = 0; i < 4; i++) {
		REQUIRE(entrances[i] == entrances2[i]);
	}
}
