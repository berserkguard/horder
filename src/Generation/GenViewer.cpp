#include "GenViewer.h"

#include "ResourceManager.h"
#include <iostream>

#define SEED 14

GenViewer::GenViewer(): mapType("Test Map", MapType::BOUNDLESS & MapType::PERSISTENT) { }

int GenViewer::show() {
	if (init()) {
		while(!execute());
    } else {
    	return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

bool GenViewer::init() {
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	window.create(sf::VideoMode(800, 800), "Gen Viewer!", sf::Style::Default, settings);
	window.setView(sf::View(sf::FloatRect(-4, -4, CHUNK_SIZE + 8, CHUNK_SIZE + 8)));

	prevTime = mainClock.getElapsedTime();

	map = std::unique_ptr<Map>(new Map(mapType, effects));
	chunk = &map->createChunk(Coord(0, 0));
	blueprint = std::unique_ptr<Blueprint>(new Blueprint(mapType, SEED, *chunk));
	blueprint->generate();

	simpleFont = &ResourceManager::getInstance().getFont("resources/fonts/OptimusPrinceps.ttf");

	return true;
}

void GenViewer::refreshBlueprint(Coord pos) {
	// Generates a new blueprint for the given position.
	chunk = &map->createChunk(pos);
	blueprint.reset(new Blueprint(mapType, SEED, *chunk));
	blueprint->generate();
}
bool GenViewer::events() {
	sf::Event event;
	while(window.pollEvent(event)) {

		switch(event.type) {
			case sf::Event::Closed: return true;
			case sf::Event::KeyPressed: {
				if (event.key.code == sf::Keyboard::Q && event.key.control) {
					return true; // quit on ctrl+q
				} else {
					switch(event.key.code) {
						case sf::Keyboard::Space: refreshBlueprint(Coord(Rand::i(-100, 100), Rand::i(-100, 100))); break;
						case sf::Keyboard::Down:  refreshBlueprint(chunk->getPosition() + Coord(0,  1)); break;
						case sf::Keyboard::Up:    refreshBlueprint(chunk->getPosition() + Coord(0, -1)); break;
						case sf::Keyboard::Left:  refreshBlueprint(chunk->getPosition() + Coord(-1, 0)); break;
						case sf::Keyboard::Right: refreshBlueprint(chunk->getPosition() + Coord(1,  0)); break;
						default: break;
					}
				}
			} break;
			case sf::Event::KeyReleased: {

			} break;
			case sf::Event::MouseButtonPressed: {

			} break;
			default: break;
		}
	}

    return false;
}

void GenViewer::render() {
    window.clear();

	// draw box
	sf::RectangleShape rect(sf::Vector2f(CHUNK_SIZE, CHUNK_SIZE));
	rect.setFillColor(sf::Color::Transparent);
	rect.setOutlineColor(sf::Color::Cyan);
	rect.setOutlineThickness(0.1);
	//window.draw(rect);

	rect.setOutlineThickness(0);
	rect.setSize(sf::Vector2f(1, 1));

	// draw all the cells
	for (int x = -1; x <= CHUNK_SIZE; x++) {
		for (int y = -1; y <= CHUNK_SIZE; y++) {
			if (blueprint->getCell(x, y)) {
				rect.setPosition(x, y);
				if (!Chunk::inBounds(Coord(x, y))) {
					rect.setFillColor(sf::Color(40, 40, 40));
				} else if (blueprint->hasRoom(x, y)) {
					rect.setFillColor(sf::Color(80, 80, 80));
				} else {
					rect.setFillColor(sf::Color(200, 200, 200));
				}
				window.draw(rect);
			}
		}
	}

	auto& innerWalls = blueprint->getInnerWalls();
	renderWalls(innerWalls, sf::Color::Red);
	renderWalls(blueprint->getOuterWalls(), sf::Color::Yellow);

	// draw all the room text and room walls
	sf::VertexArray verts(sf::Lines);
	for (size_t i = 0; i < blueprint->getNumRooms(); i++) {
		const Blueprint::Room& room = blueprint->getRoom(i);
		sf::Text roomText("Room", *simpleFont);
		roomText.setScale(0.03f, 0.03f);
		roomText.setColor(sf::Color::Green);
		roomText.setPosition(room.center.x, room.center.y);
		window.draw(roomText);

		for (size_t j = 0; j < room.wallChain.size(); j++) {
			Blueprint::Wall p = innerWalls[room.wallChain[j]];
			verts.append(sf::Vertex(p.first, sf::Color::Cyan));
			verts.append(sf::Vertex(p.second, sf::Color::Cyan));
		}
	}
	window.draw(verts);

    window.display();
}

void GenViewer::renderWalls(const std::vector<Blueprint::Wall>& walls, sf::Color color) {
	sf::VertexArray verts(sf::Lines, walls.size() * 2);
	for (size_t i = 0; i < walls.size(); i++) {
		verts.append(sf::Vertex(walls[i].first, color));
		verts.append(sf::Vertex(walls[i].second, color));
	}
	window.draw(verts);
}

void GenViewer::logic(TimeStep timeStep) {

}

bool GenViewer::execute() {
	sf::Time newTime = mainClock.getElapsedTime();
	sf::Time diff = newTime - prevTime;
	prevTime = newTime;

	static PeriodManager periodManager;
	TimeStep timeStep(diff, periodManager);

	if (events()) return true;
	logic(timeStep);
	render();

	return false;
}

