#include "TextEffect.h"

#include "ResourceManager.h"

TextEffect::TextEffect(String str, float duration, sf::Vector2f pos): Effect(TEXT, duration, pos, Z_ABOVE) {
	text.setString(str);
	text.setFont(ResourceManager::getInstance().getFont("resources/fonts/OptimusPrinceps.ttf"));
	text.setStyle(sf::Text::Bold);
}

void TextEffect::logic(TimeStep timeStep) {
	getTransform().translate(0, -timeStep.sec() * 40);
	float halfspan = getLifespan() / 2;
	if (getAge() > halfspan) {
		sf::Color color = text.getColor();
		color.a = (getLifespan() - getAge()) / halfspan * 255;
		text.setColor(color);
	}
}

sf::Text& TextEffect::getText() {
	return text;
}

void TextEffect::setColor(sf::Color c) {
	text.setColor(c);
}

void TextEffect::setSize(int s) {
	text.setCharacterSize(s);
}
