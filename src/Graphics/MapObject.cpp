#include "MapObject.h"

MapObject::MapObject(sf::Vector2i gPos, sf::Vector2i gSize, sf::Vector2i texPos): gPosition(gPos), gSize(gSize) {
	rotation = 0.f;
	gTexPositions.push_back(texPos);
	frame = 0;
}

Angle MapObject::getRotation() {
	return rotation;
}
void MapObject::setRotation(Angle angle) {
	rotation = angle;
}

sf::Vector2i MapObject::getGPosition() {
	return gPosition;
}
void MapObject::setGPosition(sf::Vector2i position) {
	gPosition = position;
}
sf::Vector2i MapObject::getGSize() {
	return gSize;
}

sf::Vector2i MapObject::getTexPosition() {
	return gTexPositions[frame];
}
void MapObject::addFrame(sf::Vector2i texPos) {
	gTexPositions.push_back(texPos);
}

int MapObject::getFrame() {
	return frame;
}
void MapObject::setFrame(int f) {
	assert(f > 0 && f < (int)gTexPositions.size());
	frame = f;
}
