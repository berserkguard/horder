#include "FadeEffect.h"

FadeEffect::FadeEffect(float fadeDuration, float waitDuration, sf::Vector2f pos)
		: Effect(FADE, fadeDuration * 2 + waitDuration, pos, Z_ABOVE_UI), fadeDuration(fadeDuration), waitDuration(waitDuration) {
	cbCounter = 0;
}

void FadeEffect::logic(TimeStep timeStep) {
	int dR = to.r - from.r;
	int dG = to.g - from.g;
	int dB = to.b - from.b;
	int dA = to.a - from.a;

	float ratio = 0.0f;
	if (getAge() < fadeDuration) {
		// Fade in
		ratio = getAge() / fadeDuration;
	} else if (getAge() < fadeDuration + waitDuration) {
		// Wait
		ratio = 1.0f;
	} else {
		// Fade out
		ratio = 1.0f - (getAge() - fadeDuration - waitDuration) / fadeDuration;
	}
	if (callback && cbCounter == 0 && getAge() > getLifespan() / 2.0f) {
		callback(FadeEvent::FADE_MIDDLE);
		cbCounter++;
	}
	if (callback && cbCounter == 1 && getAge() >= getLifespan()) {
		callback(FadeEvent::FADE_OVER);
		cbCounter++;
	}
	cur.r = from.r + ratio * dR;
	cur.g = from.g + ratio * dG;
	cur.b = from.b + ratio * dB;
	cur.a = from.a + ratio * dA;
}

void FadeEffect::setColor(sf::Color fromColor, sf::Color toColor) {
	from = fromColor;
	to = toColor;
}

sf::Color FadeEffect::getColor() {
	return cur;
}

void FadeEffect::setCallback(std::function<void(FadeEvent)> cb) {
	callback = cb;
}
