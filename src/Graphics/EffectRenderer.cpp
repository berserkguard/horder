#include "EffectRenderer.h"

#include <iostream>
#include "TextEffect.h"
#include "PoofEffect.h"
#include "FadeEffect.h"
#include "ResourceManager.h"

EffectRenderer::EffectRenderer() { }

void EffectRenderer::clear() {
	for (int i = 0; i < Effect::Z_COUNT; i++) {
		effects[i].clear();
	}
}

void EffectRenderer::update(TimeStep timeStep) {
	for (int i = 0; i < Effect::Z_COUNT; i++) {
		for (auto iter = effects[i].begin(); iter != effects[i].end(); ) {
			Effect* effect = iter->get();
			if (effect->update(timeStep)) {
				effects[i].erase(iter++);
			} else ++iter;
		}
	}
}

void EffectRenderer::renderLayer(sf::RenderWindow& window, int zLayer) {
	for (auto iter = effects[zLayer].begin(); iter != effects[zLayer].end(); ++iter) {
		Effect* effect = iter->get();
		renderEffect(window, *effect);
	}
}

Effect* EffectRenderer::addEffect(Effect& effect) {
	Effect* newEffect;
	switch (effect.getType()) {
		case Effect::TEXT: newEffect = new TextEffect((TextEffect&)effect); break;
		default: return NULL;
	}
	effects[newEffect->getZ()].emplace(newEffect);
	return newEffect;
}

TextEffect& EffectRenderer::createTextEffect(String text, float duration, sf::Vector2f pos) {
	TextEffect* newEffect = new TextEffect(text, duration, pos);
	effects[newEffect->getZ()].emplace(newEffect);
	return *newEffect;
}
PoofEffect& EffectRenderer::createPoofEffect(float size, float duration, sf::Vector2f pos) {
	PoofEffect* newEffect = new PoofEffect(size, duration, pos);
	effects[newEffect->getZ()].emplace(newEffect);
	return *newEffect;
}
FadeEffect& EffectRenderer::createFadeEffect(float fadeDuration, float waitDuration, sf::Vector2f pos) {
	FadeEffect* newEffect = new FadeEffect(fadeDuration, waitDuration, pos);
	effects[newEffect->getZ()].emplace(newEffect);
	return *newEffect;
}

void EffectRenderer::renderTextShadow(sf::RenderWindow& window, sf::Text& text, sf::RenderStates& rs) {
	sf::Color original = text.getColor();
	text.setColor(sf::Color::Black);

	for (int i = -2; i <= 2; i += 2) {
		for (int j = -2; j <= 2; j += 2) {
			if (j == 0 && i == 0) continue;

			text.setOrigin(i, j);
			window.draw(text, rs);
		}
	}
	text.setColor(original);
	text.setOrigin(0, 0);
}

void EffectRenderer::renderEffect(sf::RenderWindow& window, Effect& effect) {
	sf::RenderStates renderStates(effect.getTransform());

	Effect::Type type = effect.getType();
	switch(type) {
		case Effect::TEXT: {
			TextEffect& textEffect = (TextEffect&)effect;
			sf::Text& text = textEffect.getText();
			text.setPosition(-text.getLocalBounds().width / 2, 0);
			renderTextShadow(window, text, renderStates);
			window.draw(text, renderStates);
		} break;
		case Effect::POOF: {
			PoofEffect& poofEffect = (PoofEffect&)effect;
			sf::CircleShape poof(poofEffect.getCurrentSize() * TILE_SIZE / 2);
			poof.setFillColor(poofEffect.getColor());
			poof.setPosition(sf::Vector2f(-poof.getRadius(), -poof.getRadius()));
			window.draw(poof, renderStates);
		} break;
		case Effect::FADE: {
			FadeEffect& fadeEffect = (FadeEffect&)effect;
			sf::RectangleShape rectangle;
			rectangle.setSize(sf::Vector2f(window.getSize().x, window.getSize().y));
			rectangle.setFillColor(fadeEffect.getColor());
			rectangle.setPosition(0, 0);
			window.draw(rectangle, renderStates);
		} break;
		default: break;
	}
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("anim/effects/rendering", "Test EffectRenderer.") {
	EffectRenderer effectRenderer;
	effectRenderer.createPoofEffect(1.f, 1.f, sf::Vector2f(0, 0));
	// this is a hard class to test hmuphpmppmh
}
