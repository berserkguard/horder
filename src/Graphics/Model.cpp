#include "Model.h"

#include "Util.h"
#include "ResourceManager.h"

Model::Model(String name): name(name) { }

void Model::loadYAML(YAML::Node node) {
	float scale = readYAMLNum(node, "Scale", 1.f);

	// Load weapon sprite
	if (node["WeaponSprite"]) {
		YAML::Node wsNode = node["WeaponSprite"];
		spriteFiles[(int)SL_WEAPONS] = readYAMLStr(wsNode, "File", "");
	}

	// Load arm sprite
	if (node["ArmSprite"]) {
		YAML::Node asNode = node["ArmSprite"];
		spriteFiles[(int)SL_ARMS_RIGHT] = readYAMLStr(asNode, "File", "");
		if (spriteFiles[(int)SL_ARMS_RIGHT].length() > 0) {

			for (auto jter = node["ArmSprite"]["Joints"].begin(); jter != node["ArmSprite"]["Joints"].end(); ++jter) {
				YAML::Node jointInfo = *jter;
				Joint j((jointInfo[0].as<float>() - 64) * scale, (jointInfo[1].as<float>() - 64) * scale, jointInfo[2].as<float>());
				rightHandJoints.push_back(j);
			}

			// Load arm animations
			for (auto jter = node["ArmSprite"]["Anims"].begin(); jter != node["ArmSprite"]["Anims"].end(); ++jter) {
				std::string name = jter->first.as<std::string>();
				int numFrames = jter->second[1].as<int>() - jter->second[0].as<int>() + 1;
				anims.push_back(Animation(name, numFrames, 1.0f));
			}
		}
	}

	// Load arm sprite (left)
	if (node["LeftArmSprite"]) {
		YAML::Node asNode = node["LeftArmSprite"];
		spriteFiles[(int)SL_ARMS_LEFT] = readYAMLStr(asNode, "File", "");
		if (spriteFiles[(int)SL_ARMS_LEFT].length() > 0) {
			for (auto jter = node["LeftArmSprite"]["Joints"].begin(); jter != node["LeftArmSprite"]["Joints"].end(); ++jter) {
				YAML::Node jointInfo = *jter;
				Joint j((jointInfo[0].as<float>() - 64) * scale, (jointInfo[1].as<float>() - 64) * scale, jointInfo[2].as<float>());
				leftHandJoints.push_back(j);
			}

			for (auto jter = node["LeftArmSprite"]["Anims"].begin(); jter != node["LeftArmSprite"]["Anims"].end(); ++jter) {
				std::string name = jter->first.as<std::string>();
				int numFrames = jter->second[1].as<int>() - jter->second[0].as<int>() + 1;
				anims.push_back(Animation(name, numFrames, 1.0f));
			}
		}
	}

	// Load shield sprite
	if (node["ShieldSprite"]) {
		YAML::Node wsNode = node["ShieldSprite"];
		spriteFiles[(int)SL_SHIELDS] = readYAMLStr(wsNode, "File", "");
	}

	// Load hat sprite
	if (node["HatSprite"]) {
		YAML::Node wsNode = node["HatSprite"];
		spriteFiles[(int)SL_HAT] = readYAMLStr(wsNode, "File", "");
	}

	// Load head sprite
	spriteFiles[(int)SL_HEAD] = node["HeadSprite"]["File"].as<String>();
}

sf::Sprite& Model::createSprite(SpriteLayer type, std::vector<sf::Sprite>& spriteLayers, int* spriteIndices) const {
	String fileName = spriteFiles[(int)type];

	sf::Texture* texture = &ResourceManager::getInstance().getTexture("resources/" + fileName);
	texture->setSmooth(true);

	sf::Sprite sprite;
	sprite.setTexture(*texture);
	sprite.setOrigin(SPRITE_SIZE / 2, SPRITE_SIZE / 2);
	spriteIndices[(int)type] = spriteLayers.size();

	if (type == SL_WEAPONS) {
		// Set the origin to be where the hand is
		sprite.setOrigin(SPRITE_WEAPON_WIDTH / 2, 0.75f * SPRITE_WEAPON_HEIGHT);
		sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(SPRITE_WEAPON_WIDTH, SPRITE_WEAPON_HEIGHT)));
	} else if (type == SL_SHIELDS) {
		// Set the origin to be where the hand is
		sprite.setOrigin(SPRITE_WEAPON_WIDTH / 2, SPRITE_WEAPON_HEIGHT / 2);
		sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(SPRITE_WEAPON_WIDTH, SPRITE_WEAPON_HEIGHT)));
	} else if (type == SL_ARMS_RIGHT) {
		sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(SPRITE_SIZE, SPRITE_SIZE)));
	} else if (type == SL_ARMS_LEFT) {
		sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(SPRITE_SIZE, SPRITE_SIZE)));
	} else if (type == SL_HAT) {
		sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(SPRITE_WEAPON_WIDTH, SPRITE_WEAPON_HEIGHT)));
	}

	spriteLayers.push_back(sprite);
	return spriteLayers.at(spriteLayers.size() - 1);
}

bool Model::hasSprite(SpriteLayer layer) const {
	if (layer < 0 || layer >= SL_END) return false;
	return spriteFiles[layer].length();
}

void Model::fillAnimationContainer(AnimationContainer& container) const {
	for (auto anim : anims) {
		container.newAnimation(anim);
	}
}

const Joint& Model::getJoint(Hand hand, int frame) const {
	if (hand == RIGHT_HAND) {
		return rightHandJoints[frame];
	} else {
		return leftHandJoints[frame];
	}
}

String Model::getName() const {
	return name;
}

const Model& Model::getBlank() {
	static Model model("blank");
	return model;
}
