#include "Effect.h"

Effect::Effect(Type type, float duration, sf::Vector2f position, int z): type(type), z(z), curr(0), end(duration) {
	transform.translate(position * (float)TILE_SIZE);
	paused = false;
}

bool Effect::update(TimeStep timeStep) {
	if (paused) return false;
	curr += timeStep.sec();
	logic(timeStep);
	return curr >= end;
}

void Effect::pause() {
	paused = true;
}

void Effect::resume() {
	paused = false;
}

float Effect::getAge() {
	return curr;
}
float Effect::getLifespan() {
	return end;
}
float Effect::getTimeLeft() {
	return end - curr;
}

int Effect::getZ() {
	return z;
}

sf::Transform& Effect::getTransform() {
	return transform;
}

Effect::Type Effect::getType() {
	return type;
}
