#include "AnimData.h"

#include <iostream>

AnimData::AnimData() { }

void AnimData::update(TimeStep timeStep) {
	for (auto iter = animations.begin(); iter != animations.end(); ) {
		Animation& anim = iter->second;
		anim.curr += timeStep.sec();
		if (anim.curr >= anim.end) {
			animations.erase(iter++);
		} else ++iter;
	}
}

void AnimData::clear() {
	animations.clear();
}

void AnimData::addAnimation(Type type, float duration) {
	auto iter = animations.find(type);
	if (iter == animations.end()) {
		animations.emplace(std::make_pair(type, Animation(type, duration)));
	} else {
		float left = iter->second.end - iter->second.curr;
		if (left < duration) {
			iter->second.curr = 0;
			iter->second.end = duration;
		}
	}
}

const std::map<AnimData::Type, AnimData::Animation>& AnimData::getAnimations() {
	return animations;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("anim/data", "Test AnimData.") {
	AnimData animData;

	animData.addAnimation(AnimData::NONE,  0.5f);
	animData.addAnimation(AnimData::FLASH, 1.f);
	animData.addAnimation(AnimData::HP   , 2.f);
	animData.update(TimeStep(0.2f));
	auto& animations = animData.getAnimations();
	REQUIRE(animations.size() == 3);
	const AnimData::Animation& anim = animations.at(AnimData::FLASH);
	REQUIRE(anim.end == 1.0f);
	REQUIRE((anim.end - anim.curr) < 0.81f);
	animData.update(TimeStep(0.6f));
	REQUIRE(animations.size() == 2);
	REQUIRE(animations.count(AnimData::NONE) == 0);
	REQUIRE(animations.at(AnimData::HP).type == AnimData::HP);
	animData.update(TimeStep(1.0f));
	REQUIRE(animations.size() == 1);
	REQUIRE(animations.at(AnimData::HP).curr > 1.7f);
	animData.clear();
	REQUIRE(animations.size() == 0);
}
