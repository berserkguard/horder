#include "PoofEffect.h"

PoofEffect::PoofEffect(float size, float duration, sf::Vector2f pos): Effect(POOF, duration, pos, Z_BELOW), size(size) { }

void PoofEffect::logic(TimeStep timeStep) {
	color.a = (int)((getTimeLeft() / getLifespan()) * 255);
}

float PoofEffect::getCurrentSize() {
	return (getAge() / getLifespan()) * size;
}

sf::Color PoofEffect::getColor() {
	return color;
}
void PoofEffect::setColor(sf::Color c) {
	color = c;
}
