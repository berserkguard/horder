#include "Renderer.h"

#include "Map.h"
#include "World.h"
#include "Rand.h"
#include "ResourceManager.h"
#include "MapType.h"
#include "Monster.h"
#include "EffectsManager.h"

const int NUM_LIGHT_CASTS_QUAL[] = {20, 30, 45, 90, 180};
const int FRAMES_PER_LIGHT_QUAL[] = {4, 3, 2, 1, 1};

const int CFSIZ = CHUNK_SIZE * TILE_SIZE;

Renderer::Renderer(Game& game): game(&game), effects(&game.getEffects().graphical()) {
	iterator = 0;
	currentScene = INITIAL_SCENE;

	tileTex        = &ResourceManager::getInstance().getTexture("resources/tiles/tileset.png");
	objTex         = &ResourceManager::getInstance().getTexture("resources/tiles/tileset.png");
    itemIcons      = &ResourceManager::getInstance().getTexture("resources/items/itemset.png");
    miniItemIcons  = &ResourceManager::getInstance().getTexture("resources/items/miniitemset.png");
    miniItemIcons->setSmooth(true);
    simpleFont     = &ResourceManager::getInstance().getFont("resources/fonts/OptimusPrinceps.ttf");
    lightingShader = &ResourceManager::getInstance().getShader("blur");
    flashShader    = &ResourceManager::getInstance().getShader("flash");
	logo           = &ResourceManager::getInstance().getTexture("resources/ui/logo.png");
	itemsTiled     = &ResourceManager::getInstance().getTexture("resources/ui/items_tiled.png");
	playBtn        = &ResourceManager::getInstance().getTexture("resources/ui/play_btn.png");
	wallTex        = &ResourceManager::getInstance().getTexture("resources/tiles/wall.png");
	wallTex->setRepeated(true);
}

void Renderer::renderTile(sf::RenderTarget& target, sf::Sprite& sprite, float rotation) {
	sprite.setRotation(0.0f);
	sprite.rotate(rotation);
	target.draw(sprite);
}

void Renderer::renderEntity(sf::RenderWindow& window, Entity::Ptr entity) {
	if (!entity->isVisible()) return;

	b2Body& body = entity->getCollisionBody();
	sf::RenderStates renderStates;
	renderStates.transform.translate(toSFMLVec(body.GetPosition()) * (float)TILE_SIZE);

	float hp = 0;
	auto& animations = entity->getAnimData().getAnimations();
	for (auto jter = animations.begin(); jter != animations.end(); ++jter) {
		const AnimData::Animation& anim = jter->second;
		switch(anim.type) {
			case AnimData::FLASH: renderStates.shader = flashShader; break;
			case AnimData::HP: hp = 1 - (anim.curr / anim.end); break;
			default: break;
		}
	}

	std::vector<sf::Sprite> sprites = entity->getSprites();
	for (auto kter = sprites.begin(); kter != sprites.end(); ++kter) {
		window.draw(*kter, renderStates);
	}

	//health bars
	if (hp && entity->getType() == Entity::MOB && entity.get() != &game->getWorld().getPlayer()) {
		int alpha = (int)(std::min(hp * 4, 1.f) * 255);
		Mob& mob = (Mob&)*entity;
		float hpf = mob.getHitPoints() / mob.getStatValue(Stat::HP);
		float radius = mob.getStatValue(Stat::SIZE);
		sf::RectangleShape underbar(sf::Vector2f(radius * 2 * TILE_SIZE, 2));
		underbar.setPosition(-radius * TILE_SIZE, -radius * TILE_SIZE - 2);
		sf::RectangleShape overbar = underbar;
		underbar.setFillColor(sf::Color(127, 0, 0, alpha));
		overbar.setFillColor(sf::Color(255, 0, 0, alpha));
		overbar.setSize(sf::Vector2f(radius * 2 * TILE_SIZE * hpf, 2));
		window.draw(underbar, renderStates);
		window.draw(overbar, renderStates);
	}

	if (game->getSettings().getDebugMode() == DEBUG_MEGA) renderDebugPath(window, entity);
}

Tile* Renderer::getRelativeTile(Chunk& chunk, int x, int y){
	Map* map = game->getWorld().getPlayer().getMap();
	int diffx = x < 0 ? -1 : (x > CHUNK_SIZE - 1 ? 1 : 0);
	int diffy = y < 0 ? -1 : (y > CHUNK_SIZE - 1 ? 1 : 0);
	x -= CHUNK_SIZE * diffx;
	y -= CHUNK_SIZE * diffy;

	Chunk* newChunk = map->getChunk(chunk.getPosition() + Coord(diffx, diffy));
	if (!newChunk) return NULL;
	return &newChunk->getTile(x, y);
}

void Renderer::setScene(Scene scene) {
	currentScene = scene;
}

Scene Renderer::getScene() {
	return currentScene;
}

void Renderer::renderGame(sf::RenderWindow& window, UIManager& ui) {
	iterator++;

	sf::View view = window.getDefaultView();
	view.move(view.getSize().x * -.5, view.getSize().y * -.5);
	view.move(vecFloorF(game->getWorld().getPlayer().getPosition() * (float)TILE_SIZE));
	window.setView(view);

	const sf::Vector2f vs = window.getView().getSize();
	const sf::Vector2f vp = window.getView().getCenter() - sf::Vector2f(vs.x / 2, vs.y / 2);

	Map* currentMap = game->getWorld().getPlayer().getMap();

	// iterate through the chunks in the current map
	for (auto iter = currentMap->getChunkBegin(); iter != currentMap->getChunkEnd(); ++iter) {
		Chunk& chunk = *iter->second;
		sf::Vector2i offset = chunk.getPositionInTiles();

		if ((offset.x + CHUNK_SIZE) * TILE_SIZE < vp.x || offset.x * TILE_SIZE > vp.x + vs.x) {
			continue; // Chunk is completely off the screen in x direction
		}
		if ((offset.y + CHUNK_SIZE) * TILE_SIZE < vp.y || offset.y * TILE_SIZE > vp.y + vs.y) {
			continue; // Chunk is completely off the screen in y direction
		}

		for (int y = 0; y < CHUNK_SIZE; y++) {
			for (int x = 0; x < CHUNK_SIZE; x++) {
				Coord pos(x, y);
				Tile& tile = chunk.getTile(x, y);

				if ((offset.x + x + 1) * TILE_SIZE < vp.x || (offset.x + x) * TILE_SIZE > vp.x + vs.x) {
					continue; // Tile is completely off the screen in x direction
				}
				if ((offset.y + y + 1) * TILE_SIZE < vp.y || (offset.y + y) * TILE_SIZE > vp.y + vs.y) {
					continue; // Tile is completely off the screen in y direction
				}

				// render floor
				const TileType& type = tile.getFloor();
				Coord gloc = type.getGLoc();
				sf::Sprite tileSprite(*tileTex, sf::IntRect(TILE_SIZE * gloc, Coord(TILE_SIZE, TILE_SIZE)));
				tileSprite.setPosition(sf::Vector2f((pos + offset) * TILE_SIZE));
				if (currentMap->getType().isGross()) {
					int darken = 220 + tile.getRand() % 25;
					tileSprite.setColor(sf::Color(darken, darken, darken));
				}
				if (tile.isPath()) window.draw(tileSprite);

				// make and draw a sprite for each item in this tile pile
				for (size_t j = 0; j < tile.getNumItems(); j++) {
					Item& item = tile.getItem(j);
					const ItemType& type = item.getType();
					Coord coords; // random coordinates
					int halfMini = MINI_TILE_SIZE / 2;
					coords.x = Rand::consistent(halfMini, TILE_SIZE - halfMini, item.getUnique(), 14  , x + y * 2);
					coords.y = Rand::consistent(halfMini, TILE_SIZE - halfMini, item.getUnique(), 1337, x + y * 2);
					sf::IntRect rect(MINI_TILE_SIZE * type.getGLoc(), Coord(MINI_TILE_SIZE, MINI_TILE_SIZE));
					sf::Sprite itemSprite(*miniItemIcons, rect);
					itemSprite.setPosition(sf::Vector2f((pos + offset) * TILE_SIZE + coords));
					itemSprite.setOrigin(MINI_TILE_SIZE / 2, MINI_TILE_SIZE / 2);
					itemSprite.setRotation(Rand::consistent(0, 360, item.getUnique(), 58008, x + y * 2));
					window.draw(itemSprite);
				}
			}
		}

		// render objects
		for (size_t i = 0; i < chunk.getNumObjects(); i++) {
			MapObject& obj = chunk.getObject(i);
			sf::Sprite objSprite(*objTex, sf::IntRect(obj.getTexPosition(), obj.getGSize()));
			sf::Vector2f origin = sf::Vector2f(obj.getGSize()) * 0.5f;
			objSprite.setOrigin(origin);
			objSprite.setPosition(sf::Vector2f(obj.getGPosition() + chunk.getPositionInTiles() * TILE_SIZE) + origin);
			objSprite.setRotation(obj.getRotation() / TAU * 360);
			window.draw(objSprite);
		}
	}

	renderWalls(window, *currentMap);

	sf::View view2 = window.getDefaultView();
	view2.move(view2.getSize().x * -.5, view2.getSize().y * -.5);
	view2.move(game->getWorld().getPlayer().getPosition() * (float)TILE_SIZE);
	window.setView(view2);

	for (int i = Effect::Z_OFF; i <= Effect::Z_BELOW; i++) {
		effects->renderLayer(window, i);
	}

	// iterate through all the entities in the current map
	for (auto iter = currentMap->entitiesBegin(); iter != currentMap->entitiesEnd(); ++iter) {
		renderEntity(window, iter->first);
	}

	window.setView(window.getDefaultView());
	if (currentMap->getType().isDark()) renderLighting(window);

	window.setView(view);
	effects->renderLayer(window, Effect::Z_ABOVE);

	if (game->getSettings().getDebugMode() == DEBUG_MEGA)  renderDebugShapes(window, *currentMap);
	window.setView(window.getDefaultView());
	if (game->getSettings().getDebugMode() >= DEBUG_BASIC) renderDebugInfo(window);

	ui.render(window, UI_NORMAL);
	effects->renderLayer(window, Effect::Z_ABOVE_UI);
	ui.render(window, UI_ABOVE);
}

void Renderer::renderMenu(sf::RenderWindow& window, UIManager& ui) {
	window.setView(window.getDefaultView());

	// Render background
	sf::Sprite tileSprite(*itemsTiled);
	sf::Vector2i size(window.getSize());
	for (int i = 0; i < (int)(size.x / 256.0f) + 1; i++) {
		for (int j = 0; j < (int)(size.y / 256.0f) + 1; j++) {
			tileSprite.setPosition(sf::Vector2f(i * 256.0f, j * 256.0f));
			window.draw(tileSprite);
		}
	}

	// Render logo
	sf::Sprite logoSprite(*logo);
	logoSprite.setOrigin(logo->getSize().x / 2, logo->getSize().y / 2);
	logoSprite.setPosition(sf::Vector2f(size.x / 2, 80));
	window.draw(logoSprite);

	effects->renderLayer(window, Effect::Z_ABOVE);
	ui.render(window);
	effects->renderLayer(window, Effect::Z_ABOVE_UI);
}

void Renderer::render(sf::RenderWindow& window, UIManager& ui) {
	switch (currentScene) {
		case SCENE_MENU:
			renderMenu(window, ui);
			break;
		case SCENE_GAME:
			renderGame(window, ui);
			break;
		default: break;
	}
}

const float wallWidth = 0.15f;
const float wwi = 1 - wallWidth + 0.01f;
const float wwj = 1 - wallWidth - 0.01f;
const float wwo = 1 + wallWidth - 0.01f;
const float wwu = 1 + wallWidth + 0.01f;
void Renderer::renderLighting(sf::RenderWindow& window) {
	int quality = game->getSettings().getGraphicQuality(Settings::LIGHTING);
	if (quality == 0) return;

	Player& player = game->getWorld().getPlayer();
	static sf::Vector2f prevPos = player.getPosition();
	sf::Vector2f ppos = player.getPosition();

	sf::RenderStates renderStates;
	renderStates.transform.translate(-20, -20);

	if (!(iterator % FRAMES_PER_LIGHT_QUAL[quality])) {
		//sf::Vector2f ppost = ppos * (1.f / TILE_SIZE);
		sf::View view = window.getView();

		// get all the angles to test
		std::set<float> angles;
		for (int y = -LIGHT_DIST; y <= LIGHT_DIST + 1; y++) {
			for (int x = -LIGHT_DIST; x <= LIGHT_DIST + 1; x++) {
				Coord pos = vecFloor(ppos) + Coord(x, y);
				Tile* t1 = player.getMap()->getTileSafe(pos);
				Tile* t2 = player.getMap()->getTileSafe(pos.x + 1, pos.y);
				Tile* t3 = player.getMap()->getTileSafe(pos.x, pos.y + 1);
				Tile* t4 = player.getMap()->getTileSafe(pos.x + 1, pos.y + 1);
				bool b1 = (!t1 || t1->isPath());
				bool b2 = (!t2 || t2->isPath());
				bool b3 = (!t3 || t3->isPath());
				bool b4 = (!t4 || t4->isPath());
				sf::Vector2f point1, point2;
				if (b1 == b4) {
					if (b2 && !b3) {
						point1 = sf::Vector2f(pos.x + wwi, pos.y + wwo);
						point2 = sf::Vector2f(pos.x + wwj, pos.y + wwu);
					} else if (!b2 && b3) {
						point1 = sf::Vector2f(pos.x + wwo, pos.y + wwi);
						point2 = sf::Vector2f(pos.x + wwu, pos.y + wwj);
					}
				} else if (b2 == b3) {
					if (b1 && !b4) {
						point1 = sf::Vector2f(pos.x + wwo, pos.y + wwo);
						point2 = sf::Vector2f(pos.x + wwu, pos.y + wwu);
					} else if (!b1 && b4) {
						point1 = sf::Vector2f(pos.x + wwi, pos.y + wwi);
						point2 = sf::Vector2f(pos.x + wwj, pos.y + wwj);
					}
				}
				if (point1.x != 0 || point1.y != 0) {
					point1 -= ppos;
					point2 -= ppos;
					angles.insert(std::atan2(point1.y, point1.x));
					angles.insert(std::atan2(point2.y, point2.x));
				}
			}
		}

		int numLightCasts = NUM_LIGHT_CASTS_QUAL[quality];
		for (float r = -TAU / 2; r < TAU / 2; r += TAU / numLightCasts) {
			angles.insert(r);
		}

		std::vector<sf::Vertex> fov;
		fov.push_back(sf::Vertex(view.getSize() * .5f, sf::Color::Transparent));

		// iterate through all angles
		for (auto iter = angles.begin(); iter != angles.end(); ++iter) {
			float r = *iter;

			b2RayCastInput input;
			input.p1 = toPhysVec(ppos);
			input.p2 = input.p1 + b2Vec2(std::cos(r), std::sin(r));
			input.maxFraction = LIGHT_DIST * 4;

			b2AABB bb;
			bb.lowerBound = b2Vec2(ppos.x - LIGHT_DIST, ppos.y - LIGHT_DIST);
			bb.upperBound = b2Vec2(ppos.x + LIGHT_DIST, ppos.y + LIGHT_DIST);
			float dist = player.getMap()->rayCast(input, bb);

			sf::Vector2f angleVec = sf::Vector2f(std::cos(r) * dist, std::sin(r) * dist);
			sf::Vector2f vect = angleVec * (float)TILE_SIZE + (view.getSize() * .5f);
			fov.push_back(sf::Vertex(vect, sf::Color::Transparent));
		}
		fov.push_back(fov[1]);

		// crazy drawing
		if (lightingTexture1.getSize() != sf::Vector2u(view.getSize()) + sf::Vector2u(40, 40)) {
			lightingTexture1.create(view.getSize().x + 40, view.getSize().y + 40);
		}
		lightingTexture1.clear();
		sf::RenderStates renderStates(sf::BlendNone);
		renderStates.transform.translate(20, 20);
		lightingTexture1.draw(&fov[0], fov.size(), sf::TrianglesFan, renderStates);
		lightingTexture1.display();

		if (lightingTexture2.getSize() != lightingTexture1.getSize()) {
			lightingTexture2.create(lightingTexture1.getSize().x, lightingTexture1.getSize().y);
		}
		lightingTexture2.clear();
		int lightRadius = LIGHT_DIST * TILE_SIZE;
		sf::CircleShape circle(lightRadius, 180);
		circle.setFillColor(sf::Color::Transparent);
		circle.setPosition(view.getSize().x / 2 - lightRadius, view.getSize().y / 2 - lightRadius);
		lightingTexture2.draw(circle, sf::BlendNone);

		lightingTexture1.draw(sf::Sprite(lightingTexture2.getTexture()));

		prevPos = ppos;
	} else {
		renderStates.transform.translate((prevPos - ppos) * (float)TILE_SIZE);
	}
	sf::Sprite finalSprite(lightingTexture1.getTexture());
	if (quality > 1) {
		lightingShader->setParameter("texture", *finalSprite.getTexture());
		lightingShader->setParameter("blur_radius", .005f);
		renderStates.shader = lightingShader;
	}
	window.draw(finalSprite, renderStates);
}

void Renderer::renderWalls(sf::RenderWindow& window, Map& map) {
	const b2Body& walls = map.getWallsBody();
	for (const b2Fixture* fix = walls.GetFixtureList(); fix; fix = fix->GetNext()) {

		// The outer edge is in the physics user data
		if (!fix->GetUserData()) continue;
		PhysUserData& userData = *(PhysUserData*)fix->GetUserData();
		if (!userData.shape.graphic) continue;
		b2EdgeShape& outerEdge = (b2EdgeShape&)*userData.shape.graphic;

		// The inner edge is a fixture shape
		const b2Shape* shape = fix->GetShape();
		if (!shape->GetType() == b2Shape::e_edge) continue;
		b2EdgeShape& innerEdge = (b2EdgeShape&)*shape;

		sf::Vector2f innerVert1 = toSFMLVec(innerEdge.m_vertex1);
		sf::Vector2f innerVert2 = toSFMLVec(innerEdge.m_vertex2);
		sf::Vector2f outerVert1 = toSFMLVec(outerEdge.m_vertex1);
		sf::Vector2f outerVert2 = toSFMLVec(outerEdge.m_vertex2);
		double dist = vecMagnitude(innerVert1 - innerVert2);

		// create the vertexes
		sf::Vertex verts[4];
		verts[0].position  = innerVert1 * (float)TILE_SIZE;
		verts[1].position  = innerVert2 * (float)TILE_SIZE;
		verts[2].position  = outerVert2 * (float)TILE_SIZE;
		verts[3].position  = outerVert1 * (float)TILE_SIZE;
		verts[0].texCoords = sf::Vector2f(0, 16);
		verts[1].texCoords = sf::Vector2f(32 + dist * TILE_SIZE, 16);
		verts[2].texCoords = sf::Vector2f(16 + dist * TILE_SIZE, 0);
		verts[3].texCoords = sf::Vector2f(16, 0);
		window.draw(verts, 4, sf::Quads, wallTex);
	}
}

void Renderer::renderDebugInfo(sf::RenderWindow& window) {
	std::stringstream debugSS;

	debugSS << "FPS: " << Get::fps();
	sf::Text debugText(debugSS.str(), *simpleFont, 24);
	debugText.setColor(sf::Color::Green);
	window.draw(debugText);

	debugSS.str(String());
	debugSS.clear();
	sf::Vector2f position = game->getWorld().getPlayer().getPosition();
	debugSS << "Position: (" << position.x << ", " << position.y << ") in chunk [";
	Chunk* c = game->getWorld().getPlayer().getMap()->getChunkContainingPos(vecFloor(position));
	if (c) {
		Coord chunkPos = c->getPosition();
		debugSS << chunkPos.x << ", " << chunkPos.y << "]";
	} else debugSS << "x]";
	debugText.setString(debugSS.str());
	debugText.move(0, 24);
	window.draw(debugText);

	if (game->getSettings().isFullscreen()) {
		debugSS.str("Press Ctrl+Q to quit!");
		debugSS.clear();
		debugText.setString(debugSS.str());
		debugText.move(0, 24);
		debugText.setColor(sf::Color::Yellow);
		window.draw(debugText);
	}

	debugSS.str(std::string());
	debugSS << "Chunks Loaded: " << game->getWorld().getPlayer().getMap()->getNumChunks();
	Map* map = game->getWorld().getPlayer().getMap();
	for (auto iter = map->getChunkBegin(); iter != map->getChunkEnd(); ++iter) {
		Chunk& chunk = *iter->second;
		debugSS << " (" << chunk.getPosition().x << ", " << chunk.getPosition().y << ")";
	}
	debugText.setString(debugSS.str());
	debugText.move(0, 24);
	debugText.setColor(sf::Color::Yellow);

	window.draw(debugText);
}

void Renderer::renderDebugShapes(sf::RenderWindow& window, Map& map) {
	auto& shapes = map.getRayCastShapes();
	for (auto iter = shapes.begin(); iter != shapes.end(); ++iter) {
		b2EdgeShape* shape = (b2EdgeShape*)(*iter);
		sf::Vertex vert1 = sf::Vertex(toSFMLVec(shape->m_vertex1) * (float)TILE_SIZE);
		sf::Vertex vert2 = sf::Vertex(toSFMLVec(shape->m_vertex2) * (float)TILE_SIZE);
		sf::Vertex line[] = {vert1, vert2};
		window.draw(line, 2, sf::Lines);
	}
}

void Renderer::renderDebugPath(sf::RenderWindow& window, Entity::Ptr entity) {
	if (entity->getType() == Entity::MOB && ((Mob&)*entity).getMobType() == Mob::MONSTER) {
		Monster& mob = (Monster&)*entity;
		Path& path = mob.getPath();
		for (int i = 0; i < path.length() - 1; i++) {
			sf::Vertex vert1 = sf::Vertex(path.at(i)     * (float)TILE_SIZE, sf::Color::Cyan);
			sf::Vertex vert2 = sf::Vertex(path.at(i + 1) * (float)TILE_SIZE, sf::Color::Cyan);
			sf::Vertex line[] = {vert1, vert2};
			window.draw(line, 2, sf::Lines);
		}
	}
}
