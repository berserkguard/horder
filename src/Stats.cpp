#include "Stats.h"

#include <map>
#include "Util.h"

//                       none, hp, regen, def, eva, acc, spd, atk, crit, ar, knock, rng, atkw, tgts, rad, dens, damp, el, scale
const float DEFAULTS[19] = {0, 50,    0,   0,   0,   1,   1,   1,    0,  1,     0,   1,    1,    1, 0.5,    1, 0.99, 0.1, 1};
const bool USEFUL[19] = {false, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false};
//colors, etc.

bool mapInitilized = false;
std::map<String, Stat::Type> stringToStatMap;

Stat::Type Stat::fromString(String s) {
	s = toLowerCase(s);
	if (!mapInitilized) {
		//ringToStatMap["stat"] = stringToStatMap["long stat name"]= STAT_ENUM_VAL [implemented]
		stringToStatMap["hp"]   = stringToStatMap["hit points"]    = HP;          // [x]
		stringToStatMap["regen"]= stringToStatMap["regeneration"]  = REGEN;       // [x]
		stringToStatMap["def"]  = stringToStatMap["defense"]       = DEFENSE;     // [x]
		stringToStatMap["ev"]   = stringToStatMap["evasion"]       = EVASION;     // [x]
		stringToStatMap["acc"]  = stringToStatMap["accuracy"]      = ACCURACY;    // [x]
		stringToStatMap["speed"]= stringToStatMap["movement speed"]= MOVE_SPEED;  // [x]
		stringToStatMap["atk"]  = stringToStatMap["attack"]        = ATTACK;      // [x]
		stringToStatMap["crit"] = stringToStatMap["critical"]      = CRITICAL;    // [x]
		stringToStatMap["ar"]   = stringToStatMap["attack speed"]  = ATTACK_SPEED;// [x]
		stringToStatMap["knock"]= stringToStatMap["knockback"]     = KNOCKBACK;   // [x]
		stringToStatMap["rng"]  = stringToStatMap["range"]         = RANGE;       // [x]
		stringToStatMap["atkw"] = stringToStatMap["attack width"]  = ATTACK_WIDTH;// [x]
		stringToStatMap["tgts"] = stringToStatMap["max targets"]   = MAX_TARGETS; // [x]
		stringToStatMap["rad"]  = stringToStatMap["radius"]        = SIZE;        // [x]
		stringToStatMap["dens"] = stringToStatMap["density"]       = DENSITY;     // [x]
		stringToStatMap["damp"] = stringToStatMap["damping"]       = DAMPING;     // [x]
		stringToStatMap["el"]   = stringToStatMap["elasticity"]    = ELASTICITY;  // [x]
		stringToStatMap["scale"]= stringToStatMap["gscale"]        = GSCALE;      // [x]
	}
	auto iter = stringToStatMap.find(s);
	if (iter == stringToStatMap.end()) return NONE;
	return iter->second;
}

String Stat::toString(Stat::Type type) {
	switch (type) {
		case HP:           return "Hit Points";
		case REGEN:        return "Regeneration";
		case DEFENSE:      return "Defense";
		case EVASION:      return "Evasion";
		case ACCURACY:     return "Accuracy";
		case MOVE_SPEED:   return "Movement Speed";
		case ATTACK:       return "Attack";
		case CRITICAL:     return "Critical";
		case ATTACK_SPEED: return "Attack Speed";
		case KNOCKBACK:    return "Knockback";
		case RANGE:        return "Range";
		case ATTACK_WIDTH: return "Attack Width";
		case MAX_TARGETS:  return "Max Targets";
		case SIZE:         return "Radius";
		case DENSITY:      return "Density";
		case DAMPING:      return "Damping";
		case ELASTICITY:   return "Elasticity";
		case GSCALE:       return "GScale";
		default:           return "";
	}
}

bool Stat::isUseful(Stat::Type stat) {
	assert(stat < COUNT);
	return USEFUL[stat];
}

float Stat::getDefaultValue(Stat::Type stat) {
	assert(stat < COUNT);
	return DEFAULTS[stat];
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("stats/string", "Test Stat string conversion.") {
	REQUIRE(Stat::fromString(Stat::toString(Stat::MOVE_SPEED)) == Stat::MOVE_SPEED);
	REQUIRE(Stat::toString(Stat::ATTACK) == "Attack");
	REQUIRE(Stat::fromString("hp") == Stat::HP);
}
TEST_CASE("stats/defaults", "Test Stat defaults.") {
	REQUIRE(Stat::getDefaultValue(Stat::HP) == DEFAULTS[Stat::HP]);
	REQUIRE(Stat::getDefaultValue(Stat::GSCALE) == 1);
}
TEST_CASE("stats/useful", "Test Stat useful.") {
	// Exhaustive testing FTW
	REQUIRE(Stat::isUseful(Stat::NONE) == false);
	REQUIRE(Stat::isUseful(Stat::HP) == true);
	REQUIRE(Stat::isUseful(Stat::REGEN) == true);
	REQUIRE(Stat::isUseful(Stat::DEFENSE) == true);
	REQUIRE(Stat::isUseful(Stat::EVASION) == true);
	REQUIRE(Stat::isUseful(Stat::ACCURACY) == true);
	REQUIRE(Stat::isUseful(Stat::MOVE_SPEED) == true);
	REQUIRE(Stat::isUseful(Stat::ATTACK) == true);
	REQUIRE(Stat::isUseful(Stat::CRITICAL) == true);
	REQUIRE(Stat::isUseful(Stat::ATTACK_SPEED) == true);
	REQUIRE(Stat::isUseful(Stat::KNOCKBACK) == true);
	REQUIRE(Stat::isUseful(Stat::RANGE) == true);
	REQUIRE(Stat::isUseful(Stat::ATTACK_WIDTH) == false);
	REQUIRE(Stat::isUseful(Stat::MAX_TARGETS) == false);
	REQUIRE(Stat::isUseful(Stat::SIZE) == false);
	REQUIRE(Stat::isUseful(Stat::DENSITY) == false);
	REQUIRE(Stat::isUseful(Stat::DAMPING) == false);
	REQUIRE(Stat::isUseful(Stat::ELASTICITY) == false);
	REQUIRE(Stat::isUseful(Stat::GSCALE) == false);
}
