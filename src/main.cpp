#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include "Renderer.h"
#include "Game.h"
#include "World.h"
#include "Map.h"
#include "MapWriter.h"
#include "Construction.h"
#include "Filling.h"

#include "FadeEffect.h"

#include "UIManager.h"
#include "UIFactory.h"
#include "createUI.h"

#include "ResourceManager.h"
#include "EffectsManager.h"
#include "TimeStep.h"

sf::RenderWindow window;
sf::Clock mainClock;
sf::Time prevTime;

int frames;
float frameTime;

MapWriter* mw;
Renderer* renderer;
UIManager* ui;
Game game;
bool keyDown[Settings::K_COUNT];

FadeEffect* pauseFade;

#define CHUNK_LOAD_DIST 10

void postCreateUI();

void pauseEnd(FadeEvent evt) {
	if (evt == FadeEvent::FADE_MIDDLE) {
		pauseFade->pause();
	} else if (evt == FadeEvent::FADE_OVER) {
		game.setPaused(false);
	}
}

/// return true if success
bool init() {
	pauseFade = nullptr;
	mw = new MapWriter("savedata/save_1/");
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Horder!", sf::Style::Default, settings);

	renderer = new Renderer(game);
    ui = new UIManager();

	if (!game.load("data/game.yaml")) return false;
	std::cout << "All done." << std::endl;
	game.getSettings().apply(window);

    for (int i = 0; i < Settings::K_COUNT; i++) {
        keyDown[i] = false;
    }

	prevTime = mainClock.getElapsedTime();
	frames = 0;
	frameTime = 0;

	// Make a test UI widget
	ItemContainer& testIC = game.getWorld().getPlayer().getInventory().getItemContainer();
	int i = 0;
	while (const ItemType* it = game.getValuables().getItemType(i++)) {
		testIC.addItem(Item(*it));
	}

	// Equip wooden sword and trash cover shield
	game.getWorld().getPlayer().equipItem(Item(*game.getValuables().getItemType("Wooden Sword")));
	game.getWorld().getPlayer().equipItem(Item(*game.getValuables().getItemType("Trash Bin Cover")));

	// Create the necessary UI elements and link up callbacks
	createUI(ui);
	postCreateUI();

	return true;
}

bool menuEvents() {
    sf::Event event;
    while(window.pollEvent(event)) {
		// Handle the event for all UI elements
		ui->handleEvent(event);

        switch(event.type) {
            case sf::Event::Closed: return true;
            case sf::Event::KeyPressed: {
            	std::cout << event.key.code << std::endl;
                if (event.key.code == sf::Keyboard::L){
					renderer->setScene(SCENE_GAME);
					ui->setScene(SCENE_GAME);
                }
				if (event.key.code == sf::Keyboard::Q && event.key.control) {
                    // quit on ctrl+q
                    return true;
                }
            } break;
            default: break;
        }
    }
    return false;
}

/// return true when done
bool gameEvents() {
	sf::Event event;
	sf::Vector2i windowSize = sf::Vector2i(window.getSize());
	Player& player = game.getWorld().getPlayer();
	while(window.pollEvent(event)) {
		// Handle the event for all UI elements
		if (ui->handleEvent(event)) {
			continue; // handleEvent returned true, so don't propagate event
		}

		switch(event.type) {
			case sf::Event::Closed: return true;
			case sf::Event::KeyPressed: {
				//printf("%i\n", event.key.code);
				Settings& settings = game.getSettings();

				sf::Keyboard::Key code = event.key.code;
				if (code == sf::Keyboard::Unknown) code = sf::Keyboard::Comma; // dvorak issues. SFML really needs scancodes.
				keyDown[settings.getBinding(code)] = true;

				if (keyDown[Settings::PAUSE]) {
					if (player.getHitPoints() == 0) break;
					if (!game.isPaused()) {
						game.setPaused(true);

						pauseFade = (FadeEffect*)&game.getEffects().graphical().createFadeEffect(0.1f, 0, sf::Vector2f(0, 0));
						pauseFade->setColor(sf::Color(0, 0, 0, 0), sf::Color(0, 0, 0, 235));
						pauseFade->setCallback(pauseEnd);
					} else if (pauseFade) {
						pauseFade->resume();
					}
				} else if (event.key.code == sf::Keyboard::R) {
					frames = 0;
					frameTime = 0;
				} else if (event.key.code == sf::Keyboard::Q && event.key.control) {
					// quit on ctrl+q
					return true;
				} else if (event.key.code == sf::Keyboard::D && event.key.control) {
					if (settings.getDebugMode() == DEBUG_BASIC) {
						settings.setDebugMode(DEBUG_MEGA);
					} else if (settings.getDebugMode() == DEBUG_MEGA) {
						settings.setDebugMode(DEBUG_BASIC);
					}
				}

				// NOTE: anything after the below line will NOT work when paused (or player is dead)!
				if (game.isPaused() || player.getHitPoints() == 0) break;

				if (keyDown[Settings::CHARINFO]) {
					bool visible = ui->getWidget("charInfo")->isVisible();
					ui->getWidget("charInfo")->setVisibility(!visible);
                } else if (keyDown[Settings::BACKPACK]) {
					bool visible = ui->getWidget("playerBackpack")->isVisible();
					ui->getWidget("playerBackpack")->setVisibility(!visible);
                } else if (keyDown[Settings::INTERACT] || event.key.code == sf::Keyboard::Space) {
                	Tile* groundTile = player.getGroundTile();
					if (player.getGroundTile()->getNumItems() == 1) {
						player.getInventory().getItemContainer().addItem(groundTile->removeItem(0));
						if (player.getHome() == player.getMap()) player.recalcHoardStats(player.getGroundPos());
					} else if (player.getGroundTile()->getNumItems() > 1) {
						// There are items on floor, open second backpack
						((BackpackWindow*)ui->getWidget("lootBackpack"))->associateItems(*groundTile->getItemContainer(), player);
						ui->getWidget("lootBackpack")->setVisibility(!ui->getWidget("lootBackpack")->isVisible());
					} else {
						player.interact();
					}
				} else if (event.key.code == sf::Keyboard::LShift) {
					player.block(true);
				} else if (event.key.code == sf::Keyboard::F) {
					player.fastReturn();
				}
			} break;
			case sf::Event::KeyReleased: {
				Settings& settings = game.getSettings();

				sf::Keyboard::Key code = event.key.code;
            	if (code == sf::Keyboard::Unknown) code = sf::Keyboard::Comma;
				keyDown[settings.getBinding(code)] = false;

				/// NOTE: anything after the below line will NOT work when paused (or player is dead)!
				if (game.isPaused() || player.getHitPoints() == 0) break;

				if (event.key.code == sf::Keyboard::LShift) {
					player.block(false);
				}
			} break;
			case sf::Event::MouseButtonPressed: {
				/// NOTE: anything after the below line will NOT work when paused (or player is dead)!
				if (game.isPaused() || player.getHitPoints() == 0) break;

				if (event.mouseButton.button == sf::Mouse::Left && !ui->wasClicked()) {
					sf::Vector2f mouseRelativePosition(event.mouseButton.x - windowSize.x / 2, event.mouseButton.y - windowSize.y / 2);
					player.rotateToward(vecAngle(mouseRelativePosition));
					player.attack();
				}
			} break;
			case sf::Event::MouseMoved: {
				/// NOTE: anything after the below line will NOT work when paused (or player is dead)!
				if (game.isPaused() || player.getHitPoints() == 0) break;

				sf::Vector2f mouseRelativePosition(event.mouseMove.x - windowSize.x / 2, event.mouseMove.y - windowSize.y / 2);
				player.rotateToward(vecAngle(mouseRelativePosition));
			} break;
			default: break;
		}
	}

    return false;
}

void render(TimeStep timeStep) {
    window.clear();
    renderer->render(window, *ui);
    window.display();
}

void gameLogic(TimeStep timeStep) {
	Player& player = game.getWorld().getPlayer();
	Map* map = player.getMap();

	ui->update(timeStep);

	frames++;
	frameTime += timeStep.sec();

	if (game.isPaused()) {
		ui->getWidget("contBtn")->setVisibility(true);
		return;
	}
	/// NOTE: anything below this comment will NOT work when paused!

	ui->getWidget("contBtn")->setVisibility(false);

	static float totes = 0.f;
	static float constTimeStep = 1 / 60.f;
	totes += timeStep.sec();
	int i = 0;
	while (totes >= constTimeStep) {
		if (i++ > 3) {
			// break out if lagging
			totes = 0;
			std::cout << "too slow" << std::endl;
			break;
		}

		// consistant time step
		game.getPhysWorld().Step(constTimeStep, 6, 2);
		totes -= constTimeStep;
	}

	int dir = NONE;
	if      (keyDown[Settings::UP]    || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))    dir |= NORTH;
	else if (keyDown[Settings::DOWN]  || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))  dir |= SOUTH;
	if      (keyDown[Settings::LEFT]  || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))  dir |= WEST;
	else if (keyDown[Settings::RIGHT] || sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) dir |= EAST;
	player.move(dir, timeStep);

	map->update(timeStep);

	if (map->getType().isInfinite()) {
		// Determine if chunk needs to be paged out to the hard drive
		for (auto iter = map->getChunkBegin(); iter != map->getChunkEnd(); ) {
			Chunk& chunk = *iter->second;
			sf::Vector2i chunkPos = chunk.getPositionInTiles();
			bool chunkFarRight = chunkPos.x > player.getPosition().x + CHUNK_LOAD_DIST;
			bool chunkFarLeft = chunkPos.x + CHUNK_SIZE < player.getPosition().x - CHUNK_LOAD_DIST;
			bool chunkFarDown = chunkPos.y > player.getPosition().y + CHUNK_LOAD_DIST;
			bool chunkFarUp = chunkPos.y + CHUNK_SIZE < player.getPosition().y - CHUNK_LOAD_DIST;

			if (chunkFarRight || chunkFarLeft || chunkFarDown || chunkFarUp) {
				// Chunk is far away from player to the right, so unload it
				mw->writeChunk(game, chunk);
				iter = map->destroyChunk(chunk);
			} else {
				++iter;
			}
		}

		sf::Vector2f playerChunkX(player.getPosition().x - CHUNK_LOAD_DIST, player.getPosition().x + CHUNK_LOAD_DIST);
		sf::Vector2f playerChunkY(player.getPosition().y - CHUNK_LOAD_DIST, player.getPosition().y + CHUNK_LOAD_DIST);
		int chunkBeginX = std::floor(playerChunkX.x / (float)CHUNK_SIZE);
		int chunkEndX = std::floor(playerChunkX.y / (float)CHUNK_SIZE);
		int chunkBeginY = std::floor(playerChunkY.x / (float)CHUNK_SIZE);
		int chunkEndY = std::floor(playerChunkY.y / (float)CHUNK_SIZE);
		for (int i = chunkBeginX; i <= chunkEndX; i++) {
			for (int j = chunkBeginY; j <= chunkEndY; j++) {
				Chunk* chunk = map->getChunk(sf::Vector2i(i, j));
				if (!chunk || chunk->getState() == Chunk::EMPTY) {
					// Chunk needs to be loaded
					printf("Generating Chunk: %i, %i\n", i, j);
					Chunk& chunk = map->createChunk(sf::Vector2i(i, j));
					// Map is infinite, so re-generate the chunk
					Blueprint blueprint(map->getType(), 14/*map->getSeed()*/, chunk);
					blueprint.generate();
					Construction construction(blueprint, *map);
					construction.construct();
					Filling filling(blueprint, *map);
					filling.fill();
					chunk.setState(Chunk::GENERATED);
					//Generator::generateChunk(*map, chunk);
					mw->loadChunk(game, chunk);
				}
			}
		}
	}
}

void menuLogic(TimeStep timeStep) {
	ui->update(timeStep);
}

/// return true when done
bool execute() {
	sf::Time newTime = mainClock.getElapsedTime();
	sf::Time diff = newTime - prevTime;
	prevTime = newTime;

	static PeriodManager periodManager;
	TimeStep timeStep(diff, periodManager);

	if (timeStep.hasPeriodPassed(TimeStep::FULL)) {
		game.getEffects().sound().updateAll();
	}

	switch (renderer->getScene()) {
		case SCENE_GAME:
			if (gameEvents()) return true;
			gameLogic(timeStep);
			break;
		case SCENE_MENU:
			if (menuEvents()) return true;
			menuLogic(timeStep);
			break;
		default: break;
	}
	game.getEffects().graphical().update(timeStep);

	render(timeStep);
	return false;
}

void clean() {
	delete renderer;
	delete ui;
	window.close();
}

void unitTest(int argc, char** argv) {
	Catch::Session session;
	session.run(argc, argv);
}

int main(int argc, char** argv) {
	unitTest(argc, argv);
    if (init()) {
		while(!execute());
    } else {
    	return EXIT_FAILURE;
	}
    clean();
    return EXIT_SUCCESS;
}

b2World& Get::physWorld() {
    return game.getPhysWorld();
}

float Get::fps() {
    return frames / frameTime;
}

void fadeCallback(FadeEvent evt) {
	if (evt == FadeEvent::FADE_MIDDLE) {
		renderer->setScene(SCENE_GAME);
		ui->setScene(SCENE_GAME);
	}
}

void playCallback() {
	FadeEffect& effect = game.getEffects().graphical().createFadeEffect(0.5f, 0.0f, sf::Vector2f(0, 0));
	effect.setColor(sf::Color(0, 0, 0, 0), sf::Color(0, 0, 0, 255));
	effect.setCallback(fadeCallback);
}

void contCallback() {
	pauseFade->resume();
}

void postCreateUI() {
	Player& player = game.getWorld().getPlayer();

	ui->setScene(SCENE_MENU);
	((ButtonWidget*)ui->getWidget("playBtn"))->setCallback(playCallback);
	ui->getWidget("playBtn")->setPosition(sf::Vector2f(window.getSize().x / 2, 300));

	ui->getWidget("byLabel")->setPosition(sf::Vector2f(window.getSize().x / 2 - 537 / 2, window.getSize().y - 55));

	ui->setScene(SCENE_GAME);
	ItemContainer& ic = game.getWorld().getPlayer().getInventory().getItemContainer();
	((BackpackWindow*)ui->getWidget("playerBackpack"))->associateItems(ic, player);

	((CharInfoWindow*)ui->getWidget("charInfo"))->associatePlayer(player);

	ui->getWidget("barLabels")->setPosition(sf::Vector2f(8, window.getSize().y - 102));

	((BarWidget*)ui->getWidget("playerHP"))->associateMob((Mob&)player, BarType::BAR_HEALTH);
	ui->getWidget("playerHP")->setPosition(sf::Vector2f(60, window.getSize().y - 100));

	((BarWidget*)ui->getWidget("playerMP"))->associateMob((Mob&)player, BarType::BAR_MANA);
	ui->getWidget("playerMP")->setPosition(sf::Vector2f(60, window.getSize().y - 70));

	((MinimapWidget*)ui->getWidget("minimap"))->associatePlayer(player);
	ui->getWidget("minimap")->setPosition(sf::Vector2f(window.getSize().x - 200, window.getSize().y - 200));

	((ButtonWidget*)ui->getWidget("contBtn"))->setCallback(contCallback);
	((ButtonWidget*)ui->getWidget("contBtn"))->setPosition(sf::Vector2f(window.getSize().x / 2, 300));

	((DeathImageWidget*)ui->getWidget("deathImg"))->associatePlayer(&player);

	// Make sure we start at the main menu
	ui->setScene(SCENE_MENU);
}
