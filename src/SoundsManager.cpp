#include "SoundsManager.h"

#include "ResourceManager.h"

SoundsManager::SoundsManager() {
	for (int i = 0; i < S_COUNT; i++) {
		soundBuffers[i] = NULL;
	}
	ResourceManager& resMan = ResourceManager::getInstance();
	soundBuffers[S_SHOOT] = &resMan.getSoundBuffer("resources/audio/shoot.wav");
	soundBuffers[S_FLAME] = &resMan.getSoundBuffer("resources/audio/flame.wav");
	soundBuffers[S_HIT]   = &resMan.getSoundBuffer("resources/audio/hit.wav");
	soundBuffers[S_MISS]  = &resMan.getSoundBuffer("resources/audio/miss.wav");
	soundBuffers[S_OPEN]  = &resMan.getSoundBuffer("resources/audio/open.wav");
	soundBuffers[S_DROP]  = &resMan.getSoundBuffer("resources/audio/drop.wav");
	soundBuffers[S_GRAB]  = &resMan.getSoundBuffer("resources/audio/pickup.wav");
	soundBuffers[S_SLIME] = &resMan.getSoundBuffer("resources/audio/slime.wav");
	soundBuffers[S_CLINK] = &resMan.getSoundBuffer("resources/audio/clink.wav");
	soundBuffers[S_DEATH_SLIME]   = &resMan.getSoundBuffer("resources/audio/slime_death.wav");
	soundBuffers[S_DEATH_DORKLEN] = &resMan.getSoundBuffer("resources/audio/dorklen_death.wav");
	soundBuffers[S_STEPS_GRASS] = &resMan.getSoundBuffer("resources/audio/steps_grass.ogg");
	soundBuffers[S_STEPS_STONE] = &resMan.getSoundBuffer("resources/audio/steps_stone.ogg");
	soundBuffers[S_STEPS_WOOD]  = &resMan.getSoundBuffer("resources/audio/steps_wood.ogg");
}

SoundPtr SoundsManager::createSoundBase(Sound sound, SoundType type) {
	SoundPtr newSound(new sf::Sound());
	if (soundBuffers[sound]) {
		newSound->setBuffer(*soundBuffers[sound]);
		soundTypes[type].insert(newSound);
	}
	return SoundPtr(newSound);
}

SoundPtr SoundsManager::newSound(Sound sound, SoundType type) {
	SoundPtr newSound = createSoundBase(sound, type);
	newSound->setRelativeToListener(true);
	return newSound;
}

SoundPtr SoundsManager::newSound(Sound sound, sf::Vector2f soundPos, SoundType type) {
	SoundPtr newSound = createSoundBase(sound, type);
	newSound->setPosition(soundPos.x, soundPos.y, 0);
	return newSound;
}

void SoundsManager::updateAll() {
	for (int i = 0; i < ST_COUNT; i++) {
		for (auto iter = soundTypes[i].begin(); iter != soundTypes[i].end(); ) {
			if ((*iter).unique() && (*iter)->getStatus() != sf::Sound::Playing) {
				soundTypes[i].erase(iter++);
			} else ++iter;
		}
	}
}
void SoundsManager::pauseAll(SoundType type) {
	for (auto iter = soundTypes[type].begin(); iter != soundTypes[type].end(); ) {
		if ((*iter).unique()) {
			soundTypes[type].erase(iter++);
		} else {
			(*iter++)->pause();
		}
	}
}
void SoundsManager::playAll(SoundType type) {
	for (auto iter = soundTypes[type].begin(); iter != soundTypes[type].end(); ++iter) {
		(*iter)->play();
	}
}
void SoundsManager::stopAll(SoundType type) {
	for (auto iter = soundTypes[type].begin(); iter != soundTypes[type].end(); ) {
		if ((*iter).unique()) {
			soundTypes[type].erase(iter++);
		} else {
			(*iter++)->stop();
		}
	}
}

void SoundsManager::addSoundToTypes(std::shared_ptr<sf::Sound> sound, SoundType type) {
	soundTypes[type].insert(sound);
}
