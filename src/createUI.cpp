#include "createUI.h"

#include "UIFactory.h"
#include "ResourceManager.h"

void createMainMenuUI(UIManager* ui);
void createGameUI(UIManager* ui);
void createPauseMenuUI(UIManager* ui);

void createUI(UIManager* ui) {
	createMainMenuUI(ui);
	createGameUI(ui);
	createPauseMenuUI(ui);
}

void createMainMenuUI(UIManager* ui) {
	ui->setScene(SCENE_MENU);

	ImageWidget* by = (ImageWidget*)UIFactory::createWidget(WIDGET_IMAGE, "byLabel", ui);
	by->setImage("resources/ui/main_label.png");

	// Add a play button
	ButtonWidget* play = (ButtonWidget*)UIFactory::createWidget(WIDGET_BUTTON, "playBtn", ui);
	//play->setPosition(sf::Vector2f(window.getSize().x / 2, 300));
	play->setNormalTexture(&ResourceManager::getInstance().getTexture("resources/ui/play_btn.png"));
	play->setHoverTexture(&ResourceManager::getInstance().getTexture("resources/ui/play_btn_hover.png"));
	//play->setCallback(playCallback);
}

void createGameUI(UIManager* ui) {
	ui->setScene(SCENE_GAME);

	// Create player health & mana bars
	UIFactory::createWidget(WIDGET_BAR, "playerHP", ui);
	UIFactory::createWidget(WIDGET_BAR, "playerMP", ui);

	// Create label for them
	ImageWidget* img1 = (ImageWidget*)UIFactory::createWidget(WIDGET_IMAGE, "barLabels", ui);
	img1->setImage("resources/ui/bar/labels.png");

	// Create minimap
	UIFactory::createWidget(WIDGET_MINIMAP, "minimap", ui);

	// Create player backpack
	BackpackWindow* bp1 = (BackpackWindow*)UIFactory::createWidget(WINDOW_BACKPACK, "playerBackpack", ui);
	bp1->setTexture(&ResourceManager::getInstance().getTexture("resources/ui/window_backpack.png"));
	bp1->setButtonTexture(&ResourceManager::getInstance().getTexture("resources/ui/window_buttons.png"));
	bp1->setPosition(sf::Vector2f(100.0f, 100.0f));
	bp1->setSize(bp1->getMinSize());

	// Create loot window
	BackpackWindow* bp2 = (BackpackWindow*)UIFactory::createWidget(WINDOW_BACKPACK, "lootBackpack", ui);
	bp2->setTexture(&ResourceManager::getInstance().getTexture("resources/ui/window.png"));
	bp2->setButtonTexture(&ResourceManager::getInstance().getTexture("resources/ui/window_buttons.png"));
	bp2->setPosition(sf::Vector2f(500.0f, 200.0f));
	bp2->setSize(bp2->getMinSize());
	bp2->setVisibility(false);

	// Create character info window
	CharInfoWindow* ci = (CharInfoWindow*)UIFactory::createWidget(WINDOW_CHARINFO, "charInfo", ui);
	ci->setTexture(&ResourceManager::getInstance().getTexture("resources/ui/window.png"));
	ci->setButtonTexture(&ResourceManager::getInstance().getTexture("resources/ui/window_buttons.png"));
	ci->setPosition(sf::Vector2f(300.0f, 200.0f));
	ci->setSize(ci->getMinSize());
	ci->setVisibility(false);

	// Create death image widget
	DeathImageWidget* diw = (DeathImageWidget*)UIFactory::createWidget(WIDGET_DEATH_IMAGE, "deathImg", ui);
	diw->setLayer(UI_ABOVE);
}

void createPauseMenuUI(UIManager* ui) {
	ui->setScene(SCENE_GAME);

	ButtonWidget* cont = (ButtonWidget*)UIFactory::createWidget(WIDGET_BUTTON, "contBtn", ui);
	//cont->setPosition(sf::Vector2f(window.getSize().x / 2, 300));
	cont->setNormalTexture(&ResourceManager::getInstance().getTexture("resources/ui/play_btn.png"));
	cont->setHoverTexture(&ResourceManager::getInstance().getTexture("resources/ui/play_btn_hover.png"));
	cont->setLayer(UI_ABOVE);
	cont->setVisibility(false);
	//cont->setCallback(contCallback);
}
