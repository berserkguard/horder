#include "Filling.h"

#include "Rand.h"
#include "MapType.h"
#include "TrapArrowAction.h"
#include "Interactible.h"

#define MIN_TRAPS 5
#define MAX_TRAPS 10
#define MIN_AMMO 2
#define MAX_AMMO 4
const sf::Vector2f RUG_POS(20, 23.5f);

Filling::Filling(Blueprint& blueprint, Map& map): blueprint(&blueprint), map(&map) { }

void Filling::fill() {
	findEmptyCells();

	addTraps();
	addMonsters();
	addTreasure();

	if (blueprint->getChunk().getPosition() == ORIGIN && map->getType().isInfinite()) {
		addLadder();
	}
}

void Filling::findEmptyCells() {
	// finds all the empty cells and adds them to a bag
	for (int x = 0; x < CHUNK_SIZE; x++) {
		for (int y = 0; y < CHUNK_SIZE; y++) {
			if (blueprint->getCell(x, y) != Blueprint::WALL) {
				emptyCells.add(Coord(x, y));
			}
		}
	}
}

void Filling::addTraps() {
	int numTraps = Rand::i(MIN_TRAPS, MAX_TRAPS + 1);
	for (int i = 0; i < numTraps; i++) {

		// choose random location and trap type
		Coord empty = emptyCells.grab();
		const TrapType* trapType = map->getType().getRandomTrap();
		if (!trapType) return; // no traps

		switch (trapType->getAction()) {
			case TrapType::ARROW: {

				std::pair<Coord, Coord> res = getRandomLongDirection(empty);
				Coord dir = res.first;
				Coord pos = res.second;
				if (dir == ORIGIN) continue; // if no direction, do not make this trap

				// construct the trap
				Trap& trap = blueprint->getChunk().addTrap(*trapType, pos, empty);
				TrapArrowAction& trigger = (TrapArrowAction&)trap.getAction();
				trigger.setAmmunition(Rand::i(MIN_AMMO, MAX_AMMO));
				trigger.setDirection(Coord(-dir.x, -dir.y));

			} break;
			case TrapType::FLAME: {
				// creating flame traps is simple
				blueprint->getChunk().addTrap(*trapType, empty);
			} break;
			default: break;
		}
	}
}

void Filling::addMonsters() {
	// monster spawning is delegated to the monster spawner
	sf::IntRect rect(blueprint->getChunk().getPositionInTiles(), Coord(1, 1) * CHUNK_SIZE);
	for (int i = 0; i < 5; i++) {
		map->getSpawner().spawnEncounter(rect);
	}
}

void Filling::addTreasure() {
	for (size_t i = 0; i < blueprint->getNumRooms(); i++) {
		addTreasureToRoom(blueprint->getRoom(i));
	}
}

int sign(float num) {
	return (0 < num) - (num < 0);
}
void Filling::addTreasureToRoom(const Blueprint::Room& room) {

	// first fill a bag with all the edges the positions on the edge of the room
	auto& walls = blueprint->getInnerWalls();
	ShuffleBag<Coord> edgePositions;
	for (size_t i = 0; i < room.wallChain.size(); i++) {
		// ...by iterating through the wall chain
		Blueprint::Wall wall = walls[room.wallChain[i]];
		int xMove = sign(wall.second.x - wall.first.x);
		int yMove = sign(wall.second.y - wall.first.y);
		if ((bool)xMove == (bool)yMove) continue;
		Coord move(xMove, yMove);
		for (Coord c = Coord(wall.first); c != Coord(wall.second); c += move) {
			for (int x = -1; x <= 0; x++) {
				for (int y = -1; y <= 0; y++) {
					// and adding all the room cells on either side of the wall
					if (blueprint->getCell(x + c.x, y + c.y) == Blueprint::ROOM) {
						edgePositions.add(Coord(x + c.x, y + c.y));
					}
				}
			}
		}
	}

	int numChests = room.cells.size() * Rand::i(1, 3) / 30;
	for (int i = 0; i < numChests; i++) {
		// place a random chest
		Coord pos = edgePositions.grab() + blueprint->getChunk().getPositionInTiles();
		TreasureChest::Type type;
		float randf = Rand::f();
		if (randf < 0.05) {
			type = TreasureChest::GOLD;
		} else if (randf < 0.25) {
			type = TreasureChest::SILVER;
		} else {
			type = TreasureChest::BRONZE;
		}
		TreasureChest* newChest = new TreasureChest(type, map->getEffects(), sf::Vector2f(pos.x + 0.5f, pos.y + 0.5f));
		map->getType().fillChest(*newChest);
		map->addEntity(Entity::Ptr(newChest));
	}
}

void Filling::addLadder() {
	// creates the map iteratible for the ladder
	Coord pos = map->getSpawner().getFloorNear(Coord(1, 1) * CHUNK_SIZE / 2);
	sf::IntRect ladderObj = map->getType().getGenObj(MapType::LADDER);
	blueprint->getChunk().addObject(pos * TILE_SIZE, ladderObj);
	Interactible& ladder = blueprint->getChunk().addInteractible("Ladder", pos);
	if (map->getType().isTop()) {
		// if on the top, it connects to the house
		ladder.setConnection(UP, RUG_POS);
	} else {
		// otherwise, it connects to the previous floor
		ladder.setConnection(UP, sf::Vector2f(1, 1) * (CHUNK_SIZE / 2.f));
	}
}

std::pair<Coord, Coord> Filling::getRandomLongDirection(Coord pos) {
	// chooses a random direction from 'pos' that does not immediately run into a wall

	// fill bag with directions
	ShuffleBag<Direction> directionBag;
	directionBag.add(NORTH);
	directionBag.add(SOUTH);
	directionBag.add(EAST);
	directionBag.add(WEST);
	Coord dir, cur;

	// grab random ones until one works
	while (dir == ORIGIN && directionBag.size()) {
		Coord randDir = dirVector(directionBag.grab());
		Coord c = pos + randDir;
		if (Chunk::inBounds(c) && blueprint->getChunk().getTile(c).isPath()) {
			cur = pos + randDir;
			bool success = true;
			int dist = 0;
			while (blueprint->getChunk().getTile(cur).isPath()) {
				cur += randDir;
				dist++;
				if (dist > 10 || !Chunk::inBounds(cur)) {
					success = false;
					break;
				}
			}
			if (success) {
				cur -= randDir;
				dir = randDir;
			}
		}
	}
	return std::make_pair(dir, cur);
}
