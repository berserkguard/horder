#include "Inventory.h"

Inventory::Inventory() { }

ItemContainer& Inventory::getItemContainer(){
	return items;
}

float Inventory::getWeight() {
    return items.getWeight();
}

Item& Inventory::getEquippedItem(EquipSlot slot) {
	return equipment[(int)slot];
}

bool Inventory::equipItem(Item item) {
	if (item.isNull()) return false;
	if (!item.getType().isEquippable()) return false;

	EquipSlot slot = item.getType().getSlot();
	if (equipment[(int)slot].isNull()) {
		equipment[(int)slot] = item;
		return true;
	}
	return false;
}
Item Inventory::unequipItem(EquipSlot slot) {
	Item item = equipment[(int)slot];
	equipment[(int)slot] = Item();
	return item;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("item/inventory", "Test Inventory.") {
	Inventory inventory;

	REQUIRE(inventory.getWeight() == 0);
	Item nullItem;
	REQUIRE(inventory.equipItem(nullItem) == false);
}
