#include "ItemType.h"

ItemType::ItemType(String name, int id, Coord gLoc, float weight): name(name), id(id), weight(weight), graphicLoc(gLoc) {
	slot = EquipSlot::COUNT;
	for (int i = 0; i < Stat::COUNT; i++) {
		stats[i] = 0.0f;
	}
}

String ItemType::getName() const {
	return name;
}

int ItemType::getID() const {
	return id;
}

float ItemType::getWeight() const {
	return weight;
}

Coord ItemType::getGLoc() const {
	return graphicLoc;
}

EquipSlot ItemType::getSlot() const {
	return slot;
}

float ItemType::getStatValue(Stat::Type stat) const {
	assert(stat >= 0 && stat < Stat::COUNT);
	return stats[stat];
}
void ItemType::setStatValue(Stat::Type stat, float value) {
	assert(stat >= 0 && stat < Stat::COUNT);
	stats[stat] = value;
}

Coord ItemType::getEquipGLoc() const {
	return equipGraphicLoc;
}

bool ItemType::isEquippable() const {
	return slot != EquipSlot::COUNT;
}

void ItemType::setEquip(EquipSlot equipSlot, Coord equipGLoc) {
	slot = equipSlot;
	equipGraphicLoc = equipGLoc;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("item/type", "Test ItemType.") {
	ItemType itemType("Test Item", 3, Coord(3, 0), 0.2);
	itemType.setStatValue(Stat::HP, 1);
	itemType.setStatValue(Stat::DEFENSE, 9.9f);
	REQUIRE(itemType.getName() == "Test Item");
	REQUIRE(itemType.getID() == 3);
	REQUIRE(itemType.getWeight() > 0);
	REQUIRE(itemType.getGLoc() == Coord(3, 0));
	REQUIRE(itemType.getStatValue(Stat::HP) == 1);
	REQUIRE(itemType.getStatValue(Stat::DEFENSE) == 9.9f);
	REQUIRE(itemType.getStatValue(Stat::ACCURACY) == 0);
	REQUIRE(itemType.getStatValue(Stat::EVASION) == 0);
	REQUIRE(itemType.isEquippable() == false);
}
