#include "ItemContainer.h"

#include "ItemType.h"

ItemContainer::ItemContainer() : sortfunc(SORT_NONE), autoSort(false), weight(0.0), limit(UINT_MAX) { }
ItemContainer::ItemContainer(SortType sortfunc, bool autoSort) : sortfunc(sortfunc), autoSort(autoSort), weight(0.0), limit(UINT_MAX) { }

bool ItemContainer::addItem(Item item) {
	if (items.size() == limit) return false;
	items.push_back(item);
	if(autoSort) {
		// Auto-sort is on, so re-sort after adding an item
		sort(sortfunc); // TODO optimize (insert instead of re sort)
	}
	weight += item.getWeight();
	return true;
}

void ItemContainer::setLimit(unsigned int lim) {
	limit = lim;
}

Item ItemContainer::removeItemAt(int idx) {
	Item item = getItemAt(idx);
	items.erase(items.begin() + idx);
	weight -= item.getWeight();
	if (getNumItems() == 0) {
		weight = 0.0;
	}
	return item;
}

Item& ItemContainer::getItemAt(int idx) {
	return items.at(idx);
}

void ItemContainer::sort(SortType sortfunc) {
	switch(sortfunc) {
		case SORT_ID:
			std::sort(items.begin(), items.end(), ItemContainer::sortByID);
			break;
		case SORT_NAME:
			std::sort(items.begin(), items.end(), ItemContainer::sortByName);
		default: break;
	}
}

int ItemContainer::getNumItems() {
	return items.size();
}

int ItemContainer::getMaxSize() {
	return -1;
}

float ItemContainer::getWeight() {
	return weight;
}

void ItemContainer::clear() {
	items.clear();
	weight = 0;
}

bool ItemContainer::sortByID(Item item1, Item item2) {
	return item1.getType().getID() < item2.getType().getID();
}
bool ItemContainer::sortByName(Item item1, Item item2) {
	return item1.getType().getName() < item2.getType().getName();
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("item/container", "Test ItemContainer.") {
	ItemContainer container;

	ItemType* type1 = new ItemType("mango", 1, ORIGIN, 1);
	ItemType* type2 = new ItemType("celery", 99, ORIGIN, 0);
	ItemType* type3 = new ItemType("fish", 3, ORIGIN, 2);

	container.addItem(Item(*type1));
	container.addItem(Item(*type2));
	container.addItem(Item(*type2));
	container.addItem(Item(*type3));
	container.addItem(Item(*type1));

	REQUIRE(container.getNumItems() == 5);
	REQUIRE(container.getWeight() >= 3.99);
	REQUIRE(container.getWeight() <= 4.01);

	container.sort(SORT_ID);
	REQUIRE(container.getNumItems() == 5);
	REQUIRE(&container.getItemAt(0).getType() == type1);
	REQUIRE(&container.getItemAt(2).getType() == type3);
	REQUIRE(&container.getItemAt(3).getType() == type2);

	container.sort(SORT_NAME);
	REQUIRE(&container.getItemAt(0).getType() == type2);
	REQUIRE(&container.getItemAt(2).getType() == type3);
	REQUIRE(&container.getItemAt(3).getType() == type1);
}
