#include "Item.h"

#include "ItemType.h"

unsigned short ii = 0;

Item::Item(): type(NULL) {
	i = ii++;
}
Item::Item(const ItemType& itemType): type(&itemType) {
	i = ii++;
}

const ItemType& Item::getType() const {
    assert(type);
    return *type;
}

float Item::getWeight() const {
	if (isNull()) return 0;
	else return type->getWeight();
}

bool Item::isNull() const {
	return !type;
}

int Item::getUnique() const {
	return i;
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("item/basic", "Test Item.") {
	YAML::Node node = YAML::LoadFile("unittest/item.yaml");
	ItemType itemType("Apple", 1, ORIGIN, 1.2f);
	Item item1(itemType);
	Item item2(itemType);
	REQUIRE(item1.isNull() == false);
	REQUIRE(item1.getUnique() != item2.getUnique());
	REQUIRE(item1.getWeight() == itemType.getWeight());
	REQUIRE(item1.getType().getSlot() == itemType.getSlot());
}
