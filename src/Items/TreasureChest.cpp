#include "TreasureChest.h"

#include "VecUtil.h"
#include "Tile.h"
#include "Place.h"
#include "ResourceManager.h"

b2BodyDef* bodyDef;
b2BodyDef& initBody(sf::Vector2f position) {
	bodyDef = new b2BodyDef();
	bodyDef->type = b2_dynamicBody;
	bodyDef->userData = NULL;
	bodyDef->position = toPhysVec(position);
	bodyDef->fixedRotation = true;
	bodyDef->linearDamping = 0.99;
	return *bodyDef;
}

TreasureChest::TreasureChest(Type type, EffectsManager& effects, sf::Vector2f position)
		: Entity(CHEST, initBody(position), effects), type(type), opened(false) {
	delete bodyDef;
	b2Body& body = getCollisionBody();

	b2PolygonShape rect;
	rect.SetAsBox(18.f / TILE_SIZE, 10.f / TILE_SIZE);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &rect;
	fixtureDef.density = 10.0f;
	fixtureDef.userData = NULL;
	body.CreateFixture(&fixtureDef);

	spriteLayers.emplace_back();
	sf::Sprite& sprite = spriteLayers[0];
	sprite.setTexture(getTexture(type, false));
	sprite.setOrigin(28, 32);
}

void TreasureChest::addItem(Item item) {
	items.push_back(item);
	if (opened) {
		opened = false;
		spriteLayers[0].setTexture(getTexture(type, opened));
	}
}

void TreasureChest::open(Place& place) {
	if (!opened) {
		Coord coord = vecFloor(getPosition());
		opened = true;
		for (size_t i = 0; i < items.size(); i++) {
			place.addItem(coord, items[i]);
		}
		items.clear();
		spriteLayers[0].setTexture(getTexture(type, opened));
	}
}

TreasureChest::Type TreasureChest::getChestType() {
	return type;
}

sf::Texture* TreasureChest::bronzeClosed = nullptr;
sf::Texture* TreasureChest::bronzeOpen   = nullptr;
sf::Texture* TreasureChest::silverClosed = nullptr;
sf::Texture* TreasureChest::silverOpen   = nullptr;
sf::Texture* TreasureChest::goldClosed   = nullptr;
sf::Texture* TreasureChest::goldOpen     = nullptr;

sf::Texture& TreasureChest::getTexture(Type type, bool open) {
	if (!bronzeClosed) {
		bronzeClosed = &ResourceManager::getInstance().getTexture("resources/chests/chest_bronze.png");
		bronzeOpen   = &ResourceManager::getInstance().getTexture("resources/chests/chest_bronze_open.png");
		silverClosed = &ResourceManager::getInstance().getTexture("resources/chests/chest_silver.png");
		silverOpen   = &ResourceManager::getInstance().getTexture("resources/chests/chest_silver_open.png");
		goldClosed   = &ResourceManager::getInstance().getTexture("resources/chests/chest_gold.png");
		goldOpen     = &ResourceManager::getInstance().getTexture("resources/chests/chest_gold_open.png");
	}
	if (open) {
		switch(type) {
			case BRONZE: return *bronzeOpen; break;
			case SILVER: return *silverOpen; break;
			case GOLD:   return *goldOpen;   break;
			default: break;
		}
	} else {
		switch(type) {
			case BRONZE: return *bronzeClosed; break;
			case SILVER: return *silverClosed; break;
			case GOLD:   return *goldClosed;   break;
			default: break;
		}
	}
	return *bronzeClosed;
}
