#include "Game.h"

#include "Map.h"
#include "World.h"
#include "MobSpawner.h"
#include "MapType.h"
#include "EffectsManager.h"

Game::Game() {
	state = IN_GAME;
	paused = false;

	physWorld = std::unique_ptr<b2World>(new b2World(b2Vec2(0.0f, 0.0f)));
	world     = std::unique_ptr<World>(new World(getValuables(), effects));
	physWorld->SetContactListener(world.get());
}

GameState Game::getState() {
    return state;
}

World& Game::getWorld() {
    return *world;
}

Settings& Game::getSettings() {
    return settings;
}

b2World& Game::getPhysWorld() {
    return *physWorld;
}

ValuablesManager& Game::getValuables() {
	return valuablesManager;
}

bool Game::load(String file) {
	std::cout << "Parsing YAML files..." << std::endl;
	if (file.substr(file.size() - 4) != "yaml" && file.substr(file.size() - 3) != "yml") return false;
	try {
		YAML::Node node = YAML::LoadFile(file);

		std::cout << " Parsing default settings..." << std::endl;
		YAML::Node settingsNode = yamlWithin(node, "DefaultSettings");
		if (settingsNode == node) return false;
		if (!settings.loadYAML(settingsNode)) return false;

		std::cout << " Parsing custom settings..." << std::endl;
		settingsNode = yamlWithin(node, "CustomSettings");
		if (settingsNode == node) return false;
		if (!settings.loadYAML(settingsNode)) return false;

		std::cout << " Parsing tiles.." << std::endl;
		std::vector<YAML::Node> tileNodes = yamlWithinAll(node, "Tiles");
		for (auto iter = tileNodes.begin(); iter != tileNodes.end(); ++iter) {
			valuablesManager.loadTileType(*iter);
		}

		std::cout << " Parsing items..." << std::endl;
		std::vector<YAML::Node> itemNodes = yamlWithinAll(node, "Items");
		for (auto iter = itemNodes.begin(); iter != itemNodes.end(); ++iter) {
			valuablesManager.loadItemType(*iter);
		}

		std::cout << " Parsing models..." << std::endl;
		std::vector<YAML::Node> modelNodes = yamlWithinAll(node, "Models");
		for (auto iter = modelNodes.begin(); iter != modelNodes.end(); ++iter) {
			valuablesManager.loadModel(*iter);
		}

		std::cout << " Parsing monsters..." << std::endl;
		std::vector<YAML::Node> monsterNodes = yamlWithinAll(node, "Monsters");
		for (auto iter = monsterNodes.begin(); iter != monsterNodes.end(); ++iter) {
			valuablesManager.loadMonsterType(*iter);
		}

		std::cout << " Parsing traps..." << std::endl;
		std::vector<YAML::Node> trapNodes = yamlWithinAll(node, "Traps");
		for (auto iter = trapNodes.begin(); iter != trapNodes.end(); ++iter) {
			valuablesManager.loadTrapType(*iter);
		}

		std::cout << " Parsing maps..." << std::endl;
		std::vector<YAML::Node> mapNodes = yamlWithinAll(node, "Maps");
		std::vector<const MapType*> loadedMaps;
		for (auto iter = mapNodes.begin(); iter != mapNodes.end(); ++iter) {
			const MapType* mapType = valuablesManager.loadMapType(*iter);
			if (mapType) loadedMaps.push_back(mapType);
		}
		valuablesManager.finishLoading();
		for (size_t i = 0; i < loadedMaps.size(); i++) {
			world->addMap(*loadedMaps[i]);
		}
		world->connectMaps();

		std::cout << " Parsing player..." << std::endl;
		YAML::Node playerNode = yamlWithin(node, "Player");
		if (playerNode == node) return false;
		world->getPlayer().loadYAML(playerNode, *world->getMap(playerNode["Map"].as<String>()), valuablesManager);



		return true;
	} catch(YAML::ParserException& ex) {
		std::cout << ex.what() << "\n";
		return false;
	}
}

YAML::Node Game::yamlWithin(YAML::Node& node, String key) {
    String file = node[key].as<String>();
    try {
        YAML::Node withinNode = YAML::LoadFile("data/" + file);
        return withinNode;
    } catch(YAML::ParserException& ex) {
        std::cout << ex.what() << "\n";
        return node;
    }
}

std::vector<YAML::Node> Game::yamlWithinAll(YAML::Node& node, String key) {
    String file = node[key].as<String>();
    try {
        std::vector<YAML::Node> withinVect = YAML::LoadAllFromFile("data/" + file);
        return withinVect;
    } catch(YAML::ParserException& ex) {
        std::cout << ex.what() << "\n";
        return std::vector<YAML::Node>();
    }
}

void Game::setPaused(bool value) {
	paused = value;
}
bool Game::isPaused() {
	return paused;
}

EffectsManager& Game::getEffects() {
	return effects;
}
