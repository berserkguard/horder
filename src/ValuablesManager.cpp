#include "ValuablesManager.h"

#include "StringMatcher.h"

// ITEMS //
const ItemType* ValuablesManager::getItemType(size_t index) const {
	if (index >= itemTypes.size()) {
		return nullptr;
	}
	return itemTypes.at(index).get();
}
const ItemType* ValuablesManager::getItemType(String name) const {
	auto iter = itemTypeNameMap.find(name);
	if (iter == itemTypeNameMap.end()) {
		return nullptr;
	}
	return iter->second;
}

String sStrings[] = {         "head",           "hand",          "offhand",          "body",          "toes"};
EquipSlot sInts[] = {EquipSlot::HEAD, EquipSlot::HAND, EquipSlot::OFFHAND, EquipSlot::BODY, EquipSlot::TOES};
StringMatcher strToSlotMatch(sStrings, (int*)sInts, (int)EquipSlot::COUNT, (int)EquipSlot::NONE);
const ItemType* ValuablesManager::loadItemType(YAML::Node& node) {
	String name = readYAMLStr(node, "Name", "???", "WARNING: An item is missing a name.");
	float weight = strToWeight(readYAMLStr(node, "Weight", "0 g"));
	int id = itemTypes.size();
	Coord gloc = pairToCoord(readYAMLPair(node, "GLoc", 0, "Item lacks a GLoc!"));

	ItemType* newItemType = new ItemType(name, id, gloc, weight);
	itemTypes.emplace_back(newItemType);
	itemNodes.push_back(node);
	itemTypeNameMap[newItemType->getName()] = newItemType;

	EquipSlot slot = (EquipSlot)strToSlotMatch.match(readYAMLStr(node, "Equip", "none"));
	gloc = pairToCoord(readYAMLPair(node, "EquipGLoc", 0));
	newItemType->setEquip(slot, gloc);

	YAML::Node statsNode = node["Stats"];
	if (statsNode.IsMap()) {
		for (auto iter = statsNode.begin(); iter != statsNode.end(); ++iter) {
			String statName = iter->first.as<String>();
			Stat::Type s = Stat::fromString(statName);
			if (s == Stat::NONE) {
				std::cout << "'" << statName << "' is not a stat name." << std::endl;
				continue;
			}
			newItemType->setStatValue(s, iter->second.as<float>());
		}
	}

	return newItemType;
}
void ValuablesManager::finishItemType(YAML::Node& node, ItemType& type) {
	// does nothing, yet...
}
// END ITEMS //


// MONSTERS //
const MonsterType* ValuablesManager::getMonsterType(size_t index) const {
	if (index >= monsterTypes.size()) {
		return nullptr;
	}
	return monsterTypes.at(index).get();
}
const MonsterType* ValuablesManager::getMonsterType(String name) const {
	auto iter = monsterTypeNameMap.find(name);
	if (iter == monsterTypeNameMap.end()) {
		return nullptr;
	}
	return iter->second;
}
const MonsterType* ValuablesManager::loadMonsterType(YAML::Node& node) {
	String name = readYAMLStr(node, "Name", "???", "WARNING: A monster is missing a name.");
	int id = monsterTypes.size();

	MonsterType* newMonsterType = new MonsterType(name, id);
	monsterTypes.emplace_back(newMonsterType);
	monsterNodes.push_back(node);
	monsterTypeNameMap[newMonsterType->getName()] = newMonsterType;

	// parse the graphic
	sf::Color color = sf::Color::White;
	YAML::Node colorNode = node["Color"];
	if (colorNode) color = strToColor(colorNode.as<String>());
	const Model* model = getModel(readYAMLStr(node, "Model", ""));
	if (model) newMonsterType->setGraphic(*model, color);

	// parse the sound
	String soundName = toLowerCase(readYAMLStr(node, "Sound", ""));
	if (soundName == "slime") {
		newMonsterType->setSound(S_DEATH_SLIME);
	} else if (soundName == "dorklen") {
		newMonsterType->setSound(S_DEATH_DORKLEN);
	}

	// parse the stats
	YAML::Node statsNode = node["Stats"];
	if (statsNode.IsMap()) {
		for (auto iter = statsNode.begin(); iter != statsNode.end(); ++iter) {
			String statName = iter->first.as<String>();
			Stat::Type s = Stat::fromString(statName);
			if (s == Stat::NONE) {
				std::cout << "'" << statName << "' is not a stat name." << std::endl;
				continue;
			}
			newMonsterType->setStatValue(s, iter->second.as<float>());
		}
	}

	// parse the AI
	YAML::Node aiNode = node["AI"];
	if (aiNode) {
		String aiStr = aiNode.as<String>();
		if (aiStr == "Sprint")   newMonsterType->setAI(AI_SPRINT);
		else if (aiStr == "Hop") newMonsterType->setAI(AI_HOP);
	}

	return newMonsterType;
}
void ValuablesManager::finishMonsterType(YAML::Node& node, MonsterType& monsterType) {
	// parse the drops
	YAML::Node dropsNode = node["Drops"];
	if (dropsNode.IsSequence()) {
		for (auto iter = dropsNode.begin(); iter != dropsNode.end(); ++iter) {
			String itemName = iter->as<String>();
			const ItemType* itemType = getItemType(itemName);
			if (itemType) {
				monsterType.addDrop(*itemType);
			} else std::cout << "'" << itemName << "' is not an existing item." << std::endl;
		}
	}
}
// END MONSTERS


// TILES
const TileType* ValuablesManager::getTileType(String name) const {
	auto iter = tileTypeNameMap.find(name);
	if (iter == tileTypeNameMap.end()) {
		return nullptr;
	}
	return iter->second;
}
const TileType* ValuablesManager::getTileType(Coord coord) const {
	auto iter = tileTypeCoordMap.find(coord);
	if (iter == tileTypeCoordMap.end()) {
		return nullptr;
	}
	return iter->second;
}
const TileType* ValuablesManager::createTileType(Coord gloc) {
	if (gloc.x < 0 || gloc.y < 0 || gloc.x >= TILE_TEX_SIZE || gloc.y >= TILE_TEX_SIZE) return nullptr;
	TileType* newTileType = new TileType("", gloc);
	tileTypes.emplace_back(newTileType);
	tileTypeCoordMap[gloc] = newTileType;
	return newTileType;
}
const TileType* ValuablesManager::loadTileType(YAML::Node& node) {
	String name = readYAMLStr(node, "Name", "");
	Coord gloc = pairToCoord(readYAMLPair(node, "GLoc", 0, "Tile lacks a GLoc!"));

	TileType* newTileType = new TileType(name, gloc);
	tileTypes.emplace_back(newTileType);
	tileTypeNameMap[name] = newTileType;
	tileTypeCoordMap[gloc] = newTileType;

	String materialName = readYAMLStr(node, "Material", "");
	if (materialName == "Grass") {
		newTileType->setMaterial(TileType::GRASS);
	} else if (materialName == "Wood") {
		newTileType->setMaterial(TileType::WOOD);
	} else if (materialName == "Stone") {
		newTileType->setMaterial(TileType::STONE);
	} else if (materialName != "") std::cout << "'" + materialName + "' is not a valid material." << std::endl;

	return newTileType;
}
// END TILES


// MAPS
const MapType* ValuablesManager::getMapType(String name) const {
	auto iter = mapTypeNameMap.find(name);
	if (iter == mapTypeNameMap.end()) {
		return nullptr;
	}
	return iter->second;
}

String fStrings[]   = {"Persistent",        "Boundless",        "Dark",        "Gross",        "Top",         "Home"};
int fInts[] = {MapType::PERSISTENT, MapType::BOUNDLESS, MapType::DARK, MapType::GROSS, MapType::TOP,  MapType::HOME};
StringMatcher strToFlagMatch(fStrings, fInts, 6, -1);
const MapType* ValuablesManager::loadMapType(YAML::Node& node) {
	String name = readYAMLStr(node, "Name", "???", "Map name not specified!");

	YAML::Node traitsNode = node["Traits"];
	int flags = 0;
	if (traitsNode.IsSequence()) {
		for (auto jter = traitsNode.begin(); jter != traitsNode.end(); ++jter) {
			int flag = strToFlagMatch.match(jter->as<String>());
			if (flag == -1) std::cout << "'" << jter->as<String>() << "' is not an applicable map trait." << std::endl;
			else flags |= flag;
		}
	}

	MapType* newMapType = new MapType(name, flags);
	mapTypes.emplace_back(newMapType);
	mapNodes.push_back(node);
	mapTypeNameMap[name] = newMapType;

	newMapType->setFile(readYAMLStr(node, "File", ""));

	return newMapType;
}
void ValuablesManager::finishMapType(YAML::Node& node, MapType& mapType) {
	// parse treasure chests
	YAML::Node chestsNode = node["Chests"];
	for (auto iter = chestsNode.begin(); iter != chestsNode.end(); ++iter) {
		String typeStr = iter->first.as<String>();
		TreasureChest::Type type = TreasureChest::BRONZE;
		if (typeStr == "Bronze") type = TreasureChest::BRONZE;
		else if (typeStr == "Silver") type = TreasureChest::SILVER;
		else if (typeStr == "Gold") type = TreasureChest::GOLD;
		else std::cout << "'" << typeStr << "' is dumb." << std::endl;

		YAML::Node itemsNode = iter->second;
		assert(itemsNode.IsSequence());
		for (auto jter = itemsNode.begin(); jter != itemsNode.end(); ++jter) {
			String itemName = jter->as<String>();
			const ItemType* itemType = getItemType(itemName);
			if (itemType) {
				mapType.addChestItem(type, *itemType);
			} else std::cout << "'" << typeStr << "' is not the name of an item type." << std::endl;
		}
	}

	// parse traps
	YAML::Node trapsNode = node["Traps"];
	if (trapsNode.IsSequence()) {
		for (auto iter = trapsNode.begin(); iter != trapsNode.end(); ++iter) {
			const TrapType* trap = getTrapType(iter->as<String>());
			if (trap) mapType.addTrap(trap);
			else std::cout << "'" << iter->as<String>() << "' is not the name of a trap type." << std::endl;
		}
	}

	// parse connections
	YAML::Node connectionsNode = node["Connections"];
	for (auto iter = connectionsNode.begin(); iter != connectionsNode.end(); ++iter) {
		String direction = iter->first.as<String>();
		String mapName = iter->second.as<String>();
		const MapType* connectedTo = getMapType(mapName);
		if (connectedTo) mapType.addAdjacentMap(strToDir(direction), *connectedTo);
		else std::cout << "'" << mapName << "' is not an existing map type." << std::endl;
	}

	// parse monsters
	YAML::Node monstersNode = node["Monsters"];
	if (monstersNode.IsMap()) {
		for (auto iter = monstersNode.begin(); iter != monstersNode.end(); ++iter) {
			unsigned int level = iter->first.as<unsigned int>();
			YAML::Node levelNode = iter->second;
			for (auto jter = levelNode.begin(); jter != levelNode.end(); ++jter) {
				YAML::Node encounterNode = *jter;

				String monsterName = readYAMLStr(encounterNode, "Mon", "", "Mon expected in encounter.");
				const MonsterType* monsterType = getMonsterType(monsterName);

				if (monsterType) {
					Encounter encounter(*monsterType);
					Encounter::Rarity rarity = Encounter::strToRarity(readYAMLStr(encounterNode, "Rarity", "rare"));
					encounter.setRarity(rarity);
					std::pair<int, int> range = readYAMLPair(encounterNode, "Quantity", 1);
					encounter.setSize(range.first, range.second);

					mapType.addEncounter(level, encounter);

				} else if (monsterName != "") std::cout << "'" << monsterName << " is not the name of a monster." << std::endl;
			}
		}
	}

	// parse generation
	YAML::Node genNode = node["Generation"];
	if (genNode) {
		for (auto jter = genNode.begin(); jter != genNode.end(); ++jter) {
			String genTypeStr = jter->first.as<String>();
			MapType::GenTile genTileType = MapType::FLOOR;
			if      (genTypeStr == "Wall")  genTileType = MapType::WALL;
			else if (genTypeStr == "Floor") genTileType = MapType::FLOOR;
			else if (genTypeStr == "Room")  genTileType = MapType::ROOM;
			else if (genTypeStr == "Ladder") {
				YAML::Node rectNode = jter->second;
				sf::IntRect rect = sf::IntRect(rectNode[0].as<int>(), rectNode[1].as<int>(), rectNode[2].as<int>(), rectNode[3].as<int>());
				mapType.setGenObj(MapType::LADDER, rect);
				continue;
			}
			String tileName = jter->second.as<String>();
			const TileType* tileType = getTileType(tileName);
			if (tileType) {
				mapType.setGenTile(genTileType, *tileType);
			} else std::cout << "'" << tileName << "' is not the name of a tile." << std::endl;
		}
	}
}
// END MAPS


// TRAPS
const TrapType* ValuablesManager::getTrapType(String name) const {
	auto iter = trapTypeNameMap.find(name);
	if (iter == trapTypeNameMap.end()) {
		return nullptr;
	}
	return iter->second;
}
const TrapType* ValuablesManager::loadTrapType(YAML::Node& node) {
	trapNodes.push_back(node);
	TrapType* newTrapType = new TrapType();
	trapTypes.emplace_back(newTrapType);
	trapTypeNameMap[readYAMLStr(node, "Name", "", "Trap types need names.")] = newTrapType;
	return newTrapType;
}
void ValuablesManager::finishTrapType(YAML::Node& node, TrapType& trapType) {
	YAML::Node triggerNode = node["Trigger"];
	if (triggerNode) {
		float duration = readYAMLNum(triggerNode, "Wait", 0);
		String typeName = readYAMLStr(triggerNode, "Type", "None", "Trap triggers need types.");
		TrapType::Trigger triggerType = TrapType::NO_TRIGGER;
		if (typeName == "Pressure Plate") triggerType = TrapType::PRESSURE_PLATE;
		else if (typeName == "Timer")     triggerType = TrapType::TIMER;
		trapType.setTrigger(triggerType, duration);

		for (auto iter = triggerNode["Graphic"].begin(); iter != triggerNode["Graphic"].end(); ++iter) {
			YAML::Node rectNode = *iter;
			sf::IntRect rect = sf::IntRect(rectNode[0].as<int>(), rectNode[1].as<int>(), rectNode[2].as<int>(), rectNode[3].as<int>());
			trapType.addTriggerGraphic(rect);
		}
	}
	YAML::Node actionNode = node["Action"];
	if (actionNode) {
		const ItemType* item = getItemType(readYAMLStr(actionNode, "Item", ""));
		String typeName = readYAMLStr(actionNode, "Type", "None", "Trap actions need types.");
		TrapType::Action actionType = TrapType::NO_ACTION;
		if (typeName == "Shoot")      actionType = TrapType::ARROW;
		else if (typeName == "Flame") actionType = TrapType::FLAME;
		trapType.setAction(actionType, item);

		for (auto iter = actionNode["Graphic"].begin(); iter != actionNode["Graphic"].end(); ++iter) {
			YAML::Node rectNode = *iter;
			sf::IntRect rect = sf::IntRect(rectNode[0].as<int>(), rectNode[1].as<int>(), rectNode[2].as<int>(), rectNode[3].as<int>());
			trapType.addActionGraphic(rect);
		}
	}
}
// END TRAPS


// MODELS
const Model* ValuablesManager::getModel(String name) const {
	auto iter = modelNameMap.find(name);
	if (iter == modelNameMap.end()) {
		return nullptr;
	}
	return iter->second;
}
const Model* ValuablesManager::loadModel(YAML::Node& node) {
	Model* newModel = new Model(readYAMLStr(node, "Name", "", "Model requires name."));
	newModel->loadYAML(node);
	models.emplace_back(newModel);
	modelNameMap[newModel->getName()] = newModel;
	return newModel;
}
// END MODELS


void ValuablesManager::finishLoading() {
	for (size_t i = 0; i < itemNodes.size(); i++) {
		finishItemType(itemNodes[i], *itemTypes[i].get());
	}
	itemNodes.clear();
	for (size_t i = 0; i < monsterNodes.size(); i++) {
		finishMonsterType(monsterNodes[i], *monsterTypes[i].get());
	}
	monsterNodes.clear();
	for (size_t i = 0; i < mapNodes.size(); i++) {
		finishMapType(mapNodes[i], *mapTypes[i].get());
	}
	mapNodes.clear();
	for (size_t i = 0; i < trapNodes.size(); i++) {
		finishTrapType(trapNodes[i], *trapTypes[i].get());
	}
	trapNodes.clear();

	itemTypes.shrink_to_fit();
	monsterTypes.shrink_to_fit();
	models.shrink_to_fit();
	tileTypes.shrink_to_fit();
	mapTypes.shrink_to_fit();
	trapTypes.shrink_to_fit();
}

#include "catch.hpp"
//* UNIT TESTS
TEST_CASE("valuables/manager", "Test loading and retrieving.") {
	ValuablesManager valuables;

	YAML::Node itemNode    = YAML::LoadFile("unittest/item.yaml");
	YAML::Node mapNode     = YAML::LoadFile("unittest/map.yaml");
	YAML::Node monsterNode = YAML::LoadFile("unittest/monster.yaml");
	YAML::Node tileNode    = YAML::LoadFile("unittest/tile.yaml");
	const ItemType*   itemType = valuables.loadItemType(itemNode);
	const MapType*     mapType = valuables.loadMapType(mapNode);
	const MonsterType* monType = valuables.loadMonsterType(monsterNode);
	const TileType*   tileType = valuables.loadTileType(tileNode);
	valuables.finishLoading();

	SECTION("The loaded stuff is retrievable.") {
		REQUIRE(valuables.getItemType(itemType->getName()) == itemType);
		REQUIRE(valuables.getMapType(mapType->getName())   == mapType);
		REQUIRE(valuables.getMonsterType(monType->getID()) == monType);
		REQUIRE(valuables.getTileType(tileType->getGLoc()) == tileType);
	}
	SECTION("Item type has correct values.") {
		REQUIRE(itemType->getName() == "Test Item");
		REQUIRE(itemType->getGLoc() == Coord(3, 0));
		REQUIRE(itemType->getStatValue(Stat::HP) == 1);
		REQUIRE(itemType->getWeight() > 0);
	}
	SECTION("Map type has correct values.") {
		REQUIRE(mapType->getName() == "Test Cellar");
		REQUIRE(&mapType->getEncounters(0)->grab().getMonster() == monType);
		REQUIRE(mapType->isDark());
		REQUIRE(&mapType->getGenTile(MapType::FLOOR) == tileType);
	}
	SECTION("Monster type has correct values.") {
		REQUIRE(monType->getName() == "Test Monster");
		REQUIRE(monType->getAI() == AI_HOP);
		REQUIRE(monType->getStatValue(Stat::HP) == 4);
		REQUIRE(monType->getRandomDrop() == itemType);
	}
	SECTION("Tile type has correct values.") {
		REQUIRE(tileType->getName() == "Test Tile");
		REQUIRE(tileType->getMaterial() == TileType::STONE);
		REQUIRE(tileType->getGLoc() == Coord(1, 7));
	}
}
