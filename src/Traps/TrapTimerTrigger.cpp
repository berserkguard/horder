#include "TrapTimerTrigger.h"

TrapTimerTrigger::TrapTimerTrigger(TrapAction& trapAction): TrapTrigger(trapAction), time(0.f) { }

void TrapTimerTrigger::update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) {
	time += diff.sec();
	if (time > getType().getTriggerDuration()) {
		time -= getType().getTriggerDuration();
		trigger();
	}
}
