#include "TrapFlameAction.h"

#include "Place.h"
#include "Mob.h"
#include "Rand.h"

TrapFlameAction::TrapFlameAction(const TrapType& type, Place& place, EffectsManager& effects): TrapAction(type, place, effects) {
	on = false;
	increm = 0;
}

void TrapFlameAction::setPosition(Coord pos) {
	position = pos;
}

void TrapFlameAction::activate() {
	on = !on;
	if (on) {
		// play sound
		sf::Vector2f fpos(position.x + 0.5f, position.y + 0.5f);
		if (!claimedSound || claimedSound->getStatus() != sf::Sound::Playing) {
			claimedSound = getEffects().sound().newSound(S_FLAME, fpos);
			claimedSound->setAttenuation(3);
			claimedSound->play();
		}
	} else {
		claimedSound->stop();
	}
}

void TrapFlameAction::update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) {
	if (!on) return;

	// damage all mobs on the flame
	for (auto iter = entitiesOver.begin(); iter != entitiesOver.end(); ++iter) {
		if ((*iter)->getType() == Entity::MOB) {
			Mob& mob = (Mob&)**iter;
			mob.getDamaged(FLAME_DAMAGE_PER_SEC * diff.sec(), 0, Mob::NO_TEXT | Mob::NO_BLOCK | Mob::NO_SOUND);
		}
	}

	// animate
	sf::Color c = sf::Color(255, 127, 0);
	if (increm++ % 2) c = sf::Color::Red;
	sf::Vector2f pos = sf::Vector2f(position) + sf::Vector2f(Rand::f(.5f) - .25f, Rand::f(.5f) - .25f);
	PoofEffect& poof = getEffects().graphical().createPoofEffect(0.5f, 0.5f, pos);
	poof.setColor(c);
}
