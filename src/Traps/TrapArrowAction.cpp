#include "TrapArrowAction.h"

#include "ResourceManager.h"
#include "Place.h"
#include "EffectsManager.h"

TrapArrowAction::TrapArrowAction(const TrapType& action, Place& place, EffectsManager& effects)
		: TrapAction(action, place, effects) {
	ammo = -1;
}

void TrapArrowAction::setPosition(Coord pos) {
	position = pos;
}

void TrapArrowAction::setDirection(Coord dir) {
	direction = dir;
	MapObject* obj = getMapObject();
	if (obj) {
		obj->setRotation(vecAngle(sf::Vector2f(-dir.x, -dir.y)));
	}
}

void TrapArrowAction::setAmmunition(int a) {
	ammo = a;
}

void TrapArrowAction::activate() {
	if (!ammo) return;
	ammo--;

	sf::Vector2f fpos(position.x + 0.5f, position.y + 0.5f);
	Projectile* newProj = new Projectile(getPlace(), getEffects(), fpos);
	newProj->go(sf::Vector2f(direction * 20));
	newProj->setHit(ARROW_DAMAGE);
	if (getType().getActionItem()) newProj->setItem(*getType().getActionItem());
	newProj->setSprite(ResourceManager::getInstance().getTexture("resources/projectiles/projectiles.png"), 0);
	getPlace().addEntity(Entity::Ptr(newProj));
	getEffects().sound().newSound(S_SHOOT, fpos)->play();
}
