#include "TrapPressurePlateTrigger.h"

TrapPressurePlateTrigger::TrapPressurePlateTrigger(TrapAction& trapAction): TrapTrigger(trapAction), triggered(false) { }

void TrapPressurePlateTrigger::update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) {
	if (entitiesOver.empty()) {
		triggered = false;
	} else {
		if (!triggered) {
			trigger();
		}
		triggered = true;
	}
}
