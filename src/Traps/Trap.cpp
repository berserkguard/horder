#include "Trap.h"

#include "EffectsManager.h"
#include "Mob.h"
#include "Place.h"
#include "Projectile.h"
#include "Rand.h"
#include "ResourceManager.h"
#include "TrapArrowAction.h"
#include "TrapFlameAction.h"
#include "TrapPressurePlateTrigger.h"
#include "TrapTimerTrigger.h"

bool persist[] = {false, false, true, true};

Trap::Trap(const TrapType& type, Context context) {
	switch(type.getAction()) {
		case TrapType::ARROW:
			action.reset(new TrapArrowAction(type, context.getPlace(), context.getEffects()));
			break;
		case TrapType::FLAME:
			action.reset(new TrapFlameAction(type, context.getPlace(), context.getEffects()));
			break;
		default: assert(false);
	}
	switch(type.getTrigger()) {
		case TrapType::PRESSURE_PLATE:
			trigger.reset(new TrapPressurePlateTrigger(*action));
			break;
		case TrapType::TIMER:
			trigger.reset(new TrapTimerTrigger(*action));
			break;
		default: assert(false);
	}
}

TrapAction& Trap::getAction() {
	return *action;
}
TrapTrigger& Trap::getTrigger() {
	return *trigger;
}

void Trap::entityOverTrigger(Entity::Ptr entity) {
	entitiesOverTrigger.push_back(entity);
}
void Trap::entityOverAction(Entity::Ptr entity) {
	entitiesOverAction.push_back(entity);
}

void Trap::update(TimeStep ts) {
	trigger->update(ts, entitiesOverTrigger);
	action->update(ts, entitiesOverAction);
	entitiesOverTrigger.clear();
	entitiesOverAction.clear();
}
