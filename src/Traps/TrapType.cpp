#include "TrapType.h"

#include <cassert>

TrapType::TrapType() {
	triggerType = NO_TRIGGER;
	triggerDuration = 0.f;

	actionType = NO_ACTION;
	actionItem = nullptr;
}

void TrapType::setTrigger(Trigger type, float duration) {
	triggerType = type;
	triggerDuration = duration;
}
void TrapType::addTriggerGraphic(sf::IntRect texRect) {
	triggerGraphic.push_back(texRect);
}

TrapType::Trigger TrapType::getTrigger() const {
	return triggerType;
}
float TrapType::getTriggerDuration() const {
	return triggerDuration;
}
size_t TrapType::getNumTriggerFrames() const {
	return triggerGraphic.size();
}
sf::IntRect TrapType::getTriggerGraphic(size_t index) const {
	assert(index < getNumTriggerFrames());
	return triggerGraphic[index];
}

void TrapType::setAction(Action type, const ItemType* item) {
	actionType = type;
	actionItem = item;
}
void TrapType::addActionGraphic(sf::IntRect rect) {
	actionGraphic.push_back(rect);
}

TrapType::Action TrapType::getAction() const {
	return actionType;
}
const ItemType* TrapType::getActionItem() const {
	return actionItem;
}
size_t TrapType::getNumActionFrames() const {
	return actionGraphic.size();
}
sf::IntRect TrapType::getActionGraphic(size_t index) const {
	return actionGraphic[index];
}
