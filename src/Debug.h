#ifndef DEBUG_H
#define DEBUG_H

#include <cstdlib>
#include <cstdio>
#include <map>

class Debug{
public:
	static Debug* getInstance();

	unsigned long getMemory();
	unsigned long getMaxMemory();

	friend void* operator new(size_t);
	friend void* operator new[](size_t);
	friend void operator delete(void*) noexcept;
	friend void operator delete[](void*) noexcept;
private:
	Debug();
	static Debug* instance;

	std::map<void*, size_t> mem_map;

	unsigned long current_memory;
	unsigned long max_memory;
};
/*
void* operator new(size_t size);
void* operator new[](size_t size);
void operator delete(void* ptr);
void operator delete[](void* ptr);
*/
#endif // DEBUG_H
