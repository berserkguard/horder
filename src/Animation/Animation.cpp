#include "Animation/Animation.h"
#include <iostream>

Animation::Animation(std::string name, int numFrames, float length) {
	this->playing = false;
	this->curFrame = 0;
	this->numFrames = numFrames;
	this->elapsedTime = 0.0f;
	this->length = length;
	this->dir = ANIM_FORWARD;
	this->name = name;
}

void Animation::update(TimeStep timeStep) {
	if (!playing || length < 0.001f) {
		return;
	}
	if (dir == ANIM_FORWARD) {
		elapsedTime += timeStep.sec();
	} else if (dir == ANIM_BACKWARD) {
		elapsedTime -= timeStep.sec();
	}
	curFrame = (elapsedTime / length) * numFrames;
	if (curFrame >= numFrames || curFrame < 0) {
		stop();
	}
}

void Animation::apply(sf::Sprite& sprite, int frameWidth, int frameHeight) {
	sprite.setTextureRect(sf::IntRect(sf::Vector2i(frameWidth * curFrame, 0), sf::Vector2i(frameHeight, frameWidth)));
}

int Animation::getFrame() {
	return curFrame;
}

void Animation::setFrame(int frameIndex) {
	if (frameIndex >= numFrames) {
		frameIndex = numFrames - 1;
	}
	curFrame = frameIndex;
	elapsedTime = (length / numFrames) * curFrame;
	pause();
}

void Animation::play() {
	playing = true;
}

void Animation::stop() {
	pause();
	curFrame = 0;
	elapsedTime = 0.0f;
}

void Animation::pause() {
	playing = false;
}

bool Animation::isLastFrame() {
	return curFrame == numFrames - 1;
}

bool Animation::isPlaying() {
	return playing;
}

void Animation::setLength(float newLength) {
	length = newLength;
	stop();
}

void Animation::setDirection(AnimDir dir) {
	this->dir = dir;
}

std::string Animation::getName() {
	return name;
}
