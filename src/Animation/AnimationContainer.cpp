#include "Animation/AnimationContainer.h"

AnimationContainer::AnimationContainer() {
	animations.clear(); //???
}

Animation& AnimationContainer::newAnimation(Animation anim) {
	Animation* newAnimation = new Animation(anim);
	animations[anim.getName()].reset(newAnimation);
	return *newAnimation;
}
Animation& AnimationContainer::newAnimation(std::string name, int numFrames, float length) {
	Animation* newAnimation = new Animation(name, numFrames, length);
	animations[name].reset(newAnimation);
	return *newAnimation;
}

Animation* AnimationContainer::getAnimation(std::string name) {
	if (animations.find(name) != animations.end()) {
		return animations[name].get();
	}
	return nullptr;
}

void AnimationContainer::update(TimeStep timeStep) {
	for (auto iter = animations.begin(); iter != animations.end(); ++iter) {
		Animation& anim = *iter->second;
		anim.update(timeStep);
	}
}
