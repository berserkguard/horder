#include "Entity.h"

#include "VecUtil.h"

Entity::Entity(Type type, b2BodyDef& bodyDef, EffectsManager& effects): type(type), effects(&effects), visible(true) {
    body = Get::physWorld().CreateBody(&bodyDef);
	body->SetActive(false);
	PhysUserData* userData = new PhysUserData(UD_ENTITY);
	userData->entity = this;
	body->SetUserData(userData);
}

Entity::~Entity() {
    if (body) {
    	delete (PhysUserData*)body->GetUserData();
		Get::physWorld().DestroyBody(body);
    }
}

bool Entity::update(TimeStep timeStep) {
	animData.update(timeStep);
	return false;
}

Entity::Type Entity::getType() {
	return type;
}

b2Body& Entity::getCollisionBody() {
	return *body;
}

void Entity::setPosition(float x, float y) {
	body->SetTransform(b2Vec2(x, y), body->GetAngle());
}
void Entity::setPosition(sf::Vector2f pos) {
	body->SetTransform(toPhysVec(pos), body->GetAngle());
}
const sf::Vector2f Entity::getPosition() {
	return toSFMLVec(body->GetPosition());
}

sf::Sprite& Entity::getSprite(int idx) {
	return spriteLayers.at(idx);
}

const std::vector<sf::Sprite>& Entity::getSprites() {
	return spriteLayers;
}

AnimData& Entity::getAnimData() {
	return animData;
}

EffectsManager& Entity::getEffects() {
	return *effects;
}

void Entity::setVisible(bool v) {
	visible = v;
}
bool Entity::isVisible() {
	return visible;
}

void Entity::onEntityCollision(Entity& collidingWith) { }
void Entity::onWallCollision() { }
