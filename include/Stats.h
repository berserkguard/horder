#ifndef STATS_H_INCLUDED
#define STATS_H_INCLUDED

#include "main.h"

/**
 * Contains info about Stats and stuff.
 * @note UT [x]
 */
namespace Stat {
	enum Type {NONE,
		HP,          ///< The maximum and starting hit points of a Mob.
		REGEN,       ///< How much health a Mob heals each minute.
		DEFENSE,     ///< Damage gets reduced 0.5^(DEFENSE / 100) of it's origonal value.
		EVASION,     ///< Mobs have a 0.5^((EVASION - their ACCURACY + 10) / 100) chance of being hit.
		ACCURACY,    ///< Increases hit chance. (See EVASION)
		MOVE_SPEED,  ///< How fast Mobs can move/accelerate.
		ATTACK,      ///< The base damage of a Mob.
		CRITICAL,    ///< Mobs have a 1 - 0.5^(CRITICAL / 100) chance of doing double damage on an attack.
		ATTACK_SPEED,///< How many attacks per second a Mob gets.
		KNOCKBACK,   ///< The force an attack applies to the target.
		RANGE,       ///< The minnimum distance between the attacker an the enemy, measured in attacker radii, that the attacker can attack.
		ATTACK_WIDTH,///< The angelar width, mesaured in radians, the attacker has on attacks.
		MAX_TARGETS, ///< The maximum number of targets a Mob can hit in a single attack.
		SIZE,        ///< The radius, measured in Tiles, of a Mob.
		DENSITY,     ///< How heavy a Mob is per square inch.
		DAMPING,     ///< How much fast a Mob slows down.
		ELASTICITY,  ///< How much a Mob will bounce against a wall and each other.
		GSCALE,      ///< Scale of sprite.
	COUNT};

	/**
	 * Takes in a String and returns the stat with that name.
	 * @param s The name (or the abbreviation) of a Stat.
	 * @return The stat with that name.
	 */
	Type fromString(String s);

	/**
	 * Returns the string name for the given stat type.
	 * @param type The type of the stat.
	 * @return A string representation of the type.
	 */
	String toString(Stat::Type type);

	/**
	 * Returns the default value of a given stat.
	 * @param The stat.
	 * @return The default value of that stat.
	 */
	float getDefaultValue(Stat::Type stat);

	/**
	 * Returns true if the stat is "useful" (a stat the player should be aware of).
	 * @param The stat.
	 * @return True if useful, false if not.
	 */
	bool isUseful(Stat::Type stat);
}

#endif // STATS_H_INCLUDED
