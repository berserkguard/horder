#ifndef VALUABLESMANAGER_H
#define VALUABLESMANAGER_H

#include "main.h"
#include "ItemType.h"
#include "MonsterType.h"
#include "TileType.h"
#include "MapType.h"
#include "Model.h"

/**
 * The ValublesManager manages the valubles.
 * @note UT [_]
 */
class ValuablesManager {
	public:
		/** Returns the item type at the given index, or null if there isn't one. */
		const ItemType* getItemType(size_t index) const;

		/** Returns the item with the given name, or null if there isn't one. */
		const ItemType* getItemType(String name) const;

		/** Loads a new ItemType from a YAML node. */
		const ItemType* loadItemType(YAML::Node& node);


		/** Returns the monster type at the given index, or null if there isn't one. */
		const MonsterType* getMonsterType(size_t index) const;

		/** Returns the monster type with the given name. */
		const MonsterType* getMonsterType(String name) const;

		/** Loads a monster type from the given YAML node. */
		const MonsterType* loadMonsterType(YAML::Node& node);


		/** Returns the model with the given name. */
		const Model* getModel(String name) const;

		/** Loads a moder from the given YAML node. */
		const Model* loadModel(YAML::Node& node);


		/** Returns the tile type with the given name. */
		const TileType* getTileType(String name) const;

		/** Returns the tile type with the given coordinate. */
		const TileType* getTileType(Coord coord) const;

		/** Creates a tile type with the given graphic. */
		const TileType* createTileType(Coord gloc);

		/** Loads a tile type from the given YAML node. */
		const TileType* loadTileType(YAML::Node& node);


		/** Returns the map type with the given name. */
		const MapType* getMapType(String name) const;

		/** Loads a map type from the given YAML node. */
		const MapType* loadMapType(YAML::Node& node);


		/** Returns the trap type with the given name. */
		const TrapType* getTrapType(String name) const;

		/** Loads a trap type from the given YAML node. */
		const TrapType* loadTrapType(YAML::Node& node);


		/** To be called after calling load on everything, to finalize the loading. */
		void finishLoading();

	private:
		void finishItemType(YAML::Node& node, ItemType& type);
		void finishMonsterType(YAML::Node& node, MonsterType& type);
		void finishMapType(YAML::Node& node, MapType& type);
		void finishTrapType(YAML::Node& node, TrapType& type);

		template<typename T>
		using ptr = std::unique_ptr<T>;

		std::vector<ptr<ItemType>>  itemTypes;
		std::map<String, ItemType*> itemTypeNameMap;

		std::vector<ptr<MonsterType>>  monsterTypes;
		std::map<String, MonsterType*> monsterTypeNameMap;

		std::vector<ptr<Model>>  models;
		std::map<String, Model*> modelNameMap;

		std::vector<ptr<TileType>>  tileTypes;
		std::map<Coord, TileType*, Coordcmp> tileTypeCoordMap;
		std::map<String, TileType*> tileTypeNameMap;

		std::vector<ptr<MapType>>  mapTypes;
		std::map<String, MapType*> mapTypeNameMap;

		std::vector<ptr<TrapType>>  trapTypes;
		std::map<String, TrapType*> trapTypeNameMap;

		std::vector<YAML::Node> itemNodes;
		std::vector<YAML::Node> monsterNodes;
		std::vector<YAML::Node> mapNodes;
		std::vector<YAML::Node> trapNodes;
};

#endif // VALUABLESMANAGER_H
