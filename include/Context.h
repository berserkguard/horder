#ifndef CONTEXT_H
#define CONTEXT_H

class Place;
class EffectsManager;

class Context {
	public:
		Context(Place& place, EffectsManager& effects): place(&place), effects(&effects) { }

		Place&          getPlace()   {return *place;}
		EffectsManager& getEffects() {return *effects;}

	private:
		Place* place;
		EffectsManager* effects;
		// logger
		// other stuff?
};

#endif // CONTEXT_H

