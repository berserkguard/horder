#ifndef MAP_WRITER_H
#define MAP_WRITER_H

#include <fstream>
#include <Chunk.h>
#include <Game.h>

/// A utility class for writing map/chunk information.
class MapWriter {
	public:
		MapWriter(std::string prefix);

		void writeChunk(Game& game, Chunk& chunk);
		void loadChunk(Game& game, Chunk& chunk);
	private:
		std::string pathPrefix;
		double seed;
};

#endif // MAP_WRITER_H
