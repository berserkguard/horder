#ifndef SHUFFLEBAG_H
#define SHUFFLEBAG_H

#include <vector>
#include <algorithm>
#include "Rand.h"

/**
 * You put stuff in a ShuffleBag and can grab them one at a time.
 * @note UT [x]
 */
template <typename V>
class ShuffleBag {
	public:
		/** Create new empty ShuffleBag */
		ShuffleBag(): shuffled(false) {}

		/** Add something to the bag. */
		void add(V value) {
			values.push_back(value);
			shuffled = false;
		}

		/** Randomly remove something from the bag. */
		V grab() {
			if (!shuffled) shuffle();
			if (values.empty()) throw std::out_of_range("Bag is empty.");
			V value = values.back();
			values.pop_back();
			return value;
		}

		/** @return The number of things in the bag. */
		size_t size() {
			return values.size();
		}

		/** Empties the ShuffleBag */
		void clear() {
			values.clear();
			shuffled = false;
		}

	private:
		void shuffle() {
			std::random_shuffle(values.begin(), values.end(), Rand::simple);
			shuffled = true;
		}
		std::vector<V> values;
		bool shuffled;
};

#endif // SHUFFLEBAG_H
