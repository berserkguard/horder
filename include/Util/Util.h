#ifndef USEFULPARSING_H
#define USEFULPARSING_H

#include <string>
#include <iostream>

#include <yaml-cpp/yaml.h>
#include <SFML/Graphics.hpp>

typedef std::string String;

/**
 * Capitalizes a String.
 * @param s The string to capitalize (gets modified).
 * @return The capitalized String.
 */
String capitalize(String& s);

/**
 * Makes every character in the string lower case.
 * @param s The string to make lower case (gets modified).
 * @return The lowercase String.
 */
String toLowerCase(String s);

/**
 * Pluralizes a string.
 * @param s The string to pluralize (does not get modified).
 * @return The pluralized String.
 */
String pluralize(const String& s);

/**
 * Adds an article to a string (either a or an).
 * @param s The string to add an article to (does not get modified).
 * @return The articalized String.
 */
String article(const String& s);

/**
 * Converts a number to roman numerals.
 * @param num The number to convert to roman numerals.
 * @return The roman numerals.
 */
String toRomanNumerals(int num);

sf::Vector2f strToVec(String toVec);

enum Direction: unsigned int;
/**
 * Converts a String to a Direction.
 * @param dir The String representation of a direction.
 * @return The direction that the String is referring to.
 */
Direction strToDir(const String& dir);

sf::Color strToColor(const String& color);

float strToWeight(const String& string);

// Too many parameters.
/// Parses the String value of a thing from a YAMl file or returns the default and prints an error if the thing is not found.
String readYAMLStr(YAML::Node& node, String key, String def, String errorMess = "", std::ostream& lerr = std::cerr);
/// Parses the integer value of a thing from a YAMl file or returns the default and prints an error if the thing is not found.
int    readYAMLInt(YAML::Node& node, String key,    int def, String errorMess = "", std::ostream& lerr = std::cerr);
/// Parses the float value of a thing from a YAMl file or returns the default and prints an error if the thing is not found.
float  readYAMLNum(YAML::Node& node, String key,  float def, String errorMess = "", std::ostream& lerr = std::cerr);

std::pair<int, int> readYAMLPair(YAML::Node& node, String key, int def, String errorMess = "");

template<typename K, typename V>
V* safeMapRetrieve(std::map<K, V> map, K key) {
	auto iter = map.find(key);
	if (iter == map.end()) {
		return NULL;
	}
	return iter->second;
}

#endif // USEFULPARSING_H
