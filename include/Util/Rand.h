/*  _   _  _____ ______ ______  _____ ______
 * | | | ||  _  || ___ \|  _  \|  ___|| ___ \
 * | |_| || | | || |_/ /| | | || |__  | |_/ /
 * |  _  || | | ||    / | | | ||  __| |    /
 * | | | |\ \_/ /| |\ \ | |/ / | |___ | |\ \
 * \_| |_/ \___/ \_| \_||___/  \____/ \_| \_|
*/

#ifndef RANDOM_H
#define RANDOM_H

#include <random>

/**
 * A utility namespace for generating random numbers for various distributions.
 * @note UT [x]
 */
namespace Rand {
	/**
	 * Sets the seed to be used for random number generation.
	 * @param seed The number to use as the new seed.
	 */
	void setSeed(unsigned int seed);

	/**
	 * Shifts the state of the random number generator by z notches.
	 * @param z The number of notches to shift the state by.
	 */
	void discard(unsigned long long z);

	/**
	 * Returns an integer in the range [0, high).
	 * @param high The upper-bound (exclusive) of the generated number.
	 * @return The generated pseudorandom number.
	 */
	unsigned int simple(unsigned int high);

	/**
	 * Returns an integer in the range [low, high).
	 * @param low The lower-bound (inclusive) of the generated number.
	 * @param high The upper-bound (exclusive) of the generated number.
	 * @return An integer value in the range [low, high).
	 */
	int i(int low = 0, int high = 100);

	/**
	 * Returns a float in the range [low, high).
	 * @param low The lower-bound (inclusive) of the generated number.
	 * @param high The upper-bound (exclusive) of the generated number.
	 * @return A floating-point value in the range [low, high).
	 */
	float f(float low = 0.0f, float high = 1.0f);

	/**
	 * Returns a double in the range [low, high).
	 * @param low The lower-bound (inclusive) of the generated number.
	 * @param high The upper-bound (exclusive) of the generated number.
	 * @return A double-precision floating-point value in the range [low, high).
	 */
	double d(double low = 0.0, double high = 1.0);

	/**
	 * Randomly picks a number from a normal distribution.
	 * @param mean The mean to use for the normal distribution.
	 * @param standardDeviation The standard deviation to use for the normal distribution.
	 * @return A double-precision floating-point value in the distribution.
	 */
	double norm(double mean = 0.0, double standardDeviation = 1.0);

	/**
	 * Randomly picks a number from a logarithmic normal distribution.
	 * @param mean The mean to use for the log-normal distribution.
	 * @param standardDeviation The standard deviation to use for the log-normal distribution.
	 * @return A double-precision floating-point value in the distribution.
	 */
	double lognorm(double mean = 0.0, double standardDeviation = 1.0);

	/**
	 * Randomly picks from an exponential distribution.
	 * @param lambda The lambda value to use for the distribution.
	 * @return A double-precision floating-point value in the distribution.
	 */
	double exp(double lambda = 1.0);

	/**
	 * Rolls a number of dice with a specified number of sides and returns the sum.
	 * @param sides The number of sides on each die.
	 * @param num The number of dice to roll.
	 * @return An integer containing the sum of the dice rolls.
	 */
	unsigned int dice(unsigned int sides, unsigned int num = 1);

	/**
	 * Returns a random number in the range [low, high) that is dependent on the parameters.
	 * The difference between 'low' and 'high' is limited to the range of a short.
	 * @param low The lower-bound (inclusive) for the generated number.
	 * @param high The upper-bound (exclusive) for the generated number.
	 * @param a A required user parameter used in the generation.
	 * @param b An optional user parameter used in the generation.
	 * @param c An optional user parameter used in the generation.
	 */
	int consistent(int low, int high, int a, int b = 1, int c = 1);
};

#endif // RANDOM_H
