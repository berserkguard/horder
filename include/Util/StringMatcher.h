#ifndef STRINGMATCHER_H
#define STRINGMATCHER_H

#include <map>

class StringMatcher {
	public:
		/**
		 * @param strings An array of strings of length len to match with the integers.
		 * @param ints The ints matched with the strings. Length len.
		 * @param len The length of the string and int arrays.
		 * @param defaultValue When a string is matched with no value, this is returned instead.
		 */
		StringMatcher(std::string strings[], int ints[], size_t len, int defaultValue = 0);

		int match(std::string str);

	private:
		std::map<std::string, int> strMap;
		int def;
};

#endif // STRINGMATCHER_H
