#ifndef PATH_H
#define PATH_H

#include "VecUtil.h"

/**
 * The Grid struct is an interface that is used by the Path to get information needed to generate paths.
 * @note UT [i]
 */
struct Grid {
	/**
	 * Should return true if the given mob cannot enter the given position in this grid.
	 * @param position A position to check.
	 * @param mob The mob given to the Path that calls this.
	 * @return True if the mob can enter the position and false otherwise.
	 */
	virtual bool isObstructed(sf::Vector2i position, void* mob) = 0;

	/**
	 * Should return the width of the grid. If -1 is given, the grid is considered to be infinite.
	 * @return The width of the grid.
	 */
	virtual int getWidth() = 0;

	/**
	 * Should return the height of the grid.
	 * @return The height of the grid.
	 */
	virtual int getHeight() = 0;
};

/**
 * The Path class can generate paths using A* and will store the path it generates.
 * @note UT [x]
 */
class Path {
	public:

		/** Flags to affect path generation behavior. They can be |'d together. */
		enum Flag {NONE,
			DIAGONAL = 1 << 0, /**< If this flag is included, the mobs will be able to take diagonal steps. */
			SMOOTH   = 1 << 1, /**< If this flag is included, path nodes will be merged to minimize the number of nodes in the path.
									Nodes will no longer necessarily be adjacent to one another. */
			FLAG_COUNT};

		/**
		 * Default constructor. The Path will be unable to generate paths until a Grid is given.
		 */
		Path();

		/**
		 * Defines a new path (but does not generate it yet).
		 * @param start The start of the Path.
		 * @param dest The end of the Path.
		 * @param place The Place this Path is in.
		 */
		Path(Grid& place, void* mob = NULL, int flags = DIAGONAL | SMOOTH);

		void setGrid(Grid& place);
		void setMob(void* mob);
		void setFlags(int flags);

		/**
		 * Generates the path (with the A*).
		 * @return Returns false if it failed to make a Path (and true if it succeeded).
		 */
		bool generate(sf::Vector2f start, sf::Vector2f dest);

		/**
		 * The length of the path is the number of nodes on the path.
		 * A length of one is a path to itself.
		 * @return The length of the Path. (Returns 0 if there is no path.)
		 */
		int length();

		/**
		 * Returns the coordinates of a particular point on the Path.
		 * @param The index to get the coordinates at (0 is the start and length() is the end).
		 */
		sf::Vector2f at(unsigned int i);

		/**
		 * Returns the "distance" of the Path, measured in Tiles. Same as length except diagonals count as 1.4 instead of 1.
		 * @return The distance of the Path.
		 */
		float distance();

		/**
		 * Returns a string of the Path as a space separated list of comma separated coordinates. Ex: "13,12 13,13 14,15 15,13"
		 */
		std::string toString();

	private:
		int getH(int x, int y);
		bool walkable(sf::Vector2i from, sf::Vector2i to);

		bool diagonal, smooth;
		int dist;
		std::vector<sf::Vector2f> path;
		sf::Vector2f start, dest;
		Grid* grid;
		void* mob;
};

#endif // PATH_H
