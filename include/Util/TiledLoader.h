#ifndef TILEDLOADER_H
#define TILEDLOADER_H

#include <tinyxml2.h>
#include <map>
#include "main.h"
#include "VecUtil.h"
class ValuablesManager;
class Map;

class TiledLoader {
	public:
		TiledLoader(ValuablesManager& valuables);
		void loadFile(Map& map, String filename);

	private:
		void loadFloors(  tinyxml2::XMLElement& layer, int offset);
		void loadPaths(   tinyxml2::XMLElement& layer);
		void loadTileObjs(tinyxml2::XMLElement& layer, int offset);
		void loadInteract(tinyxml2::XMLElement& layer);
		void loadWalls(   tinyxml2::XMLElement& layer);
		void loadObjObjs( tinyxml2::XMLElement& layer, int offset);

		void loadTilesetOffsets(tinyxml2::XMLElement* element);
		std::map<String, int> offsets;
		void loadProperties(tinyxml2::XMLElement* element);
		String getProperty(String name, String def = "");
		int getIntProperty(String name, int def = 0);
		std::map<String, String> properties;

		ValuablesManager* valuables;
		Coord size;
		Map* map;
};

#endif // TILEDLOADER_H
