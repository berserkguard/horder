#ifndef VECUTIL_H
#define VECUTIL_H

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

/** Angle is in radians, always. */
typedef float Angle;

/**
 * These are used in things.
 * You can | together certain directions to form new ones.
 * For example: NORTH | EAST is the same as NORTHEAST
 */
enum Direction: unsigned int {NONE, NORTH = 1, EAST = 1 << 1, SOUTH = 1 << 2, WEST = 1 << 3,
					 NORTHEAST = NORTH | EAST, NORTHWEST = NORTH | WEST, SOUTHEAST = SOUTH | EAST, SOUTHWEST  = SOUTH | WEST,
					 UP, DOWN, DIR_COUNT};

typedef sf::Vector2i Coord;
/// The point (0, 0).
const sf::Vector2i ORIGIN(0, 0);

/** A struct for Coord comparisons, to be used in trees and stuff. */
struct Coordcmp {
	bool operator() (const Coord& lhs, const Coord& rhs) const {
		if (lhs.x == rhs.x) return lhs.y < rhs.y;
		else return lhs.x < rhs.x;
	}
};

/**
 * Turns the direction a number of degrees (divisible by 45).
 * A positive rotation is clockwise.
 * @param dir The initial direction.
 * @param degrees How much to turn the direction, in degrees, divisible by 45.
 * @return The direction after being turned the degrees.
 */
Direction turn(Direction dir, int degrees);

Angle dirToAngle(Direction dir);

/**
 * Returns the coordinate corresponding to a particular direction.
 * @param dir The direction to get the coordinate for.
 * @return The coordinate going in that direction.
 */
Coord dirVector(Direction dir);

Coord pairToCoord(std::pair<int, int>);

/**
 * Chooses whether it would be faster to rotate clockwise or counterclockwise from one angle to another.
 * Is all in radians.
 * @param from The current angle.
 * @param to The target angle.
 * @return True if it should turn clockwise and false if it should turn counterclockwise.
 */
bool toRotateClockwise(Angle from, Angle to);

/**
 * Converts an SFML vector to a Box2D vector.
 * @param vec An SFML vector.
 * @return A Box2D vector.
 */
b2Vec2 toPhysVec(sf::Vector2f vec);

/**
 * Converts a Box2D vector to an SFML vector.
 * @param vec A Box2D vector.
 * @return An SFML vector.
 */
sf::Vector2f toSFMLVec(b2Vec2 vec);

/**
 * Returns Coord(std::floor(vec.x), std::floor(vec.y)).
 * @param vec The vector to floor.
 * @return The floored integer vector.
 */
Coord vecFloor(sf::Vector2f vec);

/**
 * Same as the other one but returns a float vector.
 * @param vec The vector to floor.
 * @return The floored float vector.
 */
sf::Vector2f vecFloorF(sf::Vector2f vec);

/**
 * Returns the squared length of a vector.
 * @param vec The vector.
 * @return The magnitude squared.
 */
float vecSqrMag(sf::Vector2f vec);

/**
 * Returns the length of a vector.
 * @param vec The vector.
 * @return The length of that vector.
 */
float vecMagnitude(sf::Vector2f vec);

/**
 * Returns the unit vector with the same magnitude.
 * @param vec The vector to normalize.
 * @return The normalized vector.
 */
sf::Vector2f vecNormalize(sf::Vector2f vec);

/**
 * Returns the angle that this vector is making, in radians.
 * @param vec The vector to get an angle from.
 * @return The angle from the vector.
 */
Angle vecAngle(sf::Vector2f vec);

/**
 * Returns the unit vector for a given angle.
 * @param angle The angle.
 * @return A unit vector that points in the angle.
 */
sf::Vector2f angleVec(Angle angle);

/**
 * Returns a rotated vector.
 * @param vec The vector to rotate.
 * @param How much to rotate it in degrees.
 * @return The rotated vector.
 */
sf::Vector2f vecRotate(sf::Vector2f vec, Angle angle);

#endif // VECUTIL_H
