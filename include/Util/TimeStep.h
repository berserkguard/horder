#ifndef TIMESTEP_H
#define TIMESTEP_H

#include <SFML/System.hpp>
class PeriodManager;

/**
 * A class for storing the time that has passed between two frames.
 */
class TimeStep {
	public:
		TimeStep(float secondsPassed);
		TimeStep(sf::Time timePassed, PeriodManager& periodManager);

		/** @return The duration of the time step in milliseconds. */
		float milli() const;

		/** @return The duration of the time step in seconds. */
		float sec() const;

		/** @return The duration of the time step in minutes. */
		float min() const;

		enum Period {TENTH = 0, FIFTH, HALF, FULL, PERIOD_COUNT};

		/**
		 * Returns true if the given period of time has just passed.
		 * For example, there should be one time step that returns true for hasPeriodPassed(TimeStep::FIFTH) every tenth of a second.
		 */
		bool hasPeriodPassed(Period period) const;

	private:
		float diff;
		bool periods[PERIOD_COUNT];
};

struct PeriodManager {
	friend class TimeStep;
	public: PeriodManager();
	private: float periodCurrents[TimeStep::PERIOD_COUNT];
};

#endif // TIMESTEP_H
