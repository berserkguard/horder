#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <string>
#include <map>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

/**
 * A singleton for loading and accessing shared resources.
 * UT [x]
 */
class ResourceManager {
	public:
		/**
		 * Retrieves the static instance of the singleton.
		 * @return The instance of the ResourceManager.
		 */
		static ResourceManager& getInstance();

		/**
		 * Retrieves the sf::Texture with the specified path name and loads it if not yet in the ResourceManager.
		 * @param name The filepath (relative to the executable) of the texture resource.
		 */
		sf::Texture& getTexture(std::string name);

		/**
		 * Retrieves the sf::Shader with the specified name and loads it if not yet in the ResourceManager.
		 * @param name Name of a shader. Searches under src/Graphics/Shaders/name.frag and src/Graphics/Shaders/name.vert can load both.
		 */
		sf::Shader& getShader(std::string name);

		/**
		 * Retrieves the sf::Font with the specified path name and loads it if not yet in the ResourceManager.
		 * @param name The filepath of the font.
		 */
		 sf::Font& getFont(std::string name);

		 /**
		  * Retrieves the sf::SoundBuffer with the specified path name and loads it if not yet in the ResourceManager.
		  * @param name The filepath of the sound.
		  */
		 sf::SoundBuffer& getSoundBuffer(std::string name);

	private:
		ResourceManager();
		ResourceManager(const ResourceManager&);
		ResourceManager& operator=(const ResourceManager&);

		std::map<std::string, sf::Texture> textures;
		std::map<std::string, sf::Shader> shaders;
		std::map<std::string, sf::Font> fonts;
		std::map<std::string, sf::SoundBuffer> sounds;
};

#endif // RESOURCE_MANAGER_H
