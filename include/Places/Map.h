#ifndef MAP_H
#define MAP_H

#include "Place.h"
#include "Trap.h"
#include "MobSpawner.h"
#include "TimeStep.h"
#include "Chunk.h"
class Mob;
class MapType;
class ValuablesManager;
class Interactible;

#define LIGHT_DIST 7

/**
 * A Map has Entities, Chunks, and walls.
 * @note UT [v]
 */
class Map: public Place {
	public:
		/**
		 * Creates a new Map with the given name and flags.
		 * @param name The name of the map.
		 * @param flags MapFlags that can be applied (or 0 for no flags). They can be |'d together.
		 */
		Map(const MapType& type, EffectsManager& effects);
		virtual ~Map();

		/** Tells the Map to pass the time. */
		void update(TimeStep timeStep);

		/** @return The name of the Map. */
		String getName();

		/** @return The type of the Map. */
		const MapType& getType();

		void activate();   ///< Activates the Map. Only one Map should be active at a time.
		void deactivate(); ///< Deactivates the Map, disabling things like wall collisions and whatnot.

		void addEntity(Entity::Ptr entity) override;
		bool removeEntity(Entity::Ptr entity) override;
		void getEntitiesInRect(std::vector<Entity::Ptr>& entitiesOut, sf::FloatRect rect) override;

		typedef std::unordered_map<unsigned long, std::unique_ptr<Chunk>> ChunkMap;
		class EntityIterator: public std::iterator<std::forward_iterator_tag, std::pair<Entity::Ptr, Chunk&>> {
			public:
				typedef std::pair<Entity::Ptr, Chunk&> Epcp;
				EntityIterator();
				EntityIterator(Map& map, bool begin);
				EntityIterator& operator++();
				Epcp operator*();
				Epcp* operator->();
				const Epcp* operator->() const;
				friend bool operator==(const Map::EntityIterator& lhs, const Map::EntityIterator& rhs);
				friend bool operator!=(const Map::EntityIterator& lhs, const Map::EntityIterator& rhs);
			private:
				void endOfChunkCheck();

				Map* map;
				ChunkMap::iterator chunkIter;
				std::set<Entity::Ptr>::iterator entityIter;
				std::unique_ptr<Epcp> tmpEpcp;
		};
		EntityIterator entitiesBegin();
		EntityIterator entitiesEnd();

		/** @return The spawner of monsters. */
		MobSpawner& getSpawner();

		/** @param player The Mob that all the Monsters want to kill. */
		void setPlayer(Entity::Ptr player);

		/** Returns true if there are no Monsters going after the player. */
		bool isPlayerSafe();


		/** @return The number of Chunks in the Map. */
		size_t getNumChunks();

		/** Allocates enough chunks to cover the given area. */
		void allocateChunks(Coord size, Chunk::State chunkState = Chunk::EMPTY);

		/**
		 * Returns the Chunk in this Map that contains the given position. Returns NULL if no Chunk has this position.
		 * @param pos A position of a tile.
		 * @return The chunk that contains the given position.
		 */
		Chunk* getChunkContainingPos(Coord pos);

		Chunk& createChunkContainingPos(Coord pos);

		/**
		 * Returns the Chunk of the given coordinates.
		 * @param location A position of a chunk (measured in chunks).
		 * @return The chunk an the given position.
		 */
		Chunk* getChunk(Coord location);

		/**
		 * If there is already a chunk at the given location, it just returns that chunk instead of making a new one.
		 * @param location The position of the new Chunk.
		 * @return The created chunk (or an existing Chunk)
		 */
		Chunk& createChunk(Coord location);

		/**
		 * Removes and deletes the given chunk. (untested!)
		 * @param chunk The chunk to be destroyed.
		 * @return An iterator pointing to the next chunk in the list.
		 */
		ChunkMap::iterator destroyChunk(Chunk& chunk);

		ChunkMap::iterator getChunkBegin(); ///< This is just the beginning.
		ChunkMap::iterator getChunkEnd();   ///< This is the end.

		Interactible* getInteractible(Coord pos);
		Interactible& addInteractible(String name, Coord pos) override;
		Interactible& addInteractible(String name, const std::vector<Coord>& positions) override;
		MapObject& addObject(sf::Vector2i, sf::IntRect) override;
		MapObject& addObject(MapObject object) override;
		size_t getNumObjects()                 override;
		MapObject& getObject(size_t index)     override;

		/**
		 * Returns the Map in the given direction from this Map. If no Map is in that direction, NULL is returned.
		 * @param direction The direction to get an adjacent Map in.
		 * @return The Map in that direction, or NULL if no Map is in that direction.
		 */
		Map* getAdjacentMap(Direction direction);
		/**
		 * Tells this Map that there is a Map next to it in a given direction.
		 * @param direction The direction that the given Map is in.
		 * @param map The map that is in the given direction.
		 */
		void addAdjacentMap(Direction direction, Map* map);

		Tile& getTile(Coord pos)    override;
		Tile& getTile(int x, int y) override;
		Tile* getTileSafe(Coord pos);    ///< Both hasTile() and getTile() in one! Returns null if there is no tile. @{
		Tile* getTileSafe(int x, int y); /// @}
		bool hasTile(Coord pos)     override;
		bool hasTile(int x, int y)  override;

		bool addItem(Coord pos, Item item, bool allowAlt = true) override;

		EffectsManager& getEffects();

		/**
		 * Adds a wall to the Map.
		 * @param shape The Box2D shape that represents the wall.
		 * @return The fixture created to represent the wall. Used to pass to removeWall.
		 */
		b2Fixture* addWall(b2EdgeShape& innerWall, b2EdgeShape& outerWall, Chunk& chunk);
		b2Fixture* addWall(b2Shape& innerWall, Chunk& chunk);

		/** @param wall A fixture that was returned from addWall that you want to destroy. */
		void removeWall(b2Fixture& wall);

		/** @return The body that contains all the fixtures that make up the physical walls of the map. */
		const b2Body& getWallsBody();

		/**
		 * Performs a ray cast on all the walls inner shapes. All parameters are measured in Tiles.
		 * @param input The ray cast input to use when raycasting.
		 * @return The minnimum fraction calculated from all the ray casting.
		 */
		float rayCast(b2RayCastInput& input, b2AABB& bb);
		std::vector<const b2Shape*>& getRayCastShapes();

		/** Returns the generation seed for this map. */
		int getSeed();

		/** Fills a vector with all the entrances to a particular chunk from a given direction. */
		void getEntrances(std::vector<int>& entrances, Coord chunkPos, Direction dir);

		int getWidth()  override {return -1;}
		int getHeight() override {return -1;}

	private:
		Map(const Map&) = delete;
		Map& operator=(const Map&) = delete;

		b2Fixture* addWall(b2Shape& innerWall, PhysUserData* userData, Chunk& chunk);
		void removeWalls(Chunk& chunk);
		bool monsterSight(Monster& monster);

		const MapType* type;
		bool active;
		int seed;

		bool rayCastCloseShapesSetUp;
		std::vector<const b2Shape*> rayCastShapes;
		b2Body* walls;
		ChunkMap chunks;
		std::map<Direction, Map*> adjacentMaps;

		MobSpawner mobSpawner;
		EffectsManager* effects;

		Entity::Ptr player;
		float safe;
};

inline bool operator==(const Map::EntityIterator& lhs, const Map::EntityIterator& rhs) {
	if (lhs.chunkIter == lhs.map->getChunkEnd() && rhs.chunkIter == rhs.map->getChunkEnd()) return true;
	return lhs.chunkIter == rhs.chunkIter && lhs.entityIter == rhs.entityIter;
}
inline bool operator!=(const Map::EntityIterator& lhs, const Map::EntityIterator& rhs) {
	return !operator==(lhs, rhs);
}

#endif // MAP_H
