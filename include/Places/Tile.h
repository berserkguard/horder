#ifndef TILE_H
#define TILE_H

#include "main.h"
#include "ItemContainer.h"
#include "TileType.h"
class Trap;
class Interactible;

#define NUM_TILE_LAYERS 4
#define MAX_ITEMS 32

/**
 * Holds information about a single tile, such as its type for each layer (from Tiled Map Editor) and whether it has any items.
 * @note UT [v]
 */
class Tile {
    public:
        Tile();

        enum PathType: unsigned char {NO = 0, PARTIAL, YES};
        PathType isPath();
        void setPath(PathType path);

		/** @return The floor tile type of this tile. */
		const TileType& getFloor();

		/** Sets the type of floor for this tile. */
		void setFloor(const TileType& type);

		/** @return The number of items currently on the tile. If its positive, getItemContainer() is guarenteed to not be null. */
		size_t getNumItems();

		/**
		 * @param index The index of the item in the pile.
		 * @return A reference to the item at the specified index.
		 */
		Item& getItem(size_t index);

		/**
		 * @param item The item to add to the pile.
		 * @return true if successful, false otherwise
		 */
		bool addItem(Item item);

		/**
		 * @param index The index of the item in the pile to remove.
		 * @return The item that was removed from the pile.
		 */
		Item removeItem(size_t index);

		/**
		 * Retrieves the item container for the tile.
		 * @note Returns NULL if the tile contains 0 items.
		 * @return The tile's item container, or NULL.
		 */
		ItemContainer* getItemContainer();

		enum Special: unsigned char {NOTHING = 0, TRAP_TRIGGER, TRAP_ACTION, INTERACT};
		Special getSpecial();
		Trap& getTrap();             ///< Only guarenteed to work if getSpecial() returns Tile::TRAP_TRIGGER or Tile::TRAP_ACTION.
		Interactible& getInteract(); ///< Only guarenteed to work if getSpecial() returns Tile::INTERACT.
		void setTrapTrigger(Trap& trap);
		void setTrapAction(Trap& trap);
		void setInteract(Interactible& interact);

		int getRand();

    private:
        std::unique_ptr<ItemContainer> items;
        const TileType* floor;

        union {
        	Trap* trap;
        	Interactible* interactible;
        } s;
        Special special;

        PathType path;
        unsigned short rand;
};

#endif // TILE_H
