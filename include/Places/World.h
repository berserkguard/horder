#ifndef WORLD_H
#define WORLD_H

#include "main.h"
#include "Player.h"
#include "Map.h"
#include "TiledLoader.h"
class EffectRenderer;
class MonsterType;

/**
 * Contains information about several related maps and a player.
 */
class World: public b2ContactListener {
	public:
		World(ValuablesManager& valuables, EffectsManager& effects);

		void addMap(const MapType& type);

		/**
		 * Retrieves the map with the given name, or NULL if non-existent.
		 * @param mapName The name of the map to retrieve.
		 * @return A pointer to the Map object, or NULL if no map was found.
		 */
		Map* getMap(String mapName);

		void connectMaps();

		/**
		 * Retrieves a reference to the Player object.
		 * @return A reference to the Player.
		 */
		Player& getPlayer();

		/** See b2ContactListener */
		void BeginContact(b2Contact* contact);

	private:

		Player* player;
		ValuablesManager* valuables;
		EffectsManager* effects;
		TiledLoader tiledLoader;

		std::vector<std::unique_ptr<Map> > maps;
		std::map<String, Map*> mapNameMap;
};

#endif // WORLD_H
