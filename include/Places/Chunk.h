#ifndef CHUNK_H
#define CHUNK_H

#include "main.h"
#include "Tile.h"
#include "Entity.h"
#include "MapObject.h"
#include "Place.h"
#include "Context.h"
class TreasureChest;
class Trap;
class TrapType;

/// The size of a chunk, in tiles. Chunks can only be square and should have side length of a power of two.
#define CHUNK_SIZE 32

/**
 * A class for storing information about a chunk of tiles. Chunks are used as a way of paging regions of the map in and out of memory, to
 * allow for infinite, procedurally-generated maps without wasting resources.
 */
class Chunk: public Place {
	public:
		Chunk(Coord position, Context context);

		void update(TimeStep timeStep);

		/** @return The x, y coordinates of the chunk. */
		Coord getPosition();

		/** @return The x, y coordinates of the chunk, measured in tiles. */
		Coord getPositionInTiles();

		Tile& getTile(int x, int y) override;
		Tile& getTile(Coord pos)    override;
		bool hasTile(int x, int y)  override;
		bool hasTile(Coord pos)     override;

		int getWidth() override;
		int getHeight() override;

		bool addItem(Coord pos, Item item, bool allowAlt = true) override; ///< allowAlt does nothing

		/** @return Returns the wall set in the given direction. Used by the Map to manage per-chunk walls. */
		std::set<b2Fixture*>& getWalls();

		/** Creates a trap of the given type with the given positions. */
		Trap& addTrap(const TrapType& type, Coord actionPos = Coord(-1, -1), Coord triggerPos = Coord(-1, -1));

		void addEntity(Entity::Ptr entity)    override;
		bool removeEntity(Entity::Ptr entity) override;
		const std::set<Entity::Ptr>& getEntities(); ///< Returns a set of all the entities in this Chunk.
		void getEntitiesInRect(std::vector<Entity::Ptr>& entitiesOut, sf::FloatRect rect) override;

		MapObject& addObject(sf::Vector2i pos, sf::IntRect texRect) override;
		MapObject& addObject(MapObject object) override;
		size_t getNumObjects() override;
		MapObject& getObject(size_t index) override;

		Interactible& addInteractible(String name, Coord position) override;
		Interactible& addInteractible(String name, const std::vector<Coord>& positions) override;

		enum State {EMPTY, LOADED, GENERATED};
		State getState() const;
		void setState(State state);

		static bool inBounds(Coord pos) {
			return pos.x >= 0 && pos.y >= 0 && pos.x < CHUNK_SIZE && pos.y < CHUNK_SIZE;
		}

	private:
		Coord position;
		Context context;
		State state;

		Tile tiles[CHUNK_SIZE][CHUNK_SIZE];

		std::set<b2Fixture*> walls;
		std::set<Entity::Ptr> entities;
		std::vector<std::unique_ptr<Trap>>          traps;
		std::vector<std::unique_ptr<TreasureChest>> chests;
		std::vector<std::unique_ptr<MapObject>>     objects;
		std::vector<std::unique_ptr<Interactible>>  interactibles;
};

#endif // CHUNK_H
