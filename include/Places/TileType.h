#ifndef TILETYPE_H
#define TILETYPE_H

#include <yaml-cpp/yaml.h>

#include "main.h"
#include "VecUtil.h"

#define TILE_TEX_SIZE 16

/**
 * Defines a type of Tile.
 * @note UT [v]
 */
class TileType {
	public:

		TileType(String name, Coord gloc);
		static TileType EMPTY;

		String getName() const;
		Coord getGLoc() const;
		int getID() const;
		bool isVisible() const;

		enum Material {NONE, GRASS, WOOD, STONE};
		Material getMaterial() const;
		void setMaterial(Material material);

	private:
		String name;
		Coord gloc;
		Material material;
};

#endif // TILETYPE_H
