#ifndef PLACE_H
#define PLACE_H

#include <unordered_map>
#include "Path.h"
#include "VecUtil.h"
#include "Item.h"
#include "MapObject.h"
#include "Entity.h"
#include "Tile.h"
class AnimData;
class EffectsManager;
class Interactible;

/// An interface for the Map.
class Place: public Grid {
	public:

		/**
		 * Adds an Entity to the Place.
		 * @param entity Entity to add.
		 */
		virtual void addEntity(Entity::Ptr entity) = 0;

		/**
		 * Removes an Entity from the Place.
		 * @return true if an entity was actually in the Place.
		 */
		virtual bool removeEntity(Entity::Ptr entity) = 0;

		/**
		 * Returns all the Entities in the given rectangle.
		 * @param entitiesOut Where the entities go.
		 */
		virtual void getEntitiesInRect(std::vector<Entity::Ptr>& entitiesOut, sf::FloatRect rect) = 0;


		/**
		 * Gets a tile at the position.
		 * @pre There is actually a tile at the given position.
		 * @{ */
		virtual Tile& getTile(int x, int y) = 0;
		virtual Tile& getTile(Coord pos) = 0;
		/**@} */

		/** @return true if there is a Tile at the given position.
		 * @{ */
		virtual bool hasTile(int x, int y) = 0;
		virtual bool hasTile(Coord pos) = 0;
		/**@} */

		/** Creates a new MapObject with the given parameter(s) and returns a reference to it.
		 * @{ */
		virtual MapObject& addObject(sf::Vector2i pos, sf::IntRect texRect) = 0;
		virtual MapObject& addObject(MapObject object) = 0;
		/**@} */

		/** @return The number of objects in this Place. */
		virtual size_t getNumObjects() = 0;

		/**
		 * The MapObject at the given index.
		 * @pre 0 <= index < getNumObjects()
		 */
		virtual MapObject& getObject(size_t index) = 0;

		/**
		 * Creates an interactible that occupies the given position(s) and returns a reference to it.
		 * @param name The name of the interactible.
		 * @pre All the given positions exist within the Place.
		 * @{ */
		virtual Interactible& addInteractible(String name, Coord position) = 0;
		virtual Interactible& addInteractible(String name, const std::vector<Coord>& positions) = 0;
		/**@} */

		/**
		 * Adds an item to the given position on the map.
		 * @param allowAlt If true, the item may be placed on a tile near to the given one, if the given tile pos can hold no items.
		 * @return true if it succeeded in placing in item (more likely if allowAlt is true).
		 */
		virtual bool addItem(Coord pos, Item item, bool allowAlt = true) = 0;

		virtual bool isObstructed(sf::Vector2i position, void* mob) {
			if (!hasTile(position)) return true;
			return getTile(position).isPath() == Tile::NO;
		}

		/** @return The width of the Place, or -1 if the width is abstract or unbounded. */
		virtual int getWidth() = 0;

		/** @return The height of the Place, or -1 if the height is abstract or unbounded. */
		virtual int getHeight() = 0;
};

#endif // PLACE_H
