#ifndef GENERATOR_H
#define GENERATOR_H

#include "VecUtil.h"
#include "MapType.h"
class Map;
class Chunk;

namespace Generator {
	/**
	 * Generates the dungeon of a chunk.
	 * @param map The map this chunk is within.
	 * @param chunk The chunk to generate.
	 */
	void generateChunk(Map& map, Chunk& chunk);

	void generatePaths();
	void generateRooms();
	void generateTraps();
	void generatePhysicsWalls();
	void addLadder();

	/**
	 * Creates a random path from begin to end (filling with floor).
	 * @param begin The beginning of the path.
	 * @param end The end of the path.
	 */
	void createPath(Coord begin, Coord end);

	/**
	 * Creates a room that either contains or is adjacent to the given position.
	 * This room will make sure not to touch any other walls. Minimum size is 3x3.
	 * @param start Where to grow the room from.
	 * @return Whether it actually made a room or not.
	 */
	bool growRoom(Coord start);

	void fillRoom(sf::IntRect rect);

	/**
	 * Places a path tile at the given position.
	 * @param pos Where to place the tile.
	 * @param tile The floor tile to place.
	 */
	void doFloor(Coord pos, MapType::GenTile tile);

	void doWall(Coord pos);

	/** Does connecting walls. */
	void doConnectingWalls(Direction dir);
};

#endif // GENERATOR_H
