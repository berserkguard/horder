#ifndef MAPTYPE_H
#define MAPTYPE_H

#include "main.h"
#include "Trap.h"
#include "TreasureChest.h"
#include "EncounterBag.h"
class TileType;

/**
 * Specifies a type of Map, for example, a basement.
 * @note UT [_]
 */
class MapType {
	public:
		enum MapFlag {PERSISTENT = 1, BOUNDLESS = 1 << 1, DARK = 1 << 2, GROSS = 1 << 3, TOP = 1 << 4, HOME = 1 << 5};
		enum GenTile {FLOOR, WALL, ROOM, GENT_COUNT};
		enum GenObject {LADDER, GENO_COUNT};

		MapType(String name);
		MapType(String name, int flags);

		String getName() const;

		/** @param file The file that maps of this type will load their data from. */
		void setFile(String file);
		/** @return The file that maps of this type will load their data from. */
		String getFile() const;

		void setGenTile(GenTile genTile, const TileType& tile);
		const TileType& getGenTile(GenTile genTile) const;

		void setGenObj(GenObject getObj, sf::IntRect object);
		sf::IntRect getGenObj(GenObject genObj) const;

		void addAdjacentMap(Direction direction, const MapType& map);
		const MapType* getAdjacentMap(Direction direction) const;

		void addEncounter(int level, Encounter& encounter);
		EncounterBag* getEncounters(int level) const;

		void addTrap(const TrapType* trap);
		const TrapType* getRandomTrap() const;

		void addChestItem(TreasureChest::Type chest, const ItemType& item);
		void fillChest(TreasureChest& chest) const;

		bool isPersistent() const; ///< I don't know what this means.
		bool isInfinite() const;   ///< Goes on forever.
		bool isDark() const;       ///< Cannot see through walls.
		bool isGross() const;      ///< Floor dimness varies somewhat.
		bool isTop() const;        ///< Connects to the mansion.
		bool isHome() const;       ///< Is the mansion.

	private:
		template<typename T>
		using ptr = std::unique_ptr<T>;

		void init();

		String name, file;
		bool persistent, infinite, top, dark, gross, home;

		const MapType* adjacentMaps[DIR_COUNT];

		const TileType* genTiles[GENT_COUNT];
		sf::IntRect genObjs[GENO_COUNT];

		std::vector<ptr<EncounterBag> > encounters;
		std::vector<const TrapType*> traps;
		std::vector<const ItemType*> chestItems[TreasureChest::TYPE_COUNT];
};

#endif // MAPTYPE_H
