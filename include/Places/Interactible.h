#ifndef INTERACTIBLE_H
#define INTERACTIBLE_H

#include "main.h"
#include "VecUtil.h"

/**
 * You can interact with Interactibles. This is just a basic data structure for the notion.
 * @note UT [_]
 */
class Interactible {
	public:
		enum Type {NONE, CONNECTION, SPECIAL};
		/** Creates a new Interactible with the given name and no type. */
		Interactible(String name);

		/** @return The name of this Interactible. Is referred to for special actions. */
		String getName();

		/** @return The type of Interactible. Defaults to NONE. */
		Type getType();


		/** Makes this Interactible of the SPECIAL type. They do special things. */
		void setSpecial();


		/** Makes this Interactible of the CONNECTION type. Connections allow users to go from map to map. */
		void setConnection(Direction direction, sf::Vector2f destination);

		/** @return The direction the connection goes in. */
		Direction getDirection();

		/** @return The position you end up at on the other end of the connection. */
		sf::Vector2f getDestination();

	private:
		String name;
		Type type;

		Direction direction;
		sf::Vector2f destination;
};

#endif // INTERACTIBLE_H
