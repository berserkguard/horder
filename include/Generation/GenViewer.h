#ifndef GENVIEWER_H
#define GENVIEWER_H

#include "MapType.h"
#include "TimeStep.h"
#include "EffectsManager.h"
#include "Map.h"
#include "Blueprint.h"
#include <SFML/Graphics.hpp>

/** A class for testing the new generation by viewing it. */
class GenViewer {
	public:
		GenViewer();

		/** Creates a new SFML window to view the generation of a map in. */
		int show();

	private:
		bool init();    ///< @return true on success
		bool execute(); ///< @return true when done
		bool events();  ///< @return true when done
		void render();
		void logic(TimeStep timeStep);
		void refreshBlueprint(Coord pos);
		void renderWalls(const std::vector<std::pair<sf::Vector2f, sf::Vector2f> >& walls, sf::Color color);

		sf::Font* simpleFont;

		sf::RenderWindow window;
		sf::Clock mainClock;
		sf::Time prevTime;

		MapType mapType;
		EffectsManager effects;
		std::unique_ptr<Map> map;
		std::unique_ptr<Blueprint> blueprint;
		Chunk* chunk;
};

#endif // GENVIEWER_H
