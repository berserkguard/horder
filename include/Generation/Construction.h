#ifndef CONSTRUCTION_H
#define CONSTRUCTION_H

#include "Blueprint.h"
#include "Map.h"

/**
 * Fills a chunk with tiles using it's blueprint.
 */
class Construction {
	public:
		Construction(Blueprint& blueprint, Map& map);

		/** Fills all the tiles of the chunk with the map type's generation tiles. */
		void construct();

	private:
		void constructTiles();
		void constructWalls();

		Blueprint* blueprint;
		Map* map;
};

#endif // CONSTRUCTION_H
