#ifndef BLUEPRINT_H
#define BLUEPRINT_H

#include "main.h"
#include "VecUtil.h"
#include "Chunk.h"
#include "ShuffleBag.h"
class MapType;

#include <SFML/Graphics.hpp>
#include <vector>

class Blueprint: public Grid {
	private: struct MegaNode;
	public:
		/**
		 * Defines, but does not generate, a new blueprint.
		 * @param mapType The MapType with the generation info the Blueprint might need.
		 * @param mapSeed The seed of the map this blueprint is for. Maps with the same seed will produce the same blueprints.
		 * @param chunk   The exact chunk this blueprint is for.
		 */
		Blueprint(const MapType& mapType, unsigned int mapSeed, Chunk& chunk);

		/** Generates the Blueprint! The blueprint will be blank until this is called. Calling multiple times does nothing. */
		void generate();


		/** A room in a blueprint. You should put treasure in this. Has a bunch of cells and a center. */
		struct Room {
			/** Creates a blank room. */
			Room(): owner(nullptr) { }
			/** Creates a room that contains all the given rectangles. */
			Room(const std::vector<sf::IntRect>& rects);

			std::vector<Coord> cells;
			std::vector<size_t> wallChain;
			Coord center;
			MegaNode* owner;
		};

		/** @return The number of rooms in the blueprint. */
		size_t getNumRooms();

		/**
		 * @param index The index of a room. (0 <= index < getNumRooms())
		 * @return The room at the given index.
		 */
		const Room& getRoom(size_t index);

		/** @return Whether or not the given position is part of a room. The coordinates must be between [0 and CHUNK_SIZE). */
		bool hasRoom(int x, int y);

		/** @return The room that contains the given position. Only guarenteed to work if hasRoom(x, y) returns true. */
		const Room& getRoom(int x, int y);


		/** @return Whether or not the given position is part of a hall. The coordinates must be between [0 and CHUNK_SIZE).*/
		bool hasPath(int x, int y);

		/** @return The hall that contains the given position. Only guarenteed to work if hasPath(x, y) returns true. */
		Coord getPath(int x, int y);


		/** Cells define what a position should have generated upon it. */
		enum Cell {WALL, PATH, ROOM};
		/** @return The cell at the given position. The coordinates must be between [0 and CHUNK_SIZE). @{ */
		Cell getCell(int x, int y);
		Cell getCell(Coord pos);
		/** @} */
		/** @param cell The cell to set the position to. The coordinates must be between [0 and CHUNK_SIZE). @{ */
		void setCell(int x, int y, Cell cell);
		void setCell(Coord pos, Cell cell);
		/** @} */


		/** @return The type of map this blueprint is for. */
		const MapType& getMap();

		/** @return The chunk this blueprint is for. */
		Chunk& getChunk();


		typedef std::pair<sf::Vector2f, sf::Vector2f> Wall;

		/** @return Vector of pairs of vectors for each wall. */
		const std::vector<Wall>& getInnerWalls();

		/** @return Vector of pairs of vectors for each wall. */
		const std::vector<Wall>& getOuterWalls();


		/**
		 * Method used by the pather to generate paths in this map.
		 * You shouldn't want to use this, but if you do, know that a position is "obstructed" if it and all
		 * adjacent positions are within the blueprint and are not rooms.
		 */
		bool isObstructed(sf::Vector2i position, void* mob = nullptr);
		int getWidth()  {return CHUNK_SIZE;} ///< Blueprints are as wide as chunks.
		int getHeight() {return CHUNK_SIZE;} ///< Blueprints are as tall as chunks.

	private:
		struct Node {
			Coord pos;
			MegaNode* owner;
			Node(Coord pos): pos(pos), owner(nullptr) { }
		};
		struct MegaNode {
			std::vector<Node*> nodes;
			std::vector<Room*> rooms;
			MegaNode(Node& node) {
				nodes.push_back(&node);
				node.owner = this;
			}
			MegaNode(Room& room) {
				rooms.push_back(&room);
				room.owner = this;
			}
			void merge(MegaNode& other);
		};
		friend bool megaNodeSizeSort(MegaNode* node1, MegaNode* node2);
		bool mergeCheck(Coord c, MegaNode& node);
		bool orthMergeCheck(Coord c, MegaNode& node);
		void mergeMegaNodes(MegaNode& node1, MegaNode& node2);
		void mergeMegaNodeInward(MegaNode& node);

		void calcRooms();
		void calcEntrances();
		void entrance(Coord pos, Coord dir);

		void connectEverything();
		bool connectHalls(Node& hall1, Node& hall2);
		void straightHall(Coord pos, Direction dir, MegaNode& node);
		Coord createRandomExit(Room& room, Direction dir = NONE);
		Node& newNode(Coord pos);

		void createWalls();
		typedef std::map<Coord, size_t, Coordcmp> EPTWM;
		void createWallsInDirection(Coord dir, EPTWM& endpointMap);
		Room* getRoomPoint(Coord pos);
		void addInnerWall(sf::Vector2f end1, sf::Vector2f end2, EPTWM& endpointMap);
		bool chainWall(std::pair<Coord, size_t>& val, Room* room, EPTWM& endpMap, bool front = false);
		bool pathCell(Coord pos);
		bool shouldShift(Coord top1, Coord top2, Coord bot1, Coord bot2);
		sf::Vector2f shiftWallCorner(sf::Vector2f pos);

		void weirdRooms();
		void fillCorner(Room& room, int r, float max);
		void fillCorners(Room& room);
		bool matchWalls(Wall& wall1, Wall& wall2);

		const MapType* mapType;
		Chunk* chunk;
		unsigned int seed;

		Cell  cells[CHUNK_SIZE + 2][CHUNK_SIZE + 2];
		Room* roomsInCells[CHUNK_SIZE][CHUNK_SIZE];
		Node* nodesInCells[CHUNK_SIZE][CHUNK_SIZE];

		std::vector<Wall> innerWalls; // physics walls
		std::vector<Wall> outerWalls; // lighting walls
		std::vector<std::unique_ptr<Room> > rooms;

		std::vector<std::unique_ptr<Node> > hallNodes;
		ShuffleBag<Node*> nodeBag;

		std::vector<std::unique_ptr<MegaNode> > megaNodes;
};

#endif // BLUEPRINT_H
