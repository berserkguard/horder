#ifndef GENERATOR_H
#define GENERATOR_H

#include "VecUtil.h"
#include <SFML/Graphics.hpp>

const int ROOM_COLUMNS = 4;

namespace Generation {
	/**
	 * Returns the rectangle for the room at the given position or an empty rectangle if no room is there.
	 * @param seed The seed of the map that this room is in.
	 * @param pos The position (in 4 per chunk coordinates) of the room you want.
	 * @return The int rectangle (in tile coordinates) of the room.
	 */
	sf::IntRect getRoomRect(int seed, Coord pos);

	/**
	 * Returns a vector of the positions for the entrances of the given chunk in the given direction.
	 * @param seed The seed of the map that this room is in.
	 * @param chunkPos The position of the chunk you want entrances for.
	 * @param dir The direction the entrances are in.
	 * @return The vector of the entrances.
	 */
	std::vector<int> getEntrances(int seed, Coord chunkPos, Direction dir);
};

#endif // GENERATOR_H
