#ifndef SCROLLBAR_WIDGET_H
#define SCROLLBAR_WIDGET_H

#include "UIWidget.h"

class ScrollbarWidget : public UIWidget {
public:
	ScrollbarWidget(const std::string name);
	~ScrollbarWidget();

	void render(sf::RenderWindow& window) override;
	void update(TimeStep timeStep) override;
	void setVisibility(bool show = true) override;
	bool isVisible() override;

	void setSize(sf::Vector2f size) override;
	bool handleEvent(sf::Event& event) override;

	void setStepsShown(int stepsShown);
	int getValue();
	void configure(int minVal, int step, int bigStep, int maxVal);
	void setValue(int val);
private:
	void validate();

	bool dragging;
	int dragVal;
	sf::Vector2f mousePos;

	bool visible;
	int stepsShown;
	int minVal, step, bigStep, maxVal;
	int val;
};

#endif // SCROLLBAR_WIDGET_H
