#ifndef WINDOW_WIDGET_H
#define WINDOW_WIDGET_H

#include "UIWidget.h"

class WindowWidget : public UIWidget {
	public:
		WindowWidget(const std::string name);
		~WindowWidget();

		void render(sf::RenderWindow& window) override;
		void update(TimeStep timeStep) override;
		bool handleEvent(sf::Event& event) override;
		void setVisibility(bool show = true) override;
		bool isVisible() override;
		void setSize(sf::Vector2f size) override;

		virtual sf::Vector2f getMinSize() = 0;
		virtual void renderContent() = 0;

		void setTexture(sf::Texture* tex);
		void setButtonTexture(sf::Texture* tex);
		void setResizable(bool resizable);

	protected:
		bool visible;
		sf::Texture* tex[2];
		sf::RenderTexture content;
		bool resizable; // whether or not window can be resized by user

		// For window dragging
		short dragMode; // -1 = no drag, 0 = drag start, 1 = dragging
		bool resizing;
		short resizeMode;
		sf::Vector2f mousePos, anchor, offset;
};

#endif // WINDOW_WIDGET_H
