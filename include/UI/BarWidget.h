#ifndef BAR_WIDGET_H
#define BAR_WIDGET_H

#include "UIWidget.h"
#include "Mob.h"

enum BarType {BAR_HEALTH, BAR_MANA};

class BarWidget : public UIWidget {
public:
	BarWidget(const std::string name);
	~BarWidget();

	void render(sf::RenderWindow& window) override;
	void setVisibility(bool show = true) override;
	bool isVisible() override;

	void associateMob(Mob& mob, BarType type);
private:
	Mob* mob;
	bool visible;
	BarType type;
};

#endif // BAR_WIDGET_H
