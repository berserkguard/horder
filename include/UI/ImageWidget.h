#ifndef IMAGE_WIDGET_H
#define IMAGE_WIDGET_H

#include "UIWidget.h"

class ImageWidget : public UIWidget {
public:
	ImageWidget(const std::string name);
	~ImageWidget();

	void render(sf::RenderWindow& window) override;
	void setPosition(sf::Vector2f pos) override;

	void setVisibility(bool show = true) override;
	bool isVisible() override;

	void setImage(std::string file);
protected:
	bool visible;
	sf::Sprite image;
};

#endif // IMAGE_WIDGET_H
