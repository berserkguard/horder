#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <SFML/Graphics.hpp>
#include "UIWidgetType.h"
#include "TimeStep.h"

class UIWidget {
public:
	UIWidget(const UIWidgetType type, const std::string name);

	virtual void setPosition(sf::Vector2f pos);
	virtual sf::Vector2f getPosition();

	virtual void setSize(sf::Vector2f pos);
	virtual sf::Vector2f getSize();

	virtual void render(sf::RenderWindow& window);
	virtual void update(TimeStep timeStep);
	virtual bool handleEvent(sf::Event& event);

	virtual void setVisibility(bool show = true) = 0;
	virtual bool isVisible() = 0;

	std::string getName() const;

	void setLayer(int layer);
	int getLayer();
protected:
	sf::Vector2f position, size;
	UIWidgetType type;
	int layer;
	const std::string name;
};

#endif // UI_WIDGET_H
