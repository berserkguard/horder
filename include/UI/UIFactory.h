#ifndef UIFACTORY_H
#define UIFACTORY_H

#include "UIWidget.h"
#include "UIManager.h"

/// A Factory for instantiating various custom game widgets.
class UIFactory {
	public:
		/**
		 * Instantiates a new UIWidget of the given type.
		 * @param type The type of the widget.
		 * @return A pointer to the newly-created widget.
		 */
		static UIWidget* createWidget(UIWidgetType type, const std::string name, UIManager* ui);
};

#endif // UIFACTORY_H
