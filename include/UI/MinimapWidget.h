#ifndef MINIMAP_WIDGET_H
#define MINIMAP_WIDGET_H

#include "UIWidget.h"
#include "Player.h"

class MinimapWidget : public UIWidget {
public:
	MinimapWidget(const std::string name);
	~MinimapWidget();

	void render(sf::RenderWindow& window) override;
	void setVisibility(bool show = true) override;
	bool isVisible() override;

	void update(TimeStep timeStep);

	void associatePlayer(Player& player);
	void forget();

private:
	MinimapWidget(const Map&) = delete;
	MinimapWidget& operator=(const Map&) = delete;

	void makeMinimapTexture();

	bool visible;
	Player* player;
	bool changed;
	sf::Texture tex;

	/** map<MapType name, map<Chunk pos, MinimapTile tiles> > */
	std::map<String, std::map<Coord, unsigned char*, Coordcmp> > memory;
};

#endif // MINIMAP_WIDGET_H
