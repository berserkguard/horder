#ifndef BACKPACK_WINDOW_H
#define BACKPACK_WINDOW_H

#include "WindowWidget.h"
#include "ItemContainer.h"
#include "Player.h"
#include "ScrollbarWidget.h"
#include "TooltipWidget.h"

class BackpackWindow : public WindowWidget {
public:
	BackpackWindow(const std::string name);

	sf::Vector2f getMinSize() override;
	void renderContent() override;
	bool handleEvent(sf::Event& event) override;
	void render(sf::RenderWindow& window) override;
	void setSize(sf::Vector2f size) override;
	void update(TimeStep timeStep) override;

	bool isPlayerBackpack();

	void associateItems(ItemContainer& container, Player& player);
private:
	int getItemIndex(sf::Vector2f cursor);
	void refreshTooltip(sf::Vector2f cursor);

	int numRows, numCols;

	ItemContainer* container;
	Player* player;
	int idx;
	sf::Vector2f pos; // For making loot windows close when player walks away
	ScrollbarWidget* scrollbar;
	TooltipWidget* tooltip;
};

#endif // BACKPACK_WINDOW_H
