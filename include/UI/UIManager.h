#ifndef UI_MANAGER_H
#define UI_MANAGER_H

#include <string>
#include <map>
#include "UIWidget.h"
#include "Scene.h"
#include "TimeStep.h"

enum UILayer {UI_NORMAL, UI_ABOVE};

/// Maintains a list of all instantiated widgets and contains methods to act on them.
class UIManager {
public:
	/**
	 * Initializes the UIManager.
	 */
	UIManager();

	/**
	 * Frees all memory used by the UIManager
	 * @note Should only be called after the application window is destroyed.
	 */
	~UIManager();

	/**
	 * Sets the current scene to use for the UI.
	 * @param newScene The new scene to switch to.
	 */
	void setScene(Scene newScene);

	/**
	 * Returns the current scene.
	 * @return The current scene.
	 */
	Scene getScene();

	/**
	 * Allows the instantiated widget to be maintained by the UIManager.
	 * @note If you want to use a widget manually, do not add it to the UIManager.
	 * @param widget The widget to add. Widgets must have unique names.
	 */
	void addWidget(UIWidget* widget);

	/**
	 * Brings a widget to the front (in front of all other widgets).
	 * @param widget The widget to move to the front.
	 */
	void bringToFront(UIWidget* widget);

	/**
	 * Retrieves the widget with the specified name.
	 * @param name The name of the widget.
	 * @return The widget pointer, or NULL if no widget with name exists.
	 */
	UIWidget* getWidget(const std::string name);

	/**
	 * Removes the specified widget from the UIManager. Does NOT destroy it!
	 * @param name The name of the widget to retrieve.
	 * @return The widget that was removed. If no widget with name exists, returns NULL.
	 */
	UIWidget* removeWidget(const std::string name);

	/**
	 * Updates every widget according to the given timestep.
	 * @param timeStep The time difference since last update.
	 */
	void update(TimeStep timeStep);

	/**
	 * Invokes the event handler for each widget on the specified event.
	 * @param event The sf::Event being handled.
	 * @param false if event is to propagate, true if it is to not.
	 */
	bool handleEvent(sf::Event& event);

	/**
	 * Renders all visible widgets to the screen.
	 * @param window The RenderWindow to render to.
	 * @param layer The layer index to render, set to -1 to render all widgets.
	 */
	void render(sf::RenderWindow& window, int layer = -1);

	/**
	 * Returns whether or not a UIWidget was clicked on during the last handleEvent.
	 * @return True if any widget was clicked, false otherwise.
	 */
	bool wasClicked();

private:
	UIManager(const UIManager&) = delete;
	UIManager& operator=(const UIManager&) = delete;

	std::map<std::string, UIWidget*> widgets[SCENE_COUNT];
	std::map<std::string, bool> oldVisibilities[SCENE_COUNT];
	Scene currentScene;
};

#endif // UI_MANAGER_H
