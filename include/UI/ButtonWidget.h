#ifndef BUTTON_WIDGET_H
#define BUTTON_WIDGET_H

#include <functional>

#include "UIWidget.h"

class ButtonWidget : public UIWidget {
	public:
		ButtonWidget(const std::string name);
		~ButtonWidget();

		void setPosition(sf::Vector2f pos) override;
		void render(sf::RenderWindow& window) override;
		void update(TimeStep timeStep) override;
		void setVisibility(bool show = true) override;
		bool isVisible() override;

		void setNormalTexture(sf::Texture* tex);
		void setHoverTexture(sf::Texture* tex);
		void setCallback(std::function<void(void)> callback);
	private:
		bool visible;
		sf::Vector2f position;
		sf::Vector2f size;
		bool inside, alt;
		sf::Texture* tex[2];
		//void (*callback)(void);
		std::function<void(void)> callback;
};

#endif // BUTTON_WIDGET_H
