#ifndef DEATH_IMAGE_WIDGET_H
#define DEATH_IMAGE_WIDGET_H

#include "ImageWidget.h"
#include "ButtonWidget.h"
#include "Player.h"

class DeathImageWidget : public ImageWidget {
public:
	DeathImageWidget(const std::string name);
	~DeathImageWidget();

	void update(TimeStep timeStep) override;
	void render(sf::RenderWindow& window) override;

	void associatePlayer(Player* player);
private:
	void callback();

	ButtonWidget* btn;
	Player* player;
	bool visible;
	sf::Sprite image;
};

#endif // DEATH_IMAGE_WIDGET_H
