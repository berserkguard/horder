#ifndef CHARINFO_WINDOW_H
#define CHARINFO_WINDOW_H

#include "WindowWidget.h"
#include "Player.h"
#include "ScrollbarWidget.h"
#include "TooltipWidget.h"

class CharInfoWindow : public WindowWidget {
public:
	CharInfoWindow(const std::string name);

	sf::Vector2f getMinSize() override;
	void renderContent() override;
	bool handleEvent(sf::Event& event) override;
	void render(sf::RenderWindow& window) override;
	void setSize(sf::Vector2f size) override;
	void update(TimeStep timeStep) override;

	void associatePlayer(Player& player);
private:
	int getSlotIndex(sf::Vector2f cursor);

	Player* player;
	ScrollbarWidget* scrollbar;
	TooltipWidget* tooltip;
	int idx;
};

#endif // CHARINFO_WINDOW_H
