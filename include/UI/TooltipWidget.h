#ifndef TOOLTIP_WIDGET_H
#define TOOLTIP_WIDGET_H

#include "UIWidget.h"
#include "Item.h"
#include "ItemType.h"

enum TooltipType {TT_GENERIC, TT_BACKPACK, TT_CHARINFO};

class TooltipWidget : public UIWidget {
public:
	TooltipWidget(const std::string& name);
	~TooltipWidget();

	void render(sf::RenderWindow& window) override;
	void setVisibility(bool show = true) override;
	bool isVisible() override;

	void setType(TooltipType type);

	void associateItem(Item& item);
private:
	TooltipType ttType;
	bool visible;
	const ItemType* type;
};

#endif // TOOLTIP_WIDGET_H
