#ifndef GAME_H
#define GAME_H

#include "ItemType.h"
#include "MonsterType.h"
#include "TileType.h"
#include "Settings.h"
#include "ValuablesManager.h"
#include "EffectsManager.h"
#include "World.h"

enum GameState {NO_STATE, IN_GAME, MENU};

/// Groups the various game components together in one object.
class Game {
	public:
		Game();

		/** Parses game data from a yaml file. */
		bool load(String file);

		/** @return The current game state. */
		GameState getState();

		/** @return The game's world. */
		World& getWorld();

		/** @return The Box2D physics world. */
		b2World& getPhysWorld();

		/** @return The game's settings. */
		Settings& getSettings();

		EffectsManager& getEffects();

		ValuablesManager& getValuables();

		void setPaused(bool value);
		bool isPaused();

	private:

		YAML::Node yamlWithin(YAML::Node& node, String key);
		std::vector<YAML::Node> yamlWithinAll(YAML::Node& node, String key);

		GameState state;
		Settings settings;
		std::unique_ptr<b2World> physWorld;
		std::unique_ptr<World> world;

		EffectsManager effects;
		ValuablesManager valuablesManager;

		bool paused;
};

#endif // GAME_H
