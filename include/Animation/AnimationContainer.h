#ifndef ANIMATION_CONTAINER_H
#define ANIMATION_CONTAINER_H

#include <map>
#include <memory>
#include "Animation.h"
#include "TimeStep.h"

/**
 * Organizes animations into a logical grouping.
 */
class AnimationContainer {
public:
	AnimationContainer();

	/**
	 * Adds an animation to the container. Must be a valid object.
	 * @param animation The animation to add
	 */
	Animation& newAnimation(Animation anim);
	Animation& newAnimation(std::string name, int numFrames, float length = 0.0f);

	/**
	 * Returns the animation with the given name.
	 * @return An Animation object from the given name, or NULL if no such animation exists
	 */
	Animation* getAnimation(std::string name);

	/** Updates all the animations in the container. */
	void update(TimeStep timeStep);

private:
	std::map<std::string, std::unique_ptr<Animation> > animations;
};

#endif // ANIMATION_CONTAINER_H
