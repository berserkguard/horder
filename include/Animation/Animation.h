#ifndef ANIMATION_H
#define ANIMATION_H

#include <SFML/Graphics.hpp>
#include "TimeStep.h"

/**
 * An enumeration for possible animation directions.
 */
enum AnimDir {
	ANIM_FORWARD,	///< Play the animation forwards
	ANIM_BACKWARD	///< Play the animation backwards
};

/**
 * A class for working with a frame-animated sprite.
 */
class Animation {
public:
	/**
	 * Creates an animation.
	 * @param name The name for the animation. Should be unique
	 * @param numFrames The number of frames in the animation
	 * @param length The duration (in seconds) of the animation
	 */
	Animation(std::string name, int numFrames, float length = 0.0f);

	/** Updates the animation based on the given timestep. */
	void update(TimeStep timeStep);

	/**
	 * Applies the animation to the given sprite.
	 * @param sprite A sprite whose texture bounds to modify
	 * @param frameWidth The width of a frame in the sprite, in pixels
	 * @param frameHeight The height of a frame in the sprite, in pixels
	 */
	void apply(sf::Sprite& sprite, int frameWidth, int frameHeight);

	/**
	 * Retrieves the current frame index.
	 * @return The zero-based index of the current frame
	 */
	int getFrame();

	/**
	 * Sets the current frame and pauses the animation.
	 * @param frameIndex The zero-based index to set the current frame to
	 */
	void setFrame(int frameIndex);

	/**
	 * Plays the animation from the current frame.
	 */
	void play();

	/**
	 * Stops the animation, resetting it to the initial frame.
	 */
	void stop();

	/**
	 * Pauses the animation, leaving the current frame as it is.
	 */
	void pause();

	/**
	 * Returns whether or not the animation is on the last frame.
	 * @return True if last frame, false otherwise
	 */
	bool isLastFrame();

	/**
	 * Returns whether or not the animation is currently playing.
	 * @return True if playing, false otherwise
	 */
	bool isPlaying();

	/**
	 * Sets a new length for the animation. Note: stops & resets the animation.
	 * @param newLength The new duration for the animation, in seconds
	 */
	void setLength(float newLength);

	/**
	 * Sets the direction to play the animation.
	 * @param dir The direction of the animation
	 */
	void setDirection(AnimDir dir);

	/**
	 * Returns the name of the animation.
	 * @return A string with the animation's name
	 */
	std::string getName();

private:
	std::string name;	// The name of the animation
	bool playing;		// Whether or not the animation is playing (i.e., advancing based on time)

	int numFrames;		// The total number of frames in the animation
	int curFrame;		// The current frame in the animation

	float elapsedTime;	// The elapsed time since the start of the animation, in seconds
	float length;		// The length of the animation, in seconds

	AnimDir dir;		// The direction of the animation
};

#endif // ANIMATION_H
