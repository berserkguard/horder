#ifndef SOUNDSMANAGER_H
#define SOUNDSMANAGER_H

#include <SFML/Audio.hpp>
#include <memory>

#define MAX_SOUNDS 20

enum SoundType {ST_WORLD, ST_UI, ST_COUNT};
enum Sound {S_NONE, S_LADDER, S_STEPS_GRASS, S_STEPS_WOOD, S_STEPS_STONE,
                    S_OPEN, S_DROP, S_GRAB, S_SHOOT, S_FLAME, S_HIT, S_MISS, S_SLIME, S_CLINK,
                    S_DEATH_DORKLEN, S_DEATH_SLIME, S_COUNT};

typedef std::shared_ptr<sf::Sound> SoundPtr;

/**
 * TODO: Add wrapper for sf::Sound.
 * @note UT [_]
 */
class SoundsManager {
	public:
		SoundsManager();

		/**
		 * Starts playing the chosen sound.
		 * @param sound The sound to play.
		 * @return The sf::Sound that is playing and can be used to change volume and stuff.
		 */
		SoundPtr newSound(Sound sound, SoundType type = ST_WORLD);

		/** Starts playing the chosen sound at the given position, utilizing spatialization. */
		SoundPtr newSound(Sound sound, sf::Vector2f soundPos, SoundType type = ST_WORLD);

		void updateAll();
		void pauseAll(SoundType type);
		void playAll(SoundType type);
		void stopAll(SoundType type);

	private:
		SoundPtr createSoundBase(Sound sound, SoundType type);
		void addSoundToTypes(std::shared_ptr<sf::Sound> sound, SoundType type);

		sf::SoundBuffer* soundBuffers[S_COUNT];
		std::set<SoundPtr> soundTypes[ST_COUNT];
};

#endif // SOUNDSMANAGER_H
