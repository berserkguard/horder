#ifndef ENTITY_H
#define ENTITY_H

#include <vector>
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

#include "main.h"
#include "AnimData.h"
class EffectsManager;

/// An entity has a sprite and a collision box.
class Entity {
	public:
		/// The type of the entity.
		enum Type {NONE, MOB, PROJECTILE, CHEST};

		/// Constructs a new entity that creates a body with the given definition.
		/** @param def The definition used to create the body. */
		Entity(Type type, b2BodyDef& def, EffectsManager& effects);

		/** Destroys the entity's body. */
		virtual ~Entity();

		typedef std::shared_ptr<Entity> Ptr;

		/**
		 * Called every frame. Required.
		 * @return Returns true if it is dead and needs to be destroyed.
		 */
		virtual bool update(TimeStep timeStep);

		/** @return The type of the entity. */
		Type getType();

		/**
		 * Gives the Entity a collision body.
		 * @param bodyDef The definition used to create the body.
		 * @return The created body.
		 */
		b2Body& addCollisionBody(b2BodyDef& bodyDef);

		/** @return The Box2D collision body of the object. */
		b2Body& getCollisionBody();

		/**
		 * Sets the position of the Entity's body.
		 * Don't use this too often. Physics bodys should typically be moved with forces.
		 * @param pos The position to move the body to. (Measured in Tiles)
		 */
		void setPosition(sf::Vector2f pos);

		/// Sets the position of the Entity's body.
		/** Identical to that other method, except different parameters. */
		void setPosition(float x, float y);

		/// Returns the position of the Entity's body.
		/** @return The position of the Entity. (Measured in tiles) */
		const sf::Vector2f getPosition();

		/**
		 * Returns one of the Entity's sprite. You can modify this returned sprite.
		 * @param idx The layer index of the sprite to retrieve.
		 * @return The sprite.
		 */
		sf::Sprite& getSprite(int idx);

		/**
		 * Returns the vector list of the sprites. Cannot be modified.
		 * @return A const list of sprites.
		 */
		const std::vector<sf::Sprite>& getSprites();

		/** @return The Entity's animation data. */
		AnimData& getAnimData();

		EffectsManager& getEffects();

		/** Called whenever a collision takes place between two Entities. */
		virtual void onEntityCollision(Entity& collidingWith);

		/** Called whenever a collision takes place between this Entity and a wall. */
		virtual void onWallCollision();

		/** Sets the visibility of this Entity. */
		void setVisible(bool visible);

		/** @return true if visible, false otherwise */
		bool isVisible();

	protected:
		std::vector<sf::Sprite> spriteLayers;

	private:
		// entities can't be copied because bodies can't be copied
		Entity(const Entity&) = delete;
		virtual Entity& operator=(const Entity&) = delete;

		Type type;
		AnimData animData;
		b2Body* body;
		EffectsManager* effects;
		bool visible;
};

enum UserDataOwner {UD_UNIMPORTANT, UD_WALL, UD_ENTITY};
struct PhysUserData {
	UserDataOwner ownerType;
	union {
		struct {
			b2Shape* graphic;
			b2Shape* lighting; // for walls
		} shape;
		Entity* entity; // for entities (duh)
	};
	PhysUserData(UserDataOwner owner): ownerType(owner), shape() { }
	~PhysUserData() {
		if (ownerType == UD_WALL) {
			delete shape.lighting;
			delete shape.graphic;
		}
	}
};

#endif // ENTITY_H
