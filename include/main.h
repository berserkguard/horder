#ifndef MAIN_H
#define MAIN_H

#if defined __FAST_MATH__
#   undef isnan
#   undef isinf
#   undef isfinite
#endif

class b2World;
namespace Get {
    b2World& physWorld();
    float fps();
}

#include <string>
typedef std::string String;

const float TAU = 6.2831853f;

#define TILE_SIZE 64

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

#endif // MAIN_H
