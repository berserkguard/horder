#ifndef SCENE_H
#define SCENE_H

/// An enumeration for all possible scenes.
enum Scene {
	SCENE_MENU,
	SCENE_GAME,
	SCENE_COUNT
};

#define INITIAL_SCENE SCENE_MENU

#endif // SCENE_H
