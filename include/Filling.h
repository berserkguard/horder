#ifndef FILLING_H
#define FILLING_H

#include "Blueprint.h"
#include "Map.h"

/**
 * Fills a chunk with traps, monsters and treasure using it's blueprint.
 */
class Filling {
	public:
		Filling(Blueprint& blueprint, Map& map);

		/** Fills with traps, monsters and treasure. */
		void fill();

	private:
		void findEmptyCells();
		void addTraps();
		void addMonsters();
		void addTreasure();
		void addTreasureToRoom(const Blueprint::Room& room);
		void addLadder();
		std::pair<Coord, Coord> getRandomLongDirection(Coord pos);

		ShuffleBag<Coord> emptyCells;
		Blueprint* blueprint;
		Map* map;
};

#endif // FILLING_H
