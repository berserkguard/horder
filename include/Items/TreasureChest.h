#ifndef TREASURECHEST_H
#define TREASURECHEST_H

#include "Item.h"
#include "Entity.h"
class Place;

/**
 * TreasureChests can have items in them and then they drop those items upon opening.
 * @note UT [_]
 */
class TreasureChest: public Entity {
	public:

		enum Type {BRONZE, ///< Crappy treasure.
		           SILVER, ///< Lots of crappy treasure or some decent treasure.
				   GOLD,   ///< Lots of decent treasure or some great treasure.
				TYPE_COUNT};

		/** Creates a new TreasureChest. */
		TreasureChest(Type type, EffectsManager& effects, sf::Vector2f position);

		/**
		 * Adds an item to the treasure chest that it drops upon opening.
		 * This will also close the chest.
		 * @param item The item to put in the chest.
		 */
		void addItem(Item item);

		/**
		 * Opens the chest, spilling it's contents onto the floor.
		 * @param place Where the chest is opening.
		 */
		void open(Place& place);

		/**
		 * Returns the type of chest. They just vary in quality.
		 * @return Type of chest.
		 */
		Type getChestType();

	private:
		static sf::Texture& getTexture(Type type, bool open);
		static sf::Texture *bronzeClosed, *bronzeOpen, *silverClosed, *silverOpen, *goldClosed, *goldOpen;

		Type type;
		std::vector<Item> items;
		bool opened;
};

#endif // TREASURECHEST_H
