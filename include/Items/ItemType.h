#ifndef ITEMTYPE_H
#define ITEMTYPE_H

#include "main.h"
#include "Stats.h"
#include "Util.h"
#include "VecUtil.h"

enum class EquipSlot: int {NONE = -1, HEAD = 0, HAND, OFFHAND, BODY, TOES, COUNT};

/**
 * Each item has an ItemType.
 * @note UT [x]
 */
class ItemType {
	public:
		/**
		 * @param name What this item is called.
		 * @param id The individualized ID of the item.
		 * @param graphicLoc The location of the item icon in the item icon sheet.
		 * @param weight How much this ItemType weighs.
		 */
		ItemType(String name, int id, Coord graphicLoc, float weight = 0.0f);

		/** @return The name of the ItemType. */
		String getName() const;

		/** @return The ID of the ItemType. Each ItemType has a different ID. */
		int getID() const;

		/** @return The weight of items of this type. Measured in grams. */
		float getWeight() const;

		/** @return The coordinates of the item icon in the item icon sheet. */
		Coord getGLoc() const;

		/** @return The equipment slot that this item gets equipped to. */
		EquipSlot getSlot() const;

		/** @return The coordinates of the item icon in the equippable item icon sheet. */
		Coord getEquipGLoc() const;

		/** @return Whether the item is equippable or not. */
		bool isEquippable() const;

		void setEquip(EquipSlot slot, Coord equipGLoc);

		/**
		 * Returns the value of a given stat.
		 * @param stat The stat to get the value of.
		 * @return The value of the stat.
		 */
		float getStatValue(Stat::Type stat) const;
		void setStatValue(Stat::Type stat, float value);

	private:
		String name;
		int id;
		float weight;
		EquipSlot slot;

		float stats[Stat::COUNT];

		Coord graphicLoc, equipGraphicLoc;

		static void initilizeMaps();
		static std::map<String, float> stringToWeightMap;
		static std::map<String, EquipSlot> stringToSlotMap;
		static bool mapsInitilized;
};

#endif // ITEMTYPE_H
