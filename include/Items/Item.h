#ifndef ITEM_H
#define ITEM_H

class ItemType;

/**
 * Stores information about an item that the Player can hoard.
 * @note UT [x]
 */
class Item {
	public:
		/** Initializes an Item to have a NULL type. */
		Item();

		/** Initializes an Item with the given type. */
		Item(const ItemType& itemType);

		/**
		 * Returns the item's type.
		 * @note The ItemType MUST be set before calling this method.
		 * @return The ItemType of the item.
		 */
		const ItemType& getType() const;

		/** @return How much this item weighs. */
		float getWeight() const;

		/** @return A value unique to this particular Item. */
		int getUnique() const;

		bool isNull() const;

	private:
		const ItemType* type;
		unsigned short i;
};

#endif // ITEM_H
