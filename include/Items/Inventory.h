#ifndef INVENTORY_H
#define INVENTORY_H

#include "Item.h"
#include "ItemType.h"
#include "ItemContainer.h"

/**
 * The Inventory class holds Items in a bag and in equipment.
 * @note UT [v]
 */
class Inventory {
	public:
		/** Creates a new inventory. */
		Inventory();

		/**
		 * Retrieves the item container for the player's items.
		 * @return The tile's item container.
		 */
		ItemContainer& getItemContainer();

		/**
		 * Equips an item.
		 * @param The item to equip.
		 * @return Returns true if it equipped successfully, and false otherwise.
		 */
		bool equipItem(Item item);

		/**
		 * Unequips an item.
		 * @param slot The slot to unequip.
		 * @return The item that was in that slot.
		 */
		Item unequipItem(EquipSlot slot);

        /**
         * Returns the total weight of all Items in the inventory.
         * @return The total weight of all Items in the inventory.
         */
		float getWeight();

		/**
         * Returns the Item equipped in the given slot.
         * @return The equipped item.
         */
		Item& getEquippedItem(EquipSlot slot);

	private:
		ItemContainer items;
		Item equipment[(int)EquipSlot::COUNT];
};

#endif
