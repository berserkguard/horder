#ifndef ITEM_CONTAINER_H
#define ITEM_CONTAINER_H

#include <vector>
#include "Item.h"

/// An enumeration for holding various sorting function types.
enum SortType {
	SORT_NONE,
	SORT_ID,
	SORT_NAME
};

/**
 * Contains a sortable vector of Items and methods for manipulating them.
 * @note UT [x]
 */
class ItemContainer {
	public:
		/**
		 * Default constructor. Creates an ItemContainer with auto-sorting disabled.
		 */
		ItemContainer();

		/**
		 * Creates an ItemContainer with the specified sort function and whether or not to auto-sort.
		 * @note If auto-sort is enabled, items are sorted after every addItem() call.
		 * @param sortfunc The type of sorting to perform by default.
		 * @param autoSort Whether or not to auto-sort upon adding items to the container.
		 */
		ItemContainer(SortType sortfunc, bool autoSort = true);

		/**
		 * Sets the maximum number of items that can be in this container. Default is infinite.
		 */
		void setLimit(unsigned int limit);

		/**
		 * Retrieves a reference to the item at the specified index.
		 * @note The index for an item can change due to sorting, inserting, etc.
		 * @param idx The 0-based index of the item to retrieve.
		 * @return A reference to the item.
		 */
		Item& getItemAt(int idx);

		/**
		 * Removes the item at the specified index (and returns it).
		 * @param idx The index for the item to remove.
		 * @return A copy of the removed item.
		 */
		Item removeItemAt(int idx);

		/**
		 * Adds an item to the container (if there is available space).
		 * @note If the container is set to auto-sort, the index of the item may not be equal to getNumItems() - 1.
		 * @note If getNumItems() == getMaxSize(), the item will not be added as there is no available space.
		 * @param item The item to add to the container.
		 * @return true if successful, false if not
		 */
		bool addItem(Item item);

		/**
		 * Sorts the container with the given sorting function.
		 * @param sortfunc The sorting function type to use.
		 */
		void sort(SortType sortfunc);

		/**
		 * Returns the number of items currently in the item container.
		 * @return The number of items.
		 */
		int getNumItems();

		/**
		 * Returns the maximum size of the item container.
		 * @return The maximum size.
		 */
		int getMaxSize();

		/**
		 * Returns the total weight of the items in the container.
		 * @return The weight of the items.
		 */
		float getWeight();

		/** Empties the container. */
		void clear();

	private:
		static bool sortByID(Item item1, Item item2);
		static bool sortByName(Item item1, Item item2);

		std::vector<Item> items;
		SortType sortfunc;
		bool autoSort;
		double weight;
		unsigned int limit;
};

#endif // ITEM_CONTAINER_H
