#ifndef PLAYER_H
#define PLAYER_H

#include "Inventory.h"
#include "Mob.h"
#include "Chunk.h"
#include "FadeEffect.h"
#include "TimeStep.h"
class TextEffect;
class Map;
class ValuablesManager;

/**
 * Holds information about the logic and rendering associated with a player.
 */
class Player: public Mob {
	public:
		/// Constructor. Sets the current map to NULL.
		Player(EffectsManager& effects);

		Entity::Ptr getEntity();

		/**
		 * Calls Mob::update then also checks for when to generate new chunks.
		 * @param diff The time that has passed since the previous frame.
		 */
		bool update(TimeStep timeStep) override;

		/// dir should be a Direction
		/** @param dir The direction to move in. */
		void move(int dir, TimeStep timeStep);

		float getSpeed() override;

		/**
		 * Returns the value of a player's stat.
		 * @param stat The stat.
		 * @return The value of the stat.
		 */
		float getStatValue(Stat::Type stat);

		/** @return The map the player is currently in. */
		Map* getMap();

		/** Sets the home. */
		void setHome(Map& home);

		Map* getHome();

		/** @param map The map to move the player to. */
		void setMap(Map& map);

		/// Interact with the environment.
		void interact();

		/** @return A reference to the player's Inventory. */
		Inventory& getInventory();

		void recalcHoardStats(Coord pos = Coord(-1, -1));

		/** Overridden Mob::attack() for custom player animations. */
		bool attack() override;

		void block(bool value = true);

		/** Overridden Mob::getDamaged() to account for blocking. */
		void getDamaged(float amount, float direction, int hard = NONE) override;

		/**
		 * Equips an item, if it is equippable. Does not remove it from inventory.
		 * @param item The item to equip. Doesn't need to be in player's inventory.
		 */
		void equipItem(Item item);

		Item unequipItem(EquipSlot item);

		/// Called by DeathImageWidget upon player clicking "continue"
		void revive();

		void fastReturn();

		/**
		 * Loads player data from a YAML Node.
		 * @param node The node in the YAML file that holds the player information.
		 */
		void loadYAML(YAML::Node& node, Map& map, ValuablesManager& valuables);

	protected:
		void die() override;

    private:
    	void fastReturnTeleport(FadeEvent event);
    	void deathFadeCallback(FadeEvent event);
    	void sleepEnd(FadeEvent event);
    	void changeMap(Map& map);

		bool isBlocking;
		bool dead;

		Inventory inventory;
		Map* home;
		Entity::Ptr itself;
		sf::Vector2f homeEnter;
		float hoardTileStats[CHUNK_SIZE][CHUNK_SIZE][Stat::COUNT];

		float baseStats[Stat::COUNT];
		float equipStats[Stat::COUNT];
		float hoardStats[Stat::COUNT];

		TileType::Material curFootSoundMaterial;
		SoundPtr curFootSound;
		bool playingFootSound;

		Interactible* currentShownInteract;
		TextEffect* shownInteractEffect;
		FadeEffect* deathEffect;
};

#endif // PLAYER_H
