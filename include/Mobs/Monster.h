#ifndef MONSTER_H
#define MONSTER_H

#include "Mob.h"
#include "Path.h"
class MonsterType;
class Chunk;
class Place;

class Monster: public Mob {
	public:
		/**
		 * Creates a new Monster of the given type.
		 * @param monsterType The MonsterType of the monster.
		 * @param place The Place this Monster is in.
		 * @param pos The position this Monster is at.
		 */
		Monster(const MonsterType& monsterType, EffectsManager& effects, sf::Vector2f position);

		bool update(TimeStep timeStep) override;

		/** @return The MonsterType of this Monster. */
		const MonsterType& getMonsterType();

		/// Overridden attack to allow for animations.
		bool attack() override;

		/**
		 * Returns the value of a particular stat of this monster.
		 * @param stat The stat.
		 * @return The value of the stat.
		 */
		float getStatValue(Stat::Type stat) override;

		float getSpeed() override;

		/**
		 * Sets who this Monster wants to kill. Set to NULL if you want this Monster to pursue noone.
		 * @param target The victim.
		 */
		void setTarget(Entity::Ptr target);
		/**
		 * Returns this Mob's target.
		 * @return This Mob's target.
		 */
		Entity::Ptr getTarget();

		/** Set to true when the Monster sees its target, and false when it does not. */
		void setSeesTarget(bool does);

		void setPlace(Place& place) override;

		Path& getPath();

		void onEntityCollision(Entity& collidingWith) override;

	protected:
		void die() override;

		virtual void ai(TimeStep timeStep);

	private:
		const MonsterType* type;

		// ai related things
		Entity::Ptr target;
		float awareness, specialCooldown;
		Path path;
		int pointOnPath;
		int state;
};

#endif // MONSTER_H
