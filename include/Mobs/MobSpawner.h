#ifndef MOBSPAWNER_H
#define MOBSPAWNER_H

#include <set>
#include <SFML/Graphics.hpp>
#include "VecUtil.h"
#include "main.h"
class EffectsManager;
class Monster;
class MonsterType;
class Map;

class MobSpawner {
	public:
		MobSpawner(Map& map, EffectsManager& effects);

		void spawnEncounter(const sf::IntRect& range);

		/**
		 * Spawns a monster at the given position.
		 * @param position The position of the tile to spawn the monster on.
		 * @param type The type of monster to spawn.
		 * @param allowAlt If this is true, then if the given position is obstructed, the monster will actually be placed in a nearby
		 *           non-walled position. If this is false, the monster will be spawned at the given position even if it is in a wall.
		 * @return The spawned monster. Returns NULL if it fails for some reason.
		 */
		Monster* spawnMonster(sf::Vector2i position, const MonsterType& type, bool allowAlt = true);

		/**
		 * Spawns (a) monster(s) somewhere randomly in the given range.
		 * @param range The rectangle (measured in tiles) that the monster may spawn within.
		 * @param type The type of monster to spawn.
		 * @return The spawned monster. Returns NULL if it fails for some reason.
		 */
		void spawnMonsters(Coord pos, const MonsterType& type, int number = 1);

		/**
		 * Gets the floor near a particular position that is not in a wall.
		 */
		sf::Vector2i getFloorNear(sf::Vector2i position);

		/** Just does some memory maintenance. Does not need to be called very often. */
		void update();

	private:
		MobSpawner(const MobSpawner&) = delete;
		MobSpawner& operator=(const MobSpawner&) = delete;

		typedef struct {
			bool operator() (const Coord& lhs, const Coord& rhs) const {
				if (lhs.y == rhs.y) {
					return lhs.x < rhs.x;
				} else return lhs.y < rhs.y;
			}
		} Classcomp;

		Map* map;
		EffectsManager* effects;
		std::set<Coord, Classcomp> usedPositions;
};

#endif // MOBSPAWNER_H
