#ifndef JOINT_H
#define JOINT_H

#include <SFML/Graphics.hpp>
#include "main.h"

/// A class for storing information about attachable joints.
class Joint {
	public:
		/// Constructor. Sets the position and rotation of the joint.
		Joint(float x = 0.0f, float y = 0.0f, float rotation = 0.0f);

		/**
		 * Returns the position of the joint. Has no intrinsic meaning.
		 * @return The joint's position.
		 */
		const sf::Vector2f getPosition() const;

		/**
		 * Returns the rotation of the joint. Has no intrinsic meaning.
		 * @return The joint's rotation.
		 */
		float getRotation() const;

	private:
		sf::Vector2f position;
		float rotation;
};

#endif // JOINT_H
