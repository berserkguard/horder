#ifndef ENCOUNTER_H
#define ENCOUNTER_H

#include "main.h"
class MonsterType;

class Encounter {
	public:
		Encounter(const MonsterType& monster);

		const MonsterType& getMonster();

		enum Rarity {NOWHERE = 0, RARE = 1, UNCOMMON = 2, COMMON = 4, EXTRA_COMMON = 6};
		void setRarity(Rarity rarity);
		int getWeight();
		static Rarity strToRarity(String str);

		/** @pre min > 0, max >= min or -1 */
		void setSize(int min, int max = -1);
		int randomSize();

	private:
		short minS, maxS;
		short weight;
		const MonsterType* monster;
};

#endif // ENCOUNTER_H
