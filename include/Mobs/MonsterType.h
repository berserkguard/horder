#ifndef MONSTERTYPE_H
#define MONSTERTYPE_H

#include <yaml-cpp/yaml.h>
#include "main.h"
#include "Stats.h"
#include "Mob.h"
#include "Model.h"
class ItemType;

enum AIType {AI_NONE, AI_SPRINT, AI_HOP};

/**
 * Defines a category of monster.
 * @note UT [x]
 */
class MonsterType {
	public:
		/** Constructs a new MonsterType with the given name and ID. */
		MonsterType(String name, int id);

		/** @return Returns the base name of this MonsterType. */
		const String& getName() const;

		/**
		 * Returns the Color applied to to this MonsterType.
		 * White means to color change is made.
		 * @return The color.
		 */
		sf::Color getColor() const;

		/**
		 * Returns the graphical model for this MonsterType.
		 * @return The model.
		 */
		const Model& getModel() const;

		void setGraphic(const Model& model, sf::Color color = sf::Color::White);

		/**
		 * Returns the base value of a particular stat of this MonsterType.
		 * @param stat The stat.
		 * @return The value of the stat.
		 */
		float getStatValue(Stat::Type stat) const;
		void setStatValue(Stat::Type stat, float value);

		/**
		 * Returns the ID of this MonsterType.
		 * @return The MonsterType's ID.
		 */
		int getID() const;

		/**
		 * Returns the AI this monster is supposed to use.
		 * @return The AI this monster is supposed to use.
		 */
		AIType getAI() const;
		void setAI(AIType ai);


		/**
		 * Returns something this Monster could drop upon death. Returns NULL if there are no droppable items.
		 * @return Something this Monster could drop etc.
		 */
		const ItemType* getRandomDrop() const;
		void addDrop(const ItemType& itemType);

		/** @return The death sound for this monster type. */
		Sound getSound() const;
		void setSound(Sound sound);

	private:
		String name;
		int id;
		AIType ai;
		sf::Color color;
		float stats[Stat::COUNT];
		std::vector<const ItemType*> drops;
		const Model* model;
		Sound sound;
};

#endif // MONSTERTYPE_H
