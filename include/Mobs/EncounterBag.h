#ifndef ENCOUNTERBAG_H
#define ENCOUNTERBAG_H

#include <vector>
#include "Encounter.h"
#include "ShuffleBag.h"

class EncounterBag {
	public:
		EncounterBag();

		void add(Encounter encounter);
		size_t numEncounters();

		void reshuffle();
		Encounter& grab();

	private:
		void fillBag(int copies);

		std::vector<Encounter> encounters;
		ShuffleBag<Encounter*> bag;
		unsigned int totalWeight;

};

#endif // ENCOUNTERBAG_H
