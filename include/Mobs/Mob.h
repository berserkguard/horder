#ifndef MOB_H
#define MOB_H

#include "TileType.h"
#include "Entity.h"
#include "VecUtil.h"
#include "Stats.h"
#include "SoundsManager.h"
#include "Model.h"
class Place;
class Tile;

const float MOVE_FORCE = 10.f;
const float SPEED_FACTOR = (1 / 60.f) * TILE_SIZE * 5;
const float THRESHOLD = .0001f;
const float ROTATE_SPEED = 10.f;

class Mob: public Entity {
	public:
		enum Type {MONSTER, PLAYER};

		/** Constructs a new empty Mob of the given type. */
		Mob(Type type, b2BodyDef& body, EffectsManager& effects);

		Type getMobType();

		virtual bool update(TimeStep timeStep) override;

		/// Rotate toward that angle. dur seconds later the mob will go back to rotating toward movement.
		/** @param angle The angle to rotate toward, in radians. */
		void rotateToward(Angle angle, float dur = 1.f);

		/** Tells the Mob to immediately smack something in front of it. */
		virtual bool attack();

		bool isMoving();
		bool isMovingX();
		bool isMovingY();

		/**
		 * Returns the value of a particular stat.
		 * @param stat The stat type to get the value of.
		 * @return The value of the Stat.
		 */
		virtual float getStatValue(Stat::Type stat) = 0;

		/** @return The number of hit points left. */
		float getHitPoints();

		/** @return The total number of hit points. */
		float getMaxHitPoints();

		/** @param amount How many hit points to add. */
		void restoreHitPoints(int amount);

		/**
		 * @param amount Amount to damage this Mob by.
		 * @param direction The angle at which this mob is damaged from. Used for blocking.
		 * @param flags Various boolean settings for the damage. Use DamageFlags.
		 */
		virtual void getDamaged(float amount, Angle direction, int flags = NONE);
		enum DamageFlags {NONE, NO_TEXT = 1 << 1, NO_SOUND = 1 << 2, NO_BLOCK = 1 << 3, CRIT = 1 << 4};

		/** @param place Where to set this Mob's place to. */
		virtual void setPlace(Place& place);
		/** @return this Mob's place. */
		Place* getPlace();

		/** @return The tile directly below the mob. */
		Tile* getGroundTile();
		/** @return The position directly below the mob. */
		Coord getGroundPos();

		virtual float getSpeed();
		float getRubbleSlow();

		/**
		 * @param entity The entity to test range with.
		 * @return Whether or not the given entity is within range.
		 */
		bool inRange(Entity::Ptr entity);

		Angle getAngle();

		void setModel(const Model& model, float scale = 1.f, sf::Color color = sf::Color::White);
		const Model& getModel();
		AnimationContainer& getAnims();
		int getSpriteIndex(SpriteLayer type);

	protected:
		/** Initilizes the Mob. Should be called by superclasses after setting the stats. */
		void init();

		void moveRaw(float x, float y);

		/** Called when this Mob dies. */
		virtual void die() = 0;

	private:
		/** Does rotational stuff. */
		void doRotes(float diff);
		void hitMob(Mob& mob);

		Type type;

		float hitPoints;
		float cooldown;

		bool movingX, movingY;
		Angle angle, targetAngle;
		float rotateTowardDelay; // the time between when rotateToward is called and the Mob decides to rotate towards its velocity
		Place* place;

		const Model* model;
		AnimationContainer animations;
		int spriteIndices[SL_END]; // Maps sprite layers to sprite indices
};
#endif // MOB_H

