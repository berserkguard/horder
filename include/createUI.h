#ifndef CREATE_UI_H
#define CREATE_UI_H

#include "ButtonWidget.h"
#include "WindowWidget.h"
#include "BackpackWindow.h"
#include "CharInfoWindow.h"
#include "BarWidget.h"
#include "MinimapWidget.h"
#include "ImageWidget.h"
#include "DeathImageWidget.h"

#include "UIManager.h"

void createUI(UIManager* ui);

#endif // CREATE_UI_H
