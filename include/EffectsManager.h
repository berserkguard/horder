#ifndef EFFECTSMANAGER_H
#define EFFECTSMANAGER_H

#include "EffectRenderer.h"
#include "SoundsManager.h"

/// SOUND EFFECTS AND ALSO VISUAL EFFECTS
class EffectsManager {
	public:
		EffectRenderer& graphical();
		SoundsManager& sound();

	private:
		EffectRenderer effectRenderer;
		SoundsManager soundsManager;
};

#endif // EFFECTSMANAGER_H
