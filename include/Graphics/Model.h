#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <yaml-cpp/yaml.h>
#include <SFML/Graphics.hpp>
#include "Joint.h"
#include "Animation/Animation.h"
#include "Animation/AnimationContainer.h"
#include "main.h"

/// An enumeration for distinguishing between the various sprite layers.
enum SpriteLayer {SL_WEAPONS, SL_ARMS_RIGHT, SL_ARMS_LEFT, SL_SHIELDS, SL_HEAD, SL_HAT, SL_END};
enum Hand {LEFT_HAND, RIGHT_HAND};

/// The size of a tile for the sprites. Needed for animation strips.
#define SPRITE_SIZE 128

/// Sizing stuff for weapons.
#define SPRITE_WEAPON_WIDTH 64
#define SPRITE_WEAPON_HEIGHT 128

/**
 * Models hold a bunch of sprite info.
 * @note UT [_]
 */
class Model {
	public:
		Model(String name);

		void loadYAML(YAML::Node node);

		bool hasSprite(SpriteLayer layer) const;
		void fillAnimationContainer(AnimationContainer& anims) const;
		const Joint& getJoint(Hand hand, int frame) const;

		/**
		 * Loads a sprite from the given file and applies it to the mob.
		 * @param type The type of the sprite to load.
		 * @return The sprite that the file was loaded into.
		 */
		sf::Sprite& createSprite(SpriteLayer type, std::vector<sf::Sprite>& spriteLayers, int* spriteIndices) const;

		String getName() const;

		static const Model& getBlank();

	private:
		String name;

		String spriteFiles[SL_END];
		std::vector<Joint> rightHandJoints; ///< A list of hand attachment joints (for weapons, shields, etc.)
		std::vector<Joint> leftHandJoints; ///< A list of hand attachment joints (for weapons, shields, etc.)
		std::vector<Animation> anims;
};

#endif // MODEL_H
