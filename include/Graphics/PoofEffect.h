#ifndef POOF_H
#define POOF_H

#include <Effect.h>


class PoofEffect: public Effect {
	public:
		PoofEffect(float size, float duration, sf::Vector2f pos);

		virtual void logic(TimeStep timeStep) override;

		float getCurrentSize();

		sf::Color getColor();
		void setColor(sf::Color color);

	private:
		sf::Color color;
		float size;
};

#endif // POOF_H
