#ifndef ANIMATIONDATA_H
#define ANIMATIONDATA_H

#include <map>
#include "main.h"
#include "TimeStep.h"

/**
 * AnimData holds data about simple animations.
 * @note UT [x]
 */
class AnimData {
	public:
		/** Constructor. */
		AnimData();

		/** The different avaliable types of Animation. */
		enum Type {NONE,
			FLASH, ///< Turns the owner's color white for the duration of the Animation.
			HP,    ///< Shows the HP bar.
		A_COUNT};

		/** The structure that holds all the info about a particular Animation. */
		struct Animation {
			/** How many seconds have passed since the creation of this Animation. */
			float curr;
			/** How long this Animation should last total, in seconds. */
			float end;
			/** The Type of Animation. */
			Type type;
			/** Constructs a new Animation. */
			Animation(Type type, float duration): curr(0), end(duration), type(type) {}
		};

		/** Should be called every frame. Updates all the animations. */
		void update(TimeStep timeStep);

		/** Ends and deletes all the current animations. */
		void clear();

		/**
		 * Adds a new animation to the data.
		 * @param type The type of animation to add.
		 * @param duration How long, in seconds, this animation should last.
		 */
		void addAnimation(Type type, float duration);

		/**
		 * Returns the set of animations currently in this data.
		 * @return The set of animations in this data.
		 */
		const std::map<Type, Animation>& getAnimations();

	private:
		std::map<Type, Animation> animations;
};

#endif // ANIMATIONDATA_H
