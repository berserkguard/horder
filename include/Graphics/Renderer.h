#ifndef RENDERER_H
#define RENDERER_H

#include "main.h"
#include "EffectRenderer.h"
#include "Scene.h"
#include "Chunk.h"
#include "Game.h"
#include "UIManager.h"
class Map;

#define MINI_TILE_SIZE 16

class Renderer {
    public:

    	/**
    	 * Everything that the renderer needs to know should be in this game.
    	 * @param game The game that the Renderer will render.
    	 */
    	Renderer(Game& game);

		/**
		 * Renders to the given window.
		 * @param window The window to render to.
		 * @param ui The UI to render
		 */
		void render(sf::RenderWindow& window, UIManager& ui);

		/**
		 * Renders debug information to the screen. Only runs when "Debug" is "yes" in the settings file.
		 * @param window The window to render debug info to.
		 */
		void renderDebugInfo(sf::RenderWindow& window);
		void renderDebugShapes(sf::RenderWindow& window, Map& map);
		void renderDebugPath(sf::RenderWindow& window, Entity::Ptr entity);

		void renderLighting(sf::RenderWindow& window);

		void setScene(Scene scene);
		Scene getScene();

	private:
		Renderer(const Renderer&) = delete;
		Renderer& operator=(const Renderer&) = delete;

		/// For rendering tiles with rotation.
		void renderTile(sf::RenderTarget& target, sf::Sprite& sprite, float rotation = 0.0f);
		void renderEntity(sf::RenderWindow& window, Entity::Ptr entity);
		void renderWalls(sf::RenderWindow& window, Map& map);

		/**
		 * Helper function for retrieving a tile relative to the given chunk.
		 * For example, if the position given is (-1, -1),
		 * it will return the lower-right tile in the neighboring upper-left chunk.
		 * If (9, 32) is given, it will return tile (9, 0) in the chunk directly below the current one.
		 */
		Tile* getRelativeTile(Chunk& chunk, int x, int y);

		void renderMenu(sf::RenderWindow& window, UIManager& ui);
		void renderGame(sf::RenderWindow& window, UIManager& ui);

		Game* game; ///< A stored pointer to the Game object for easier access.
		Scene currentScene;
		EffectRenderer* effects;

		int iterator;

		sf::VertexArray vertexArray;

		sf::Texture *tileTex, *objTex;
		sf::Texture *itemIcons, *miniItemIcons;
		sf::Font* simpleFont;
		sf::Shader *lightingShader, *flashShader;
		sf::Texture* logo, *itemsTiled;
		sf::Texture* playBtn;
		sf::Texture* wallTex;

		sf::RenderTexture lightingTexture1, lightingTexture2;
};

#endif // RENDERER_H
