#ifndef MAPOBJECT_H
#define MAPOBJECT_H

#include <SFML/Graphics.hpp>
#include "VecUtil.h"

/**
 * A MapObject is a graphical object that can be on maps, like beds and rugs.
 * @note UT [_]
 */
class MapObject {
	public:
		MapObject(sf::Vector2i gPos, sf::Vector2i gSize, sf::Vector2i texPos);

		/** @return How much rotated this object is. */
		Angle getRotation();

		/** Sets the rotation of this object. Defaults to 0. */
		void setRotation(Angle angle);


		/** @return The position of this object, in pixels, within the chunk this MapObject is in. */
		sf::Vector2i getGPosition();

		/** Sets the position of the object. */
		void setGPosition(sf::Vector2i);


		/** @return The size of this object in pixels. */
		sf::Vector2i getGSize();

		/** @return The position in the object texture of this object's graphic in the current frame. */
		sf::Vector2i getTexPosition();

		/** Adds a frame. */
		void addFrame(sf::Vector2i texPos);

		/** @return The current frame this MapObject is set to. */
		int getFrame();

		/**
		 * Sets the current frame.
		 * @param frame Ranges from 0 (default) to the number of times addFrame is called.
		 */
		void setFrame(int frame);

	private:
		Angle rotation;
		sf::Vector2i gPosition;
		sf::Vector2i gSize;
		std::vector<sf::Vector2i> gTexPositions;
		int frame;
};

#endif // MAPOBJECT_H
