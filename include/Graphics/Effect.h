#ifndef EFFECT_H
#define EFFECT_H

#include <SFML/Graphics.hpp>
#include "main.h"
#include "TimeStep.h"

class Effect {
	public:

		enum Type {NONE, TEXT, FADE, POOF};
		enum Z {Z_OFF, Z_FLOOR, Z_WALL, Z_BELOW, Z_ABOVE, Z_ABOVE_UI, Z_SUPER_UI, Z_COUNT};

		Effect(Type type, float duration, sf::Vector2f position = sf::Vector2f(0, 0), int z = Z_ABOVE);
		virtual ~Effect() = default;

		/**
		 * Updates the clock of this Effect. Returns true when it dies.
		 * @return Returns true if the time ran out and the Effect should die.
		 */
		bool update(TimeStep timeStep);

		void pause();
		void resume();

		float getAge();
		float getLifespan();
		float getTimeLeft();

		/**
		 * Higher = renders above more things.
		 * See enum for details.
		 * @return The z-level this Effect is rendered at.
		 */
		int getZ();

		/**
		 * Returns the transformation of this Effect. Used to move/rotate/resize the Effect.
		 * @return The sf::Transform of this Effect.
		 */
		sf::Transform& getTransform();

		/**
		 * Returns what type of Effect this is. I guess you can use this to know when to cast or whatever.
		 * @return The type of Effect.
		 */
		Type getType();

	protected:
		/**
		 * A method called by update().
		 */
		virtual void logic(TimeStep timeStep) = 0;

	private:
		Type type;
		int z;
		sf::Transform transform;
		float curr, end;
		bool paused;
};

#endif // EFFECT_H
