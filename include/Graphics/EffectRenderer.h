#ifndef EFFECTRENDERER_H
#define EFFECTRENDERER_H

#include <set>
#include <memory>
#include "FadeEffect.h"
#include "TextEffect.h"
#include "PoofEffect.h"

/**
 * Renders Effects.
 * @note UT [v]
 */
class EffectRenderer {
	public:
		EffectRenderer();

		/**
		 * Should be called every frame. Updates all the effects.
		 */
		void update(TimeStep timeStep);

		/**
		 * Renders a particular layer of effects.
		 * Since Z ordering is not automatically handled, when to call the different layers of this method is how you control it.
		 * @param window The window to render the effects to.
		 * @param The Z layer of effects that are to render.
		 */
		void renderLayer(sf::RenderWindow& window, int zLayer);

		/** Adds an effect. It gets copied. You don't use both this and one of the create effect methods, just one or the other. */
		Effect* addEffect(Effect& effect);

		/** Creates a new text effect with the given parameters. */
		TextEffect& createTextEffect(String text, float duration, sf::Vector2f pos);

		/** Creates a new poof effect with the given parameters. */
		PoofEffect& createPoofEffect(float size, float duration, sf::Vector2f pos);

		/** Creates a new fade effect with the given parameters. */
		FadeEffect& createFadeEffect(float fadeDuration, float waitDuration, sf::Vector2f pos);

		/**
		 * Renders a single particular effect.
		 * @param window Where to render the effect.
		 * @param effect The effect to render.
		 */
		void renderEffect(sf::RenderWindow& window, Effect& effect);

		/** Ends and deletes all effects. */
		void clear();

	private:
		EffectRenderer(const EffectRenderer&) = delete;
		EffectRenderer& operator=(const EffectRenderer&) = delete;
		void renderTextShadow(sf::RenderWindow& window, sf::Text& text, sf::RenderStates& rs);

		std::set<std::unique_ptr<Effect> > effects[Effect::Z_COUNT];
};

#endif // EFFECTRENDERER_H
