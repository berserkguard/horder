#ifndef TEXTEFFECT_H
#define TEXTEFFECT_H

#include "Effect.h"

class TextEffect: public Effect {
	public:
		TextEffect(String text, float duration, sf::Vector2f pos);

		virtual void logic(TimeStep timeStep) override;

		sf::Text& getText();

		sf::Color getColor();
		void setColor(sf::Color color);

		/** Returns the height of the text effect, in pixels.
		 * @return The height of the text effect. */
		int getSize();

		/** Sets the height of the text effect, in pixels.
		 * @param size The text height to set the effect to. */
		void setSize(int size);

	private:
		sf::Text text;
};

#endif // TEXTEFFECT_H
