#ifndef FADEEFFECT_H
#define FADEEFFECT_H

#include <functional>

#include "Effect.h"

enum FadeEvent {FADE_MIDDLE, FADE_OVER};

class FadeEffect: public Effect {
	public:
		FadeEffect(float fadeDuration, float waitDuration, sf::Vector2f pos);

		virtual void logic(TimeStep timeStep) override;

		void setColor(sf::Color from, sf::Color to);

		sf::Color getColor();

		void setCallback(std::function<void(FadeEvent)> callback);

	private:
		float fadeDuration, waitDuration;
		sf::Color cur, from, to;
		std::function<void(FadeEvent)> callback;
		short cbCounter;
};

#endif // FADEEFFECT_H
