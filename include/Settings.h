#ifndef SETTINGS_H
#define SETTINGS_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <yaml-cpp/yaml.h>
#include "main.h"

enum DebugMode {DEBUG_OFF, DEBUG_BASIC, DEBUG_MEGA};

/// Settings are important.
class Settings {
	public:
		/// Creates a new Settings with all settings set to defaults.
		Settings();

		/// The actions that can be binded to keys.
		enum KeyAction{NONE, UP, DOWN, LEFT, RIGHT, INTERACT, BACKPACK, PAUSE, CHARINFO, K_COUNT};

		enum GraphicSetting{LIGHTING, G_COUNT};

		/**
		 * Loads settings from a yaml node.
		 * @param node The node to load the settings from.
		 * @return Returns false if it failed and true otherwise.
		 */
		bool loadYAML(YAML::Node& node);

		/**
		 * Returns the action a particular key is binded to.
		 * @param key The key to get the binding for.
		 * @return The action binded to the given key.
		 */
		KeyAction getBinding(sf::Keyboard::Key key);

		/// Applys settings to the window. Should be called after certain window settings are changed.
		/** @param window The window */
		void apply(sf::RenderWindow& window);

		/** @return Whether in debug mode or not. */
		DebugMode getDebugMode();
		void setDebugMode(DebugMode mode);

		/** @return Whether in fullscreen or not. */
		bool isFullscreen();

		int getGraphicQuality(GraphicSetting setting);
	private:
		KeyAction stringToKeyAction(String s);

		String keyboardLayout;
		std::map<String, std::map<sf::Keyboard::Key, KeyAction>> keyBindings;

		int graphicSettings[G_COUNT];

		bool vsync;
		DebugMode debug;
		bool fullscreen;
		sf::Vector2i screenSize;

		bool windowSettingsHaveChanged;
};

#endif // SETTINGS_H
