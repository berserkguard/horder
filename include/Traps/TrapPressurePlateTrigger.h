#ifndef TRAPPRESSUREPLATETRIGGER_H
#define TRAPPRESSUREPLATETRIGGER_H

#include "TrapTrigger.h"

class TrapPressurePlateTrigger: public TrapTrigger {
	public:
		/** Creates a new pressure plate that triggers the given action. */
		TrapPressurePlateTrigger(TrapAction& trapAction);

		/** Simply updates the pressure plate.*/
		void update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) override;

	private:
		bool triggered;
};

#endif // TRAPPRESSUREPLATETRIGGER_H
