#ifndef TRAPACTION_H
#define TRAPACTION_H

#include "Entity.h"
#include "TrapType.h"
#include "VecUtil.h"
#include "MapObject.h"
class Context;
class Place;

class TrapAction {
	public:
		/** Creates a new TrapAction with the given type. */
		TrapAction(const TrapType& type, Place& place, EffectsManager& effects):
			type(&type), place(&place), effects(&effects), mapObject(nullptr) { }
		virtual ~TrapAction() = default;

		/** Does whatever the action for this trap does. */
		virtual void activate() = 0;

		/** Called every frame by Trap with all the entities positioned over this trap. */
		virtual void update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) { }

		/** Sets this action's position, if it needs one. */
		virtual void setPosition(Coord pos) { }

		/** @return The type of trap this action is for. */
		const TrapType& getType() {return *type;}

		/** @return The place. */
		Place& getPlace() {return *place;}

		/** @return The effects. */
		EffectsManager& getEffects() {return *effects;}

		/** @param mapObject The map object that graphically represents this trigger. */
		void setMapObject(MapObject& mo) {mapObject = &mo;}

		/** @return The graphic object this action has. Might be null if there is none. */
		MapObject* getMapObject() {return mapObject;}

	private:
		const TrapType* type;
		Place* place;
		EffectsManager* effects;
		MapObject* mapObject;
};

#endif // TRAPACTION_H
