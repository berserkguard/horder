#ifndef TRAPTYPE_H
#define TRAPTYPE_H

#include <SFML/Graphics.hpp>
class ItemType;

class TrapType {
	public:
		enum Trigger {NO_TRIGGER, PRESSURE_PLATE, TIMER};
		enum Action {NO_ACTION, ARROW, FLAME};
		TrapType();

		/**
		 * @param type The type of trigger to give this trap type.
		 * @param duration A value used by TIMER. Need not be set otherwise.
		 */
		void setTrigger(Trigger type, float duration = 0.0f);

		/** Adds a frame of graphic to this TrapType. */
		void addTriggerGraphic(sf::IntRect);

		Trigger getTrigger() const;
		float getTriggerDuration() const;
		size_t getNumTriggerFrames() const;
		sf::IntRect getTriggerGraphic(size_t index = 0) const;

		void setAction(Action type, const ItemType* item = nullptr);
		void addActionGraphic(sf::IntRect);

		Action getAction() const;
		const ItemType* getActionItem() const;
		size_t getNumActionFrames() const;
		sf::IntRect getActionGraphic(size_t index = 0) const;

	private:

		Trigger triggerType;
		float triggerDuration;
		std::vector<sf::IntRect> triggerGraphic;

		Action actionType;
		const ItemType* actionItem;
		std::vector<sf::IntRect> actionGraphic;
};

#endif // TRAPTYPE_H
