#ifndef TRAP_H
#define TRAP_H

#include "main.h"
#include "TrapType.h"
#include "TrapAction.h"
#include "TrapTrigger.h"
#include "Context.h"

class Trap {
	public:
		/**
		 * @param type Must contain both an action and a trigger.
		 */
		Trap(const TrapType& type, Context context);

		TrapAction& getAction();
		TrapTrigger& getTrigger();

		/** To be called every frame for every entity over this trigger. */
		void entityOverTrigger(Entity::Ptr entity);

		/** To be called every frame for every entity over this action. */
		void entityOverAction(Entity::Ptr entity);

		/** Should be called every frame, after the entityOver() calls. */
		void update(TimeStep timeStep);

	private:
		std::unique_ptr<TrapTrigger> trigger;
		std::vector<Entity::Ptr> entitiesOverTrigger;

		std::unique_ptr<TrapAction> action;
		std::vector<Entity::Ptr> entitiesOverAction;
};

#endif // TRAP_H
