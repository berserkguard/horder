#ifndef TRAPARROWACTION_H
#define TRAPARROWACTION_H

#include "TrapAction.h"
#include "Projectile.h"

#define ARROW_DAMAGE 6

class TrapArrowAction: public TrapAction {
	public:
		TrapArrowAction(const TrapType& action, Place& place, EffectsManager& effects);

		void setPosition(Coord position) override;
		void setDirection(Coord direction);
		void setAmmunition(int ammo);

		void activate() override;

	private:
		Coord position;
		Coord direction;
		int ammo;
};

#endif // TRAPARROWACTION_H
