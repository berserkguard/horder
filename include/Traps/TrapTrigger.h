#ifndef TRAPTRIGGER_H
#define TRAPTRIGGER_H

#include "Entity.h"
#include "TrapAction.h"
#include "MapObject.h"

class TrapTrigger {
	public:
		/** Constructs a new TrapTrigger that triggers the given action. */
		TrapTrigger(TrapAction& trapAction): trapAction(&trapAction), mapObject(nullptr) { }
		virtual ~TrapTrigger() = default;

		/** Called every frame by Trap with all the entities positioned over this trigger. */
		virtual void update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) { }

		/** Sets this trigger's position, if it needs one. */
		virtual void setPosition(Coord pos) { }

		/** @return The type of trap this trigger is for. */
		const TrapType& getType() {return trapAction->getType();}

		/** @param mapObject The map object that graphically represents this trigger. */
		void setMapObject(MapObject& mo) {mapObject = &mo;}

		/** @return The graphic object this action has. Might be null if there is none. */
		MapObject* getMapObject() {return mapObject;}

	protected:
		/** Should be called by superclasses to trigger the activation of the trap. */
		void trigger() {trapAction->activate();}

	private:
		TrapAction* trapAction;
		MapObject* mapObject;
};

#endif // TRAPTRIGGER_H
