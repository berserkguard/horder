#ifndef TRAPFLAMEACTION_H
#define TRAPFLAMEACTION_H

#include "TrapAction.h"
#include "EffectsManager.h"

#define FLAME_DAMAGE_PER_SEC 10

class TrapFlameAction: public TrapAction {
	public:
		TrapFlameAction(const TrapType& type, Place& place, EffectsManager& effects);

		void activate() override;
		void update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) override;
		void setPosition(Coord position) override;

	private:
		Place* place;
		Coord position;
		SoundPtr claimedSound;

		bool on;
		int increm;
};

#endif // TRAPFLAMEACTION_H
