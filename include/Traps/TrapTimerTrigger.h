#ifndef TRAPTIMERTRIGGER_H
#define TRAPTIMERTRIGGER_H

#include "TrapTrigger.h"

/** A timer trigger just triggers the action periodically. */
class TrapTimerTrigger: public TrapTrigger {
	public:
		/** Creates a new timer that triggers the given action. */
		TrapTimerTrigger(TrapAction& trapAction);

		/** Simply updates the timer. When it reaches a certain amount, the action is triggered. The entities aren't even used. */
		void update(TimeStep diff, const std::vector<Entity::Ptr>& entitiesOver) override;

	private:
		float time;
};

#endif // TRAPTIMERTRIGGER_H
