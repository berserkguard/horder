#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "Entity.h"
#include "VecUtil.h"
class ItemType;
class Place;

class Projectile: public Entity {
	public:
		Projectile(Place& place, EffectsManager& effects, sf::Vector2f pos);

		void go(sf::Vector2f velocity);
		void go(Angle angle, float speed);

		void setItem(const ItemType& itemType);
		void setHit(float damage);

		void setSprite(sf::Texture& texture, int index);

		bool update(TimeStep timeStep) override;

		Angle getAngle();

		void onEntityCollision(Entity&
								 collidingWith) override;
		void onWallCollision() override;

	private:
		void constructBody(sf::Vector2f pos, sf::Vector2f velocity);
		void die();

		const ItemType* itemType;
		float damage;
		bool deaded;

		Place* place;
};

#endif // PROJECTILE_H
